<?php

/**
 * Escribir Arbol Categorias
 */
  include(dirname(__FILE__).'/config/config.inc.php');

  listar_categories(1, 0);


/**
 * Recursively listar categorias
 *
 * @param int $id_category
 * @param int $level_depth
 */
function listar_categories($id_category, $level_depth)
{
	$categories = Db::getInstance()->executeS('SELECT `id_category` FROM `'._DB_PREFIX_.'category` WHERE `id_parent` = '.(int)$id_category);
	if (!$categories)
		return;
	$new_depth = (int)$level_depth + 1;
	$cat_ids = "";
	foreach($categories as $category)
	{
		$names = Db::getInstance()->executeS('SELECT `level_depth` FROM `'._DB_PREFIX_.'category` WHERE `id_category` = '.(int)$category);
        $cat_ids .= (string)$category['id_category'].',';
        echo($cat_ids . " - " . $names . " --- \n");
		listar_categories($category['id_category'], $new_depth);
	}
	$cat_ids = substr($cat_ids, 0, -1);
}
