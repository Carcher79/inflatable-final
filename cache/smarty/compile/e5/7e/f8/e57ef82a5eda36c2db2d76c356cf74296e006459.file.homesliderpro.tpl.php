<?php /* Smarty version Smarty-3.1.14, created on 2014-09-23 09:29:15
         compiled from "/Users/chriscallaway/Sites/orig-inflatables/modules/homesliderpro/homesliderpro.tpl" */ ?>
<?php /*%%SmartyHeaderCode:189798924542175ab84a8f1-73113856%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e57ef82a5eda36c2db2d76c356cf74296e006459' => 
    array (
      0 => '/Users/chriscallaway/Sites/orig-inflatables/modules/homesliderpro/homesliderpro.tpl',
      1 => 1409074312,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '189798924542175ab84a8f1-73113856',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'homeslider_slides' => 0,
    'hookid' => 0,
    'slideName' => 0,
    'configuration' => 0,
    'number' => 0,
    'slide' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_542175aba545a3_29689647',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_542175aba545a3_29689647')) {function content_542175aba545a3_29689647($_smarty_tpl) {?><!-- Module HomeSliderPro -->
<?php if (isset($_smarty_tpl->tpl_vars['homeslider_slides']->value)&&count($_smarty_tpl->tpl_vars['homeslider_slides']->value)>0){?>
	<div class="SEslider notLoaded seslider_<?php echo $_smarty_tpl->tpl_vars['hookid']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['slideName']->value;?>
 mode_<?php echo $_smarty_tpl->tpl_vars['configuration']->value['mode'];?>
 <?php if ($_smarty_tpl->tpl_vars['configuration']->value['autoControls']){?>withControls<?php }?> sliderFigo" style="max-width:<?php echo $_smarty_tpl->tpl_vars['configuration']->value['width'];?>
px;">
		<ul id="SEslider_<?php echo $_smarty_tpl->tpl_vars['hookid']->value;?>
" style="margin:0;">
		<?php $_smarty_tpl->tpl_vars["number"] = new Smarty_variable("0", null, 0);?> 
		<?php  $_smarty_tpl->tpl_vars['slide'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['slide']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['homeslider_slides']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['slide']->key => $_smarty_tpl->tpl_vars['slide']->value){
$_smarty_tpl->tpl_vars['slide']->_loop = true;
?>
			<?php $_smarty_tpl->tpl_vars['number'] = new Smarty_variable($_smarty_tpl->tpl_vars['number']->value+1, null, 0);?>
			<?php if ($_smarty_tpl->tpl_vars['slide']->value['active']&&$_smarty_tpl->tpl_vars['slide']->value['image']!=''){?>
				<li <?php if ($_smarty_tpl->tpl_vars['number']->value==1){?>class="primo active"<?php }?> title="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['description'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" style="padding:0;">
					<?php if ($_smarty_tpl->tpl_vars['configuration']->value['show_title']==1&&$_smarty_tpl->tpl_vars['slide']->value['title']!=''){?>
						<h1 class="slidetitle<?php if ($_smarty_tpl->tpl_vars['configuration']->value['title_pos']==1){?> right<?php }else{ ?> left<?php }?>"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['title'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</h1>
					<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['slide']->value['url']!=''){?>
						<a class="SElink" href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['url'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['slide']->value['new_window']==1){?>target="_blank"<?php }?>>
					<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['slide']->value['description']!=''){?>
					<span class="slide_description"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['description'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</span>
					<?php }?>
					<img class="SEimage" src="<?php echo @constant('_MODULE_DIR_');?>
/homesliderpro/images/<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['image'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" alt="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['legend'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" height="<?php echo intval($_smarty_tpl->tpl_vars['configuration']->value['height']);?>
" width="<?php echo intval($_smarty_tpl->tpl_vars['configuration']->value['width']);?>
" />
					<?php if ($_smarty_tpl->tpl_vars['slide']->value['url']!=''){?>
						</a>
					<?php }?>
				</li>
			<?php }?>
		<?php } ?>
		</ul>
	</div>
<?php }?>

<?php if (count($_smarty_tpl->tpl_vars['homeslider_slides']->value)>1){?>
	<script type="text/javascript">

	
	function initSlide_<?php echo $_smarty_tpl->tpl_vars['hookid']->value;?>
() {
		<?php if (count($_smarty_tpl->tpl_vars['homeslider_slides']->value)>1){?>
			var auto = <?php echo $_smarty_tpl->tpl_vars['configuration']->value['auto'];?>
,
				controls = <?php echo $_smarty_tpl->tpl_vars['configuration']->value['controls'];?>
,
				pager = <?php echo $_smarty_tpl->tpl_vars['configuration']->value['pager'];?>
;
			<?php if (isset($_smarty_tpl->tpl_vars['configuration']->value['autoControls'])&&$_smarty_tpl->tpl_vars['configuration']->value['autoControls']){?>
				var autoControls = true,
					autoControlsCombine = true;
			<?php }else{ ?>
				var autoControls = false,
					autoControlsCombine = false;
			<?php }?>
		<?php }else{ ?>
			var auto = false,
				controls = false,
				pager = false;
		<?php }?>
		
		var confWidth = <?php echo $_smarty_tpl->tpl_vars['configuration']->value['width'];?>
;
		var confHeight = <?php echo $_smarty_tpl->tpl_vars['configuration']->value['height'];?>
;
		var imgnum = <?php echo count($_smarty_tpl->tpl_vars['homeslider_slides']->value);?>
;
	
	
		var $slider = $('#SEslider_<?php echo $_smarty_tpl->tpl_vars['hookid']->value;?>
'); //cache for performance
		$slider.bxSlider({
			arrowClass: 'myarrow',
			nextClass: 'mynext',
			prevClass: 'myprev',
			playClass: 'fa fa-play',
			stopClass: 'fa fa-pause',
			pagerClass: 'mypager',
			infiniteLoop: <?php echo $_smarty_tpl->tpl_vars['configuration']->value['loop'];?>
,
			hideControlOnEnd: true,
			controls: controls,
			pager: pager,
			autoHover: true,
			preloadImages: 'visible',
			auto: auto,
			speed: <?php echo $_smarty_tpl->tpl_vars['configuration']->value['speed'];?>
,
			pause: <?php echo $_smarty_tpl->tpl_vars['configuration']->value['pause'];?>
,
			autoControls: autoControls,
			autoControlsCombine : autoControlsCombine,
			mode: '<?php echo $_smarty_tpl->tpl_vars['configuration']->value['mode'];?>
',
			autoDirection: '<?php echo $_smarty_tpl->tpl_vars['configuration']->value['direction'];?>
',
			onSlideBefore: function($slideElement, oldIndex, newIndex){
				$slider.find('li').removeClass('old active-slide next prev');
				if (oldIndex < newIndex || (oldIndex == $slider.find('li').length-1 && newIndex == 0) ) {
					$slider.find('li').removeClass('left');
					$slider.find('li').eq(oldIndex).addClass('old next');	
					$slideElement.addClass('active-slide next');
				} else {
					$slider.find('li').addClass('left');
					$slider.find('li').eq(oldIndex).addClass('old prev');		
					$slideElement.addClass('active-slide prev');
				}
			},
			onSlideNext: function($slideElement, oldIndex, newIndex){
			},
			onSlidePrev: function($slideElement, oldIndex, newIndex){		
			},
			onSlideAfter: function($slideElement, oldIndex, newIndex){
				
			},
			onSliderLoad: function (currentIndex) {
				$('.seslider_<?php echo $_smarty_tpl->tpl_vars['hookid']->value;?>
').removeClass('notLoaded');
				//$('.seslider_<?php echo $_smarty_tpl->tpl_vars['hookid']->value;?>
').append('<div class="imgNav"/>');
				$slider.find('li').eq(currentIndex).addClass('active-slide');
				if ('<?php echo $_smarty_tpl->tpl_vars['configuration']->value['direction'];?>
' != 'next')
					$slider.find('li').addClass('left');
				var perspective = $slider.width()*3+'px';
				$slider.css({
					'perspective':perspective ,
					'-webkit-perspective':perspective 
				})
			},
			//minSlides: 2,
			//maxSlides: 2,
			//slideWidth:<?php echo $_smarty_tpl->tpl_vars['configuration']->value['width'];?>
/2
		});
		
		if (<?php echo $_smarty_tpl->tpl_vars['configuration']->value['restartAuto'];?>
){
			$slider.mouseleave(function(){
				$slider.startAuto();
			})
		}
		
		
	}
	
	initSlide_<?php echo $_smarty_tpl->tpl_vars['hookid']->value;?>
();

	</script>
<?php }?>
<!-- /Module HomeSliderPro --><?php }} ?>