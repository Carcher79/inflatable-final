<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class StSocial extends Module
{
    private $_html = '';
    public $fields_form;
    public $fields_value;
	public function __construct()
	{
		$this->name          = 'stsocial';
		$this->tab           = 'front_office_features';
		$this->version       = '1.0';
		$this->author        = 'SUNNYTOO.COM';
		$this->need_instance = 0;
        $this->bootstrap     = true;
		
		parent::__construct();
		
		$this->displayName = $this->l('Social networking block');
		$this->description = $this->l('Allows you to add information about your brand\'s social networking sites.');
	}

	public function install()
	{
		if (!parent::install() 
			|| !$this->registerHook('displayHeader')
			|| !$this->registerHook('displayFooter')
            || !Configuration::updateValue('ST_SOCIAL_COLOR', '#666666')
            || !Configuration::updateValue('ST_SOCIAL_HOVER_COLOR', '#00A161')
            || !Configuration::updateValue('ST_SOCIAL_BG', '')
            || !Configuration::updateValue('ST_SOCIAL_HOVER_BG', '')
            || !Configuration::updateValue('ST_SOCIAL_FACEBOOK', 'http://www.facebook.com/prestashop')
            || !Configuration::updateValue('ST_SOCIAL_TWITTER', 'http://www.twitter.com/prestashop')
            || !Configuration::updateValue('ST_SOCIAL_RSS', '')
            || !Configuration::updateValue('ST_SOCIAL_YOUTUBE', 'http://www.youtube.com/prestashop')
            || !Configuration::updateValue('ST_SOCIAL_PINTEREST', 'http://www.pinterest.com/prestashop')
            || !Configuration::updateValue('ST_SOCIAL_GOOGLE', '')
            || !Configuration::updateValue('ST_SOCIAL_WORDPRESS', '')
            || !Configuration::updateValue('ST_SOCIAL_DRUPAL', '')
            || !Configuration::updateValue('ST_SOCIAL_VIMEO', '')
            || !Configuration::updateValue('ST_SOCIAL_FLICKR', '')
            || !Configuration::updateValue('ST_SOCIAL_DIGG', '')
            || !Configuration::updateValue('ST_SOCIAL_EBAY', '')
            || !Configuration::updateValue('ST_SOCIAL_AMAZON', '')
            || !Configuration::updateValue('ST_SOCIAL_INSTAGRAM', '') 
            || !Configuration::updateValue('ST_SOCIAL_LINKEDIN', '')
            || !Configuration::updateValue('ST_SOCIAL_BLOGGER', '')
            || !Configuration::updateValue('ST_SOCIAL_TUMBLR', '')
            || !Configuration::updateValue('ST_SOCIAL_VKONTAKTE', '')
            || !Configuration::updateValue('ST_SOCIAL_SKYPE', '')
            || !Configuration::updateValue('ST_SOCIAL_NEW_WINDOW', '')
            )
			return false;
		$this->_clearCache('stsocial.tpl');  
		$this->_clearCache('stsocial_column.tpl');   
		return true;
	}
	
	public function uninstall()
	{
		$this->_clearCache('stsocial.tpl');  
		$this->_clearCache('stsocial_column.tpl');   
		return parent::uninstall();
	}

                
    public function getContent()
	{
		if (isset($_POST['savestsocial']))
		{
            if (!Configuration::updateValue('ST_SOCIAL_COLOR', Tools::getValue('social_color'))
                || !Configuration::updateValue('ST_SOCIAL_HOVER_COLOR', Tools::getValue('social_hover_color'))
                || !Configuration::updateValue('ST_SOCIAL_BG', Tools::getValue('social_bg'))
                || !Configuration::updateValue('ST_SOCIAL_HOVER_BG', Tools::getValue('social_hover_bg'))
                || !Configuration::updateValue('ST_SOCIAL_FACEBOOK', Tools::getValue('facebook_url'))
                || !Configuration::updateValue('ST_SOCIAL_TWITTER', Tools::getValue('twitter_url'))
                || !Configuration::updateValue('ST_SOCIAL_RSS', Tools::getValue('rss_url'))
                || !Configuration::updateValue('ST_SOCIAL_YOUTUBE', Tools::getValue('youtube_url'))
                || !Configuration::updateValue('ST_SOCIAL_PINTEREST', Tools::getValue('pinterest_url'))
                || !Configuration::updateValue('ST_SOCIAL_GOOGLE', Tools::getValue('google_url'))
                || !Configuration::updateValue('ST_SOCIAL_WORDPRESS', Tools::getValue('wordpress_url'))
                || !Configuration::updateValue('ST_SOCIAL_DRUPAL', Tools::getValue('drupal_url'))
                || !Configuration::updateValue('ST_SOCIAL_VIMEO', Tools::getValue('vimeo_url'))
                || !Configuration::updateValue('ST_SOCIAL_FLICKR', Tools::getValue('flickr_url'))
                || !Configuration::updateValue('ST_SOCIAL_DIGG', Tools::getValue('digg_url'))
                || !Configuration::updateValue('ST_SOCIAL_EBAY', Tools::getValue('eaby_url'))
                || !Configuration::updateValue('ST_SOCIAL_AMAZON', Tools::getValue('amazon_url'))
                || !Configuration::updateValue('ST_SOCIAL_INSTAGRAM', Tools::getValue('instagram_url'))
                || !Configuration::updateValue('ST_SOCIAL_LINKEDIN', Tools::getValue('linkedin_url'))
                || !Configuration::updateValue('ST_SOCIAL_BLOGGER', Tools::getValue('blogger_url'))
                || !Configuration::updateValue('ST_SOCIAL_TUMBLR', Tools::getValue('tumblr_url'))
                || !Configuration::updateValue('ST_SOCIAL_VKONTAKTE', Tools::getValue('vkontakte_url'))
                || !Configuration::updateValue('ST_SOCIAL_SKYPE', Tools::getValue('skype_url'))
                || !Configuration::updateValue('ST_SOCIAL_NEW_WINDOW', (int)Tools::getValue('new_window'))
            )
                $this->_html .= $this->displayError($this->l('Cannot update settings'));
            else
                $this->_html .= $this->displayConfirmation($this->l('Settings updated'));
            	
			$this->_clearCache('stsocial.tpl');  
			$this->_clearCache('stsocial_column.tpl');            
        }
		$helper = $this->initForm();
		return $this->_html.$helper->generateForm($this->fields_form);
	}
    protected function initForm()
	{
		$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

		$this->fields_form[0]['form'] = array(
			'legend' => array(
				'title' => $this->displayName,
                'icon' => 'icon-cogs' 
			),
			'input' => array(
				 array(
					'type' => 'color',
					'label' => $this->l('Icon text color:'),
					'name' => 'social_color',
					'class' => 'color',
					'size' => 20,
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Icon hover color:'),
					'name' => 'social_hover_color',
					'class' => 'color',
					'size' => 20,
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Icon background:'),
					'name' => 'social_bg',
					'class' => 'color',
					'size' => 20,
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Icon hover background:'),
					'name' => 'social_hover_bg',
					'class' => 'color',
					'size' => 20,
			     ),
                array(
					'type' => 'switch',
					'label' => $this->l('Open in a new window:'),
					'name' => 'new_window',
					'is_bool' => true,
                    'default_value' => 1,
					'values' => array(
						array(
							'id' => 'new_window_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'new_window_off',
							'value' => 0,
							'label' => $this->l('No')),
					),
				), 
                array(
					'type' => 'text',
					'label' => $this->l('Your Facebook Link:'),
					'name' => 'facebook_url',
				),
                array(
					'type' => 'text',
					'label' => $this->l('Your Twitter Link:'),
					'name' => 'twitter_url',
				),
                array(
					'type' => 'text',
					'label' => $this->l('Your RSS Link:'),
					'name' => 'rss_url',
				),
                array(
					'type' => 'text',
					'label' => $this->l('Your Youtube Link:'),
					'name' => 'youtube_url',
				),
                array(
					'type' => 'text',
					'label' => $this->l('Your Pinterest Link:'),
					'name' => 'pinterest_url',
				),
                array(
					'type' => 'text',
					'label' => $this->l('Your Google Link:'),
					'name' => 'google_url',
				),
                array(
					'type' => 'text',
					'label' => $this->l('Your Wordpress Link:'),
					'name' => 'wordpress_url',
				),
                array(
					'type' => 'text',
					'label' => $this->l('Your Drupal Link:'),
					'name' => 'drupal_url',
				),
                array(
					'type' => 'text',
					'label' => $this->l('Your Vimeo Link:'),
					'name' => 'vimeo_url',
				),
                array(
					'type' => 'text',
					'label' => $this->l('Your Flickr Link:'),
					'name' => 'flickr_url',
				),
                array(
					'type' => 'text',
					'label' => $this->l('Your Digg Link:'),
					'name' => 'digg_url',
				),
                array(
					'type' => 'text',
					'label' => $this->l('Your Ebay Link:'),
					'name' => 'eaby_url',
				),
                array(
					'type' => 'text',
					'label' => $this->l('Your Amazon Link:'),
					'name' => 'amazon_url',
				),
                array(
					'type' => 'text',
					'label' => $this->l('Your Instagram Link:'),
					'name' => 'instagram_url',
				),
                array(
					'type' => 'text',
					'label' => $this->l('Your LinkedIn Link:'),
					'name' => 'linkedin_url',
				),
                array(
					'type' => 'text',
					'label' => $this->l('Your Blogger Link:'),
					'name' => 'blogger_url',
				),
                array(
					'type' => 'text',
					'label' => $this->l('Your Tumblr Link:'),
					'name' => 'tumblr_url',
				),
                array(
					'type' => 'text',
					'label' => $this->l('Your Vkontakte Link:'),
					'name' => 'vkontakte_url',
				),
                array(
					'type' => 'text',
					'label' => $this->l('Your Skype Link:'),
					'name' => 'skype_url',
				),
			),
			'submit' => array(
				'title' => $this->l('   Save   '),
			)
		);
        
        $helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table =  $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;

		$helper->identifier = $this->identifier;
		$helper->submit_action = 'savestsocial';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
		return $helper;
	}
    public function hookDisplayHeader()
    {
		$this->context->controller->addCSS($this->_path.'views/css/stsocial.css');
        $this->smarty->assign(array(
    			'social_color' => Configuration::get('ST_SOCIAL_COLOR'),
    			'social_hover_color' => Configuration::get('ST_SOCIAL_HOVER_COLOR'),
    			'social_bg' => Configuration::get('ST_SOCIAL_BG'),
    			'social_hover_bg' => Configuration::get('ST_SOCIAL_HOVER_BG'),
    		));
		return $this->display(__FILE__, 'header.tpl');
    }
    

    private function _prepareHook()
    {
        $this->smarty->assign(array(
			'facebook_url' => Configuration::get('ST_SOCIAL_FACEBOOK'),
			'twitter_url' => Configuration::get('ST_SOCIAL_TWITTER'),
			'rss_url' => Configuration::get('ST_SOCIAL_RSS'),
			'youtube_url' => Configuration::get('ST_SOCIAL_YOUTUBE'),
			'pinterest_url' => Configuration::get('ST_SOCIAL_PINTEREST'),
			'google_url' => Configuration::get('ST_SOCIAL_GOOGLE'),
			'wordpress_url' => Configuration::get('ST_SOCIAL_WORDPRESS'),
			'drupal_url' => Configuration::get('ST_SOCIAL_DRUPAL'),
			'vimeo_url' => Configuration::get('ST_SOCIAL_VIMEO'),
			'flickr_url' => Configuration::get('ST_SOCIAL_FLICKR'),
			'digg_url' => Configuration::get('ST_SOCIAL_DIGG'),
			'eaby_url' => Configuration::get('ST_SOCIAL_EBAY'),
			'amazon_url' => Configuration::get('ST_SOCIAL_AMAZON'),
			'instagram_url' => Configuration::get('ST_SOCIAL_INSTAGRAM'),
			'linkedin_url' => Configuration::get('ST_SOCIAL_LINKEDIN'),
			'blogger_url' => Configuration::get('ST_SOCIAL_BLOGGER'),
			'tumblr_url' => Configuration::get('ST_SOCIAL_TUMBLR'),
			'vkontakte_url' => Configuration::get('ST_SOCIAL_VKONTAKTE'),
			'skype_url' => Configuration::get('ST_SOCIAL_SKYPE'),
			'social_new_window' => Configuration::get('ST_SOCIAL_NEW_WINDOW'),
		));
        return true;
    }
    
    public function hookDisplayFooter($params)
    {
		if (!$this->isCached('stsocial.tpl', $this->getCacheId('stsocial')))
            $this->_prepareHook();
		return $this->display(__FILE__, 'stsocial.tpl', $this->getCacheId('stsocial'));
    }
    
    public function hookDisplayFooterSecondary($params)
    {
        return $this->hookDisplayFooter($params);
    }
    
    public function hookDisplayFooterTop($params)
    {
        return $this->hookDisplayFooter($params);
    }

	public function hookDisplayLeftColumn($params)
	{
		if (!$this->isCached('stsocial-column.tpl', $this->getCacheId('stsocial-column')))
            $this->_prepareHook();
		return $this->display(__FILE__, 'stsocial-column.tpl', $this->getCacheId('stsocial-column'));
	}
	public function hookDisplayRightColumn($params)
	{
	   return $this->hookDisplayLeftColumn($params);
	}
	public function hookDisplayStBlogLeftColumn($params)
	{
	   return $this->hookDisplayLeftColumn($params);
	}
	public function hookDisplayStBlogRightColumn($params)
	{
	   return $this->hookDisplayLeftColumn($params);
	}
	public function hookDisplayTopBar($params)
	{
		if (!$this->isCached('stsocial-topbar.tpl', $this->getCacheId('stsocial-topbar')))
            $this->_prepareHook();
		return $this->display(__FILE__, 'stsocial-topbar.tpl', $this->getCacheId('stsocial-topbar'));
	}
	public function hookDisplayNav($params)
	{
        return $this->hookDisplayTopBar($params);
	}
    private function getConfigFieldsValues()
    {
        $fields_values = array(
            'social_color' => Configuration::get('ST_SOCIAL_COLOR'),
            'social_hover_color' => Configuration::get('ST_SOCIAL_HOVER_COLOR'),
            'social_bg' => Configuration::get('ST_SOCIAL_BG'),
            'social_hover_bg' => Configuration::get('ST_SOCIAL_HOVER_BG'),
            'facebook_url' => Configuration::get('ST_SOCIAL_FACEBOOK'),
            'twitter_url' => Configuration::get('ST_SOCIAL_TWITTER'),
            'rss_url' => Configuration::get('ST_SOCIAL_RSS'),
            'youtube_url' => Configuration::get('ST_SOCIAL_YOUTUBE'),
            'pinterest_url' => Configuration::get('ST_SOCIAL_PINTEREST'),
            'google_url' => Configuration::get('ST_SOCIAL_GOOGLE'),
            'wordpress_url' => Configuration::get('ST_SOCIAL_WORDPRESS'),
            'drupal_url' => Configuration::get('ST_SOCIAL_DRUPAL'),
            'vimeo_url' => Configuration::get('ST_SOCIAL_VIMEO'),
            'flickr_url' => Configuration::get('ST_SOCIAL_FLICKR'),
            'digg_url' => Configuration::get('ST_SOCIAL_DIGG'),
            'eaby_url' => Configuration::get('ST_SOCIAL_EBAY'),
            'amazon_url' => Configuration::get('ST_SOCIAL_AMAZON'),
            'instagram_url' => Configuration::get('ST_SOCIAL_INSTAGRAM'),
            'linkedin_url' => Configuration::get('ST_SOCIAL_LINKEDIN'),
            'blogger_url' => Configuration::get('ST_SOCIAL_BLOGGER'),
            'tumblr_url' => Configuration::get('ST_SOCIAL_TUMBLR'),
            'vkontakte_url' => Configuration::get('ST_SOCIAL_VKONTAKTE'),
            'skype_url' => Configuration::get('ST_SOCIAL_SKYPE'),
            'new_window' => Configuration::get('ST_SOCIAL_NEW_WINDOW'),
        );
        return $fields_values;
    }
}