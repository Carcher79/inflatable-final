
{capture name=path}
	<a href="{$link->getPageLink('my-account', true)|escape:'htmlall':'UTF-8'}">
		{l s='My account' mod='sociallogin'}</a>
		<span class="navigation-pipe">{$navigationPipe}</span>{l s='Social Account Linking' mod='sociallogin'}
{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}
	{if $socialloginlrmessage}
<p class="warning">{$socialloginlrmessage}</p>
{/if}
<div id="favoriteproducts_block_account">
	<h3 class="heading">{l s='Social Account Linking' mod='sociallogin'}</h3>
	{if $sociallogin}
		<div class="mar_b2">
			<div class="favoriteproduct clearfix">
				{$sociallogin}
				
			</div>
		</div>
	{else}
		<p class="warning">{l s='Your Api Key is Wrong' mod='sociallogin'}</p>
	{/if}

    <ul class="footer_links clearfix">
    	<li>
            <a href="{$link->getPageLink('my-account', true)|escape:'html'}" title="{l s='Back to Your Account' mod='sociallogin'}" rel="nofollow"><i class="icon-left icon-mar-lr2"></i>{l s='Back to Your Account' mod='sociallogin'}</a>
        </li>
    	<li class="f_right">
            <a href="{$base_dir}" title="{l s='Home' mod='sociallogin'}"><i class="icon-home icon-mar-lr2"></i>{l s='Home' mod='sociallogin'}</a>
        </li>
    </ul>
</div>