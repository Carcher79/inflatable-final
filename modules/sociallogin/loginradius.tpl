<!-- Block mymodule -->
{if $position=='create_form'}
	<h3 class="title_block">{l s='Social Login' mod='sociallogin'}</h3>
    <div class="form_content_inner" {if $social_login_box_height} style="height:{$social_login_box_height}px;overflow:hidden;" {/if}>{$iframe}</div>
{elseif $right}
<div id="sociallogin_block_left" class="block" >
  <h4 class="title_block">{l s='Social Login' mod='sociallogin'}</h4>
  <div class="block_content">
	  <div class="sociallogin_block_left_inner">{$iframe}</div>
  </div>
</div>
{else}
<div id="sociallogin_block"  {$margin_style}>
  <div class="block_content">
	  <div class="sociallogin_block_inner">{$iframe}</div>
  </div>
</div>
{/if}


<!-- /Block mymodule -->


