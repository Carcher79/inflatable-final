<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class HomeFeatured_mod extends Module
{
    protected static $cache_products = false;
	private $_html = '';
    public $fields_form;
    public $fields_value;
    public static $sort_by = array(
        1 => array('id' =>1 , 'name' => 'Product Name: A to Z'),
        2 => array('id' =>2 , 'name' => 'Product Name: Z to A'),
        3 => array('id' =>3 , 'name' => 'Price: Lowest first'),
        4 => array('id' =>4 , 'name' => 'Price: Highest first'),
        5 => array('id' =>5 , 'name' => 'Product ID: Asc'),
        6 => array('id' =>6 , 'name' => 'Product ID: Desc'),
        7 => array('id' =>7 , 'name' => 'Random'),
        8 => array('id' =>8 , 'name' => 'Position: Asc'),
        9 => array('id' =>9 , 'name' => 'Position: Desc'),
        10 => array('id' =>10 , 'name' => 'Date update: Asc'),
        11 => array('id' =>11 , 'name' => 'Date update: Desc'),
        12 => array('id' =>12 , 'name' => 'Date add: Asc'),
        13 => array('id' =>13 , 'name' => 'Date add: Desc'),
    );
	function __construct()
	{
		$this->name = 'homefeatured_mod';
		$this->tab = 'front_office_features';
		$this->version = '1.2.5';
		$this->author = 'PrestaShop';
		$this->need_instance = 0;
        $this->bootstrap = true;

        $this->bootstrap = true;
		parent::__construct();

		$this->displayName = $this->l('Featured products on the homepage mod.');
		$this->description = $this->l('Displays featured products in the middle of your homepage.');
	}

	function install()
	{
		if (!Configuration::updateValue('HOME_FEATURED_NBR_MOD', 6) 
            || !Configuration::updateValue('HOME_FEATURED_SOBY', 6) 
            || !parent::install() 
			|| !$this->registerHook('addproduct')
			|| !$this->registerHook('updateproduct')
			|| !$this->registerHook('deleteproduct')
            || !$this->registerHook('displayHomeSecondaryLeft')
            || !Configuration::updateValue('STSN_FMOD_PRO_PER_LG_0', 3)
            || !Configuration::updateValue('STSN_FMOD_PRO_PER_MD_0', 3)
            || !Configuration::updateValue('STSN_FMOD_PRO_PER_SM_0', 2)
            || !Configuration::updateValue('STSN_FMOD_PRO_PER_XS_0', 2)
            || !Configuration::updateValue('STSN_FMOD_PRO_PER_XXS_0', 1)
        )
			return false;
		$this->_clearCache('homefeatured.tpl');
		return true;
	}
    public function uninstall()
	{
		$this->_clearCache('homefeatured.tpl');
		return parent::uninstall();
	}
    
    public function getContent()
	{
		if (isset($_POST['savehomefeatured_mod']))
		{
            $error = array();
			$nbr = (int)(Tools::getValue('nbr'));
			if (!$nbr OR $nbr <= 0 OR !Validate::isInt($nbr))
				$error[] = $this->l('An invalid number of products has been specified.');
            if (!count($error))
            {
                Configuration::updateValue('HOME_FEATURED_NBR_MOD', $nbr);
                Configuration::updateValue('HOME_FEATURED_SOBY', (int)Tools::getValue('soby'));
                $this->updateCatePerRow();
            }

            if (count($error))
                $this->_html .= count($errors) ? implode('',$error) : $this->displayError($this->l('Cannot update settings'));
            else
            {
		        $this->_clearCache('homefeatured.tpl');
                $this->_html .= $this->displayConfirmation($this->l('Settings updated'));  
            }          
        }
        
		$helper = $this->initForm();
		return $this->_html.$helper->generateForm($this->fields_form);
	}

    public function updateCatePerRow() {
        $arr = $this->findCateProPer();
        foreach ($arr as $v)
            if($gv = Tools::getValue($v['id']))
                Configuration::updateValue('STSN_'.strtoupper($v['id']), (int)$gv);
    }
    
    protected function initForm()
	{
		$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

		$this->fields_form[0]['form'] = array(
			'legend' => array(
				'title' => $this->l('Settings'),
                'icon'  => 'icon-cogs'
			),
			'input' => array(
                array(
					'type' => 'text',
					'label' => $this->l('Define the number of products to be displayed:'),
					'name' => 'nbr',
                    'default_value' => 10,
                    'desc' => array(
                        $this->l('To add products to your homepage, simply add them to the "home" category.'),
                        $this->l('Define the number of products that you would like to display on homepage (default: 6).'),
                    ),
                    'class' => 'fixed-width-sm'
				),
                array(
                    'type' => 'html',
                    'id' => 'fmod_pro_per_0',
                    'label'=> $this->l('The number of columns'),
                    'name' => $this->BuildDropListGroup($this->findCateProPer()),
                ),
                array(
					'type' => 'select',
        			'label' => $this->l('Sort by:'),
        			'name' => 'soby',
                    'options' => array(
        				'query' => self::$sort_by,
        				'id' => 'id',
        				'name' => 'name',
        			),
				), 
			),
			'submit' => array(
				'title' => $this->l('   Save   ')
			)
		);
        $helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table =  $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;

		$helper->identifier = $this->identifier;
		$helper->submit_action = 'savehomefeatured_mod';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
		return $helper;
	}
    
	public function hookDisplayHome($params)
	{
	    $is_random = Configuration::get('HOME_FEATURED_SOBY')==7;
	    if ($is_random || !$this->isCached('homefeatured.tpl', $this->getCacheId()))
    		if(!$this->_prepareHook())
                return false;
                
		return $is_random ? $this->display(__FILE__, 'homefeatured.tpl') : $this->display(__FILE__, 'homefeatured.tpl', $this->getCacheId());
	}
    
    public function hookDisplayHomeSecondaryLeft($params)
	{
	    return $this->hookDisplayHome($params);
    }
    private function _prepareHook()
    {
		$nb = $random_number_products =(int)(Configuration::get('HOME_FEATURED_NBR_MOD'));
        
        $order_by = 'id_product';
        $order_way = 'DESC';
        $random = false;
        switch(Configuration::get('HOME_FEATURED_SOBY'))
        {
            case 1:
                $order_by = 'name';
                $order_way = 'ASC';
            break;
            case 2:
                $order_by = 'name';
                $order_way = 'DESC';
            break;
            case 3:
                $order_by = 'price';
                $order_way = 'ASC';
            break;
            case 4:
                $order_by = 'price';
                $order_way = 'DESC';
            break;
            case 5:
                $order_by = 'id_product';
                $order_way = 'ASC';
            break;
            case 7:
                $order_by = null;
                $order_way = null;
                $random = true;
            break;
            case 8:
                $order_by = 'position';
                $order_way = 'ASC';
            break;
            case 9:
                $order_by = 'position';
                $order_way = 'DESC';
            break;
            case 10:
                $order_by = 'date_upd';
                $order_way = 'ASC';
            break;
            case 11:
                $order_by = 'date_upd';
                $order_way = 'DESC';
            break;
            case 12:
                $order_by = 'date_add';
                $order_way = 'ASC';
            break;
            case 13:
                $order_by = 'date_add';
                $order_way = 'DESC';
            break;
            default:
            break;
        }
        
        if (!empty(self::$cache_products))
            $products = self::$cache_products ;
        else
        {
            $category = new Category(Context::getContext()->shop->getCategory(), (int)Context::getContext()->language->id);
    		$products = $category->getProducts((int)Context::getContext()->language->id, 1, ($nb ? $nb : 10), $order_by, $order_way, false, true, $random, $random_number_products);
            self::$cache_products = $products;
        }
        
        /*
        if(!$products)
            return false;
		*/
        $this->smarty->assign(array(
			'products' => $products,
			'add_prod_display' => Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY'),
			'homeSize' => Image::getSize(ImageType::getFormatedName('home')),
            'mediumSize' => Image::getSize(ImageType::getFormatedName('medium')),

            'pro_per_lg'       => (int)Configuration::get('STSN_FMOD_PRO_PER_LG_0'),
            'pro_per_md'       => (int)Configuration::get('STSN_FMOD_PRO_PER_MD_0'),
            'pro_per_sm'       => (int)Configuration::get('STSN_FMOD_PRO_PER_SM_0'),
            'pro_per_xs'       => (int)Configuration::get('STSN_FMOD_PRO_PER_XS_0'),
            'pro_per_xxs'       => (int)Configuration::get('STSN_FMOD_PRO_PER_XXS_0'),
		));
        return true;
    }
    public function hookAddProduct($params)
	{
		$this->_clearCache('homefeatured.tpl');
	}

	public function hookUpdateProduct($params)
	{
		$this->_clearCache('homefeatured.tpl');
	}

	public function hookDeleteProduct($params)
	{
		$this->_clearCache('homefeatured.tpl');
	}
    private function getConfigFieldsValues()
    {
        $fields_values = array( 
            'nbr' => Configuration::get('HOME_FEATURED_NBR_MOD'),
            'soby' => Configuration::get('HOME_FEATURED_SOBY'),
        );
        return $fields_values;
    }

    public function BuildDropListGroup($group)
    {
        if(!is_array($group) || !count($group))
            return false;

        $html = '<div class="row">';
        foreach($group AS $key => $k)
        {
             if($key==3)
                 $html .= '</div><div class="row">';

             $html .= '<div class="col-xs-4 col-sm-3"><label '.(isset($k['tooltip']) ? ' data-html="true" data-toggle="tooltip" class="label-tooltip" data-original-title="'.$k['tooltip'].'" ':'').'>'.$k['label'].'</label>'.
             '<select name="'.$k['id'].'" 
             id="'.$k['id'].'" 
             class="'.(isset($k['class']) ? $k['class'] : 'fixed-width-md').'"'.
             (isset($k['onchange']) ? ' onchange="'.$k['onchange'].'"':'').' >';
            
            for ($i=1; $i < 7; $i++){
                $html .= '<option value="'.$i.'" '.(Configuration::get('STSN_'.strtoupper($k['id'])) == $i ? ' selected="selected"':'').'>'.$i.'</option>';
            }
                                
            $html .= '</select></div>';
        }
        return $html.'</div>';
    }
    public function findCateProPer()
    {
        return array(
            array(
                'id' => 'fmod_pro_per_lg_0',
                'label' => $this->l('Large devices'),
                'tooltip' => $this->l('Desktops (>1200px)'),
            ),
            array(
                'id' => 'fmod_pro_per_md_0',
                'label' => $this->l('Medium devices'),
                'tooltip' => $this->l('Desktops (>992px)'),
            ),
            array(
                'id' => 'fmod_pro_per_sm_0',
                'label' => $this->l('Small devices'),
                'tooltip' => $this->l('Tablets (>768px)'),
            ),
            array(
                'id' => 'fmod_pro_per_xs_0',
                'label' => $this->l('Extra small devices'),
                'tooltip' => $this->l('Phones (>480px)'),
            ),
            array(
                'id' => 'fmod_pro_per_xxs_0',
                'label' => $this->l('Extra extra small devices'),
                'tooltip' => $this->l('Phones (<480px)'),
            ),
        );
    }
}
