<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class HomeFeaturedSlider extends Module
{
    protected static $cache_featured_products = array();
	private $_html = '';
    public $fields_form;
    public $fields_value;
    public $validation_errors = array();
    public static $easing = array(
		array('id' => 0, 'name' => 'swing'),
		array('id' => 1, 'name' => 'easeInQuad'),
		array('id' => 2, 'name' => 'easeOutQuad'),
		array('id' => 3, 'name' => 'easeInOutQuad'),
		array('id' => 4, 'name' => 'easeInCubic'),
		array('id' => 5, 'name' => 'easeOutCubic'),
		array('id' => 6, 'name' => 'easeInOutCubic'),
		array('id' => 7, 'name' => 'easeInQuart'),
		array('id' => 8, 'name' => 'easeOutQuart'),
		array('id' => 9, 'name' => 'easeInOutQuart'),
		array('id' => 10, 'name' => 'easeInQuint'),
		array('id' => 11, 'name' => 'easeOutQuint'),
		array('id' => 12, 'name' => 'easeInOutQuint'),
		array('id' => 13, 'name' => 'easeInSine'),
		array('id' => 14, 'name' => 'easeOutSine'),
		array('id' => 15, 'name' => 'easeInOutSine'),
		array('id' => 16, 'name' => 'easeInExpo'),
		array('id' => 17, 'name' => 'easeOutExpo'),
		array('id' => 18, 'name' => 'easeInOutExpo'),
		array('id' => 19, 'name' => 'easeInCirc'),
		array('id' => 20, 'name' => 'easeOutCirc'),
		array('id' => 21, 'name' => 'easeInOutCirc'),
		array('id' => 22, 'name' => 'easeInElastic'),
		array('id' => 23, 'name' => 'easeOutElastic'),
		array('id' => 24, 'name' => 'easeInOutElastic'),
		array('id' => 25, 'name' => 'easeInBack'),
		array('id' => 26, 'name' => 'easeOutBack'),
		array('id' => 27, 'name' => 'easeInOutBack'),
		array('id' => 28, 'name' => 'easeInBounce'),
		array('id' => 29, 'name' => 'easeOutBounce'),
		array('id' => 30, 'name' => 'easeInOutBounce'),
	);
    public static $items = array(
		array('id' => 2, 'name' => '2'),
		array('id' => 3, 'name' => '3'),
		array('id' => 4, 'name' => '4'),
		array('id' => 5, 'name' => '5'),
		array('id' => 6, 'name' => '6'),
    );
    public static $sort_by = array(
        1 => array('id' =>1 , 'name' => 'Product Name: A to Z'),
        2 => array('id' =>2 , 'name' => 'Product Name: Z to A'),
        3 => array('id' =>3 , 'name' => 'Price: Lowest first'),
        4 => array('id' =>4 , 'name' => 'Price: Highest first'),
        5 => array('id' =>5 , 'name' => 'Product ID: Asc'),
        6 => array('id' =>6 , 'name' => 'Product ID: Desc'),
        7 => array('id' =>7 , 'name' => 'Random'),
        8 => array('id' =>8 , 'name' => 'Position: Asc'),
        9 => array('id' =>9 , 'name' => 'Position: Desc'),
        10 => array('id' =>10 , 'name' => 'Date update: Asc'),
        11 => array('id' =>11 , 'name' => 'Date update: Desc'),
        12 => array('id' =>12 , 'name' => 'Date add: Asc'),
        13 => array('id' =>13 , 'name' => 'Date add: Desc'),
    );
	function __construct()
	{
		$this->name = 'homefeaturedslider';
		$this->tab = 'front_office_features';
		$this->version = '1.3.9';
		$this->author = 'SUNNYTOO.COM';
		$this->need_instance = 0;
        $this->bootstrap = true;

		parent::__construct();

		$this->displayName = $this->l('Featured products slider');
		$this->description = $this->l('Displays featured products on homepage.');
	}

	function install()
	{
		if (!Configuration::updateValue('HOME_FEATURED_S_NBR_MOD', 8) 
            || !parent::install() 
			|| !$this->registerHook('addproduct')
			|| !$this->registerHook('updateproduct')
			|| !$this->registerHook('deleteproduct')
            || !$this->registerHook('displayLeftColumn')
            || !Configuration::updateValue('HOME_FEATURED_S_EASING', 0)
            || !Configuration::updateValue('HOME_FEATURED_S_SLIDESHOW', 0)
            || !Configuration::updateValue('HOME_FEATURED_S_S_SPEED', 7000)
            || !Configuration::updateValue('HOME_FEATURED_S_A_SPEED', 400)
            || !Configuration::updateValue('HOME_FEATURED_S_PAUSE', 1)
            || !Configuration::updateValue('HOME_FEATURED_S_LOOP', 0)
            || !Configuration::updateValue('HOME_FEATURED_S_MOVE', 0)
            || !Configuration::updateValue('HOME_FEATURED_S_SOBY', 6)
            || !Configuration::updateValue('HOME_FEATURED_S_NBR_MOD_COL', 8) 
            || !Configuration::updateValue('HOME_FEATURED_S_EASING_COL', 0)
            || !Configuration::updateValue('HOME_FEATURED_S_SLIDESHOW_COL', 0)
            || !Configuration::updateValue('HOME_FEATURED_S_S_SPEED_COL', 7000)
            || !Configuration::updateValue('HOME_FEATURED_S_A_SPEED_COL', 400)
            || !Configuration::updateValue('HOME_FEATURED_S_PAUSE_COL', 1)
            || !Configuration::updateValue('HOME_FEATURED_S_LOOP_COL', 0)
            || !Configuration::updateValue('HOME_FEATURED_S_MOVE_COL', 0)
            || !Configuration::updateValue('HOME_FEATURED_S_ITEMS_COL', 4)
            || !Configuration::updateValue('HOME_FEATURED_S_SOBY_COL', 6)
            || !Configuration::updateValue('HOME_FEATURED_S_HIDE_MOB', 0)
            || !Configuration::updateValue('HOME_FEATURED_S_HIDE_MOB_COL', 0)
            || !Configuration::updateValue('HOME_FEATURED_S_NBR_FOT', 4) 
            || !Configuration::updateValue('HOME_FEATURED_S_SOBY_FOT', 1)
            || !Configuration::updateValue('HOME_FEATURED_S_DISPLAY_SD', 0)
            || !Configuration::updateValue('HOME_FEATURED_S_GRID', 0)
            || !Configuration::updateValue('STSN_FEATURED_PRO_PER_LG_0', 4)
            || !Configuration::updateValue('STSN_FEATURED_PRO_PER_MD_0', 4)
            || !Configuration::updateValue('STSN_FEATURED_PRO_PER_SM_0', 3)
            || !Configuration::updateValue('STSN_FEATURED_PRO_PER_XS_0', 2)
            || !Configuration::updateValue('STSN_FEATURED_PRO_PER_XXS_0', 1)
        )
			return false;
		$this->clearSliderCache();
		return true;
	}
    public function uninstall()
	{
		$this->clearSliderCache();
		return parent::uninstall();
	}
    public function getContent()
	{            
	    $this->initFieldsForm();
		if (isset($_POST['savehomefeaturedslider']))
		{
            foreach($this->fields_form as $form)
                foreach($form['form']['input'] as $field)
                    if(isset($field['validation']))
                    {
                        $errors = array();       
                        $value = Tools::getValue($field['name']);
                        if (isset($field['required']) && $field['required'] && $value==false && (string)$value != '0')
        						$errors[] = sprintf(Tools::displayError('Field "%s" is required.'), $field['label']);
                        elseif($value)
                        {
        					if (!Validate::$field['validation']($value))
        						$errors[] = sprintf(Tools::displayError('Field "%s" is invalid.'), $field['label']);
                        }
        				// Set default value
        				if ($value === false && isset($field['default_value']))
        					$value = $field['default_value'];
                            
                        if(count($errors))
                        {
                            $this->validation_errors = array_merge($this->validation_errors, $errors);
                        }
                        elseif($value==false)
                        {
                            switch($field['validation'])
                            {
                                case 'isUnsignedId':
                                case 'isUnsignedInt':
                                case 'isInt':
                                case 'isBool':
                                    $value = 0;
                                break;
                                default:
                                    $value = '';
                                break;
                            }
                            Configuration::updateValue('HOME_FEATURED_S_'.strtoupper($field['name']), $value);
                        }
                        else
                            Configuration::updateValue('HOME_FEATURED_S_'.strtoupper($field['name']), $value);
                    }
            $this->updateCatePerRow();
            if(count($this->validation_errors))
                $this->_html .= $this->displayError(implode('<br/>',$this->validation_errors));
            else 
            {
		        $this->clearSliderCache();
                $this->_html .= $this->displayConfirmation($this->l('Settings updated'));  
            }    
        }
        $this->fields_form[0]['form']['input']['featured_pro_per_0']['name'] = $this->BuildDropListGroup($this->findCateProPer());
		$helper = $this->initForm();
		return $this->_html.$helper->generateForm($this->fields_form);
	}

    public function updateCatePerRow() {
        $arr = $this->findCateProPer();
        foreach ($arr as $v)
            if($gv = Tools::getValue($v['id']))
                Configuration::updateValue('STSN_'.strtoupper($v['id']), (int)$gv);
    }
    public function initFieldsForm()
    {
		$this->fields_form[0]['form'] = array(
			'legend' => array(
				'title' => $this->l('Slide on homepage'),
                'icon'  => 'icon-cogs'
			),
			'input' => array(
                array(
					'type' => 'text',
					'label' => $this->l('Define the number of products to be displayed:'),
					'name' => 'nbr_mod',
                    'default_value' => 8,
                    'desc' => array(
                        $this->l('To add products to your homepage, simply add them to the "home" category.'),
                        $this->l('Define the number of products that you would like to display on homepage (default: 8).'),
                    ),
                    'validation' => 'isUnsignedInt',
                    'class' => 'fixed-width-sm'
				),
                array(
					'type' => 'select',
        			'label' => $this->l('Sort by:'),
        			'name' => 'soby',
                    'options' => array(
        				'query' => self::$sort_by,
        				'id' => 'id',
        				'name' => 'name',
        			),
                    'validation' => 'isUnsignedInt',
				), 
                array(
					'type' => 'radio',
                    'type' => 'switch',
                    'label' => $this->l('Using category grid view to display products, do not use slider:'),
                    'name' => 'grid',
                    'is_bool' => true,
                    'default_value' => 0,
                    'values' => array(
                        array(
                            'id' => 'grid_on',
                            'value' => 1,
                            'label' => $this->l('Yes')),
                        array(
                            'id' => 'grid_off',
                            'value' => 0,
                            'label' => $this->l('No')),
                    ),
                    'validation' => 'isBool',
                ), 
                'featured_pro_per_0' => array(
                    'type' => 'html',
                    'id' => 'featured_pro_per_0',
                    'label'=> $this->l('The number of columns'),
                    'name' => '',
                ),
                array(
					'type' => 'switch',
					'label' => $this->l('Autoplay:'),
					'name' => 'slideshow',
					'is_bool' => true,
                    'default_value' => 1,
					'values' => array(
						array(
							'id' => 'slideshow_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'slideshow_off',
							'value' => 0,
							'label' => $this->l('No')),
					),
                    'validation' => 'isBool',
				), 
                array(
					'type' => 'text',
					'label' => $this->l('Time:'),
					'name' => 's_speed',
                    'default_value' => 7000,
                    'desc' => $this->l('The period, in milliseconds, between the end of a transition effect and the start of the next one.'),
                    'validation' => 'isUnsignedInt',
                    'class' => 'fixed-width-sm'
				),
                array(
					'type' => 'text',
					'label' => $this->l('Transition period:'),
					'name' => 'a_speed',
                    'default_value' => 400,
                    'desc' => $this->l('The period, in milliseconds, of the transition effect.'),
                    'validation' => 'isUnsignedInt',
                    'class' => 'fixed-width-sm'
				),
                array(
					'type' => 'switch',
					'label' => $this->l('Pause On Hover:'),
					'name' => 'pause',
                    'default_value' => 1,
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'pause_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'pause_off',
							'value' => 0,
							'label' => $this->l('No')),
					),
                    'validation' => 'isBool',
				),
                array(
					'type' => 'select',
        			'label' => $this->l('Easing method:'),
        			'name' => 'easing',
                    'options' => array(
        				'query' => self::$easing,
        				'id' => 'id',
        				'name' => 'name',
        			),
                    'desc' => $this->l('The type of easing applied to the transition animation'),
                    'validation' => 'isUnsignedInt',
				), 
                array(
					'type' => 'switch',
					'label' => $this->l('Loop:'),
					'name' => 'loop',
                    'default_value' => 0,
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'loop_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'loop_off',
							'value' => 0,
							'label' => $this->l('No')),
					),
                    'desc' => $this->l('"No" if you want to perform the animation once; "Yes" to loop the animation'),
                    'validation' => 'isBool',
				),
                array(
					'type' => 'radio',
					'label' => $this->l('Move:'),
					'name' => 'move',
                    'default_value' => 0,
					'values' => array(
						array(
							'id' => 'move_on',
							'value' => 1,
							'label' => $this->l('1 item')),
						array(
							'id' => 'move_off',
							'value' => 0,
							'label' => $this->l('All visible items')),
					),
                    'validation' => 'isBool',
				),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Hide slideshow on mobile devices:'),
                    'name' => 'hide_mob',
                    'default_value' => 0,
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'hide_mob_on',
                            'value' => 1,
                            'label' => $this->l('Yes')),
                        array(
                            'id' => 'hide_mob_off',
                            'value' => 0,
                            'label' => $this->l('No')),
                    ),
                    'desc' => $this->l('if set to Yes, slider will be hidden on mobile devices (if screen width is less than 768 pixels).'),
                    'validation' => 'isBool',
                ),
                array(
					'type' => 'switch',
					'label' => $this->l('Display product short description'),
					'name' => 'display_sd',
                    'default_value' => 0,
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'display_sd_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'display_sd_off',
							'value' => 0,
							'label' => $this->l('No')),
					),
                    'validation' => 'isBool',
				),
			),
			'submit' => array(
				'title' => $this->l('   Save all   ')
			),
		);
        
		$this->fields_form[1]['form'] = array(
			'legend' => array(
				'title' => $this->l('Slide on sidebar'),
                'icon' => 'icon-cogs'
			),
			'input' => array(
                array(
					'type' => 'text',
					'label' => $this->l('Define the number of products to be displayed:'),
					'name' => 'nbr_mod_col',
                    'default_value' => 8,
                    'desc' => array(
                        $this->l('To add products to your homepage, simply add them to the "home" category.'),
                        $this->l('Define the number of products that you would like to display on sidebar (default: 8).'),
                    ),
                    'validation' => 'isUnsignedInt',
                    'class' => 'fixed-width-sm'
				),
                array(
					'type' => 'select',
        			'label' => $this->l('Sort by:'),
        			'name' => 'soby_col',
                    'options' => array(
        				'query' => self::$sort_by,
        				'id' => 'id',
        				'name' => 'name',
        			),
                    'validation' => 'isUnsignedInt',
				), 
                array(
					'type' => 'select',
        			'label' => $this->l('The number of columns:'),
        			'name' => 'items_col',
                    'options' => array(
        				'query' => self::$items,
        				'id' => 'id',
        				'name' => 'name',
        			),
                    'desc' => array(
                        $this->l('Set number of columns for default screen resolution(980px).'),
                    ),
                    'validation' => 'isUnsignedInt',
				), 
                array(
					'type' => 'switch',
					'label' => $this->l('Autoplay:'),
					'name' => 'slideshow_col',
					'is_bool' => true,
                    'default_value' => 1,
					'values' => array(
						array(
							'id' => 'slideshow_col_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'slideshow_col_off',
							'value' => 0,
							'label' => $this->l('No')),
					),
                    'validation' => 'isBool',
				), 
                array(
					'type' => 'text',
					'label' => $this->l('Time:'),
					'name' => 's_speed_col',
                    'default_value' => 7000,
                    'desc' => $this->l('The period, in milliseconds, between the end of a transition effect and the start of the next one.'),
                    'validation' => 'isUnsignedInt',
                    'class' => 'fixed-width-sm'
				),
                array(
					'type' => 'text',
					'label' => $this->l('Transition period:'),
					'name' => 'a_speed_col',
                    'default_value' => 400,
                    'desc' => $this->l('The period, in milliseconds, of the transition effect.'),
                    'validation' => 'isUnsignedInt',
                    'class' => 'fixed-width-sm'
				),
                array(
					'type' => 'switch',
					'label' => $this->l('Pause On Hover:'),
					'name' => 'pause_col',
                    'default_value' => 1,
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'pause_col_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'pause_col_off',
							'value' => 0,
							'label' => $this->l('No')),
					),
                    'validation' => 'isBool',
				),
                array(
					'type' => 'select',
        			'label' => $this->l('Easing method:'),
        			'name' => 'easing_col',
                    'options' => array(
        				'query' => self::$easing,
        				'id' => 'id',
        				'name' => 'name',
        			),
                    'desc' => $this->l('The type of easing applied to the transition animation'),
                    'validation' => 'isUnsignedInt',
				), 
                array(
					'type' => 'switch',
					'label' => $this->l('Loop:'),
					'name' => 'loop_col',
                    'default_value' => 0,
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'loop_col_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'loop_col_off',
							'value' => 0,
							'label' => $this->l('No')),
					),
                    'desc' => $this->l('"No" if you want to perform the animation once; "Yes" to loop the animation'),
                    'validation' => 'isBool',
				),
                array(
					'type' => 'hidden',
					'name' => 'move_col',
                    'default_value' => 1,
                    'validation' => 'isBool',
				),
                array(
					'type' => 'switch',
					'label' => $this->l('Hide slideshow on mobile devices:'),
					'name' => 'hide_mob_col',
                    'default_value' => 0,
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'hide_mob_col_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'hide_mob_col_off',
							'value' => 0,
							'label' => $this->l('No')),
					),
                    'desc' => $this->l('if set to Yes, slider will be hidden on mobile devices (if screen width is less than 768 pixels).'),
                    'validation' => 'isBool',
				)
			),
			'submit' => array(
				'title' => $this->l('   Save all   ')
			),
		);
        $this->fields_form[2]['form'] = array(
			'legend' => array(
				'title' => $this->l('Slide on footer'),
                'icon' => 'icon-cogs'
			),
			'input' => array(
                array(
					'type' => 'text',
					'label' => $this->l('Define the number of products to be displayed:'),
					'name' => 'nbr_fot',
                    'default_value' => 4,
                    'required' => true,
                    'desc' => $this->l('Define the number of products that you would like to display on footer (default: 4).'),
                    'validation' => 'isUnsignedInt',
                    'class' => 'fixed-width-sm'
				),
                array(
					'type' => 'select',
        			'label' => $this->l('Sort by:'),
        			'name' => 'soby_fot',
                    'options' => array(
        				'query' => self::$sort_by,
        				'id' => 'id',
        				'name' => 'name',
        			),
                    'validation' => 'isUnsignedInt',
				), 
			),
			'submit' => array(
				'title' => $this->l('   Save all  ')
			),
		);        
    }
    protected function initForm()
	{
	    $helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table =  $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;

		$helper->identifier = $this->identifier;
		$helper->submit_action = 'savehomefeaturedslider';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
		return $helper;
	}
    public function hookDisplayHomeTop($params)
    {
        return $this->hookDisplayHome($params);
    }
    public function hookDisplayHomeBottom($params)
    {
        return $this->hookDisplayHome($params);
    }    
    public function hookDisplayHome($params,$flag=0)
    {
        if (!$this->isCached('homefeaturedslider.tpl', $this->stGetCacheId($flag)))
        {
            if(!$this->_prepareHook(0))
                return false;
            $this->smarty->assign(array(
                'column_slider'         => false,
                'homeverybottom'         => ($flag==2 ? true : false),
                'pro_per_lg'       => (int)Configuration::get('STSN_FEATURED_PRO_PER_LG_0'),
                'pro_per_md'       => (int)Configuration::get('STSN_FEATURED_PRO_PER_MD_0'),
                'pro_per_sm'       => (int)Configuration::get('STSN_FEATURED_PRO_PER_SM_0'),
                'pro_per_xs'       => (int)Configuration::get('STSN_FEATURED_PRO_PER_XS_0'),
                'pro_per_xxs'       => (int)Configuration::get('STSN_FEATURED_PRO_PER_XXS_0'),
            ));
        }

        return $this->display(__FILE__, 'homefeaturedslider.tpl', $this->stGetCacheId($flag));
    }

    public function hookDisplayHomeVeryBottom($params)
    {
        return $this->hookDisplayHome($params,2);
    }    
    public function hookDisplayHomeTertiaryLeft($params)
    {
        return $this->hookDisplayHome($params);
    }
    public function hookDisplayHomeTertiaryRight($params)
    {
        return $this->hookDisplayHome($params);
    }
 
	public function hookDisplayLeftColumn($params)
	{
	    if (!$this->isCached('homefeaturedslider.tpl', $this->stGetCacheId(1)))
    	{
            if(!$this->_prepareHook(1))
                return false;
            $this->smarty->assign(array(
                'column_slider'         => true,
                'homeverybottom'         => false,
            ));
        }
		return $this->display(__FILE__, 'homefeaturedslider.tpl', $this->stGetCacheId(1));
	}
	public function hookDisplayRightColumn($params)
    {
        return $this->hookDisplayLeftColumn($params);
    }
    
	public function hookDisplayHomeSecondaryLeft($params)
	{
        $this->smarty->assign(array(
            'is_homepage_secondary_left' => true,
        ));
        return $this->hookDisplayHome($params); 
    }
    
	public function hookDisplayHomeSecondaryRight($params)
    {
        return $this->hookDisplayLeftColumn($params);
    }
	public function hookDisplayProductSecondaryColumn($params)
	{
        return $this->hookDisplayLeftColumn($params);
	}
	public function hookDisplayStBlogLeftColumn($params)
	{
	    if(!Module::isInstalled('stblog') || !Module::isEnabled('stblog'))
            return false;
        return $this->hookDisplayLeftColumn($params);
	}
	public function hookDisplayStBlogRightColumn($params)
	{
	    if(!Module::isInstalled('stblog') || !Module::isEnabled('stblog'))
            return false;
        return $this->hookDisplayLeftColumn($params);
	}
    private function _prepareHook($col=0)
    {
        $ext = $col ? '_COL' : '';
        $nbr = Configuration::get('HOME_FEATURED_S_NBR_MOD'.$ext);
        ($nbr===false && $col) && $nbr = Configuration::get('HOME_FEATURED_S_NBR_MOD');
        
        if(!$nbr)
            return false;
            
        $nb = $random_number_products = (int)$nbr;
        
        $order_by = 'id_product';
        $order_way = 'DESC';
        $random = false;
        
        $soby = (int)Configuration::get('HOME_FEATURED_S_SOBY'.$ext);
        (!$soby && $col) && $soby = (int)Configuration::get('HOME_FEATURED_S_SOBY');
        switch($soby)
        {
            case 1:
                $order_by = 'name';
                $order_way = 'ASC';
            break;
            case 2:
                $order_by = 'name';
                $order_way = 'DESC';
            break;
            case 3:
                $order_by = 'price';
                $order_way = 'ASC';
            break;
            case 4:
                $order_by = 'price';
                $order_way = 'DESC';
            break;
            case 5:
                $order_by = 'id_product';
                $order_way = 'ASC';
            break;
            case 7:
                $order_by = null;
                $order_way = null;
                $random = true;
            break;
            case 8:
                $order_by = 'position';
                $order_way = 'ASC';
            break;
            case 9:
                $order_by = 'position';
                $order_way = 'DESC';
            break;
            case 10:
                $order_by = 'date_upd';
                $order_way = 'ASC';
            break;
            case 11:
                $order_by = 'date_upd';
                $order_way = 'DESC';
            break;
            case 12:
                $order_by = 'date_add';
                $order_way = 'ASC';
            break;
            case 13:
                $order_by = 'date_add';
                $order_way = 'DESC';
            break;
            default:
            break;
        }
        
        $homeSize = Image::getSize(ImageType::getFormatedName('home'));
        
        $key = $col ? $col : 0;
        if (isset(self::$cache_featured_products[$key]) && self::$cache_featured_products[$key])
            $products = self::$cache_featured_products[$key];
        else
        {
            $category = new Category(Context::getContext()->shop->getCategory(), (int)Context::getContext()->language->id);
    		$products = $category->getProducts((int)Context::getContext()->language->id, 1, ($nb ? $nb : 10), $order_by, $order_way, false, true, $random, $random_number_products);

            if(is_array($products) && count($products))
            {
                $module_stthemeeditor = Module::getInstanceByName('stthemeeditor');
                if ($module_stthemeeditor && $module_stthemeeditor->id)
                    $id_module_stthemeeditor = $module_stthemeeditor->id;
                    
                $module_sthoverimage = Module::getInstanceByName('sthoverimage');
                if ($module_sthoverimage && $module_sthoverimage->id)
                    $id_module_sthoverimage = $module_sthoverimage->id;
                    
                foreach($products as &$product)
                {
                    if(isset($id_module_stthemeeditor))
                    {
                        $product['pro_a_wishlist'] = Hook::exec('displayAnywhere', array('function'=>'getAddToWhishlistButton','id_product'=>$product['id_product'],'show_icon'=>0,'caller'=>'stthemeeditor'), $id_module_stthemeeditor);
                        $product['pro_rating_average'] = Hook::exec('displayAnywhere', array('function'=>'getProductRatingAverage','id_product'=>$product['id_product'],'caller'=>'stthemeeditor'), $id_module_stthemeeditor);
                    }
                    if(isset($id_module_sthoverimage))
                    {
                        $product['hover_image'] = Hook::exec('displayAnywhere', array('function'=>'getHoverImage','id_product'=>$product['id_product'],'product_link_rewrite'=>$product['link_rewrite'],'product_name'=>$product['name'],'home_default_height'=>$homeSize['height'],'home_default_width'=>$homeSize['width'],'caller'=>'sthoverimage'), $id_module_sthoverimage);
                    }
                }
            }

            self::$cache_featured_products[$key] = $products;
        }
        
        /*
        if(!$products)
            return false;
		*/
        
        $easing = Configuration::get('HOME_FEATURED_S_EASING'.$ext);
        ($easing===false && $col) && $easing = Configuration::get('HOME_FEATURED_S_EASING');
        
        $slideshow = Configuration::get('HOME_FEATURED_S_SLIDESHOW'.$ext);
        ($slideshow===false && $col) && $slideshow = Configuration::get('HOME_FEATURED_S_SLIDESHOW');
        
        $s_speed = Configuration::get('HOME_FEATURED_S_S_SPEED'.$ext);
        ($s_speed===false && $col) && $s_speed = Configuration::get('HOME_FEATURED_S_S_SPEED');
        
        $a_speed = Configuration::get('HOME_FEATURED_S_A_SPEED'.$ext);
        ($a_speed===false && $col) && $a_speed = Configuration::get('HOME_FEATURED_S_A_SPEED');
        
        $pause_on_hover = Configuration::get('HOME_FEATURED_S_PAUSE'.$ext);
        ($pause_on_hover===false && $col) && $pause_on_hover = Configuration::get('HOME_FEATURED_S_PAUSE');
        
        $loop = Configuration::get('HOME_FEATURED_S_LOOP'.$ext);
        ($loop===false && $col) && $loop = Configuration::get('HOME_FEATURED_S_LOOP');
        
        $move = Configuration::get('HOME_FEATURED_S_MOVE'.$ext);
        ($move===false && $col) && $move = Configuration::get('HOME_FEATURED_S_MOVE');
        
        $items = Configuration::get('HOME_FEATURED_S_ITEMS_COL');
        
        $hide_mob = Configuration::get('HOME_FEATURED_S_HIDE_MOB'.$ext);
        ($hide_mob===false && $col) && $hide_mob = Configuration::get('HOME_FEATURED_S_HIDE_MOB');

        $display_sd = Configuration::get('HOME_FEATURED_S_DISPLAY_SD');

        $this->smarty->assign(array(
            'products'              => $products,
            'add_prod_display'      => Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY'),
            'homeSize'              => $homeSize,
            'mediumSize'            => Image::getSize(ImageType::getFormatedName('medium')),
            'thumbSize'             => Image::getSize(ImageType::getFormatedName('thumb')),
            'slider_easing'         => self::$easing[$easing]['name'],
            'slider_slideshow'      => $slideshow,
            'slider_s_speed'        => $s_speed,
            'slider_a_speed'        => $a_speed,
            'slider_pause_on_hover' => $pause_on_hover,
            'slider_loop'           => $loop,
            'slider_move'           => $move,
            'slider_items'          => $items,
            'hide_mob'              => (int)$hide_mob,
            'display_sd'            => (int)$display_sd,
            'display_as_grid'       => Configuration::get('HOME_FEATURED_S_GRID'),
		));
        return true;
    }
    
    public function hookDisplayFooter($params)
    {
	    if (!$this->isCached('homefeaturedslider-footer.tpl', $this->getCacheId()))
	    {
            $nbr = Configuration::get('HOME_FEATURED_S_NBR_FOT');
            $nbr = $nbr ? (int)$nbr : 4;
                
            $nb = $random_number_products = (int)$nbr;
            
            $order_by = 'id_product';
            $order_way = 'DESC';
            $random = false;
            
            $soby = (int)Configuration::get('HOME_FEATURED_S_SOBY_FOT');
            switch($soby)
            {
                case 1:
                    $order_by = 'name';
                    $order_way = 'ASC';
                break;
                case 2:
                    $order_by = 'name';
                    $order_way = 'DESC';
                break;
                case 3:
                    $order_by = 'price';
                    $order_way = 'ASC';
                break;
                case 4:
                    $order_by = 'price';
                    $order_way = 'DESC';
                break;
                case 5:
                    $order_by = 'id_product';
                    $order_way = 'ASC';
                break;
                case 7:
                    $order_by = null;
                    $order_way = null;
                    $random = true;
                break;
                case 8:
                    $order_by = 'position';
                    $order_way = 'ASC';
                break;
                case 8:
                    $order_by = 'position';
                    $order_way = 'DESC';
                break;
                default:
                break;
            }
            
            $key = 'FOT';
            if (isset(self::$cache_featured_products[$key]) && self::$cache_featured_products[$key])
                $products = self::$cache_featured_products[$key];
            else
            {
                $category = new Category(Context::getContext()->shop->getCategory(), (int)Context::getContext()->language->id);
        		$products = $category->getProducts((int)Context::getContext()->language->id, 1, ($nb ? $nb : 10), $order_by, $order_way, false, true, $random, $random_number_products);
                self::$cache_featured_products[$key] = $products;
            }
            
            $this->smarty->assign(array(
    			'products' => $products,
    			'add_prod_display' => Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY'),
    			'thumbSize' => Image::getSize(ImageType::getFormatedName('thumb')),
    		));
	    }
		return $this->display(__FILE__, 'homefeaturedslider-footer.tpl', $this->getCacheId());
    }
    public function hookDisplayFooterTop($params)
    {
        return $this->hookDisplayFooter($params);
    }
    public function hookDisplayFooterSecondary($params)
    {
        return $this->hookDisplayFooter($params);        
    }
    public function hookAddProduct($params)
	{
		$this->clearSliderCache();
	}

	public function hookUpdateProduct($params)
	{
		$this->clearSliderCache();
	}

	public function hookDeleteProduct($params)
	{
		$this->clearSliderCache();
	}
	private function clearSliderCache()
	{
		$this->_clearCache('homefeaturedslider.tpl');
		$this->_clearCache('homefeaturedslider-footer.tpl');
		$this->_clearCache('homefeaturedslider.tpl',$this->stGetCacheId(0));
        $this->_clearCache('homefeaturedslider.tpl',$this->stGetCacheId(1));
		$this->_clearCache('homefeaturedslider.tpl',$this->stGetCacheId(2));
    }
	protected function stGetCacheId($key,$name = null)
	{
		$cache_id = parent::getCacheId($name);
		return $cache_id.'_'.$key;
	}
    private function getConfigFieldsValues()
    {
        $fields_values = array(
            'nbr_mod' => Configuration::get('HOME_FEATURED_S_NBR_MOD'),
            'easing' => Configuration::get('HOME_FEATURED_S_EASING'),
            'slideshow' => Configuration::get('HOME_FEATURED_S_SLIDESHOW'),
            's_speed' => Configuration::get('HOME_FEATURED_S_S_SPEED'),
            'a_speed' => Configuration::get('HOME_FEATURED_S_A_SPEED'),
            'pause' => Configuration::get('HOME_FEATURED_S_PAUSE'),
            'loop' => Configuration::get('HOME_FEATURED_S_LOOP'),
            'move' => Configuration::get('HOME_FEATURED_S_MOVE'),
            'soby' => Configuration::get('HOME_FEATURED_S_SOBY'),
            'hide_mob' => Configuration::get('HOME_FEATURED_S_HIDE_MOB'),
            'display_sd' => Configuration::get('HOME_FEATURED_S_DISPLAY_SD'),
            'grid'=> Configuration::get('HOME_FEATURED_S_GRID'),
            
            'nbr_mod_col' => Configuration::get('HOME_FEATURED_S_NBR_MOD_COL'),
            'easing_col' => Configuration::get('HOME_FEATURED_S_EASING_COL'),
            'slideshow_col' => Configuration::get('HOME_FEATURED_S_SLIDESHOW_COL'),
            's_speed_col' => Configuration::get('HOME_FEATURED_S_S_SPEED_COL'),
            'a_speed_col' => Configuration::get('HOME_FEATURED_S_A_SPEED_COL'),
            'pause_col' => Configuration::get('HOME_FEATURED_S_PAUSE_COL'),
            'loop_col' => Configuration::get('HOME_FEATURED_S_LOOP_COL'),
            'move_col' => Configuration::get('HOME_FEATURED_S_MOVE_COL'),
            'items_col' => Configuration::get('HOME_FEATURED_S_ITEMS_COL'),
            'soby_col' => Configuration::get('HOME_FEATURED_S_SOBY_COL'),
            'hide_mob_col' => Configuration::get('HOME_FEATURED_S_HIDE_MOB_COL'),
            
            'nbr_fot' =>Configuration::get('HOME_FEATURED_S_NBR_FOT'),
            'soby_fot' =>Configuration::get('HOME_FEATURED_S_SOBY_FOT'),        
        );
        return $fields_values;
    }
    public function BuildDropListGroup($group)
    {
        if(!is_array($group) || !count($group))
            return false;

        $html = '<div class="row">';
        foreach($group AS $key => $k)
        {
             if($key==3)
                 $html .= '</div><div class="row">';

             $html .= '<div class="col-xs-4 col-sm-3"><label '.(isset($k['tooltip']) ? ' data-html="true" data-toggle="tooltip" class="label-tooltip" data-original-title="'.$k['tooltip'].'" ':'').'>'.$k['label'].'</label>'.
             '<select name="'.$k['id'].'" 
             id="'.$k['id'].'" 
             class="'.(isset($k['class']) ? $k['class'] : 'fixed-width-md').'"'.
             (isset($k['onchange']) ? ' onchange="'.$k['onchange'].'"':'').' >';
            
            for ($i=1; $i < 7; $i++){
                $html .= '<option value="'.$i.'" '.(Configuration::get('STSN_'.strtoupper($k['id'])) == $i ? ' selected="selected"':'').'>'.$i.'</option>';
            }
                                
            $html .= '</select></div>';
        }
        return $html.'</div>';
    }
    public function findCateProPer()
    {
        return array(
            array(
                'id' => 'featured_pro_per_lg_0',
                'label' => $this->l('Large devices'),
                'tooltip' => $this->l('Desktops (>1200px)'),
            ),
            array(
                'id' => 'featured_pro_per_md_0',
                'label' => $this->l('Medium devices'),
                'tooltip' => $this->l('Desktops (>992px)'),
            ),
            array(
                'id' => 'featured_pro_per_sm_0',
                'label' => $this->l('Small devices'),
                'tooltip' => $this->l('Tablets (>768px)'),
            ),
            array(
                'id' => 'featured_pro_per_xs_0',
                'label' => $this->l('Extra small devices'),
                'tooltip' => $this->l('Phones (>480px)'),
            ),
            array(
                'id' => 'featured_pro_per_xxs_0',
                'label' => $this->l('Extra extra small devices'),
                'tooltip' => $this->l('Phones (<480px)'),
            ),
        );
    }
}