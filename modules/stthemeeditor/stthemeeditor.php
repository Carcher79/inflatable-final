<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class StThemeEditor extends Module
{	
    protected static $access_rights = 0775;
    public $imgtype = array('jpg', 'gif', 'jpeg', 'png');
    public $defaults;
    private $_html;
    private $_hooks;
    public $fields_form; 
    public $fields_value;   
    public $validation_errors = array();
    private $systemFonts = array("Helvetica","Arial","Verdana","Georgia","Tahoma","Times New Roman","sans-serif");
    private $googleFonts = array('ABeeZee','Abel','Abril Fatface','Aclonica','Acme','Actor','Adamina','Advent Pro','Aguafina Script','Akronim','Aladin','Aldrich','Alef','Alegreya','Alegreya Sans SC','Alegreya SC','Alex Brush','Alfa Slab One','Alice','Alike','Alike Angular','Allan','Allerta','Allerta Stencil','Allura','Almendra','Almendra Display','Almendra SC','Amarante','Amaranth','Amatic SC','Amethysta','Anaheim','Andada','Andika','Angkor','Annie Use Your Telescope','Anonymous Pro','Antic','Antic Didone','Antic Slab','Anton','Arapey','Arbutus','Arbutus Slab','Architects Daughter','Archivo Black','Archivo Narrow','Arimo','Arizonia','Armata','Artifika','Arvo','Asap','Asset','Astloch','Asul','Atomic Age','Aubrey','Audiowide','Autour One','Average','Average Sans','Averia Gruesa Libre','Averia Libre','Averia Sans Libre','Averia Serif Libre','Bad Script','Balthazar','Bangers','Basic','Battambang','Baumans','Bayon','Belgrano','Belleza','BenchNine','Bentham','Berkshire Swash','Bevan','Bigelow Rules','Bigshot One','Bilbo','Bilbo Swash Caps','Bitter','Black Ops One','Bokor','Bonbon','Boogaloo','Bowlby One','Bowlby One SC','Brawler','Bree Serif','Bubblegum Sans','Bubbler One','Buda','Buenard','Butcherman','Butterfly Kids','Cabin','Cabin Condensed','Cabin Sketch','Caesar Dressing','Cagliostro','Calligraffitti','Cambo','Candal','Cantarell','Cantata One','Cantora One','Capriola','Cardo','Carme','Carrois Gothic','Carrois Gothic SC','Carter One','Caudex','Cedarville Cursive','Ceviche One','Changa One','Chango','Chau Philomene One','Chela One','Chelsea Market','Chenla','Cherry Cream Soda','Cherry Swash','Chewy','Chicle','Chivo','Cinzel','Cinzel Decorative','Clicker Script','Coda','Coda Caption','Codystar','Combo','Comfortaa','Coming Soon','Concert One','Condiment','Content','Contrail One','Convergence','Cookie','Copse','Corben','Courgette','Cousine','Coustard','Covered By Your Grace','Crafty Girls','Creepster','Crete Round','Crimson Text','Croissant One','Crushed','Cuprum','Cutive','Cutive Mono','Damion','Dancing Script','Dangrek','Dawning of a New Day','Days One','Delius','Delius Swash Caps','Delius Unicase','Della Respira','Denk One','Devonshire','Didact Gothic','Diplomata','Diplomata SC','Domine','Donegal One','Doppio One','Dorsa','Dosis','Dr Sugiyama','Droid Sans','Droid Sans Mono','Droid Serif','Duru Sans','Dynalight','EB Garamond','Eagle Lake','Eater','Economica','Electrolize','Elsie','Elsie Swash Caps','Emblema One','Emilys Candy','Engagement','Englebert','Enriqueta','Erica One','Esteban','Euphoria Script','Ewert','Exo','Expletus Sans','Fanwood Text','Fascinate','Fascinate Inline','Faster One','Fasthand','Federant','Federo','Felipa','Fenix','Finger Paint','Fjalla One','Fjord One','Flamenco','Flavors','Fondamento','Fontdiner Swanky','Forum','Francois One','Freckle Face','Fredericka the Great','Fredoka One','Freehand','Fresca','Frijole','Fruktur','Fugaz One','GFS Didot','GFS Neohellenic','Gabriela','Gafata','Galdeano','Galindo','Gentium Basic','Gentium Book Basic','Geo','Geostar','Geostar Fill','Germania One','Gilda Display','Give You Glory','Glass Antiqua','Glegoo','Gloria Hallelujah','Goblin One','Gochi Hand','Gorditas','Goudy Bookletter 1911','Graduate','Grand Hotel','Gravitas One','Great Vibes','Griffy','Gruppo','Gudea','Habibi','Hammersmith One','Hanalei','Hanalei Fill','Handlee','Hanuman','Happy Monkey','Headland One','Henny Penny','Herr Von Muellerhoff','Holtwood One SC','Homemade Apple','Homenaje','IM Fell DW Pica','IM Fell DW Pica SC','IM Fell Double Pica','IM Fell Double Pica SC','IM Fell English','IM Fell English SC','IM Fell French Canon','IM Fell French Canon SC','IM Fell Great Primer','IM Fell Great Primer SC','Iceberg','Iceland','Imprima','Inconsolata','Inder','Indie Flower','Inika','Irish Grover','Istok Web','Italiana','Italianno','Jacques Francois','Jacques Francois Shadow','Jim Nightshade','Jockey One','Jolly Lodger','Josefin Sans','Josefin Slab','Joti One','Judson','Julee','Julius Sans One','Junge','Jura','Just Another Hand','Just Me Again Down Here','Kameron','Karla','Kaushan Script','Kavoon','Keania One','Kelly Slab','Kenia','Khmer','Kite One','Knewave','Kotta One','Koulen','Kranky','Kreon','Kristi','Krona One','La Belle Aurore','Lancelot','Lato','League Script','Leckerli One','Ledger','Lekton','Lemon','Libre Baskerville','Life Savers','Lilita One','Limelight','Linden Hill','Lobster','Lobster Two','Londrina Outline','Londrina Shadow','Londrina Sketch','Londrina Solid','Lora','Love Ya Like A Sister','Loved by the King','Lovers Quarrel','Luckiest Guy','Lusitana','Lustria','Macondo','Macondo Swash Caps','Magra','Maiden Orange','Mako','Marcellus','Marcellus SC','Marck Script','Margarine','Marko One','Marmelad','Marvel','Mate','Mate SC','Maven Pro','McLaren','Meddon','MedievalSharp','Medula One','Megrim','Meie Script','Merienda','Merienda One','Merriweather','Merriweather Sans','Metal','Metal Mania','Metamorphous','Metrophobic','Michroma','Milonga','Miltonian','Miltonian Tattoo','Miniver','Miss Fajardose','Modern Antiqua','Molengo','Molle','Monda','Monofett','Monoton','Monsieur La Doulaise','Montaga','Montez','Montserrat','Montserrat Alternates','Montserrat Subrayada','Moul','Moulpali','Mountains of Christmas','Mouse Memoirs','Mr Bedfort','Mr Dafoe','Mr De Haviland','Mrs Saint Delafield','Mrs Sheppards','Muli','Mystery Quest','Neucha','Neuton','New Rocker','News Cycle','Niconne','Nixie One','Nobile','Nokora','Norican','Nosifer','Nothing You Could Do','Noticia Text','Nova Cut','Nova Flat','Nova Mono','Nova Oval','Nova Round','Nova Script','Nova Slim','Nova Square','Numans','Nunito','Odor Mean Chey','Offside','Old Standard TT','Oldenburg','Oleo Script','Oleo Script Swash Caps','Open Sans','Open Sans Condensed','Oranienbaum','Orbitron','Oregano','Orienta','Original Surfer','Oswald','Over the Rainbow','Overlock','Overlock SC','Ovo','Oxygen','Oxygen Mono','PT Mono','PT Sans','PT Sans Caption','PT Sans Narrow','PT Serif','PT Serif Caption','Pacifico','Paprika','Parisienne','Passero One','Passion One','Patrick Hand','Patrick Hand SC','Patua One','Paytone One','Peralta','Permanent Marker','Petit Formal Script','Petrona','Philosopher','Piedra','Pinyon Script','Pirata One','Plaster','Play','Playball','Playfair Display','Playfair Display SC','Podkova','Poiret One','Poller One','Poly','Pompiere','Pontano Sans','Port Lligat Sans','Port Lligat Slab','Prata','Preahvihear','Press Start 2P','Princess Sofia','Prociono','Prosto One','Puritan','Purple Purse','Quando','Quantico','Quattrocento','Quattrocento Sans','Questrial','Quicksand','Quintessential','Qwigley','Racing Sans One','Radley','Raleway','Raleway Dots','Rambla','Rammetto One','Ranchers','Rancho','Rationale','Redressed','Reenie Beanie','Revalia','Ribeye','Ribeye Marrow','Righteous','Risque','Roboto','Roboto Condensed','Rochester','Rock Salt','Rokkitt','Romanesco','Ropa Sans','Rosario','Rosarivo','Rouge Script','Ruda','Rufina','Ruge Boogie','Ruluko','Rum Raisin','Ruslan Display','Russo One','Ruthie','Rye','Sacramento','Sail','Salsa','Sanchez','Sancreek','Sansita One','Sarina','Satisfy','Scada','Schoolbell','Seaweed Script','Sevillana','Seymour One','Shadows Into Light','Shadows Into Light Two','Shanti','Share','Share Tech','Share Tech Mono','Shojumaru','Short Stack','Siemreap','Sigmar One','Signika','Signika Negative','Simonetta','Sintony','Sirin Stencil','Six Caps','Skranji','Slackey','Smokum','Smythe','Sniglet','Snippet','Snowburst One','Sofadi One','Sofia','Sonsie One','Sorts Mill Goudy','Source Code Pro','Source Sans Pro','Special Elite','Spicy Rice','Spinnaker','Spirax','Squada One','Stalemate','Stalinist One','Stardos Stencil','Stint Ultra Condensed','Stint Ultra Expanded','Stoke','Strait','Sue Ellen Francisco','Sunshiney','Supermercado One','Suwannaphum','Swanky and Moo Moo','Syncopate','Tangerine','Taprom','Tauri','Telex','Tenor Sans','Text Me One','The Girl Next Door','Tienne','Tinos','Titan One','Titillium Web','Trade Winds','Trocchi','Trochut','Trykker','Tulpen One','Ubuntu','Ubuntu Condensed','Ubuntu Mono','Ultra','Uncial Antiqua','Underdog','Unica One','UnifrakturCook','UnifrakturMaguntia','Unkempt','Unlock','Unna','VT323','Vampiro One','Varela','Varela Round','Vast Shadow','Vibur','Vidaloka','Viga','Voces','Volkhov','Vollkorn','Voltaire','Waiting for the Sunrise','Wallpoet','Walter Turncoat','Warnes','Wellfleet','Wendy One','Wire One','Yanone Kaffeesatz','Yellowtail','Yeseva One','Yesteryear','Zeyada');
    private $predefinedColor;
    public static $position_right_panel = array(
		array('id' => '1_0', 'name' => 'At bottom of screen'),
		array('id' => '1_10', 'name' => 'Bottom 10%'),
		array('id' => '1_20', 'name' => 'Bottom 20%'),
		array('id' => '1_30', 'name' => 'Bottom 30%'),
		array('id' => '1_40', 'name' => 'Bottom 40%'),
		array('id' => '1_50', 'name' => 'Bottom 50%'),
		array('id' => '2_0', 'name' => 'At top of screen'),
		array('id' => '2_10', 'name' => 'Top 10%'),
		array('id' => '2_20', 'name' => 'Top 20%'),
		array('id' => '2_30', 'name' => 'Top 30%'),
		array('id' => '2_40', 'name' => 'Top 40%'),
		array('id' => '2_50', 'name' => 'Top 50%'),
    );
    
    public static $easing = array(
		array('id' => 0, 'name' => 'swing'),
		array('id' => 1, 'name' => 'easeInQuad'),
		array('id' => 2, 'name' => 'easeOutQuad'),
		array('id' => 3, 'name' => 'easeInOutQuad'),
		array('id' => 4, 'name' => 'easeInCubic'),
		array('id' => 5, 'name' => 'easeOutCubic'),
		array('id' => 6, 'name' => 'easeInOutCubic'),
		array('id' => 7, 'name' => 'easeInQuart'),
		array('id' => 8, 'name' => 'easeOutQuart'),
		array('id' => 9, 'name' => 'easeInOutQuart'),
		array('id' => 10, 'name' => 'easeInQuint'),
		array('id' => 11, 'name' => 'easeOutQuint'),
		array('id' => 12, 'name' => 'easeInOutQuint'),
		array('id' => 13, 'name' => 'easeInSine'),
		array('id' => 14, 'name' => 'easeOutSine'),
		array('id' => 15, 'name' => 'easeInOutSine'),
		array('id' => 16, 'name' => 'easeInExpo'),
		array('id' => 17, 'name' => 'easeOutExpo'),
		array('id' => 18, 'name' => 'easeInOutExpo'),
		array('id' => 19, 'name' => 'easeInCirc'),
		array('id' => 20, 'name' => 'easeOutCirc'),
		array('id' => 21, 'name' => 'easeInOutCirc'),
		array('id' => 22, 'name' => 'easeInElastic'),
		array('id' => 23, 'name' => 'easeOutElastic'),
		array('id' => 24, 'name' => 'easeInOutElastic'),
		array('id' => 25, 'name' => 'easeInBack'),
		array('id' => 26, 'name' => 'easeOutBack'),
		array('id' => 27, 'name' => 'easeInOutBack'),
		array('id' => 28, 'name' => 'easeInBounce'),
		array('id' => 29, 'name' => 'easeOutBounce'),
		array('id' => 30, 'name' => 'easeInOutBounce'),
	);
    public static $items = array(
		array('id' => 2, 'name' => '2'),
		array('id' => 3, 'name' => '3'),
		array('id' => 4, 'name' => '4'),
		array('id' => 5, 'name' => '5'),
		array('id' => 6, 'name' => '6'),
    );
    public static $textTransform = array(
		array('id' => 0, 'name' => 'none'),
		array('id' => 1, 'name' => 'uppercase'),
		array('id' => 2, 'name' => 'lowercase'),
		array('id' => 3, 'name' => 'capitalize'),
    );
    public static $tabs        = array(
        array('id'  => '0', 'name' => 'General'),
        array('id'  => '1', 'name' => 'Category page'),
        array('id'  => '16', 'name' => 'Product page'),
        array('id'  => '2', 'name' => 'Color'),
        array('id'  => '3', 'name' => 'Font'),
        array('id'  => '15', 'name' => 'Stickers'),
        array('id'  => '4', 'name' => 'Header'),
        array('id'  => '5', 'name' => 'Menu'),
        array('id'  => '6', 'name' => 'Body'),
        array('id'  => '7', 'name' => 'Footer-Top'),
        array('id'  => '8', 'name' => 'Footer'),
        array('id'  => '9', 'name' => 'Footer-Secondary'),
        array('id'  => '10', 'name' => 'Footer-Copyright'),
        array('id'  => '11,12,13', 'name' => 'Slides'),
        array('id'  => '14', 'name' => 'Custom codes'),
        array('id'  => '17', 'name' => 'Module navigation'),
        array('id'  => '18', 'name' => 'Iphone/Ipad icons'),
    );
    
	public function __construct()
	{
		$this->name = 'stthemeeditor';
		$this->tab = 'administration';
		$this->version = '3.0.1';
		$this->author = 'SUNNYTOO.COM';
		$this->need_instance = 0;
        $this->bootstrap = true;

        $this->bootstrap = true;
	 	parent::__construct();

		$this->displayName = $this->l('Theme editor');
		$this->description = $this->l('Allows to change theme design');
        
        $this->defaults = array(
            'responsive' => 1,
            'responsive_max' => 1,
            'boxstyle' => 1,
            'version_switching' => 0,
            'welcome' => array('1'=>'Welcome'),
            'welcome_logged' => array('1'=>'Welcome'),
            'welcome_link' => '',
            'product_view' => 'grid_view',
            'copyright_text' => array(1=>'&copy; 2014 Powered by Presta Shop&trade;. All Rights Reserved'),
            'search_label' => array(1=>'Search here'),
            'newsletter_label' => array(1=>'Your e-mail'),
		    'footer_img' => 'img/payment-options.png', 
		    'icon_iphone_57' => 'img/touch-icon-iphone-57.png', 
		    'icon_iphone_72' => 'img/touch-icon-iphone-72.png', 
		    'icon_iphone_114' => 'img/touch-icon-iphone-114.png', 
		    'icon_iphone_144' => 'img/touch-icon-iphone-144.png', 
		    'custom_css' => '', 
		    'custom_js' => '', 
		    'tracking_code' => '', 
            'scroll_to_top' => 1,
            'addtocart_animation' => 0,
            'google_rich_snippets' => 1,
            'display_tax_label' => 0,
            'position_right_panel' => '1_40',
            'flyout_buttons' => 0,
            'length_of_product_name' => 0,
            'logo_position' => 0,
            'logo_height' => 0,
            'megamenu_position' => 0,
            //font
    		"font_text" => '',
    		"font_price" => '',
    		"font_price_size" => 0,
    		"font_old_price_size" => 0,
    		"font_heading" => 'Fjalla One',
    		"font_heading_weight" => 0,
    		"font_heading_trans" => 1,
    		"font_heading_size" => 0,
    		"footer_heading_size" => 0,
            /*
    		"font_title" => 'Fjalla One',
    		"font_title_weight" => 0,
    		"font_title_trans" => 1,
    		"font_title_size" => '',
            */
    		"font_menu" => 'Fjalla One',
    		"font_menu_weight" => 0,
    		"font_menu_trans" => 1,
    		"font_menu_size" => 0,
    		"font_cart_btn" => 'Fjalla One',
            "font_latin_support" => 0,
            "font_cyrillic_support" => 0,
            "font_vietnamese" => 0,
            "font_greek_support" => 0,
            //style
            'display_comment_rating' => 1,
            'display_category_title' => 1,
            'display_category_desc' => 0,
            'display_category_image' => 0,
            'display_subcate' => 1,
            'display_pro_attr' => 0,
            'product_secondary' => 1,
            'product_big_image' => 0,
            'show_brand_logo' => 1,
            'display_cate_desc_full' => 0,
            'show_short_desc_on_grid' => 0,
            'display_color_list' => 0,
            //footer
            'footer_border_color' => '',
            'second_footer_color' => '',
            'footer_color' => '',
            'footer_link_color' => '',
            'footer_link_hover_color' => '',
            
            'footer_top_border_color' => '',
            'footer_top_bg' => '',
            'footer_top_con_bg' => '',
    		"f_top_bg_img" => '',
    		"f_top_bg_repeat" => 0, 
    		"f_top_bg_position" => 0, 
    		"f_top_bg_pattern" => 0, 
            'footer_bg_color' => '',
            'footer_con_bg_color' => '',
    		"footer_bg_img" => '',
    		"footer_bg_repeat" => 0, 
    		"footer_bg_position" => 0, 
    		"footer_bg_pattern" => 0, 
            'footer_secondary_bg' => '',
            'footer_secondary_con_bg' => '',
    		"f_secondary_bg_img" => '',
    		"f_secondary_bg_repeat" => 0, 
    		"f_secondary_bg_position" => 0, 
    		"f_secondary_bg_pattern" => 0, 
            'footer_info_bg' => '',
            'footer_info_con_bg' => '',
    		"f_info_bg_img" => '',
    		"f_info_bg_repeat" => 0, 
    		"f_info_bg_position" => 0, 
    		"f_info_bg_pattern" => 0, 
            //header
            'header_text_color' => '',
            'header_link_color' => '',
            'header_link_hover_color' => '',
            'header_link_hover_bg' => '',
            'dropdown_hover_color' => '',
            'dropdown_bg_color' => '',
    		"header_topbar_bg" => '', 
    		//"header_topbar_bc" => '',
    		"header_topbar_sep" => '',
            'header_bg_color' => '',
            'header_con_bg_color' => '',
    		"header_bg_img" => '',
    		"header_bg_repeat" => 0, 
    		"header_bg_position" => 0, 
            "header_bg_pattern" => 0,  
    		"display_banner_bg" => 0,  
            //body
    		"body_bg_color" => '',
            "body_con_bg_color" => '',
    		"body_bg_img" => '',
    		"body_bg_repeat" => 0, 
    		"body_bg_position" => 0, 
    		"body_bg_fixed" => 0,
    		"body_bg_pattern" => 0, 
            'main_con_bg_color' => '',
            //crossselling
            'cs_easing' => 0,
            'cs_slideshow' => 0,
            'cs_s_speed' => 7000,
            'cs_a_speed' => 400,
            'cs_pause_on_hover' => 1,
            'cs_loop' => 0,
            'cs_move' => 0,
            'cs_per_lg_0' => 5,
            'cs_per_md_0' => 5,
            'cs_per_sm_0' => 4,
            'cs_per_xs_0' => 3,
            'cs_per_xxs_0' => 2,
            //productcategory
            'pc_easing' => 0,
            'pc_slideshow' => 0,
            'pc_s_speed' => 7000,
            'pc_a_speed' => 400,
            'pc_pause_on_hover' => 1,
            'pc_loop' => 0,
            'pc_move' => 0,
            'pc_per_lg_0' => 5,
            'pc_per_md_0' => 5,
            'pc_per_sm_0' => 4,
            'pc_per_xs_0' => 3,
            'pc_per_xxs_0' => 2,
            //accessories
            'ac_easing' => 0,
            'ac_slideshow' => 0,
            'ac_s_speed' => 7000,
            'ac_a_speed' => 400,
            'ac_pause_on_hover' => 1,
            'ac_loop' => 0,
            'ac_move' => 0,
            'ac_per_lg_0' => 5,
            'ac_per_md_0' => 5,
            'ac_per_sm_0' => 4,
            'ac_per_xs_0' => 3,
            'ac_per_xxs_0' => 2,
            //color
            'text_color' => '',
            'link_color' => '',
            'link_hover_color' => '',
            'breadcrumb_color' => '',
            'breadcrumb_hover_color' => '',
            'breadcrumb_bg' => '',
            'price_color' => '',
            'icon_color' => '',
            'icon_hover_color' => '',
            'icon_bg_color' => '',
            'icon_hover_bg_color' => '',
            'icon_disabled_color' => '',
            'right_panel_border' => '',
            'starts_color' => '',
            'circle_number_color' => '',
            'circle_number_bg' => '',
            'block_headings_color' => '',
            'headings_color' => '',
            'f_top_h_color' => '',
            'footer_h_color' => '',
            'f_secondary_h_color' => '',
            //button
            'btn_color' => '',
            'btn_hover_color' => '',
            'btn_bg_color' => '',
            'btn_hover_bg_color' => '',
            'p_btn_color' => '',
            'p_btn_hover_color' => '',
            'p_btn_bg_color' => '',
            'p_btn_hover_bg_color' => '',
            //menu
            'sticky_menu' => 1,
            'menu_color' => '',
            'menu_bg_color' => '',
            'menu_hover_color' => '',
            'menu_hover_bg' => '',
            'second_menu_color' => '',
            'second_menu_hover_color' => '',
            'third_menu_color' => '',
            'third_menu_hover_color' => '',
            'menu_mob_color' => '',
            'menu_mob_bg' => '',
            'menu_mob_hover_color' => '',
            'menu_mob_hover_bg' => '',
            'menu_mob_items1_color' => '',
            'menu_mob_items2_color' => '',
            'menu_mob_items3_color' => '',
            'menu_mob_items1_bg' => '',
            'menu_mob_items2_bg' => '',
            'menu_mob_items3_bg' => '',
            //sticker
            'new_color' => '',
            'new_style' => 0,
            'new_bg_color' => '',
            'new_bg_img' => '',
            'new_stickers_width' => '',
            'new_stickers_top' => 25,
            'new_stickers_right' => 0,
            'sale_color' => '',
            'sale_style' => 0,
            'sale_bg_color' => '',
            'sale_bg_img' => '',
            'sale_stickers_width' => '',
            'sale_stickers_top' => 25,
            'sale_stickers_left' => 0,
            'discount_percentage' => 0,
            'price_drop_border_color' => '',
            'price_drop_bg_color' => '',
            'price_drop_color' => '',
            'price_drop_bottom' => 50,
            'price_drop_right' => 10,
            'price_drop_width' => 0,
            //
            'cart_icon' => 0,
            'wishlist_icon' => 0,
            'compare_icon' => 0,
            //
            'pro_tab_color' => '',
            'pro_tab_active_color' => '',
            'pro_tab_bg' => '',
            'pro_tab_active_bg' => '',
            'pro_tab_content_bg' => '',
            'display_pro_tags' => 0,
            //
            'category_pro_per_lg_3' => 3,
            'category_pro_per_md_3' => 3,
            'category_pro_per_sm_3' => 2,
            'category_pro_per_xs_3' => 2,
            'category_pro_per_xxs_3' => 1,

            'category_pro_per_lg_2' => 4,
            'category_pro_per_md_2' => 4,
            'category_pro_per_sm_2' => 3,
            'category_pro_per_xs_2' => 2,
            'category_pro_per_xxs_2' => 1,
            
            'category_pro_per_lg_1' => 5,
            'category_pro_per_md_1' => 5,
            'category_pro_per_sm_1' => 4,
            'category_pro_per_xs_1' => 3,
            'category_pro_per_xxs_1' => 2,

            'hometab_pro_per_lg_0' => 4,
            'hometab_pro_per_md_0' => 4,
            'hometab_pro_per_sm_0' => 3,
            'hometab_pro_per_xs_0' => 2,
            'hometab_pro_per_xxs_0' => 1,

            'packitems_pro_per_lg_0' => 4,
            'packitems_pro_per_md_0' => 4,
            'packitems_pro_per_sm_0' => 3,
            'packitems_pro_per_xs_0' => 2,
            'packitems_pro_per_xxs_0' => 1,

            'categories_per_lg_0' => 5,
            'categories_per_md_0' => 5,
            'categories_per_sm_0' => 4,
            'categories_per_xs_0' => 3,
            'categories_per_xxs_0' => 2,
            //1.6
            'category_show_all_btn' => 0,
            'zoom_type' => 0,

            'breadcrumb_width' => 0,
            'breadcrumb_bg_style' => 0,
            'megamenu_width' => 0,
            //
            'flyout_buttons_bg' => 0,
            
        );
        $this->predefinedColor = array(
            'responsive_max' => array(
                'green' => '1',
                'gray' => '0',
                'red' => '1',
                'blue' => '1',
                'brown' => '1',
                'yellow' => '1',
            ),
            'boxstyle' => array(
                'green' => '1',
                'gray' => '2',
                'red' => '1',
                'blue' => '2',
                'brown' => '1',
                'yellow' => '1',
            ),
            'link_hover_color' => array(
                'green' => '',
                'gray' => '#00A161',
                'red' => '#F64C65',
                'blue' => '#47B0BA',
                'brown' => '#9f824a',
                'yellow' => '#FECC0D',
            ),
            'price_color' => array(
                'green' => '',
                'gray' => '#666666',
                'red' => '#F64C65',
                'blue' => '#47B0BA',
                'brown' => '#9f824a',
                'yellow' => '#666666',
            ),
            'icon_color' => array(
                'green' => '',
                'gray' => '#666666',
                'red' => '#F64C65',
                'blue' => '#47B0BA',
                'brown' => '#666666',
                'yellow' => '#666666',
            ),
            'icon_hover_color' => array(
                'green' => '',
                'gray' => '',
                'red' => '',
                'blue' => '',
                'brown' => '#ffffff',
                'yellow' => '#666666',
            ),
            'icon_bg_color' => array(
                'green' => '',
                'gray' => '',
                'red' => '',
                'blue' => '',
                'brown' => '#ffffff',
                'yellow' => '#EDE9E9',
            ),
            'icon_hover_bg_color' =>  array(
                'green' => '',
                'gray' => '#666666',
                'red' => '#F64C65',
                'blue' => '#47B0BA',
                'brown' => '#9f824a',
                'yellow' => '#FECC0D',
            ),
            'icon_disabled_color' => array(
                'green' => '',
                'gray' => '',
                'red' => '',
                'blue' => '',
                'brown' => '',
                'yellow' => '',
            ),
            'right_panel_border' => array(
                'green' => '',
                'gray' => '',
                'red' => '',
                'blue' => '',
                'brown' => '',
                'yellow' => '',
            ),
            'new_style' => array(
                'green' => 0,
                'gray' => 0,
                'red' => 1,
                'blue' => 0,
                'brown' => 0,
                'yellow' => 1,
            ),
            'new_stickers_right' => array(
                'green' => 0,
                'gray' => 0,
                'red' => 10,
                'blue' => 0,
                'brown' => 0,
                'yellow' => 5,
            ),
            'new_stickers_top' => array(
                'green' => 25,
                'gray' => 25,
                'red' => 10,
                'blue' => 25,
                'brown' => 25,
                'yellow' => 5,
            ),
            'new_bg_color' => array(
                'green' => '',
                'gray' => '',
                'red' => '#66CFF1',
                'blue' => '#1BD4E6',
                'brown' => '#9f824a',
                'yellow' => '',
            ),
            'sale_style' => array(
                'green' => 0,
                'gray' => 0,
                'red' => 1,
                'blue' => 0,
                'brown' => 0,
                'yellow' => 1,
            ),
            'sale_stickers_left' => array(
                'green' => 0,
                'gray' => 0,
                'red' => 10,
                'blue' => 0,
                'brown' => 0,
                'yellow' => 5,
            ),
            'sale_stickers_top' => array(
                'green' => 25,
                'gray' => 25,
                'red' => 10,
                'blue' => 25,
                'brown' => 25,
                'yellow' => 5,
            ),
            'sale_bg_color' =>  array(
                'green' => '',
                'gray' => '#FF8A00',
                'red' => '',
                'blue' => '#F64C65',
                'brown' => '#F44C64',
                'yellow' => '#FECC0D',
            ),
            'btn_bg_color' =>  array(
                'green' => '',
                'gray' => '',
                'red' => '#515963',
                'blue' => '',
                'brown' => '#666666',
                'yellow' => '',
            ),
            'btn_hover_color' =>  array(
                'green' => '',
                'gray' => '',
                'red' => '',
                'blue' => '',
                'brown' => '',
                'yellow' => '#666666',
            ),
            'btn_hover_bg_color' =>  array(
                'green' => '',
                'gray' => '#333333',
                'red' => '#F64C65',
                'blue' => '#47B0BA',
                'brown' => '#9f824a',
                'yellow' => '#FECC0D',
            ),
            'flyout_buttons_bg' =>  array(
                'green' => '',
                'gray' => '',
                'red' => '',
                'blue' => '',
                'brown' => '',
                'yellow' => '#EDE9E9',
            ),
            'p_btn_color' =>  array(
                'green' => '',
                'gray' => '',
                'red' => '',
                'blue' => '',
                'brown' => '',
                'yellow' => '#444444',
            ),
            'p_btn_hover_color' =>  array(
                'green' => '',
                'gray' => '',
                'red' => '',
                'blue' => '',
                'brown' => '',
                'yellow' => '#444444',
            ),
            'p_btn_bg_color' =>  array(
                'green' => '',
                'gray' => '#666666',
                'red' => '#F64C65',
                'blue' => '#47B0BA',
                'brown' => '#9f824a',
                'yellow' => '#FECC0D',
            ),
            'p_btn_hover_bg_color' =>  array(
                'green' => '',
                'gray' => '#333333',
                'red' => '#E0394D',
                'blue' => '#3695A0',
                'brown' => '#906C2E',
                'yellow' => '#D8AD0A',
            ),
            'header_text_color' =>  array(
                'green' => '',
                'gray' => '#cccccc',
                'red' => '#e5e5e5',
                'blue' => '#ffffff',
                'brown' => '#666666',
                'yellow' => '#cccccc',
            ),
            'header_link_color' =>  array(
                'green' => '',
                'gray' => '#cccccc',
                'red' => '#e5e5e5',
                'blue' => '#ffffff',
                'brown' => '#666666',
                'yellow' => '#cccccc',
            ),
            'header_link_hover_color' =>  array(
                'green' => '',
                'gray' => '#00A161',
                'red' => '#F64C65',
                'blue' => '#47B0BA',
                'brown' => '#9f824a',
                'yellow' => '#FECC0D',
            ),
            'header_link_hover_bg' =>  array(
                'green' => '',
                'gray' => '#ffffff',
                'red' => '#ffe4e8',
                'blue' => '#DEF2F3',
                'brown' => '#efefef',
                'yellow' => '#ffffff',
            ),
            'header_topbar_bg' =>  array(
                'green' => '',
                'gray' => '#666666',
                'red' => '#515963',
                'blue' => '#47B0BA',
                'brown' => '#f9f9f9',
                'yellow' => '#292929',
            ),
            'header_topbar_sep' =>  array(
                'green' => '',
                'gray' => '#666666',
                'red' => '#6D757F',
                'blue' => '#53BDC7',
                'brown' => '#f2f2f2',
                'yellow' => '#292929',
            ),
            'dropdown_hover_color' =>  array(
                'green' => '',
                'gray' => '#00A161',
                'red' => '#F64C65',
                'blue' => '#47B0BA',
                'brown' => '#9f824a',
                'yellow' => '#FECC0D',
            ),
            'dropdown_bg_color' =>  array(
                'green' => '',
                'gray' => '#ffffff',
                'red' => '#ffe4e8',
                'blue' => '#DEF2F3',
                'brown' => '#efefef',
                'yellow' => '#ffffff',
            ),
            'header_bg_color' =>  array(
                'green' => '',
                'gray' => '#ffffff',
                'red' => '',
                'blue' => '#ffffff',
                'brown' => '',
                'yellow' => '#292929',
            ),
            'font_menu_trans' => array(
                'green' => 1,
                'gray' => 1,
                'red' => 1,
                'blue' => 1,
                'brown' => 1,
                'yellow' => 3,
            ),
            'menu_color' => array(
                'green' => '',
                'gray' => '',
                'red' => '',
                'blue' => '',
                'brown' => '',
                'yellow' => '#ffffff',
            ),
            'menu_hover_color' => array(
                'green' => '',
                'gray' => '',
                'red' => '',
                'blue' => '',
                'brown' => '#9f824a',
                'yellow' => '#666666',
            ),
            'menu_hover_bg' =>  array(
                'green' => '',
                'gray' => '#333333',
                'red' => '#F64C65',
                'blue' => '#47B0BA',
                'brown' => '#ffffff',
                'yellow' => '#FECC0D',
            ),
            'second_menu_hover_color' => array(
                'green' => '',
                'gray' => '',
                'red' => '#F64C65',
                'blue' => '#47B0BA',
                'brown' => '#9f824a',
                'yellow' => '#FECC0D',
            ),
            'third_menu_hover_color' => array(
                'green' => '',
                'gray' => '',
                'red' => '#F64C65',
                'blue' => '#47B0BA',
                'brown' => '#9f824a',
                'yellow' => '#FECC0D',
            ),
            'menu_mob_hover_color' => array(
                'green' => '',
                'gray' => '',
                'red' => '#F64C65',
                'blue' => '#47B0BA',
                'brown' => '#9f824a',
                'yellow' => '#FECC0D',
            ),
            'menu_mob_items1_color' => array(
                'green' => '',
                'gray' => '',
                'red' => '',
                'blue' => '',
                'brown' => '',
                'yellow' => '#666666',
            ),
            'megamenu_width' => array(
                'green' => 0,
                'gray' => 0,
                'red' => 0,
                'blue' => 0,
                'brown' => 0,
                'yellow' => 1,
            ),
            'menu_bg_color' => array(
                'green' => '',
                'gray' => '',
                'red' => '',
                'blue' => '',
                'brown' => '',
                'yellow' => '#484A49',
            ),
            'breadcrumb_bg_style' => array(
                'green' => 0,
                'gray' => 0,
                'red' => 0,
                'blue' => 0,
                'brown' => 0,
                'yellow' => 1,
            ),
            'breadcrumb_bg' => array(
                'green' => '',
                'gray' => '',
                'red' => '',
                'blue' => '',
                'brown' => '',
                'yellow' => '#ffffff',
            ),            
            'body_bg_color' =>  array(
                'green' => '',
                'gray' => '#f9f9f9',
                'red' => '#f6f6f6',
                'blue' => '#E8F8FA',
                'brown' => '',
                'yellow' => '',
            ),
            'body_con_bg_color' =>  array(
                'green' => '#ffffff',
                'gray' => '#ffffff',
                'red' => '',
                'blue' => '#ffffff',
                'brown' => '',
                'yellow' => '#ffffff',
            ),
            'footer_top_border_color'=>  array(
                'green' => '',
                'gray' => '',
                'red' => '#ffffff',
                'blue' => '',
                'brown' => '#e5e5e5',
                'yellow' => '',
            ),
            'footer_top_bg'=>  array(
                'green' => '',
                'gray' => '#ffffff',
                'red' => '#ffffff',
                'blue' => '#ffffff',
                'brown' => '#ffffff',
                'yellow' => '#FAFAFA',
            ),
            'footer_bg_color'=>  array(
                'green' => '',
                'gray' => '#ffffff',
                'red' => '',
                'blue' => '#fafafa',
                'brown' => '#fafafa',
                'yellow' => '#ffffff',
            ),
            'footer_secondary_bg' =>  array(
                'green' => '',
                'gray' => '#ffffff',
                'red' => '',
                'blue' => '#ffffff',
                'brown' => '',
                'yellow' => '#ffffff',
            ),
            'footer_border_color' =>  array(
                'green' => '',
                'gray' => '#e5e5e5',
                'red' => '#fafafa',
                'blue' => '#fafafa',
                'brown' => '#fafafa',
                'yellow' => '#fafafa',
            ),
            'footer_info_bg' =>  array(
                'green' => '',
                'gray' => '',
                'red' => '#515963',
                'blue' => '',
                'brown' => '#000000',
                'yellow' => '',
            ),
            'circle_number_bg' => array(
                'green' => '',
                'gray' => '',
                'red' => '',
                'blue' => '',
                'brown' => '#9f824a',
                'yellow' => '',
            ),
            'font_text' => array(
                'green' => '',
                'gray' => '',
                'red' => '',
                'blue' => '',
                'brown' => 'Times New Roman',
                'yellow' => '',
            ),
            'font_heading' => array(
                'green' => 'Fjalla One',
                'gray' => 'Fjalla One',
                'red' => 'Fjalla One',
                'blue' => 'Fjalla One',
                'brown' => 'Tienne',
                'yellow' => 'Arvo',
            ),
            'block_headings_color' => array(
                'green' => '',
                'gray' => '',
                'red' => '',
                'blue' => '',
                'brown' => '',
                'yellow' => '#444444',
            ),
            'font_heading_size' => array(
                'green' => 0,
                'gray' => 0,
                'red' => 0,
                'blue' => 0,
                'brown' => 16,
                'yellow' => 0,
            ),
            'font_heading_trans' => array(
                'green' => 1,
                'gray' => 1,
                'red' => 1,
                'blue' => 1,
                'brown' => 1,
                'yellow' => 3,
            ),
            'font_cart_btn' => array(
                'green' => 'Fjalla One',
                'gray' => 'Fjalla One',
                'red' => 'Fjalla One',
                'blue' => 'Fjalla One',
                'brown' => 'Tienne',
                'yellow' => 'Arvo',
            ),
            'font_menu' => array(
                'green' => 'Fjalla One',
                'gray' => 'Fjalla One',
                'red' => 'Fjalla One',
                'blue' => 'Fjalla One',
                'brown' => 'Tienne',
                'yellow' => 'Arvo',
            ),
            'font_menu_size' => array(
                'green' => 0,
                'gray' => 0,
                'red' => 0,
                'blue' => 0,
                'brown' => 14,
                'yellow' => 0,
            ),
            'logo_position' => array(
                'green' => 0,
                'gray' => 0,
                'red' => 0,
                'blue' => 0,
                'brown' => 1,
                'yellow' => 0,
            ),
            'megamenu_position' => array(
                'green' => 0,
                'gray' => 0,
                'red' => 0,
                'blue' => 0,
                'brown' => 1,
                'yellow' => 0,
            ),
            'custom_css' =>  array(
                'green' => '',
                'gray' => '#body_wrapper{padding-top:20px;padding-bottom:25px;}',
                'red' => '',
                'blue' => '',
                'brown' => '',
                'yellow' => '',
            ),
        );
        $this->_hooks = array(
            array('displayCategoryFooter','displayCategoryFooter','Display some specific informations on the category page',1),
            array('displayCategoryHeader','displayCategoryHeader','Display some specific informations on the category page',1),
            array('displayTopSecondary','displayTopSecondary','Bottom of the header',1),
            array('displayAnywhere','displayAnywhere','It is easy to call a hook from tpl',1),
            array('displayProductSecondaryColumn','displayProductSecondaryColumn','Product secondary column',1),
            array('displayFooterTop','displayFooterTop','Footer top',1),
            array('displayFooterSecondary','displayFooterSecondary','Footer secondary',1),
            array('displayHomeSecondaryLeft','displayHomeSecondaryLeft','Home secondary left',1),
            array('displayHomeSecondaryRight','displayHomeSecondaryRight','Home secondary right',1),
            array('displayHomeTop','displayHomeTop','Home page top',1),
            array('displayHomeBottom','displayHomeBottom','Hom epage bottom',1),
            array('displayTopLeft','displayTopLeft','Top left-hand side of the page',1),
            array('displayManufacturerHeader','displayManufacturerHeader','Display some specific informations on the manufacturer page',1),
            array('displayHomeVeryBottom','displayHomeVeryBottom','Very bottom of the home page',1),
            array('displayHomeTertiaryRight','displayHomeTertiaryRight','Home tertiary right',1),
            array('displayHomeTertiaryLeft','displayHomeTertiaryLeft','Home tertiary left',1),
        );
	}
	
	public function install()
	{
	    if ( $this->_addHook() &&
            parent::install() && 
            $this->registerHook('header') && 
            $this->registerHook('displayAnywhere') &&
            $this->registerHook('actionShopDataDuplication') &&
            $this->registerHook('displayRightColumnProduct') &&
            $this->_useDefault()
        ){
            if ($id_hook = Hook::getIdByName('displayHeader'))
                $this->updatePosition($id_hook, 0, 1);
            return true;
        }
        return false;
	}
	
    private function _addHook()
	{
        $res = true;
        foreach($this->_hooks as $v)
        {
            if(!$res)
                break;
            if (!Validate::isHookName($v[0]))
                continue;
                
            $id_hook = Hook::getIdByName($v[0]);
    		if (!$id_hook)
    		{
    			$new_hook = new Hook();
    			$new_hook->name = pSQL($v[0]);
    			$new_hook->title = pSQL($v[1]);
    			$new_hook->description = pSQL($v[2]);
    			$new_hook->position = pSQL($v[3]);
    			$new_hook->live_edit  = 0;
    			$new_hook->add();
    			$id_hook = $new_hook->id;
    			if (!$id_hook)
    				$res = false;
    		}
            else
            {
                Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'hook` set `title`="'.$v[1].'", `description`="'.$v[2].'", `position`="'.$v[3].'", `live_edit`=0 where `id_hook`='.$id_hook);
            }
        }
		return $res;
	}

	private function _removeHook()
	{
	    $sql = 'DELETE FROM `'._DB_PREFIX_.'hook` WHERE ';
        foreach($this->_hooks as $v)
            $sql .= ' `name` = "'.$v[0].'" OR';
		return Db::getInstance()->execute(rtrim($sql,'OR').';');
	}
    
	public function uninstall()
	{
	    if(!parent::uninstall() ||
            !$this->_deleteConfiguration()
        )
			return false;
		return true;
	}
    
    private function _deleteConfiguration()
    {
        $res = true;
        foreach($this->defaults as $k=>$v)
            $res &= Configuration::deleteByName('STSN_'.strtoupper($k));
        return $res;
    }
	
    private function _useDefault($html = false, $id_shop_group = null, $id_shop = null)
    {
        $res = true;
        foreach($this->defaults as $k=>$v)
		    $res &= Configuration::updateValue('STSN_'.strtoupper($k), $v, $html, $id_shop_group, $id_shop);
        return $res;
    }
    private function _usePredefinedColor($color)
    {
        $res = true;
        foreach($this->predefinedColor as $k=>$v)
        {
            if($k=='custom_css')
            {
                $custom_css = is_array($v) ? $v[$color] : $v;
                $custom_css_exist =  Configuration::get('STSN_CUSTOM_CSS');
                if($custom_css_exist)
                    foreach($v as $n)
                        $custom_css_exist = str_replace($n,'',$custom_css_exist);
                $res &= Configuration::updateValue('STSN_'.strtoupper($k), ($custom_css.$custom_css_exist));
            }
            else
                $res &= Configuration::updateValue('STSN_'.strtoupper($k), (is_array($v) ? $v[$color] : $v));
        }
        return $res;
    }
    public function uploadCheckAndGetName($name)
    {
		$type = strtolower(substr(strrchr($name, '.'), 1));
        if(!in_array($type, $this->imgtype))
            return false;
        $filename = Tools::encrypt($name.sha1(microtime()));
		while (file_exists(_PS_UPLOAD_DIR_.$filename.'.'.$type)) {
            $filename .= rand(10, 99);
        } 
        return $filename.'.'.$type;
    }
    private function _checkImageDir($dir)
    {
        $result = '';
        if (!file_exists($dir))
        {
            $success = @mkdir($dir, self::$access_rights, true)
						|| @chmod($dir, self::$access_rights);
            if(!$success)
                $result = $this->displayError('"'.$dir.'" '.$this->l('An error occurred during new folder creation'));
        }

        if (!is_writable($dir))
            $result = $this->displayError('"'.$dir.'" '.$this->l('directory isn\'t writable.'));
        
        return $result;
    }
    	
	public function getContent()
	{
	    $this->initFieldsForm();
		$this->context->controller->addCSS(($this->_path).'views/css/admin.css');
		$this->context->controller->addJS(($this->_path).'views/js/admin.js');
        $this->_html .= '<script type="text/javascript">var stthemeeditor_base_uri = "'.__PS_BASE_URI__.'";var stthemeeditor_refer = "'.(int)Tools::getValue('ref').'";</script>';
        if (Tools::isSubmit('resetstthemeeditor'))
        {
            $this->_useDefault();
            $this->writeCss();
            Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'));
        }
        if (Tools::isSubmit('predefinedcolorstthemeeditor') && Tools::getValue('predefinedcolorstthemeeditor'))
        {
            $this->_usePredefinedColor(Tools::getValue('predefinedcolorstthemeeditor'));
            $this->writeCss();
            Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'));
        }
        if(Tools::getValue('act')=='delete_image' && $identi = Tools::getValue('identi'))
        {
            $identi = strtoupper($identi);
            $themeeditor = new StThemeEditor();
            $image  = Configuration::get('STSN_'.$identi);
        	if (Configuration::get('STSN_'.$identi))
                if (file_exists(_PS_UPLOAD_DIR_.$image))
                    @unlink(_PS_UPLOAD_DIR_.$image);
                elseif(file_exists(_PS_MODULE_DIR_.'stthemeeditor/'.$image) && strpos($image, $identi) === false)
                    @unlink(_PS_MODULE_DIR_.'stthemeeditor/'.$image);
        	Configuration::updateValue('STSN_'.$identi, '');
            $themeeditor->writeCss();
            $result['r'] = true;
            die(json_encode($result));
        }
        if(isset($_POST['savestthemeeditor']))
		{
            $res = true;
            foreach($this->fields_form as $form)
                foreach($form['form']['input'] as $field)
                    if(isset($field['validation']))
                    {
                        $ishtml = ($field['validation']=='isAnything') ? true : false;
                        $errors = array();       
                        $value = Tools::getValue($field['name']);
                        if (isset($field['required']) && $field['required'] && $value==false && (string)$value != '0')
        						$errors[] = sprintf(Tools::displayError('Field "%s" is required.'), $field['label']);
                        elseif($value)
                        {
        					if (!Validate::$field['validation']($value))
        						$errors[] = sprintf(Tools::displayError('Field "%s" is invalid.'), $field['label']);
                        }
        				// Set default value
        				if ($value === false && isset($field['default_value']))
        					$value = $field['default_value'];
                            
                        if(count($errors))
                        {
                            $this->validation_errors = array_merge($this->validation_errors, $errors);
                        }
                        elseif($value==false)
                        {
                            switch($field['validation'])
                            {
                                case 'isUnsignedId':
                                case 'isUnsignedInt':
                                case 'isInt':
                                case 'isBool':
                                    $value = 0;
                                break;
                                default:
                                    $value = '';
                                break;
                            }
                            Configuration::updateValue('STSN_'.strtoupper($field['name']), $value);
                        }
                        else
                            Configuration::updateValue('STSN_'.strtoupper($field['name']), $value, $ishtml);
                    }
            //
            if(Configuration::get('STSN_PRODUCT_BIG_IMAGE'))
                Configuration::updateValue('STSN_PRODUCT_SECONDARY', false);

            $this->updateWelcome();
            $this->updateCopyright();
            $this->updateSearchLabel();
            $this->updateNewsletterLabel();
            $this->updateCatePerRow();
			$this->updateConfigurableModules();
                
            $bg_array = array('body','header','f_top','footer','f_secondary','f_info','new','sale');
            foreach($bg_array as $v)
            {
        			if (isset($_FILES[$v.'_bg_image_field']) && isset($_FILES[$v.'_bg_image_field']['tmp_name']) && !empty($_FILES[$v.'_bg_image_field']['tmp_name'])) 
                    {
        				if ($error = ImageManager::validateUpload($_FILES[$v.'_bg_image_field'], Tools::convertBytes(ini_get('upload_max_filesize'))))
    					   $this->validation_errors[] = Tools::displayError($error);
                        else 
                        {
                            $footer_image = $this->uploadCheckAndGetName($_FILES[$v.'_bg_image_field']['name']);
                            if(!$footer_image)
                                $this->validation_errors[] = Tools::displayError('Image format not recognized');
        					if (!move_uploaded_file($_FILES[$v.'_bg_image_field']['tmp_name'], $this->local_path.'img/'.$footer_image))
        						$this->validation_errors[] = Tools::displayError('Error move uploaded file');
                            else
                            {
        					   Configuration::updateValue('STSN_'.strtoupper($v).'_BG_IMG', 'img/'.$footer_image);
                            }
        				}
        			}
            }
            
			if (isset($_FILES['footer_image_field']) && isset($_FILES['footer_image_field']['tmp_name']) && !empty($_FILES['footer_image_field']['tmp_name'])) 
            {
				if ($error = ImageManager::validateUpload($_FILES['footer_image_field'], Tools::convertBytes(ini_get('upload_max_filesize'))))
					$this->validation_errors[] = Tools::displayError($error);
                else 
                {
                    $footer_image = $this->uploadCheckAndGetName($_FILES['footer_image_field']['name']);
                    if(!$footer_image)
                        $this->validation_errors[] = Tools::displayError('Image format not recognized');
					else if (!move_uploaded_file($_FILES['footer_image_field']['tmp_name'], _PS_UPLOAD_DIR_.$footer_image))
						$this->validation_errors[] = Tools::displayError('Error move uploaded file');
                    else
                    {
					   Configuration::updateValue('STSN_FOOTER_IMG', $footer_image);
                    }
				}
			}
            $iphone_icon_array = array('57','72','114','144');
            foreach($iphone_icon_array as $v)
            {
        			if (isset($_FILES['icon_iphone_'.$v.'_field']) && isset($_FILES['icon_iphone_'.$v.'_field']['tmp_name']) && !empty($_FILES['icon_iphone_'.$v.'_field']['tmp_name'])) 
                    {
                        $this->_checkImageDir(_PS_MODULE_DIR_.$this->name.'/img/'.$this->context->shop->id.'/');
        				if ($error = ImageManager::validateUpload($_FILES['icon_iphone_'.$v.'_field'], Tools::convertBytes(ini_get('upload_max_filesize'))))
    					   $this->validation_errors[] = Tools::displayError($error);
                        else 
                        {
        					if (!move_uploaded_file($_FILES['icon_iphone_'.$v.'_field']['tmp_name'], $this->local_path.'img/'.$this->context->shop->id.'/touch-icon-iphone-'.$v.'.png'))
        						$this->validation_errors[] = Tools::displayError('Error move uploaded file');
                            else
                            {
        					   Configuration::updateValue('STSN_ICON_IPHONE_'.strtoupper($v), 'img/'.$this->context->shop->id.'/touch-icon-iphone-'.$v.'.png');
                            }
        				}
        			}
            }   
            
            if(count($this->validation_errors))
                $this->_html .= $this->displayError(implode('<br/>',$this->validation_errors));
            else
            {
                $this->writeCss();
                $this->_html .= $this->displayConfirmation($this->l('Settings updated'));
            } 
        }
        
        if (Tools::isSubmit('deleteimagestthemeeditor'))
        {
            if($identi = Tools::getValue('identi'))
            {
                $identi = strtoupper($identi);
                $image  = Configuration::get('STSN_'.$identi);
            	if (Configuration::get('STSN_'.$identi))
                    if (file_exists(_PS_UPLOAD_DIR_.$image))
		                @unlink(_PS_UPLOAD_DIR_.$image);
                    elseif(file_exists($this->_path.$image))
                        @unlink($this->_path.$image);
            	Configuration::updateValue('STSN_'.$identi, '');
                Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&conf=7&ref='.(int)Tools::getValue('ref').'&token='.Tools::getAdminTokenLite('AdminModules'));  
             }else
                $this->_html .= $this->displayError($this->l('An error occurred while delete banner.'));
        }
        $this->initDropListGroup();
		$helper = $this->initForm();
        return $this->_html.$this->initToolbarBtn().'<div class="tabbable row stthemeeditor">'.$this->initTab().'<div id="stthemeeditor" class="col-xs-12 col-lg-10 tab-content">'.$helper->generateForm($this->fields_form).'</div></div>';
	}
    
    public function initDropListGroup()
    {
        $this->fields_form[0]['form']['input']['hometab_pro_per_0']['name'] = $this->BuildDropListGroup($this->findCateProPer(4));
        $this->fields_form[1]['form']['input']['categories_per_0']['name'] = $this->BuildDropListGroup($this->findCateProPer(6));
        $this->fields_form[1]['form']['input']['category_pro_per_1']['name'] = $this->BuildDropListGroup($this->findCateProPer(1));
        $this->fields_form[1]['form']['input']['category_pro_per_2']['name'] = $this->BuildDropListGroup($this->findCateProPer(2));
        $this->fields_form[1]['form']['input']['category_pro_per_3']['name'] = $this->BuildDropListGroup($this->findCateProPer(3));
        $this->fields_form[11]['form']['input']['cs_pro_per_0']['name'] = $this->BuildDropListGroup($this->findCateProPer(7));
        $this->fields_form[12]['form']['input']['pc_pro_per_0']['name'] = $this->BuildDropListGroup($this->findCateProPer(8));
        $this->fields_form[13]['form']['input']['ac_pro_per_0']['name'] = $this->BuildDropListGroup($this->findCateProPer(9));
        $this->fields_form[16]['form']['input']['packitems_pro_per_0']['name'] = $this->BuildDropListGroup($this->findCateProPer(5));
    }
    
    public function updateWelcome() {
		$languages = Language::getLanguages(false);
		$welcome = $welcome_logged  = $welcome_link = array();
        $defaultLanguage = new Language((int)(Configuration::get('PS_LANG_DEFAULT')));
		foreach ($languages as $language)
		{
            $welcome[$language['id_lang']] = Tools::getValue('welcome_'.$language['id_lang']) ? Tools::getValue('welcome_'.$language['id_lang']) : Tools::getValue('welcome_'.$defaultLanguage->id);
			$welcome_logged[$language['id_lang']] = Tools::getValue('welcome_logged_'.$language['id_lang']) ? Tools::getValue('welcome_logged_'.$language['id_lang']) : Tools::getValue('welcome_logged_'.$defaultLanguage->id);
			$welcome_link[$language['id_lang']] = Tools::getValue('welcome_link_'.$language['id_lang']) ? Tools::getValue('welcome_link_'.$language['id_lang']) : Tools::getValue('welcome_link_'.$defaultLanguage->id);
		}
        Configuration::updateValue('STSN_WELCOME_LINK', $welcome_link);
        Configuration::updateValue('STSN_WELCOME', $welcome);
        Configuration::updateValue('STSN_WELCOME_LOGGED', $welcome_logged);
	}
    public function updateCopyright() {
		$languages = Language::getLanguages();
		$result = array();
        $defaultLanguage = new Language((int)(Configuration::get('PS_LANG_DEFAULT')));
		foreach ($languages as $language)
			$result[$language['id_lang']] = Tools::getValue('copyright_text_' . $language['id_lang']) ? Tools::getValue('copyright_text_'.$language['id_lang']) : Tools::getValue('copyright_text_'.$defaultLanguage->id);

        if(!$result[$defaultLanguage->id])
            $this->validation_errors[] = Tools::displayError('The field "Copyright text" is required at least in '.$defaultLanguage->name);
		else
            Configuration::updateValue('STSN_COPYRIGHT_TEXT', $result, true);
	}
    public function updateSearchLabel() {
		$languages = Language::getLanguages();
		$result = array();
        $defaultLanguage = new Language((int)(Configuration::get('PS_LANG_DEFAULT')));
		foreach ($languages as $language)
			$result[$language['id_lang']] = Tools::getValue('search_label_' . $language['id_lang']) ? Tools::getValue('search_label_' . $language['id_lang']) : Tools::getValue('search_label_'.$defaultLanguage->id);

        if(!$result[$defaultLanguage->id])
            $this->validation_errors[] = Tools::displayError('The field "Search label" is required at least in '.$defaultLanguage->name);
		else
            Configuration::updateValue('STSN_SEARCH_LABEL', $result);
	}        
    public function updateNewsletterLabel() {
        $languages = Language::getLanguages();
        $result = array();
        $defaultLanguage = new Language((int)(Configuration::get('PS_LANG_DEFAULT')));
        foreach ($languages as $language)
            $result[$language['id_lang']] = Tools::getValue('newsletter_label_' . $language['id_lang']) ? Tools::getValue('newsletter_label_' . $language['id_lang']) : Tools::getValue('newsletter_label_'.$defaultLanguage->id);

        if(!$result[$defaultLanguage->id])
            $this->validation_errors[] = Tools::displayError('The field "Newsletter label" is required at least in '.$defaultLanguage->name);
        else
            Configuration::updateValue('STSN_NEWSLETTER_LABEL', $result);
    }     
    public function updateCatePerRow() {
		$arr = $this->findCateProPer();
        foreach ($arr as $key => $value)
            foreach ($value as $v)
                if ($gv = Tools::getValue($v['id']))
                    Configuration::updateValue('STSN_'.strtoupper($v['id']), (int)$gv);
	}
    public function initFieldsForm()
    {
		$this->fields_form[0]['form'] = array(
			'input' => array(
                array(
					'type' => 'switch',
					'label' => $this->l('Enable responsive layout:'),
					'name' => 'responsive',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'responsive_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'responsive_off',
							'value' => 0,
							'label' => $this->l('No')),
					),
                    'desc' => $this->l('Enable responsive design for mobile devices.'),
                    'validation' => 'isBool',
				), 
                array(
					'type' => 'switch',
					'label' => $this->l('Display switch back to desktop version link on mobile devices:'),
					'name' => 'version_switching',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'version_switching_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'version_switching_off',
							'value' => 0,
							'label' => $this->l('No')),
					),
                    'desc' => $this->l('This option allows visitors to manually switch between mobile and desktop versions on mobile devices.'),
                    'validation' => 'isBool',
				), 
                array(
					'type' => 'radio',
					'label' => $this->l('Maximum Page Width:'),
					'name' => 'responsive_max',
					'values' => array(
						array(
							'id' => 'responsive_max_0',
							'value' => 0,
							'label' => $this->l('980')),
						array(
							'id' => 'responsive_max_1',
							'value' => 1,
							'label' => $this->l('1200')),
					),
                    'desc' => $this->l('Maximum width of the page'),
                    'validation' => 'isBool',
				), 
                array(
					'type' => 'radio',
					'label' => $this->l('Box style:'),
					'name' => 'boxstyle',
					'values' => array(
						array(
							'id' => 'boxstyle_on',
							'value' => 1,
							'label' => $this->l('Stretched style')),
						array(
							'id' => 'boxstyle_off',
							'value' => 2,
							'label' => $this->l('Boxed style')),
					),
                    'validation' => 'isUnsignedInt',
				), 
                array(
					'type' => 'radio',
					'label' => $this->l('Show comment rating:'),
					'name' => 'display_comment_rating',
					'values' => array(
						array(
							'id' => 'display_comment_rating_off',
							'value' => 0,
							'label' => $this->l('NO')),
						array(
							'id' => 'display_comment_rating_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'display_comment_rating_always',
							'value' => 2,
							'label' => $this->l('Always display')),
					),
                    'desc' => $this->l('Always display: show star even if no rating'),
                    'validation' => 'isUnsignedInt',
				), 
                array(
					'type' => 'radio',
					'label' => $this->l('Length of product name:'),
					'name' => 'length_of_product_name',
					'values' => array(
						array(
							'id' => 'length_of_product_name_normal',
							'value' => 0,
							'label' => $this->l('Normal(one line)')),
						array(
							'id' => 'length_of_product_name_long',
							'value' => 1,
							'label' => $this->l('Long(70 characters)')),
						array(
							'id' => 'length_of_product_name_full',
							'value' => 2,
							'label' => $this->l('Full name')),
					),
                    'validation' => 'isUnsignedInt',
				), 
                array(
					'type' => 'radio',
					'label' => $this->l('Logo position:'),
					'name' => 'logo_position',
					'values' => array(
						array(
							'id' => 'logo_position_left',
							'value' => 0,
							'label' => $this->l('Left')),
						array(
							'id' => 'logo_position_center',
							'value' => 1,
							'label' => $this->l('Center')),
					),
                    'validation' => 'isUnsignedInt',
				), 
                'logo_height' => array(
					'type' => 'text',
					'label' => $this->l('Header height:'),
					'name' => 'logo_height',
                    'validation' => 'isUnsignedInt',
                    'prefix' => 'px',
                    'class' => 'fixed-width-lg',
                    'desc' => array(
                        $this->l('This option makes it possible to change the height of header.'),
                        $this->l('If the height of your logo is bigger than 86px then you will need to fill out this filed.'),
                        $this->l('Please make sure the value is lagger than the height of your logo. Currently the logo height is ').Configuration::get('SHOP_LOGO_HEIGHT'),
                        $this->l('Only for logo center.')
                    ),
				),
                'hometab_pro_per_0' => array(
                    'type' => 'html',
                    'id' => 'hometab_pro_per_0',
                    'label'=> $this->l('The number of columns for Homepage tab'),
                    'name' => '',
                ),
                array(
					'type' => 'radio',
					'label' => $this->l('Fly-out buttons:'),
					'name' => 'flyout_buttons',
					'values' => array(
						array(
							'id' => 'flyout_buttons_on',
							'value' => 1,
							'label' => $this->l('Show it all the times')),
						array(
							'id' => 'flyout_buttons_off',
							'value' => 0,
							'label' => $this->l('Button appears on mouse-over of the product image')),
					),
                    'validation' => 'isBool',
				), 
                array(
					'type' => 'switch',
					'label' => $this->l('Show scroll to top button:'),
					'name' => 'scroll_to_top',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'scroll_to_top_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'scroll_to_top_off',
							'value' => 0,
							'label' => $this->l('NO')),
					),
                    'validation' => 'isBool',
				), 
                array(
					'type' => 'radio',
					'label' => $this->l('Add to cart animation:'),
					'name' => 'addtocart_animation',
                    'default_value' => 0,
					'values' => array(
						array(
							'id' => 'addtocart_animation_dialog',
							'value' => 0,
							'label' => $this->l('Pop-up dialog')),
						array(
							'id' => 'addtocart_animation_flying',
							'value' => 1,
							'label' => $this->l('Flying image to cart(Page scroll to top)')),
						array(
							'id' => 'addtocart_animation_flying_scroll',
							'value' => 2,
							'label' => $this->l('Flying image to cart')),
					),
                    'validation' => 'isUnsignedInt',
				), 
                array(
					'type' => 'radio',
					'label' => $this->l('Cart icon:'),
					'name' => 'cart_icon',
                    'default_value' => 0,
					'values' => array(
						array(
							'id' => 'cart_icon_0',
							'value' => 0,
							'label' => '<img src="'.$this->_path.'img/icon_1.jpg">',),
						array(
							'id' => 'cart_icon_1',
							'value' => 1,
							'label' => '<img src="'.$this->_path.'img/icon_2.jpg">',),
					),
                    'validation' => 'isUnsignedInt',
				), 
                array(
					'type' => 'radio',
					'label' => $this->l('Wishlist icon:'),
					'name' => 'wishlist_icon',
                    'default_value' => 0,
					'values' => array(
						array(
							'id' => 'wishlist_icon_0',
							'value' => 0,
							'label' => '<img src="'.$this->_path.'img/icon_3.jpg">',),
						array(
							'id' => 'wishlist_icon_1',
							'value' => 1,
							'label' => '<img src="'.$this->_path.'img/icon_4.jpg">',),
					),
                    'validation' => 'isUnsignedInt',
				),
                array(
					'type' => 'radio',
					'label' => $this->l('Compare icon:'),
					'name' => 'compare_icon',
                    'default_value' => 0,
					'values' => array(
						array(
							'id' => 'compare_icon_0',
							'value' => 0,
							'label' => '<img src="'.$this->_path.'img/icon_5.jpg">',),
						array(
							'id' => 'compare_icon_1',
							'value' => 1,
							'label' => '<img src="'.$this->_path.'img/icon_6.jpg">',),
					),
                    'validation' => 'isUnsignedInt',
				), 
                array(
    				'type' => 'select',
        			'label' => $this->l('Set the vertical right panel position on the screen:'),
        			'name' => 'position_right_panel',
                    'options' => array(
        				'query' => self::$position_right_panel,
        				'id' => 'id',
        				'name' => 'name',
        			),
                    'validation' => 'isGenericName',
    			),
                array(
					'type' => 'text',
					'label' => $this->l('Guest welcome msg:'),
					'name' => 'welcome',
                    'size' => 64,
                    'lang' => true,
				),
                array(
					'type' => 'text',
					'label' => $this->l('Logged welcome msg:'),
					'name' => 'welcome_logged',
                    'size' => 64,
                    'lang' => true,
				),
                array(
					'type' => 'text',
					'label' => $this->l('Add a link to welcome msg:'),
					'name' => 'welcome_link',
                    'size' => 64,
                    'lang' => true,
				),
                array(
					'type' => 'textarea',
					'label' => $this->l('Copyright text:'),
					'name' => 'copyright_text',
                    'lang' => true,
                    'required' => true,
					'cols' => 60,
					'rows' => 2,
				),
                array(
					'type' => 'text',
					'label' => $this->l('Search label:'),
					'name' => 'search_label',
                    'lang' => true,
                    'required' => true,
				),
                array(
					'type' => 'text',
					'label' => $this->l('Newsletter label:'),
					'name' => 'newsletter_label',
                    'lang' => true,
                    'required' => true,
				),
                /*
                array(
					'type' => 'color',
					'label' => $this->l('Iframe background:'),
					'name' => 'lb_bg_color',
			        'size' => 33,
                    'desc' => $this->l('Set iframe background if transparency is not allowed.'),
				),
                */
				'payment_icon' => array(
					'type' => 'file',
					'label' => $this->l('Payment icon:'),
					'name' => 'footer_image_field',
                    'desc' => '',
				),
			),
			'submit' => array(
				'title' => $this->l('   Save all   '),
			),
		);
        
        $this->fields_form[1]['form'] = array(
			'input' => array(
                array(
					'type' => 'radio',
					'label' => $this->l('Default product listing:'),
					'name' => 'product_view',
					'values' => array(
						array(
							'id' => 'product_view_grid',
							'value' => 'grid_view',
							'label' => $this->l('Grid')),
						array(
							'id' => 'product_view_list',
							'value' => 'list_view',
							'label' => $this->l('List')),
					),
                    'validation' => 'isGenericName',
				),  
                array(
					'type' => 'switch',
					'label' => $this->l('Show category title on category page:'),
					'name' => 'display_category_title',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'display_category_title_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'display_category_title_off',
							'value' => 0,
							'label' => $this->l('NO')),
					),
                    'validation' => 'isBool',
				), 
                array(
					'type' => 'switch',
					'label' => $this->l('Show category description on category page:'),
					'name' => 'display_category_desc',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'display_category_desc_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'display_category_desc_off',
							'value' => 0,
							'label' => $this->l('NO')),
					),
                    'validation' => 'isBool',
				), 
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show the full category description on category page:'),
                    'name' => 'display_cate_desc_full',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'display_cate_desc_full_on',
                            'value' => 1,
                            'label' => $this->l('Yes')),
                        array(
                            'id' => 'display_cate_desc_full_off',
                            'value' => 0,
                            'label' => $this->l('NO')),
                    ),
                    'validation' => 'isBool',
                ), 
                array(
					'type' => 'switch',
					'label' => $this->l('Show category image on category page:'),
					'name' => 'display_category_image',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'display_category_image_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'display_category_image_off',
							'value' => 0,
							'label' => $this->l('NO')),
					),
                    'validation' => 'isBool',
				), 
                array(
					'type' => 'radio',
					'label' => $this->l('Show subcategories:'),
					'name' => 'display_subcate',
					'values' => array(
						array(
							'id' => 'display_subcate_off',
							'value' => 0,
							'label' => $this->l('NO')),
						array(
							'id' => 'display_subcate_gird',
							'value' => 1,
							'label' => $this->l('Grid view')),
						array(
							'id' => 'display_subcate_list',
							'value' => 2,
							'label' => $this->l('List view')),
					),
                    'validation' => 'isUnsignedInt',
				), 
                'categories_per_0' => array(
                    'type' => 'html',
                    'id' => 'categories_per_0',
                    'label'=> $this->l('Subcategories per row in grid view:'),
                    'name' => '',
                ),
                array(
					'type' => 'radio',
					'label' => $this->l('Show product attributes:'),
					'name' => 'display_pro_attr',
					'values' => array(
						array(
							'id' => 'display_pro_attr_off',
							'value' => 0,
							'label' => $this->l('NO')),
						array(
							'id' => 'display_pro_attr_all',
							'value' => 1,
							'label' => $this->l('All')),
						array(
							'id' => 'display_pro_attr_in_stock',
							'value' => 2,
							'label' => $this->l('In stock only')),
					),
                    'validation' => 'isUnsignedInt',
				), 
                array(
                    'type' => 'switch',
                    'label' => $this->l('Display each product short description in category grid view:'),
                    'name' => 'show_short_desc_on_grid',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'show_short_desc_on_grid_on',
                            'value' => 1,
                            'label' => $this->l('Yes')),
                        array(
                            'id' => 'show_short_desc_on_grid_off',
                            'value' => 0,
                            'label' => $this->l('NO')),
                    ),
                    'validation' => 'isBool',
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show color list:'),
                    'name' => 'display_color_list',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'display_color_list_on',
                            'value' => 1,
                            'label' => $this->l('Yes')),
                        array(
                            'id' => 'display_color_list_off',
                            'value' => 0,
                            'label' => $this->l('NO')),
                    ),
                    'validation' => 'isBool',
                ),
                /*
                array(
					'type' => 'switch',
					'label' => $this->l('Display "Show all" button:'),
					'name' => 'category_show_all_btn',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'category_show_all_btn_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'category_show_all_btn_off',
							'value' => 0,
							'label' => $this->l('NO')),
					),
                    'validation' => 'isBool',
				),
                */
                'category_pro_per_1' => array(
                    'type' => 'html',
                    'id' => 'category_pro_per_1',
                    'label'=> $this->l('1 column'),
                    'label'=> $this->l('The number of columns for one column products listing page'),
                    'name' => '',
                ),
                'category_pro_per_2' => array(
                    'type' => 'html',
                    'id' => 'category_pro_per_2',
                    'label'=> $this->l('The number of columns for two columns products listing page'),
                    'name' => '',
                ),
                'category_pro_per_3' => array(
                    'type' => 'html',
                    'id' => 'category_pro_per_3',
                    'label'=> $this->l('The number of columns for three columns products listing page'),
                    'name' => '',
                ),
            ),
			'submit' => array(
				'title' => $this->l('   Save all   '),
			),
		);
        
        $this->fields_form[2]['form'] = array(
			'input' => array(
				 array(
					'type' => 'color',
					'label' => $this->l('Body font color:'),
					'name' => 'text_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('General links color:'),
					'name' => 'link_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('General link hover color:'),
					'name' => 'link_hover_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Breadcrumb font color:'),
					'name' => 'breadcrumb_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Breadcrumb link hover color:'),
					'name' => 'breadcrumb_hover_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
                array(
                    'type' => 'radio',
                    'label' => $this->l('Breadcrumb width:'),
                    'name' => 'breadcrumb_width',
                    'values' => array(
                        array(
                            'id' => 'breadcrumb_width_fullwidth',
                            'value' => 0,
                            'label' => $this->l('Full width')),
                        array(
                            'id' => 'breadcrumb_width_normal',
                            'value' => 1,
                            'label' => $this->l('Boxed')),
                    ),
                    'validation' => 'isUnsignedInt',
                ),
                array(
                    'type' => 'radio',
                    'label' => $this->l('Breadcrumb background:'),
                    'name' => 'breadcrumb_bg_style',
                    'values' => array(
                        array(
                            'id' => 'breadcrumb_bg_style_gradient',
                            'value' => 0,
                            'label' => $this->l('Gradient color')),
                        array(
                            'id' => 'breadcrumb_bg_style_pure',
                            'value' => 1,
                            'label' => $this->l('Pure color')),
                    ),
                    'validation' => 'isUnsignedInt',
                ),
				 array(
					'type' => 'color',
					'label' => $this->l('Breadcrumb background:'),
					'name' => 'breadcrumb_bg',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Price color:'),
					'name' => 'price_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Icon text color:'),
					'name' => 'icon_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Icon text hover color:'),
					'name' => 'icon_hover_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Icon background:'),
					'name' => 'icon_bg_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Icon hover background:'),
					'name' => 'icon_hover_bg_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Icon disabled text color:'),
					'name' => 'icon_disabled_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Right vertical panel border color:'),
					'name' => 'right_panel_border',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Starts color:'),
					'name' => 'starts_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),    
				 array(
					'type' => 'color',
					'label' => $this->l('Circle number color:'),
					'name' => 'circle_number_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),  
				 array(
					'type' => 'color',
					'label' => $this->l('Circle number background:'),
					'name' => 'circle_number_bg',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),             
				 array(
					'type' => 'color',
					'label' => $this->l('Buttons text color:'),
					'name' => 'btn_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Buttons text hover color:'),
					'name' => 'btn_hover_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
                 
				 array(
					'type' => 'color',
					'label' => $this->l('Buttons background:'),
					'name' => 'btn_bg_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
                 array(
                    'type' => 'color',
                    'label' => $this->l('Buttons background hover:'),
                    'name' => 'btn_hover_bg_color',
                    'class' => 'color',
                    'size' => 20,
                    'validation' => 'isColor',
                 ),
				 array(
					'type' => 'color',
					'label' => $this->l('Flyout Buttons background:'),
					'name' => 'flyout_buttons_bg',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Primary buttons text color:'),
					'name' => 'p_btn_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Primary buttons text hover color:'),
					'name' => 'p_btn_hover_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
                 
				 array(
					'type' => 'color',
					'label' => $this->l('Primary buttons background:'),
					'name' => 'p_btn_bg_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Primary buttons background hover:'),
					'name' => 'p_btn_hover_bg_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
            ),
			'submit' => array(
				'title' => $this->l('   Save all   '),
			),
        );
        
        $this->fields_form[3]['form'] = array(
			'input' => array(
                array(
					'type' => 'switch',
					'label' => $this->l('Latin extended support:'),
					'name' => 'font_latin_support',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'font_latin_support_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'font_latin_support_off',
							'value' => 0,
							'label' => $this->l('NO')),
					),
                    'desc' => $this->l('You have to check your selected font whether support Latin extended here').' :<a href="http://www.google.com/webfonts">www.google.com/webfonts</a>',
                    'validation' => 'isBool',
				), 
                array(
					'type' => 'switch',
					'label' => $this->l('Cyrylic support:'),
					'name' => 'font_cyrillic_support',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'font_cyrillic_support_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'font_cyrillic_support_off',
							'value' => 0,
							'label' => $this->l('NO')),
					),
                    'desc' => $this->l('You have to check your selected font whether support Cyrylic here').' :<a href="http://www.google.com/webfonts">www.google.com/webfonts</a>',
                    'validation' => 'isBool',
				),  
                array(
					'type' => 'switch',
					'label' => $this->l('Vietnamese support:'),
					'name' => 'font_vietnamese',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'font_vietnamese_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'font_vietnamese_off',
							'value' => 0,
							'label' => $this->l('NO')),
					),
                    'desc' => $this->l('You have to check your selected font whether support Vietnamese here').' :<a href="http://www.google.com/webfonts">www.google.com/webfonts</a>',
                    'validation' => 'isBool',
				),  
                array(
					'type' => 'switch',
					'label' => $this->l('Greek support:'),
					'name' => 'font_greek_support',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'font_greek_support_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'font_greek_support_off',
							'value' => 0,
							'label' => $this->l('NO')),
					),
                    'desc' => $this->l('You have to check your selected font whether support Greek here').' :<a href="http://www.google.com/webfonts">www.google.com/webfonts</a>',
                    'validation' => 'isBool',
				), 
				array(
					'type' => 'select',
					'label' => $this->l('Body font:'),
					'name' => 'font_text',
					'onchange' => 'handle_font_change(this,\''.implode(',',$this->systemFonts).'\');',
                    'class' => 'fontOptions',
					'options' => array(
                        'optiongroup' => array (
							'query' => $this->fontOptions(),
							'label' => 'name'
						),
						'options' => array (
							'query' => 'query',
							'id' => 'id',
							'name' => 'name'
						),
						'default' => array(
							'value' => 0,
							'label' => $this->l('Use default')
						),
					),
                    'desc' => '<p id="font_text_example" class="fontshow">Example normal text</p>',
                    'validation' => 'isGenericName',
				),
				array(
					'type' => 'select',
					'label' => $this->l('Headings font:'),
					'name' => 'font_heading',
					'onchange' => 'handle_font_change(this,\''.implode(',',$this->systemFonts).'\');',
                    'class' => 'fontOptions',
					'options' => array(
                        'optiongroup' => array (
							'query' => $this->fontOptions(),
							'label' => 'name'
						),
						'options' => array (
							'query' => 'query',
							'id' => 'id',
							'name' => 'name'
						),
						'default' => array(
							'value' => 0,
							'label' => $this->l('Use default')
						),
					),
                    'desc' => '<p id="font_heading_example" class="fontshow">Example heading</p>',
                    'validation' => 'isGenericName',
				),
				array(
					'type' => 'color',
					'label' => $this->l('Block heading color:'),
					'name' => 'block_headings_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			    ),
				array(
					'type' => 'color',
					'label' => $this->l('Heading color:'),
					'name' => 'headings_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			    ),
                array(
					'type' => 'text',
					'label' => $this->l('Headings font size:'),
					'name' => 'font_heading_size',
                    'validation' => 'isUnsignedInt',
                    'prefix' => 'px',
                    'class' => 'fixed-width-lg',
				), 
                array(
					'type' => 'text',
					'label' => $this->l('Footer headings font size:'),
					'name' => 'footer_heading_size',
                    'validation' => 'isUnsignedInt',
                    'prefix' => 'px',
                    'class' => 'fixed-width-lg',
				), 
                array(
					'type' => 'radio',
					'label' => $this->l('Headings font weight:'),
					'name' => 'font_heading_weight',
					'values' => array(
						array(
							'id' => 'font_heading_weight_off',
							'value' => 0,
							'label' => $this->l('Normal')),
						array(
							'id' => 'font_heading_weight_on',
							'value' => 1,
							'label' => $this->l('Bold')),
					),
                    'validation' => 'isBool',
				),
                array(
					'type' => 'select',
        			'label' => $this->l('Headings transform:'),
        			'name' => 'font_heading_trans',
                    'options' => array(
        				'query' => self::$textTransform,
        				'id' => 'id',
        				'name' => 'name',
        			),
                    'validation' => 'isUnsignedInt',
				),
				array(
					'type' => 'select',
					'label' => $this->l('Price font:'),
					'name' => 'font_price',
					'onchange' => 'handle_font_change(this,\''.implode(',',$this->systemFonts).'\');',
                    'class' => 'fontOptions',
					'options' => array(
                        'optiongroup' => array (
							'query' => $this->fontOptions(),
							'label' => 'name'
						),
						'options' => array (
							'query' => 'query',
							'id' => 'id',
							'name' => 'name'
						),
						'default' => array(
							'value' => 0,
							'label' => $this->l('Use default')
						),
					),
                    'desc' => '<p id="font_price_example" class="fontshow">$12345.67890</p>',
                    'validation' => 'isGenericName',
				),
                array(
					'type' => 'text',
					'label' => $this->l('Price font size:'),
					'name' => 'font_price_size',
                    'validation' => 'isUnsignedInt',
                    'prefix' => 'px',
                    'class' => 'fixed-width-lg',
				),
                array(
					'type' => 'text',
					'label' => $this->l('Old price font size:'),
					'name' => 'font_old_price_size',
                    'validation' => 'isUnsignedInt',
                    'prefix' => 'px',
                    'class' => 'fixed-width-lg',
				),
				array(
					'type' => 'select',
					'label' => $this->l('Add to cart button font:'),
					'name' => 'font_cart_btn',
					'onchange' => 'handle_font_change(this,\''.implode(',',$this->systemFonts).'\');',
                    'class' => 'fontOptions',
					'options' => array(
                        'optiongroup' => array (
							'query' => $this->fontOptions(),
							'label' => 'name'
						),
						'options' => array (
							'query' => 'query',
							'id' => 'id',
							'name' => 'name'
						),
						'default' => array(
							'value' => 0,
							'label' => $this->l('Use default')
						),
					),
                    'desc' => '<p id="font_cart_btn_example" class="fontshow">ADD TO CART</p>',
                    'validation' => 'isGenericName',
				),
            ),
			'submit' => array(
				'title' => $this->l('   Save all   '),
			),
        );
        
        $this->fields_form[4]['form'] = array(
			'input' => array(
				 array(
					'type' => 'color',
					'label' => $this->l('Text color:'),
					'name' => 'header_text_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Link color:'),
					'name' => 'header_link_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Link hover color:'),
					'name' => 'header_link_hover_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Link hover background:'),
					'name' => 'header_link_hover_bg',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Top bar background:'),
					'name' => 'header_topbar_bg',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Top bar list separators color:'),
					'name' => 'header_topbar_sep',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Dropdown text hover color:'),
					'name' => 'dropdown_hover_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Dropdown background hover:'),
					'name' => 'dropdown_bg_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
                 array(
					'type' => 'select',
        			'label' => $this->l('Select a pattern number:'),
        			'name' => 'header_bg_pattern',
                    'options' => array(
        				'query' => $this->getPatternsArray(),
        				'id' => 'id',
        				'name' => 'name',
    					'default' => array(
    						'value' => 0,
    						'label' => $this->l('None'),
    					),
        			),
                    'desc' => $this->getPatterns(),
                    'validation' => 'isUnsignedInt',
				),
				'header_bg_image_field' => array(
					'type' => 'file',
					'label' => $this->l('Upload your own pattern as background image:'),
					'name' => 'header_bg_image_field',
                    'desc' => '',
				),
                array(
					'type' => 'radio',
					'label' => $this->l('Repeat:'),
					'name' => 'header_bg_repeat',
					'values' => array(
						array(
							'id' => 'header_bg_repeat_xy',
							'value' => 0,
							'label' => $this->l('Repeat xy')),
						array(
							'id' => 'header_bg_repeat_x',
							'value' => 1,
							'label' => $this->l('Repeat x')),
						array(
							'id' => 'header_bg_repeat_y',
							'value' => 2,
							'label' => $this->l('Repeat y')),
						array(
							'id' => 'header_bg_repeat_no',
							'value' => 3,
							'label' => $this->l('No repeat')),
					),
                    'validation' => 'isUnsignedInt',
				), 
                array(
					'type' => 'radio',
					'label' => $this->l('Position:'),
					'name' => 'header_bg_position',
					'values' => array(
						array(
							'id' => 'header_bg_repeat_left',
							'value' => 0,
							'label' => $this->l('Left')),
						array(
							'id' => 'header_bg_repeat_center',
							'value' => 1,
							'label' => $this->l('Center')),
						array(
							'id' => 'header_bg_repeat_right',
							'value' => 2,
							'label' => $this->l('Right')),
					),
                    'validation' => 'isUnsignedInt',
				),
				 array(
					'type' => 'color',
					'label' => $this->l('Background color:'),
					'name' => 'header_bg_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
                 array(
                    'type' => 'color',
                    'label' => $this->l('Container background color:'),
                    'name' => 'header_con_bg_color',
                    'class' => 'color',
                    'size' => 20,
                    'validation' => 'isColor',
                 ),
				 array(
					'type' => 'color',
					'label' => $this->l('The background of the most top of the page(Above the top bar):'),
					'name' => 'display_banner_bg',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
            ),
			'submit' => array(
				'title' => $this->l('   Save all   '),
			),
        );
        
        $this->fields_form[5]['form'] = array(
			'input' => array(
                array(
					'type' => 'switch',
					'label' => $this->l('Sticky Menu:'),
					'name' => 'sticky_menu',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'sticky_menu_off',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'sticky_menu_on',
							'value' => 0,
							'label' => $this->l('No')),
					),
                    'validation' => 'isBool',
				),
                array(
					'type' => 'radio',
					'label' => $this->l('Megamenu position:'),
					'name' => 'megamenu_position',
					'values' => array(
						array(
							'id' => 'megamenu_position_left',
							'value' => 0,
							'label' => $this->l('Left')),
						array(
							'id' => 'megamenu_position_center',
							'value' => 1,
							'label' => $this->l('Center')),
						array(
							'id' => 'megamenu_position_right',
							'value' => 2,
							'label' => $this->l('Right')),
					),
                    'validation' => 'isUnsignedInt',
				), 
				array(
					'type' => 'select',
					'label' => $this->l('Menu font:'),
					'name' => 'font_menu',
					'onchange' => 'handle_font_change(this,\''.implode(',',$this->systemFonts).'\');',
                    'class' => 'fontOptions',
					'options' => array(
                        'optiongroup' => array (
							'query' => $this->fontOptions(),
							'label' => 'name'
						),
						'options' => array (
							'query' => 'query',
							'id' => 'id',
							'name' => 'name'
						),
						'default' => array(
							'value' => 0,
							'label' => $this->l('Use default')
						),
					),
                    'desc' => '<p id="font_menu_example" class="fontshow">Home Fashion</p>',
                    'validation' => 'isGenericName',
				),
                array(
					'type' => 'text',
					'label' => $this->l('Menu font size:'),
					'name' => 'font_menu_size',
                    'validation' => 'isUnsignedInt',
                    'prefix' => 'px',
                    'class' => 'fixed-width-lg',
				),
                array(
					'type' => 'radio',
					'label' => $this->l('Menu font weight:'),
					'name' => 'font_menu_weight',
					'values' => array(
						array(
							'id' => 'font_menu_weight_off',
							'value' => 0,
							'label' => $this->l('Normal')),
						array(
							'id' => 'font_menu_weight_on',
							'value' => 1,
							'label' => $this->l('Bold')),
					),
                    'validation' => 'isBool',
				),
                array(
					'type' => 'select',
        			'label' => $this->l('Menu text transform:'),
        			'name' => 'font_menu_trans',
                    'options' => array(
        				'query' => self::$textTransform,
        				'id' => 'id',
        				'name' => 'name',
        			),
                    'validation' => 'isUnsignedInt',
				),
                array(
                    'type' => 'radio',
                    'label' => $this->l('Megamenu width:'),
                    'name' => 'megamenu_width',
                    'values' => array(
                        array(
                            'id' => 'megamenu_width_normal',
                            'value' => 0,
                            'label' => $this->l('Boxed')),
                        array(
                            'id' => 'megamenu_width_fullwidth',
                            'value' => 1,
                            'label' => $this->l('Full width')),
                    ),
                    'validation' => 'isUnsignedInt',
                ),
				 array(
					'type' => 'color',
					'label' => $this->l('Menu background:'),
					'name' => 'menu_bg_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Main menu color:'),
					'name' => 'menu_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Main menu hover color:'),
					'name' => 'menu_hover_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Main menu hover background:'),
					'name' => 'menu_hover_bg',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('2rd level color:'),
					'name' => 'second_menu_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('2rd level hover color:'),
					'name' => 'second_menu_hover_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('3rd level color:'),
					'name' => 'third_menu_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('3rd level hover color:'),
					'name' => 'third_menu_hover_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Mobile menu button color:'),
					'name' => 'menu_mob_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Mobile menu btton hover color:'),
					'name' => 'menu_mob_hover_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Background color for the mobile menu button:'),
					'name' => 'menu_mob_bg',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Hover background color for the mobile menu button:'),
					'name' => 'menu_mob_hover_bg',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Links color on mobile version:'),
					'name' => 'menu_mob_items1_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('2nd level color on mobile version:'),
					'name' => 'menu_mob_items2_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('3rd level color on mobile version:'),
					'name' => 'menu_mob_items3_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Background color on mobile version:'),
					'name' => 'menu_mob_items1_bg',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('2rd level background color on mobile version:'),
					'name' => 'menu_mob_items2_bg',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('3rd level background color on mobile version:'),
					'name' => 'menu_mob_items3_bg',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
            ),
			'submit' => array(
				'title' => $this->l('   Save all   '),
			),
        );
        
        $this->fields_form[6]['form'] = array(
			'input' => array(
                array(
					'type' => 'select',
        			'label' => $this->l('Select a pattern number:'),
        			'name' => 'body_bg_pattern',
                    'options' => array(
        				'query' => $this->getPatternsArray(),
        				'id' => 'id',
        				'name' => 'name',
    					'default' => array(
    						'value' => 0,
    						'label' => $this->l('None'),
    					),
        			),
                    'desc' => $this->getPatterns(),
                    'validation' => 'isUnsignedInt',
				),
				'body_bg_image_field' => array(
					'type' => 'file',
					'label' => $this->l('Upload your own pattern as background image:'),
					'name' => 'body_bg_image_field',
                    'desc' => '',
				),
                array(
					'type' => 'radio',
					'label' => $this->l('Repeat:'),
					'name' => 'body_bg_repeat',
					'values' => array(
						array(
							'id' => 'body_bg_repeat_xy',
							'value' => 0,
							'label' => $this->l('Repeat xy')),
						array(
							'id' => 'body_bg_repeat_x',
							'value' => 1,
							'label' => $this->l('Repeat x')),
						array(
							'id' => 'body_bg_repeat_y',
							'value' => 2,
							'label' => $this->l('Repeat y')),
						array(
							'id' => 'body_bg_repeat_no',
							'value' => 3,
							'label' => $this->l('No repeat')),
					),
                    'validation' => 'isUnsignedInt',
				), 
                array(
					'type' => 'radio',
					'label' => $this->l('Position:'),
					'name' => 'body_bg_position',
					'values' => array(
						array(
							'id' => 'body_bg_repeat_left',
							'value' => 0,
							'label' => $this->l('Left')),
						array(
							'id' => 'body_bg_repeat_center',
							'value' => 1,
							'label' => $this->l('Center')),
						array(
							'id' => 'body_bg_repeat_right',
							'value' => 2,
							'label' => $this->l('Right')),
					),
                    'validation' => 'isUnsignedInt',
				),
                array(
					'type' => 'switch',
					'label' => $this->l('Fixed background attachment:'),
					'name' => 'body_bg_fixed',
					'is_bool' => true,
                    'default_value' => 0,
					'values' => array(
						array(
							'id' => 'body_bg_fixed_off',
							'value' => 0,
							'label' => $this->l('No')),
						array(
							'id' => 'body_bg_fixed_on',
							'value' => 1,
							'label' => $this->l('Yes')),
					),
                    'validation' => 'isBool',
				),
                array(
                    'type' => 'color',
                    'label' => $this->l('Body background color:'),
                    'name' => 'body_bg_color',
                    'class' => 'color',
                    'size' => 20,
                    'validation' => 'isColor',
                 ),
                array(
                    'type' => 'color',
                    'label' => $this->l('Main content area background color:'),
                    'name' => 'body_con_bg_color',
                    'class' => 'color',
                    'size' => 20,
                    'validation' => 'isColor',
                 ),
				array(
					'type' => 'color',
					'label' => $this->l('Inner main content area  background color:'),
					'name' => 'main_con_bg_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
			),
			'submit' => array(
				'title' => $this->l('   Save all   '),
			),
		);
        
        
        $this->fields_form[7]['form'] = array(
			'input' => array(
                 array(
					'type' => 'select',
        			'label' => $this->l('Select a pattern number:'),
        			'name' => 'f_top_bg_pattern',
                    'options' => array(
        				'query' => $this->getPatternsArray(),
        				'id' => 'id',
        				'name' => 'name',
    					'default' => array(
    						'value' => 0,
    						'label' => $this->l('None'),
    					),
        			),
                    'desc' => $this->getPatterns(),
                    'validation' => 'isUnsignedInt',
				),
				'f_top_bg_image_field' => array(
					'type' => 'file',
					'label' => $this->l('Upload your own pattern as background image:'),
					'name' => 'f_top_bg_image_field',
                    'desc' => '',
				),
                array(
					'type' => 'radio',
					'label' => $this->l('Repeat:'),
					'name' => 'f_top_bg_repeat',
					'values' => array(
						array(
							'id' => 'f_top_bg_repeat_xy',
							'value' => 0,
							'label' => $this->l('Repeat xy')),
						array(
							'id' => 'f_top_bg_repeat_x',
							'value' => 1,
							'label' => $this->l('Repeat x')),
						array(
							'id' => 'f_top_bg_repeat_y',
							'value' => 2,
							'label' => $this->l('Repeat y')),
						array(
							'id' => 'f_top_bg_repeat_no',
							'value' => 3,
							'label' => $this->l('No repeat')),
					),
                    'validation' => 'isUnsignedInt',
				), 
                array(
					'type' => 'radio',
					'label' => $this->l('Position:'),
					'name' => 'f_top_bg_position',
					'values' => array(
						array(
							'id' => 'f_top_bg_repeat_left',
							'value' => 0,
							'label' => $this->l('Left')),
						array(
							'id' => 'f_top_bg_repeat_center',
							'value' => 1,
							'label' => $this->l('Center')),
						array(
							'id' => 'f_top_bg_repeat_right',
							'value' => 2,
							'label' => $this->l('Right')),
					),
                    'validation' => 'isUnsignedInt',
				),
				array(
					'type' => 'color',
					'label' => $this->l('Headings color:'),
					'name' => 'f_top_h_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			    ),
				 array(
					'type' => 'color',
					'label' => $this->l('border color:'),
					'name' => 'footer_top_border_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Background color:'),
					'name' => 'footer_top_bg',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Container background color:'),
					'name' => 'footer_top_con_bg',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
            ),
			'submit' => array(
				'title' => $this->l('   Save all   '),
			),
        );
        
        $this->fields_form[8]['form'] = array(
			'input' => array(
				 array(
					'type' => 'color',
					'label' => $this->l('Font color:'),
					'name' => 'footer_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Links color:'),
					'name' => 'footer_link_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Links hover color:'),
					'name' => 'footer_link_hover_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
                 array(
					'type' => 'select',
        			'label' => $this->l('Select a pattern number:'),
        			'name' => 'footer_bg_pattern',
                    'options' => array(
        				'query' => $this->getPatternsArray(),
        				'id' => 'id',
        				'name' => 'name',
    					'default' => array(
    						'value' => 0,
    						'label' => $this->l('None'),
    					),
        			),
                    'desc' => $this->getPatterns(),
                    'validation' => 'isUnsignedInt',
				),
				'footer_bg_image_field' => array(
					'type' => 'file',
					'label' => $this->l('Upload your own pattern as background image:'),
					'name' => 'footer_bg_image_field',
                    'desc' => '',
				),
                array(
					'type' => 'radio',
					'label' => $this->l('Repeat:'),
					'name' => 'footer_bg_repeat',
					'values' => array(
						array(
							'id' => 'footer_bg_repeat_xy',
							'value' => 0,
							'label' => $this->l('Repeat xy')),
						array(
							'id' => 'footer_bg_repeat_x',
							'value' => 1,
							'label' => $this->l('Repeat x')),
						array(
							'id' => 'footer_bg_repeat_y',
							'value' => 2,
							'label' => $this->l('Repeat y')),
						array(
							'id' => 'footer_bg_repeat_no',
							'value' => 3,
							'label' => $this->l('No repeat')),
					),
                    'validation' => 'isUnsignedInt',
				), 
                array(
					'type' => 'radio',
					'label' => $this->l('Position:'),
					'name' => 'footer_bg_position',
					'values' => array(
						array(
							'id' => 'footer_bg_repeat_left',
							'value' => 0,
							'label' => $this->l('Left')),
						array(
							'id' => 'footer_bg_repeat_center',
							'value' => 1,
							'label' => $this->l('Center')),
						array(
							'id' => 'footer_bg_repeat_right',
							'value' => 2,
							'label' => $this->l('Right')),
					),
                    'validation' => 'isUnsignedInt',
				),
				array(
					'type' => 'color',
					'label' => $this->l('Headings color:'),
					'name' => 'footer_h_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			    ),
				 array(
					'type' => 'color',
					'label' => $this->l('Background color:'),
					'name' => 'footer_bg_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Container background color:'),
					'name' => 'footer_con_bg_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Footer border color:'),
					'name' => 'footer_border_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),        
            ),
			'submit' => array(
				'title' => $this->l('   Save all   '),
			),
        );
        
        $this->fields_form[9]['form'] = array(
			'input' => array(
                 array(
					'type' => 'select',
        			'label' => $this->l('Select a pattern number:'),
        			'name' => 'f_secondary_bg_pattern',
                    'options' => array(
        				'query' => $this->getPatternsArray(),
        				'id' => 'id',
        				'name' => 'name',
    					'default' => array(
    						'value' => 0,
    						'label' => $this->l('None'),
    					),
        			),
                    'desc' => $this->getPatterns(),
                    'validation' => 'isUnsignedInt',
				),
				'f_secondary_bg_image_field' => array(
					'type' => 'file',
					'label' => $this->l('Upload your own pattern as background image:'),
					'name' => 'f_secondary_bg_image_field',
                    'desc' => '',
				),
                array(
					'type' => 'radio',
					'label' => $this->l('Repeat:'),
					'name' => 'f_secondary_bg_repeat',
					'values' => array(
						array(
							'id' => 'f_secondary_bg_repeat_xy',
							'value' => 0,
							'label' => $this->l('Repeat xy')),
						array(
							'id' => 'f_secondary_bg_repeat_x',
							'value' => 1,
							'label' => $this->l('Repeat x')),
						array(
							'id' => 'f_secondary_bg_repeat_y',
							'value' => 2,
							'label' => $this->l('Repeat y')),
						array(
							'id' => 'f_secondary_bg_repeat_no',
							'value' => 3,
							'label' => $this->l('No repeat')),
					),
                    'validation' => 'isUnsignedInt',
				), 
                array(
					'type' => 'radio',
					'label' => $this->l('Position:'),
					'name' => 'f_secondary_bg_position',
					'values' => array(
						array(
							'id' => 'f_secondary_bg_repeat_left',
							'value' => 0,
							'label' => $this->l('Left')),
						array(
							'id' => 'f_secondary_bg_repeat_center',
							'value' => 1,
							'label' => $this->l('Center')),
						array(
							'id' => 'f_secondary_bg_repeat_right',
							'value' => 2,
							'label' => $this->l('Right')),
					),
                    'validation' => 'isUnsignedInt',
				),
				array(
					'type' => 'color',
					'label' => $this->l('Headings color:'),
					'name' => 'f_secondary_h_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			    ),
				 array(
					'type' => 'color',
					'label' => $this->l('Background color:'),
					'name' => 'footer_secondary_bg',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Container background color:'),
					'name' => 'footer_secondary_con_bg',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
            ),
			'submit' => array(
				'title' => $this->l('   Save all   '),
			),
        );
        
        $this->fields_form[10]['form'] = array(
			'input' => array(
                 array(
					'type' => 'select',
        			'label' => $this->l('Select a pattern number:'),
        			'name' => 'f_info_bg_pattern',
                    'options' => array(
        				'query' => $this->getPatternsArray(),
        				'id' => 'id',
        				'name' => 'name',
    					'default' => array(
    						'value' => 0,
    						'label' => $this->l('None'),
    					),
        			),
                    'desc' => $this->getPatterns(),
                    'validation' => 'isUnsignedInt',
				),
				'f_info_bg_image_field' => array(
					'type' => 'file',
					'label' => $this->l('Upload your own pattern as background image:'),
					'name' => 'f_info_bg_image_field',
                    'desc' => '',
				),
                array(
					'type' => 'radio',
					'label' => $this->l('Repeat:'),
					'name' => 'f_info_bg_repeat',
					'values' => array(
						array(
							'id' => 'f_info_bg_repeat_xy',
							'value' => 0,
							'label' => $this->l('Repeat xy')),
						array(
							'id' => 'f_info_bg_repeat_x',
							'value' => 1,
							'label' => $this->l('Repeat x')),
						array(
							'id' => 'f_info_bg_repeat_y',
							'value' => 2,
							'label' => $this->l('Repeat y')),
						array(
							'id' => 'f_info_bg_repeat_no',
							'value' => 3,
							'label' => $this->l('No repeat')),
					),
                    'validation' => 'isUnsignedInt',
				), 
                array(
					'type' => 'radio',
					'label' => $this->l('Position:'),
					'name' => 'f_info_bg_position',
					'values' => array(
						array(
							'id' => 'f_info_bg_repeat_left',
							'value' => 0,
							'label' => $this->l('Left')),
						array(
							'id' => 'f_info_bg_repeat_center',
							'value' => 1,
							'label' => $this->l('Center')),
						array(
							'id' => 'f_info_bg_repeat_right',
							'value' => 2,
							'label' => $this->l('Right')),
					),
                    'validation' => 'isUnsignedInt',
				),
				 array(
					'type' => 'color',
					'label' => $this->l('Background color:'),
					'name' => 'footer_info_bg',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Container background color:'),
					'name' => 'footer_info_con_bg',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Font color:'),
					'name' => 'second_footer_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
            ),
			'submit' => array(
				'title' => $this->l('   Save all   '),
			),
        );
        
        $this->fields_form[11]['form'] = array(
			'legend' => array(
				'title' => $this->l('Cross selling'),
			),
			'input' => array(
                'cs_pro_per_0' => array(
                    'type' => 'html',
                    'id' => 'cs_pro_per_0',
                    'label'=> $this->l('The number of columns'),
                    'name' => '',
                ),
                array(
					'type' => 'switch',
					'label' => $this->l('Autoplay:'),
					'name' => 'cs_slideshow',
					'is_bool' => true,
                    'default_value' => 1,
					'values' => array(
						array(
							'id' => 'cs_slide_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'cs_slide_off',
							'value' => 0,
							'label' => $this->l('No')),
					),
                    'validation' => 'isBool',
				), 
                array(
					'type' => 'text',
					'label' => $this->l('Time:'),
					'name' => 'cs_s_speed',
                    'desc' => $this->l('The period, in milliseconds, between the end of a transition effect and the start of the next one.'),
                    'validation' => 'isUnsignedInt',
                    'class' => 'fixed-width-sm'
				),
                array(
					'type' => 'text',
					'label' => $this->l('Transition period:'),
					'name' => 'cs_a_speed',
                    'desc' => $this->l('The period, in milliseconds, of the transition effect.'),
                    'validation' => 'isUnsignedInt',
                    'class' => 'fixed-width-sm'
				),
                array(
					'type' => 'switch',
					'label' => $this->l('Pause On Hover:'),
					'name' => 'cs_pause_on_hover',
                    'default_value' => 1,
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'cs_pause_on_hover_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'cs_pause_on_hover_off',
							'value' => 0,
							'label' => $this->l('No')),
					),
                    'validation' => 'isBool',
				),
                array(
					'type' => 'select',
        			'label' => $this->l('Easing method:'),
        			'name' => 'cs_easing',
                    'options' => array(
        				'query' => self::$easing,
        				'id' => 'id',
        				'name' => 'name',
        			),
                    'desc' => $this->l('The type of easing applied to the transition animation'),
                    'validation' => 'isUnsignedInt',
				), 
                array(
					'type' => 'switch',
					'label' => $this->l('Loop:'),
					'name' => 'cs_loop',
                    'default_value' => 0,
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'cs_loop_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'cs_loop_off',
							'value' => 0,
							'label' => $this->l('No')),
					),
                    'desc' => $this->l('"No" if you want to perform the animation once; "Yes" to loop the animation'),
                    'validation' => 'isBool',
				),
                array(
					'type' => 'radio',
					'label' => $this->l('Move:'),
					'name' => 'cs_move',
                    'default_value' => 0,
					'values' => array(
						array(
							'id' => 'cs_move_on',
							'value' => 1,
							'label' => $this->l('1 item')),
						array(
							'id' => 'cs_move_off',
							'value' => 0,
							'label' => $this->l('All visible items')),
					),
                    'validation' => 'isBool',
				),
            ),
			'submit' => array(
				'title' => $this->l('   Save all   '),
			),
        );
        
        
        $this->fields_form[12]['form'] = array(
			'legend' => array(
				'title' => $this->l('Products category'),
			),
			'input' => array(
               'pc_pro_per_0' => array(
                    'type' => 'html',
                    'id' => 'pc_pro_per_0',
                    'label'=> $this->l('The number of columns'),
                    'name' => '',
                ),
                array(
					'type' => 'switch',
					'label' => $this->l('Autoplay:'),
					'name' => 'pc_slideshow',
					'is_bool' => true,
                    'default_value' => 1,
					'values' => array(
						array(
							'id' => 'pc_slide_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'pc_slide_off',
							'value' => 0,
							'label' => $this->l('No')),
					),
                    'validation' => 'isBool',
				), 
                array(
					'type' => 'text',
					'label' => $this->l('Time:'),
					'name' => 'pc_s_speed',
                    'default_value' => 7000,
                    'desc' => $this->l('The period, in milliseconds, between the end of a transition effect and the start of the next one.'),
                    'validation' => 'isUnsignedInt',
                    'class' => 'fixed-width-sm'
				),
                array(
					'type' => 'text',
					'label' => $this->l('Transition period:'),
					'name' => 'pc_a_speed',
                    'default_value' => 400,
                    'desc' => $this->l('The period, in milliseconds, of the transition effect.'),
                    'validation' => 'isUnsignedInt',
                    'class' => 'fixed-width-sm'
				),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Pause On Hover:'),
                    'name' => 'pc_pause_on_hover',
                    'default_value' => 1,
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'pc_pause_on_hover_on',
                            'value' => 1,
                            'label' => $this->l('Yes')),
                        array(
                            'id' => 'pc_pause_on_hover_off',
                            'value' => 0,
                            'label' => $this->l('No')),
                    ),
                    'validation' => 'isBool',
                ),
                array(
					'type' => 'select',
        			'label' => $this->l('Easing method:'),
        			'name' => 'pc_easing',
                    'options' => array(
        				'query' => self::$easing,
        				'id' => 'id',
        				'name' => 'name',
        			),
                    'desc' => $this->l('The type of easing applied to the transition animation'),
                    'validation' => 'isUnsignedInt',
				), 
                array(
					'type' => 'switch',
					'label' => $this->l('Loop:'),
					'name' => 'pc_loop',
                    'default_value' => 0,
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'pc_loop_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'pc_loop_off',
							'value' => 0,
							'label' => $this->l('No')),
					),
                    'desc' => $this->l('"No" if you want to perform the animation once; "Yes" to loop the animation'),
                    'validation' => 'isBool',
				),
                array(
					'type' => 'radio',
					'label' => $this->l('Move:'),
					'name' => 'pc_move',
                    'default_value' => 0,
					'values' => array(
						array(
							'id' => 'pc_move_on',
							'value' => 1,
							'label' => $this->l('1 item')),
						array(
							'id' => 'pc_move_off',
							'value' => 0,
							'label' => $this->l('All visible items')),
					),
                    'validation' => 'isBool',
				),
            ),
			'submit' => array(
				'title' => $this->l('   Save all   '),
			),
        );
        
        $this->fields_form[13]['form'] = array(
			'legend' => array(
				'title' => $this->l('Accessories'),
			),
			'input' => array(
                'ac_pro_per_0' => array(
                    'type' => 'html',
                    'id' => 'ac_pro_per_0',
                    'label'=> $this->l('The number of columns'),
                    'name' => '',
                ),
                array(
					'type' => 'switch',
					'label' => $this->l('Autoplay:'),
					'name' => 'ac_slideshow',
					'is_bool' => true,
                    'default_value' => 1,
					'values' => array(
						array(
							'id' => 'ac_slide_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'ac_slide_off',
							'value' => 0,
							'label' => $this->l('No')),
					),
                    'validation' => 'isBool',
				), 
                array(
					'type' => 'text',
					'label' => $this->l('Time:'),
					'name' => 'ac_s_speed',
                    'default_value' => 7000,
                    'desc' => $this->l('The period, in milliseconds, between the end of a transition effect and the start of the next one.'),
                    'validation' => 'isUnsignedInt',
                    'class' => 'fixed-width-sm'
				),
                array(
					'type' => 'text',
					'label' => $this->l('Transition period:'),
					'name' => 'ac_a_speed',
                    'default_value' => 400,
                    'desc' => $this->l('The period, in milliseconds, of the transition effect.'),
                    'validation' => 'isUnsignedInt',
                    'class' => 'fixed-width-sm'
				),
                array(
					'type' => 'switch',
					'label' => $this->l('Pause On Hover:'),
					'name' => 'ac_pause_on_hover',
                    'default_value' => 1,
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'ac_pause_on_hover_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'ac_pause_on_hover_off',
							'value' => 0,
							'label' => $this->l('No')),
					),
                    'validation' => 'isBool',
				),
                array(
					'type' => 'select',
        			'label' => $this->l('Easing method:'),
        			'name' => 'ac_easing',
                    'options' => array(
        				'query' => self::$easing,
        				'id' => 'id',
        				'name' => 'name',
        			),
                    'desc' => $this->l('The type of easing applied to the transition animation'),
                    'validation' => 'isUnsignedInt',
				), 
                array(
					'type' => 'switch',
					'label' => $this->l('Loop:'),
					'name' => 'ac_loop',
                    'default_value' => 0,
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'ac_loop_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'ac_loop_off',
							'value' => 0,
							'label' => $this->l('No')),
					),
                    'desc' => $this->l('"No" if you want to perform the animation once; "Yes" to loop the animation'),
                    'validation' => 'isBool',
				),
                array(
					'type' => 'radio',
					'label' => $this->l('Move:'),
					'name' => 'ac_move',
                    'default_value' => 0,
					'values' => array(
						array(
							'id' => 'ac_move_on',
							'value' => 1,
							'label' => $this->l('1 item')),
						array(
							'id' => 'ac_move_off',
							'value' => 0,
							'label' => $this->l('All visible items')),
					),
                    'validation' => 'isBool',
				),
            ),
			'submit' => array(
				'title' => $this->l('   Save all   '),
			),
        );
        
        $this->fields_form[14]['form'] = array(
			'input' => array(
                array(
					'type' => 'textarea',
					'label' => $this->l('Custom CSS Code:'),
					'name' => 'custom_css',
					'cols' => 80,
					'rows' => 10,
                    'desc' => $this->l('Override css with your custom code'),
                    'validation' => 'isAnything',
				),
                array(
					'type' => 'textarea',
					'label' => $this->l('Custom JAVASCRIPT Code:'),
					'name' => 'custom_js',
					'cols' => 80,
					'rows' => 10,
                    'desc' => $this->l('Override js with your custom code'),
                    'validation' => 'isAnything',
				),
                array(
					'type' => 'textarea',
					'label' => $this->l('Tracking code:'),
					'name' => 'tracking_code',
					'cols' => 80,
					'rows' => 10,
                    'validation' => 'isAnything',
				),
            ),
			'submit' => array(
				'title' => $this->l('   Save all   '),
			),
        );
        
        $this->fields_form[15]['form'] = array(
			'input' => array(
                array(
					'type' => 'radio',
					'label' => $this->l('New stickers style:'),
					'name' => 'new_style',
					'values' => array(
						array(
							'id' => 'new_style_flag',
							'value' => 0,
							'label' => $this->l('Flag')),
						array(
							'id' => 'new_style_circle',
							'value' => 1,
							'label' => $this->l('Circle')),
					),
                    'validation' => 'isUnsignedInt',
				), 
				 array(
					'type' => 'color',
					'label' => $this->l('New stickers color:'),
					'name' => 'new_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('New stickers background color:'),
					'name' => 'new_bg_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				'new_bg_image_field' => array(
					'type' => 'file',
					'label' => $this->l('New stickers background image(only for circle stickers):'),
					'name' => 'new_bg_image_field',
                    'desc' => '',
				),
                array(
					'type' => 'text',
					'label' => $this->l('New stickers width:'),
					'name' => 'new_stickers_width',
                    'validation' => 'isUnsignedInt',
                    'prefix' => 'px',
                    'class' => 'fixed-width-lg',
				),
                array(
					'type' => 'text',
					'label' => $this->l('New stickers top postion:'),
					'name' => 'new_stickers_top',
                    'validation' => 'isUnsignedInt',
                    'prefix' => 'px',
                    'class' => 'fixed-width-lg',
				),
                array(
					'type' => 'text',
					'label' => $this->l('New stickers right postion:'),
					'name' => 'new_stickers_right',
                    'validation' => 'isUnsignedInt',
                    'prefix' => 'px',
                    'class' => 'fixed-width-lg',
				),
                array(
					'type' => 'radio',
					'label' => $this->l('Sale stickers style:'),
					'name' => 'sale_style',
					'values' => array(
						array(
							'id' => 'sale_style_flag',
							'value' => 0,
							'label' => $this->l('Flag')),
						array(
							'id' => 'sale_style_circle',
							'value' => 1,
							'label' => $this->l('Circle')),
					),
                    'validation' => 'isUnsignedInt',
				), 
				 array(
					'type' => 'color',
					'label' => $this->l('Sale stickers color:'),
					'name' => 'sale_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Sale stickers background color:'),
					'name' => 'sale_bg_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),   
				'sale_bg_image_field' => array(
					'type' => 'file',
					'label' => $this->l('Sale stickers sticker image(only for circle stickers):'),
					'name' => 'sale_bg_image_field',
                    'desc' => '',
				),
                array(
					'type' => 'text',
					'label' => $this->l('Sale stickers width:'),
					'name' => 'sale_stickers_width',
                    'validation' => 'isUnsignedInt',
                    'prefix' => 'px',
                    'class' => 'fixed-width-lg',
				),
                array(
					'type' => 'text',
					'label' => $this->l('Sale stickers top postion:'),
					'name' => 'sale_stickers_top',
                    'validation' => 'isUnsignedInt',
                    'prefix' => 'px',
                    'class' => 'fixed-width-lg',
				),
                array(
					'type' => 'text',
					'label' => $this->l('Sale stickers left postion:'),
					'name' => 'sale_stickers_left',
                    'validation' => 'isUnsignedInt',
                    'prefix' => 'px',
                    'class' => 'fixed-width-lg',
				),
                array(
					'type' => 'radio',
					'label' => $this->l('Show price drop percentage/amount:'),
					'name' => 'discount_percentage',
					'values' => array(
						array(
							'id' => 'discount_percentage_off',
							'value' => 0,
							'label' => $this->l('No')),
						array(
							'id' => 'discount_percentage_text',
							'value' => 1,
							'label' => $this->l('Text')),
						array(
							'id' => 'discount_percentage_sticker',
							'value' => 2,
							'label' => $this->l('Sticker')),
					),
                    'validation' => 'isUnsignedInt',
				), 
				 array(
					'type' => 'color',
					'label' => $this->l('Price drop stickers text color:'),
					'name' => 'price_drop_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Price drop stickers border color:'),
					'name' => 'price_drop_border_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
				 array(
					'type' => 'color',
					'label' => $this->l('Price drop stickers background color:'),
					'name' => 'price_drop_bg_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			     ),
                array(
					'type' => 'text',
					'label' => $this->l('Price drop stickers bottom postion:'),
					'name' => 'price_drop_bottom',
                    'validation' => 'isUnsignedInt',
                    'prefix' => 'px',
                    'class' => 'fixed-width-lg',
				),
                array(
					'type' => 'text',
					'label' => $this->l('Price drop stickers right postion:'),
					'name' => 'price_drop_right',
                    'validation' => 'isUnsignedInt',
                    'prefix' => 'px',
                    'class' => 'fixed-width-lg',
				),  
                array(
					'type' => 'text',
					'label' => $this->l('Price drop stickers width:'),
					'name' => 'price_drop_width',
                    'validation' => 'isUnsignedInt',
                    'desc' => $this->l('Number of width must be greater than 28'),
                    'prefix' => 'px',
                    'class' => 'fixed-width-lg',
				),           
            ),
			'submit' => array(
				'title' => $this->l('   Save all   '),
			),
        );
        
        
        $this->fields_form[16]['form'] = array(
			'input' => array(
                array(
                    'type' => 'switch',
                    'label' => $this->l('Enable big image:'),
                    'name' => 'product_big_image',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'product_big_image_on',
                            'value' => 1,
                            'label' => $this->l('Enable')),
                        array(
                            'id' => 'product_big_image_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')),
                    ),
                    'desc' => $this->l('If you set this option to YES, "Show product secondary column" option will be disabled automatically'),
                    'validation' => 'isBool',
                ), 
                array(
					'type' => 'switch',
					'label' => $this->l('Show product secondary column:'),
					'name' => 'product_secondary',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'product_secondary_on',
							'value' => 1,
							'label' => $this->l('Enable')),
						array(
							'id' => 'product_secondary_off',
							'value' => 0,
							'label' => $this->l('Disabled')),
					),
                    'validation' => 'isBool',
				), 
                array(
					'type' => 'radio',
					'label' => $this->l('Show brand logo on product page:'),
					'name' => 'show_brand_logo',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'show_brand_logo_off',
							'value' => 0,
							'label' => $this->l('No')),
						array(
							'id' => 'show_brand_logo_on_secondary_column',
							'value' => 1,
							'label' => $this->l('Display the brand logo on the product secondary column.')),
						array(
							'id' => 'show_brand_logo_under_product_name',
							'value' => 2,
							'label' => $this->l('Display the brand logo under the product name.')),
					),
                    'desc' => $this->l('Brand logo on product secondary column'),
                    'validation' => 'isUnsignedInt',
				), 
                array(
                    'type' => 'radio',
                    'label' => $this->l('Dispaly product tags:'),
                    'name' => 'display_pro_tags',
                    'default_value' => 0,
                    'values' => array(
                        array(
                            'id' => 'display_pro_tags_disable',
                            'value' => 0,
                            'label' => $this->l('No')),
                        array(
                            'id' => 'display_pro_tags_as_a_tab',
                            'value' => 1,
                            'label' => $this->l('Tags tab')),
                        array(
                            'id' => 'display_pro_tags_at_bottom_of_description',
                            'value' => 2,
                            'label' => $this->l('Display tags at the bottom of the descriptions.')),
                    ),
                    'validation' => 'isUnsignedInt',
                ),
                array(
					'type' => 'radio',
					'label' => $this->l('Zoom_type:'),
					'name' => 'zoom_type',
                    'default_value' => 0,
					'values' => array(
						array(
							'id' => 'zoom_type_standrad',
							'value' => 0,
							'label' => $this->l('Standard')),
						array(
							'id' => 'zoom_type_innerzoom',
							'value' => 1,
							'label' => $this->l('Innerzoom')),
					),
                    'validation' => 'isUnsignedInt',
				),
                array(
					'type' => 'switch',
					'label' => $this->l('Display tax label:'),
					'name' => 'display_tax_label',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'display_tax_label_on',
							'value' => 1,
							'label' => $this->l('Enable')),
						array(
							'id' => 'display_tax_label_off',
							'value' => 0,
							'label' => $this->l('Disabled')),
					),
                    'desc' => array(
                        $this->l('Set number of products in a row for default screen resolution(980px).'),
                        $this->l('On wide screens the number of columns will be automatically increased.'),
                    ),
                    'desc' => $this->l('In order to display the tax incl label, you need to activate taxes (Localization -> taxes -> Enable tax), make sure your country displays the label (Localization -> countries -> select your country -> display tax label) and to make sure the group of the customer is set to display price with taxes (BackOffice -> customers -> groups).'),
                    'validation' => 'isBool',
				), 
                array(
					'type' => 'radio',
					'label' => $this->l('Google rich snippets:'),
					'name' => 'google_rich_snippets',
                    'default_value' => 1,
					'values' => array(
						array(
							'id' => 'google_rich_snippets_disable',
							'value' => 0,
							'label' => $this->l('Disable')),
						array(
							'id' => 'google_rich_snippets_enable',
							'value' => 1,
							'label' => $this->l('Enable')),
						array(
							'id' => 'google_rich_snippets_except_for_review_aggregate',
							'value' => 2,
							'label' => $this->l('Enable except for Review-aggregate')),
					),
                    'validation' => 'isUnsignedInt',
				),
				array(
					'type' => 'color',
					'label' => $this->l('Tab color:'),
					'name' => 'pro_tab_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			    ),
				array(
					'type' => 'color',
					'label' => $this->l('Active tab color:'),
					'name' => 'pro_tab_active_color',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			    ),
				array(
					'type' => 'color',
					'label' => $this->l('Tab background:'),
					'name' => 'pro_tab_bg',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			    ),
				array(
					'type' => 'color',
					'label' => $this->l('Active tab background:'),
					'name' => 'pro_tab_active_bg',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			    ),
				array(
					'type' => 'color',
					'label' => $this->l('Tab content background:'),
					'name' => 'pro_tab_content_bg',
					'class' => 'color',
					'size' => 20,
                    'validation' => 'isColor',
			    ),
                'packitems_pro_per_0' => array(
                    'type' => 'html',
                    'id' => 'packitems_pro_per_0',
                    'label'=> $this->l('The number of columns for Pack items'),
                    'name' => '',
                ),
			),
			'submit' => array(
				'title' => $this->l('   Save all   '),
			),
		);
        
        $inputs = array();
		foreach ($this->getConfigurableModules() as $module)
		{
			$desc = '';
			if (isset($module['is_module']) && $module['is_module'])
			{
				$module_instance = Module::getInstanceByName($module['name']);
				if (Validate::isLoadedObject($module_instance) && method_exists($module_instance, 'getContent'))
					$desc = '<a class="btn btn-default" href="'.$this->context->link->getAdminLink('AdminModules', true).'&configure='.urlencode($module_instance->name).'&tab_module='.$module_instance->tab.'&module_name='.urlencode($module_instance->name).'">'.$this->l('Configure').' <i class="icon-external-link"></i></a>';
			}
			if (isset($module['desc']) && $module['desc'])
				$desc = $desc.'<p class="help-block">'.$module['desc'].'</p>';

			$inputs[] = array(
				'type' => 'switch',
				'label' => $module['label'],
				'name' => $module['name'],
				'desc' => $desc,
				'values' => array(
					array(
						'id' => 'active_on',
						'value' => 1,
						'label' => $this->l('Enabled')
					),
					array(
						'id' => 'active_off',
						'value' => 0,
						'label' => $this->l('Disabled')
					)
				),
			);
		}
        
        $this->fields_form[17]['form'] = array(
            'input' => $inputs,
			'submit' => array(
				'title' => $this->l('   Save all   '),
			),
        );
        
        $this->fields_form[18]['form'] = array(
            'input' => array(
                'icon_iphone_57_field' => array(
					'type' => 'file',
					'label' => $this->l('Iphone/iPad Favicons 57 (PNG):'),
					'name' => 'icon_iphone_57_field',
                    'desc' => '',
				),
				'icon_iphone_72_field' => array(
					'type' => 'file',
					'label' => $this->l('Iphone/iPad Favicons 72 (PNG):'),
					'name' => 'icon_iphone_72_field',
                    'desc' => '',
				),
				'icon_iphone_114_field' => array(
					'type' => 'file',
					'label' => $this->l('Iphone/iPad Favicons 114 (PNG):'),
					'name' => 'icon_iphone_114_field',
                    'desc' => '',
				),
				'icon_iphone_144_field' => array(
					'type' => 'file',
					'label' => $this->l('Iphone/iPad Favicons 144 (PNG):'),
					'name' => 'icon_iphone_144_field',
                    'desc' => '',
				),
            ),
			'submit' => array(
				'title' => $this->l('   Save all   '),
			),
        );
    }
	
    protected function initForm()
	{
        $footer_img = Configuration::get('STSN_FOOTER_IMG');
		if ($footer_img != "") {
		    $this->fields_form[0]['form']['input']['payment_icon']['image'] = $this->getImageHtml(($footer_img!=$this->defaults["footer_img"] ? _THEME_PROD_PIC_DIR_.$footer_img : $this->_path.$footer_img),'footer_img');
		}
		if (Configuration::get('STSN_ICON_IPHONE_57') != "") {
		    $this->fields_form[18]['form']['input']['icon_iphone_57_field']['image'] = $this->getImageHtml($this->_path.Configuration::get('STSN_ICON_IPHONE_57'),'icon_iphone_57');
		}
		if (Configuration::get('STSN_ICON_IPHONE_72') != "") {
		    $this->fields_form[18]['form']['input']['icon_iphone_72_field']['image'] = $this->getImageHtml($this->_path.Configuration::get('STSN_ICON_IPHONE_72'),'icon_iphone_72');
		}
		if (Configuration::get('STSN_ICON_IPHONE_114') != "") {
		    $this->fields_form[18]['form']['input']['icon_iphone_114_field']['image'] = $this->getImageHtml($this->_path.Configuration::get('STSN_ICON_IPHONE_114'),'icon_iphone_114');
		}
		if (Configuration::get('STSN_ICON_IPHONE_144') != "") {
		    $this->fields_form[18]['form']['input']['icon_iphone_144_field']['image'] = $this->getImageHtml($this->_path.Configuration::get('STSN_ICON_IPHONE_144'),'icon_iphone_144');
		}
        
		if (Configuration::get('STSN_HEADER_BG_IMG') != "") {
		    $this->fields_form[4]['form']['input']['header_bg_image_field']['image'] = $this->getImageHtml($this->_path.Configuration::get('STSN_HEADER_BG_IMG'), 'header_bg_img');
		}
		if (Configuration::get('STSN_BODY_BG_IMG') != "") {
		    $this->fields_form[6]['form']['input']['body_bg_image_field']['image'] = $this->getImageHtml($this->_path.Configuration::get('STSN_BODY_BG_IMG'),'body_bg_img');
		}
		if (Configuration::get('STSN_F_TOP_BG_IMG') != "") {
		    $this->fields_form[7]['form']['input']['f_top_bg_image_field']['image'] = $this->getImageHtml($this->_path.Configuration::get('STSN_F_TOP_BG_IMG'),'f_top_bg_img');
		}
		if (Configuration::get('STSN_FOOTER_BG_IMG') != "") {
		    $this->fields_form[8]['form']['input']['footer_bg_image_field']['image'] = $this->getImageHtml($this->_path.Configuration::get('STSN_FOOTER_BG_IMG'),'footer_bg_img');
		}
		if (Configuration::get('STSN_F_SECONDARY_BG_IMG') != "") {
		    $this->fields_form[9]['form']['input']['f_secondary_bg_image_field']['image'] = $this->getImageHtml($this->_path.Configuration::get('STSN_F_SECONDARY_BG_IMG'),'f_secondary_bg_img');
		}
		if (Configuration::get('STSN_F_INFO_BG_IMG') != "") {
		    $this->fields_form[10]['form']['input']['f_info_bg_image_field']['image'] = $this->getImageHtml($this->_path.Configuration::get('STSN_F_INFO_BG_IMG'),'f_info_bg_img');
		}
		if (Configuration::get('STSN_NEW_BG_IMG') != "") {
            $this->fields_form[15]['form']['input']['new_bg_image_field']['image'] = $this->getImageHtml($this->_path.Configuration::get('STSN_NEW_BG_IMG'),'new_bg_img');
        }
        if (Configuration::get('STSN_SALE_BG_IMG') != "") {
            $this->fields_form[15]['form']['input']['sale_bg_image_field']['image'] = $this->getImageHtml($this->_path.Configuration::get('STSN_SALE_BG_IMG'),'sale_bg_img');
        }
        if(!Configuration::get('STSN_LOGO_POSITION'))
            $this->fields_form[0]['form']['input']['logo_height']['disabled']=true;           
        
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table =  $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;

		$helper->identifier = $this->identifier;
		$helper->submit_action = 'savestthemeeditor';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		); 
        
		return $helper;
	}
    
    public function fontOptions() {
        $system = $google = array();
        foreach($this->systemFonts as $v)
            $system[] = array('id'=>$v,'name'=>$v);
        foreach($this->googleFonts as $v)
            $google[] = array('id'=>$v,'name'=>$v);
        $module = new StThemeEditor();
        return array(
            array('name'=>$module->l('System Web fonts'),'query'=>$system),
            array('name'=>$module->l('Google Web Fonts'),'query'=>$google),
        );
	}
    public function getPatterns()
    {
        $html = '';
        foreach(range(1,9) as $v)
            $html .= '<div class="parttern_wrap"><span>'.$v.'</span><img src="'.$this->_path.'patterns/'.$v.'.png" /></div>';
        $html .= '<div>Pattern credits:<a href="http://subtlepatterns.com" target="_blank">subtlepatterns.com</a></div>';
        return $html;
    }
    public function getPatternsArray()
    {
        $arr = array();
        for($i=1;$i<=9;$i++)
            $arr[] = array('id'=>$i,'name'=>$i); 
        return $arr;   
    }
    public function writeCss()
    {
        $id_shop = (int)Shop::getContextShopID();
        $css = '';
    	$fontText = Configuration::get('STSN_FONT_TEXT');
    	$fontHeading = Configuration::get('STSN_FONT_HEADING');
    	$fontPrice = Configuration::get('STSN_FONT_PRICE');
    	$fontMenu = Configuration::get('STSN_FONT_MENU');
    	$fontCartBtn = Configuration::get('STSN_FONT_CART_BTN');
    	        
        if($fontText)
    	   $css .='body{font-family:'.$fontText.', Tahoma, sans-serif, Arial;}';
    	if($fontPrice != $fontText)
        	$css .='.price,#our_price_display,.old_price,.sale_percentage{font-family: "'.$fontPrice.'", Tahoma, sans-serif, Arial;}';
        if($fontCartBtn != $fontText)
            $css .='.product_list.list .ajax_add_to_cart_button, .product_list.list .view_button,#buy_block #add_to_cart .btn_primary,#create-account_form .submit .btn_primary, #login_form .submit .btn_primary, .camera_caption_box .btn_primary, .iosSlider_text .btn_primary{font-family: "'.$fontCartBtn.'", Tahoma, sans-serif, Arial;}';
        $css .= '.btn-default.btn_primary, .btn-medium.btn_primary, .btn-large.btn_primary{text-transform: '.self::$textTransform[(int)Configuration::get('STSN_FONT_HEADING_TRANS')]['name'].';}';
        $css_font_heading = 'font-weight: '.(Configuration::get('STSN_FONT_HEADING_WEIGHT') ? 'bold' : 'normal').';text-transform: '.self::$textTransform[(int)Configuration::get('STSN_FONT_HEADING_TRANS')]['name'].';'.($fontHeading != $fontText ? 'font-family: "'.$fontHeading.'";' : '');
        if(Configuration::get('STSN_FONT_HEADING_SIZE'))
            $css_font_heading .='font-size: '.Configuration::get('STSN_FONT_HEADING_SIZE').'px;';            
            
        $css_font_menu = $css_font_mobile_menu = 'font-weight: '.(Configuration::get('STSN_FONT_MENU_WEIGHT') ? 'bold' : 'normal').';text-transform: '.self::$textTransform[(int)Configuration::get('STSN_FONT_MENU_TRANS')]['name'].';'.($fontMenu != $fontText ? 'font-family: "'.$fontMenu.'";' : '');
        if($fontMenu != $fontText)
        {
            $css_font_menu .= 'font-family: "'.$fontMenu.'";';
            $css_font_mobile_menu .= 'font-family: "'.$fontMenu.'";';
            $css .= '.style_wide .ma_level_1{font-family: "'.$fontMenu.'"}';
        }
        if(Configuration::get('STSN_FONT_MENU_SIZE'))
            $css_font_menu .='font-size: '.Configuration::get('STSN_FONT_MENU_SIZE').'px;';
            
        $css .= '.block .title_block, .block a.title_block, .block .title_block a, .idTabs a,.product_accordion_title,.heading,.page-heading,.page-subheading,#pc_slider_tabs a, #home-page-tabs li a, #home-page-tabs li span{'.$css_font_heading.'}';
        $css .= '#st_mega_menu .ma_level_0{'.$css_font_menu.'}'; 
        $css .= '#stmobilemenu .ma_level_0{'.$css_font_mobile_menu.'}'; 
        $css .= '.style_wide .ma_level_1{text-transform: '.self::$textTransform[(int)Configuration::get('STSN_FONT_MENU_TRANS')]['name'].';}'; 
        
        
        if(Configuration::get('STSN_FONT_PRICE_SIZE'))
            $css .='.price_container .price{font-size: '.Configuration::get('STSN_FONT_PRICE_SIZE').'px;}';  
        if(Configuration::get('STSN_FONT_OLD_PRICE_SIZE'))
            $css .='.price_container .old_price{font-size: '.Configuration::get('STSN_FONT_OLD_PRICE_SIZE').'px;}';     
            
        if(Configuration::get('STSN_FOOTER_HEADING_SIZE'))
            $css .='#footer .title_block{font-size: '.Configuration::get('STSN_FOOTER_HEADING_SIZE').'px;}';
            
        if(Configuration::get('STSN_BLOCK_HEADINGS_COLOR'))
            $css .='.block .title_block, .block a.title_block, .block .title_block a, #home-page-tabs li a, #home-page-tabs li span{color: '.Configuration::get('STSN_BLOCK_HEADINGS_COLOR').';}';
        if(Configuration::get('STSN_HEADINGS_COLOR'))
            $css .='.heading,.page-heading,.page-subheading, a.heading,a.page-heading,a.page-subheading,#home-page-tabs > li a{color: '.Configuration::get('STSN_HEADINGS_COLOR').';}';
            

        if(Configuration::get('STSN_F_TOP_H_COLOR'))
            $css .='#footer-top .block .title_block, #footer-top .block a.title_block, #footer-top .block .title_block a{color: '.Configuration::get('STSN_F_TOP_H_COLOR').';}';
        if(Configuration::get('STSN_FOOTER_H_COLOR'))
            $css .='#footer-primary .block .title_block, #footer-primary .block a.title_block, #footer-primary .block .title_block a{color: '.Configuration::get('STSN_FOOTER_H_COLOR').';}';
        if(Configuration::get('STSN_F_SECONDARY_H_COLOR'))
            $css .='#footer-secondary .block .title_block, #footer-secondary .block a.title_block, #footer-secondary .block .title_block a{color: '.Configuration::get('STSN_F_SECONDARY_H_COLOR').';}';
            
        //color
        if(Configuration::get('STSN_TEXT_COLOR'))
            $css .='body{color: '.Configuration::get('STSN_TEXT_COLOR').';}';
        if(Configuration::get('STSN_LINK_COLOR'))
            $css .='a,.show_all_products{color: '.Configuration::get('STSN_LINK_COLOR').';}';
        if(Configuration::get('STSN_LINK_HOVER_COLOR'))
        {
            $css .='a:active,a:hover,
            #layered_block_left ul li a:hover,
            #product_comments_block_extra a:hover,
            .breadcrumb a:hover,
            a.color_666:hover,
            #pc_slider_tabs a.selected,
            #footer_info a:hover,
            .blog_info a:hover,
            .block .title_block a:hover,
            .show_all_products,
            .content_sortPagiBar .display li.selected a, .content_sortPagiBar .display_m li.selected a,
            .content_sortPagiBar .display li a:hover, .content_sortPagiBar .display_m li a:hover,
            #home-page-tabs > li.active a, #home-page-tabs li a:hover,
            .fancybox-skin .fancybox-close:hover{color: '.Configuration::get('STSN_LINK_HOVER_COLOR').';}';
        }

        if(Configuration::get('STSN_PRICE_COLOR'))
            $css .='.price, #our_price_display, .sale_percentage{color: '.Configuration::get('STSN_PRICE_COLOR').';}';
        if(Configuration::get('STSN_BREADCRUMB_COLOR'))
            $css .='.breadcrumb, .breadcrumb a{color: '.Configuration::get('STSN_BREADCRUMB_COLOR').';}';
        if(Configuration::get('STSN_BREADCRUMB_HOVER_COLOR'))
            $css .='.breadcrumb a:hover{color: '.Configuration::get('STSN_BREADCRUMB_HOVER_COLOR').';}';

        if($breadcrumb_bg_hex = Configuration::get('STSN_BREADCRUMB_BG'))
        {
            if(Configuration::get('STSN_BREADCRUMB_BG_STYLE'))
            {
                $css .='#breadcrumb_wrapper{
    background: '.$breadcrumb_bg_hex.';
    background: -webkit-linear-gradient(none);
    background: -moz-linear-gradient(none);
    background: -o-linear-gradient(none);
    background: linear-gradient(none);
                    }';
            }
            else{
                $breadcrumb_bg = self::hex2rgb($breadcrumb_bg_hex);
                if(is_array($breadcrumb_bg))
                {
                    $breadcrumb_bg_star = ($breadcrumb_bg[0]-16).','.($breadcrumb_bg[1]-16).','.($breadcrumb_bg[2]-16);
                    $breadcrumb_bg_end = implode(',',$breadcrumb_bg);
                    $css .='#breadcrumb_wrapper{
    background: '.$breadcrumb_bg_hex.';
    background: -webkit-linear-gradient(top, rgb('.$breadcrumb_bg_star.') , rgb('.$breadcrumb_bg_end.') 5%, rgb('.$breadcrumb_bg_end.') 95%, rgb('.$breadcrumb_bg_star.'));
    background: -moz-linear-gradient(top, rgb('.$breadcrumb_bg_star.'), rgb('.$breadcrumb_bg_end.') 5%, rgb('.$breadcrumb_bg_end.') 95%, rgb('.$breadcrumb_bg_star.'));
    background: -o-linear-gradient(top, rgb('.$breadcrumb_bg_star.'), rgb('.$breadcrumb_bg_end.') 5%, rgb('.$breadcrumb_bg_end.') 95%, rgb('.$breadcrumb_bg_star.'));
    background: linear-gradient(top, rgb('.$breadcrumb_bg_star.'), rgb('.$breadcrumb_bg_end.') 5%, rgb('.$breadcrumb_bg_end.') 95%, rgb('.$breadcrumb_bg_star.'));
                    }';
                }
            }
        }
        
        if(Configuration::get('STSN_ICON_COLOR'))
            $css .='a.icon_wrap, .icon_wrap,#shopping_cart .ajax_cart_right{color: '.Configuration::get('STSN_ICON_COLOR').';}';
        if(Configuration::get('STSN_ICON_HOVER_COLOR'))
            $css .='a.icon_wrap.active,.icon_wrap.active,a.icon_wrap:hover,.icon_wrap:hover,#searchbox_inner.active #submit_searchbox.icon_wrap,.logo_center #searchbox_inner:hover #submit_searchbox.icon_wrap,#shopping_cart:hover .icon_wrap,#shopping_cart.active .icon_wrap,.myaccount-link-list a:hover .icon_wrap{color: '.Configuration::get('STSN_ICON_HOVER_COLOR').';}';
        if($icon_bg_color = Configuration::get('STSN_ICON_BG_COLOR'))
            $css .='a.icon_wrap, .icon_wrap,#shopping_cart .ajax_cart_right,#rightbar{background-color: '.$icon_bg_color.';}';    
        if($icon_hover_bg_color = Configuration::get('STSN_ICON_HOVER_BG_COLOR'))
        {
            $css .='a.icon_wrap.active,.icon_wrap.active,a.icon_wrap:hover,.icon_wrap:hover,#searchbox_inner.active #submit_searchbox.icon_wrap,.logo_center #searchbox_inner:hover #submit_searchbox.icon_wrap,#shopping_cart:hover .icon_wrap,#shopping_cart.active .icon_wrap,.myaccount-link-list a:hover .icon_wrap{background-color: '.$icon_hover_bg_color.';}';    
            $css .='#submit_searchbox:hover,#searchbox_inner.active #search_query_top,#searchbox_inner.active #submit_searchbox.icon_wrap,.logo_center #searchbox_inner:hover #submit_searchbox.icon_wrap,#shopping_cart.active .icon_wrap,#shopping_cart:hover .icon_wrap{border-color:'.$icon_hover_bg_color.';}';
        }
        if(Configuration::get('STSN_ICON_DISABLED_COLOR'))
            $css .='a.icon_wrap.disabled,.icon_wrap.disabled{color: '.Configuration::get('STSN_ICON_DISABLED_COLOR').';}';
        if(Configuration::get('STSN_RIGHT_PANEL_BORDER'))
            $css .='#rightbar,.rightbar_wrap a.icon_wrap,#to_top_wrap a.icon_wrap,#switch_left_column_wrap a.icon_wrap,#switch_right_column_wrap a.icon_wrap{border-color: '.Configuration::get('STSN_RIGHT_PANEL_BORDER').';}';
        if(Configuration::get('STSN_STARTS_COLOR'))
            $css .='div.star.star_on:after,div.star.star_hover:after,.rating_box i.light{color: '.Configuration::get('STSN_STARTS_COLOR').';}';
        if(Configuration::get('STSN_CIRCLE_NUMBER_COLOR'))
            $css .='.amount_circle{color: '.Configuration::get('STSN_CIRCLE_NUMBER_COLOR').';}';
        if(Configuration::get('STSN_CIRCLE_NUMBER_BG'))
            $css .='.amount_circle{background-color: '.Configuration::get('STSN_CIRCLE_NUMBER_BG').';}';
            
        if($percent_of_screen = Configuration::get('STSN_POSITION_RIGHT_PANEL'))
        {
            $percent_of_screen_arr = explode('_',$percent_of_screen);
            $css .='#rightbar{top:'.($percent_of_screen_arr[0]==2 ? $percent_of_screen_arr[1].'%' : 'auto').'; bottom:'.($percent_of_screen_arr[0]==1 ? $percent_of_screen_arr[1].'%' : 'auto').';}';
        }
        //button  
        $button_css = $button_hover_css = $primary_button_css = $primary_button_hover_css = '';   
        if(Configuration::get('STSN_BTN_COLOR'))   
            $button_css .='color: '.Configuration::get('STSN_BTN_COLOR').';';
        if(Configuration::get('STSN_BTN_HOVER_COLOR'))   
            $button_hover_css .='color: '.Configuration::get('STSN_BTN_HOVER_COLOR').';';
        if(Configuration::get('STSN_BTN_BG_COLOR'))   
            $button_css .='background-color: '.Configuration::get('STSN_BTN_BG_COLOR').';border-color:'.Configuration::get('STSN_BTN_BG_COLOR').';';
        if(Configuration::get('STSN_BTN_HOVER_BG_COLOR'))   
            $button_hover_css .='background-color: '.Configuration::get('STSN_BTN_HOVER_BG_COLOR').';border-color:'.Configuration::get('STSN_BTN_HOVER_BG_COLOR').';';
        if(Configuration::get('STSN_P_BTN_COLOR'))   
        {
            $primary_button_css .='color: '.Configuration::get('STSN_P_BTN_COLOR').';';
            $css .= '.hover_fly a,.hover_fly a:hover,.hover_fly a:first-child,.hover_fly a:first-child:hover{color:'.Configuration::get('STSN_P_BTN_COLOR').'!important;}';
        }
        if(Configuration::get('STSN_P_BTN_HOVER_COLOR'))   
            $primary_button_hover_css .='color: '.Configuration::get('STSN_P_BTN_HOVER_COLOR').';';
        if(Configuration::get('STSN_P_BTN_BG_COLOR'))   
        {
            $primary_button_css .='background-color: '.Configuration::get('STSN_P_BTN_BG_COLOR').';border-color:'.Configuration::get('STSN_P_BTN_BG_COLOR').';';
            $css .= '.hover_fly a:first-child{background-color: '.Configuration::get('STSN_P_BTN_BG_COLOR').';}.itemlist_action a{background-color: '.Configuration::get('STSN_P_BTN_BG_COLOR').';}.hover_fly a:hover{background-color: '.Configuration::get('STSN_P_BTN_BG_COLOR').'!important;}.itemlist_action a:hover{background-color: '.Configuration::get('STSN_P_BTN_BG_COLOR').';}';
        }
        if(Configuration::get('STSN_P_BTN_HOVER_BG_COLOR'))
            $primary_button_hover_css .='background-color: '.Configuration::get('STSN_P_BTN_HOVER_BG_COLOR').';border-color:'.Configuration::get('STSN_P_BTN_HOVER_BG_COLOR').';';
            
        if($button_css)
            $css .= '.btn-default, .btn-medium, .btn-large,
                input.button_mini,
                input.button_small,
                input.button,
                input.button_large,
                input.button_mini_disabled,
                input.button_small_disabled,
                input.button_disabled,
                input.button_large_disabled,
                input.exclusive_mini,
                input.exclusive_small,
                input.exclusive,
                input.exclusive_large,
                input.exclusive_mini_disabled,
                input.exclusive_small_disabled,
                input.exclusive_disabled,
                input.exclusive_large_disabled,
                a.button_mini,
                a.button_small,
                a.button,
                a.button_large,
                a.exclusive_mini,
                a.exclusive_small,
                a.exclusive,
                a.exclusive_large,
                span.button_mini,
                span.button_small,
                span.button,
                span.button_large,
                span.exclusive_mini,
                span.exclusive_small,
                span.exclusive,
                span.exclusive_large,
                span.exclusive_large_disabled{'.$button_css.'}';
        if($button_hover_css)
            $css .= '.btn-default:hover, .btn-default.active, 
                .btn-medium:hover, .btn-medium.active, 
                .btn-large:hover, .btn-large.active,
                input.button_mini:hover,
                input.button_small:hover,
                input.button:hover,
                input.button_large:hover,
                input.exclusive_mini:hover,
                input.exclusive_small:hover,
                input.exclusive:hover,
                input.exclusive_large:hover,
                a.button_mini:hover,
                a.button_small:hover,
                a.button:hover,
                a.button_large:hover,
                a.exclusive_mini:hover,
                a.exclusive_small:hover,
                a.exclusive:hover,
                a.exclusive_large:hover,
                input.button_mini:active,
                input.button_small:active,
                input.button:active,
                input.button_large:active,
                input.exclusive_mini:active,
                input.exclusive_small:active,
                input.exclusive:active,
                input.exclusive_large:active,
                a.button_mini:active,
                a.button_small:active,
                a.button:active,
                a.button_large:active,
                a.exclusive_mini:active,
                a.exclusive_small:active,
                a.exclusive:active,
                a.exclusive_large:active{'.$button_hover_css.'}';
        if($primary_button_css)
            $css .= '.product_list.list .button.ajax_add_to_cart_button, .btn-default.btn_primary, .btn-medium.btn_primary, .btn-large.btn_primary {'.$primary_button_css.'}';
        if($primary_button_hover_css)
            $css .= '.product_list.list .button.ajax_add_to_cart_button:hover, .itemlist_action a.exclusive:hover,
                .btn-default.btn_primary:hover, .btn-default.btn_primary.active, 
                .btn-medium.btn_primary:hover, .btn-medium.btn_primary.active, 
                .btn-large.btn_primary:hover, .btn-large.btn_primary.active{'.$primary_button_hover_css.'}';
           
        if(Configuration::get('STSN_FLYOUT_BUTTONS_BG'))   
            $css .='.hover_fly, .hover_fly a, .hover_fly:hover a:first-child{background-color: '.Configuration::get('STSN_FLYOUT_BUTTONS_BG').';}';
    
        //header
        if(Configuration::get('STSN_HEADER_TEXT_COLOR'))
            $css .='#top_bar{color:'.Configuration::get('STSN_HEADER_TEXT_COLOR').';}';
        if(Configuration::get('STSN_HEADER_LINK_COLOR'))
            $css .='#top_bar a{color:'.Configuration::get('STSN_HEADER_LINK_COLOR').';}.dropdown_tri_inner b{border-color: '.Configuration::get('STSN_HEADER_LINK_COLOR').' transparent transparent;}';
        if(Configuration::get('STSN_HEADER_LINK_HOVER_COLOR'))
            $css .='#top_bar a:hover,#top_bar .open .dropdown_tri_inner{color:'.Configuration::get('STSN_HEADER_LINK_HOVER_COLOR').';}.open .dropdown_tri_inner b{border-color: '.Configuration::get('STSN_HEADER_LINK_HOVER_COLOR').' transparent transparent;}';
        if(Configuration::get('STSN_HEADER_LINK_HOVER_BG'))
            $css .='#top_bar a:hover,#top_bar .open .dropdown_tri_inner{background-color:'.Configuration::get('STSN_HEADER_LINK_HOVER_BG').';}';
        if(Configuration::get('STSN_DROPDOWN_HOVER_COLOR'))
            $css .='#top_bar .dropdown_list li a:hover{color:'.Configuration::get('STSN_DROPDOWN_HOVER_COLOR').';}';   
        if(Configuration::get('STSN_DROPDOWN_BG_COLOR'))
            $css .='#top_bar .dropdown_list li a:hover{background-color:'.Configuration::get('STSN_DROPDOWN_BG_COLOR').';}'; 
        if(Configuration::get('STSN_HEADER_TOPBAR_BG'))
            $css .='#top_bar{background-color:'.Configuration::get('STSN_HEADER_TOPBAR_BG').';}'; 
        if(Configuration::get('STSN_HEADER_TOPBAR_SEP'))
            $css .='#top_bar #header_user_info a, #top_bar #header_user_info span, #stsocial_list_topbar li a, #contact-link a, .shop-phone, .dropdown_tri_inner{border-color:'.Configuration::get('STSN_HEADER_TOPBAR_SEP').';}'; 
                    
        //menu
        if(Configuration::get('STSN_MENU_COLOR'))
            $css .='.ma_level_0{color:'.Configuration::get('STSN_MENU_COLOR').';}'; 
        if(Configuration::get('STSN_MENU_HOVER_COLOR'))
            $css .='.sttlevel0.current .ma_level_0, .sttlevel0.active .ma_level_0{color:'.Configuration::get('STSN_MENU_HOVER_COLOR').';}'; 
        if(Configuration::get('STSN_MENU_HOVER_BG'))
            $css .='.sttlevel0.current .ma_level_0, .sttlevel0.active .ma_level_0{background-color:'.Configuration::get('STSN_MENU_HOVER_BG').';}'; 
        if($menu_bg_color = Configuration::get('STSN_MENU_BG_COLOR'))
        {
            if(Configuration::get('STSN_MEGAMENU_WIDTH'))
            {
                $css .='#st_mega_menu_container{background-color:'.$menu_bg_color.';padding-bottom:0;}'; 
                $megamenu_bg = self::hex2rgb($menu_bg_color );
                if(is_array($megamenu_bg))
                    $css .='#st_mega_menu_container.sticky{background: '.$menu_bg_color .';background:rgba('.$megamenu_bg[0].','.$megamenu_bg[1].','.$megamenu_bg[2].',0.9);}';
            }
            else
                $css .='#st_mega_menu{background-color:'.$menu_bg_color.';}'; 
        }
        if(Configuration::get('STSN_SECOND_MENU_COLOR'))
            $css .='.ma_level_1,.stmenu_sub.style_classic .ma_level_1{color:'.Configuration::get('STSN_SECOND_MENU_COLOR').';}'; 
        if(Configuration::get('STSN_SECOND_MENU_HOVER_COLOR'))
            $css .='.ma_level_1:hover,.stmenu_sub.style_classic .show .ma_level_1{color:'.Configuration::get('STSN_SECOND_MENU_HOVER_COLOR').';}'; 
        if(Configuration::get('STSN_THIRD_MENU_COLOR'))
            $css .='.ma_level_2{color:'.Configuration::get('STSN_THIRD_MENU_COLOR').';}'; 
        if(Configuration::get('STSN_THIRD_MENU_HOVER_COLOR'))
            $css .='.ma_level_2:hover{color:'.Configuration::get('STSN_THIRD_MENU_HOVER_COLOR').';}'; 
        if(Configuration::get('STSN_MENU_MOB_COLOR'))
            $css .='#stmobilemenu_tri{color:'.Configuration::get('STSN_MENU_MOB_COLOR').';}'; 
        if(Configuration::get('STSN_MENU_MOB_HOVER_COLOR'))
            $css .='#stmobilemenu_tri:hover,#stmobilemenu_tri.active{color:'.Configuration::get('STSN_MENU_MOB_HOVER_COLOR').';}'; 
        if(Configuration::get('STSN_MENU_MOB_BG'))
            $css .='#stmobilemenu_tri{background-color:'.Configuration::get('STSN_MENU_MOB_BG').';}'; 
        if(Configuration::get('STSN_MENU_MOB_HOVER_BG'))
            $css .='#stmobilemenu_tri:hover,#stmobilemenu_tri.active{background-color:'.Configuration::get('STSN_MENU_MOB_HOVER_BG').';}';
        if(Configuration::get('STSN_MENU_MOB_ITEMS1_COLOR'))
            $css .='#stmobilemenu .ma_level_0,#stmobilemenu a.ma_level_0{color:'.Configuration::get('STSN_MENU_MOB_ITEMS1_COLOR').';}';
        if(Configuration::get('STSN_MENU_MOB_ITEMS2_COLOR'))
            $css .='#stmobilemenu .ma_level_1,#stmobilemenu a.ma_level_1{color:'.Configuration::get('STSN_MENU_MOB_ITEMS2_COLOR').';}';
        if(Configuration::get('STSN_MENU_MOB_ITEMS3_COLOR'))
            $css .='#stmobilemenu .ma_level_2,#stmobilemenu a.ma_level_2{color:'.Configuration::get('STSN_MENU_MOB_ITEMS3_COLOR').';}';
        if(Configuration::get('STSN_MENU_MOB_ITEMS1_BG'))
            $css .='#stmobilemenu .stmlevel0{background-color:'.Configuration::get('STSN_MENU_MOB_ITEMS1_BG').';}';
        if(Configuration::get('STSN_MENU_MOB_ITEMS2_BG'))
            $css .='#stmobilemenu .stmlevel1 > li{background-color:'.Configuration::get('STSN_MENU_MOB_ITEMS2_BG').';}';
        if(Configuration::get('STSN_MENU_MOB_ITEMS3_BG'))
            $css .='#stmobilemenu .stmlevel2 > li{background-color:'.Configuration::get('STSN_MENU_MOB_ITEMS3_BG').';}';
        //footer
        if(Configuration::get('STSN_FOOTER_BORDER_COLOR'))
            $css .='#footer-primary .container{border-color:'.Configuration::get('STSN_FOOTER_BORDER_COLOR').';}';
        if(Configuration::get('STSN_SECOND_FOOTER_COLOR')) 
            $css .='.footer-container #footer_info,.footer-container #footer_info a{color:'.Configuration::get('STSN_SECOND_FOOTER_COLOR').';}';   
        if(Configuration::get('STSN_FOOTER_COLOR')) 
            $css .='#footer{color:'.Configuration::get('STSN_FOOTER_COLOR').';}'; 
        if(Configuration::get('STSN_FOOTER_LINK_COLOR')) 
            $css .='#footer a{color:'.Configuration::get('STSN_FOOTER_LINK_COLOR').';}'; 
        if(Configuration::get('STSN_FOOTER_LINK_HOVER_COLOR')) 
            $css .='#footer a:hover{color:'.Configuration::get('STSN_FOOTER_LINK_HOVER_COLOR').';}';    
        
        
        if (Configuration::get('STSN_BODY_BG_COLOR'))
        {
            $css .= 'body{background-color:'.Configuration::get('STSN_BODY_BG_COLOR').';}';
            if (!Configuration::get('STSN_BODY_CON_BG_COLOR'))
                $css .= '.section .title_block span, .section .title_block a,.nav_top_right .flex-direction-nav, #home-page-tabs li a, #home-page-tabs li span{background-color:'.Configuration::get('STSN_BODY_BG_COLOR').';}';
        }
        if (Configuration::get('STSN_BODY_CON_BG_COLOR'))
			$css .= '.main_content_area,.main_content_area_top,.main_content_area_footer,.section .title_block span, .section .title_block a,.nav_top_right .flex-direction-nav, #home-page-tabs li a, #home-page-tabs li span{background-color:'.Configuration::get('STSN_BODY_CON_BG_COLOR').';}';
        if (Configuration::get('STSN_MAIN_CON_BG_COLOR'))
            $css .= '.main_content_area > .wide_container,.main_content_area_top .wide_container,.main_content_area_footer .wide_container,.section .title_block span, .section .title_block a,.nav_top_right .flex-direction-nav, #home-page-tabs li a, #home-page-tabs li span{background-color:'.Configuration::get('STSN_MAIN_CON_BG_COLOR').';}';
        if (Configuration::get('STSN_BODY_BG_PATTERN') && (Configuration::get('STSN_BODY_BG_IMG')==""))
			$css .= 'body{background-image: url(../../patterns/'.Configuration::get('STSN_BODY_BG_PATTERN').'.png);}';
        if (Configuration::get('STSN_BODY_BG_IMG'))
			$css .= 'body{background-image:url(../../'.Configuration::get('STSN_BODY_BG_IMG').');}';
		if (Configuration::get('STSN_BODY_BG_REPEAT')) {
			switch(Configuration::get('STSN_BODY_BG_REPEAT')) {
				case 1 :
					$repeat_option = 'repeat-x';
					break;
				case 2 :
					$repeat_option = 'repeat-y';
					break;
				case 3 :
					$repeat_option = 'no-repeat';
					break;
				default :
					$repeat_option = 'repeat';
			}
			$css .= 'body{background-repeat:'.$repeat_option.';}';
		}
		if (Configuration::get('STSN_BODY_BG_POSITION')) {
			switch(Configuration::get('STSN_BODY_BG_POSITION')) {
				case 1 :
					$position_option = 'center top';
					break;
				case 2 :
					$position_option = 'right top';
					break;
				default :
					$position_option = 'left top';
			}
			$css .= 'body{background-position: '.$position_option.';}';
		}
		if (Configuration::get('STSN_BODY_BG_FIXED')) {
			$css .= 'body{background-attachment: fixed;}';
		}
        if (Configuration::get('STSN_HEADER_BG_COLOR'))
			$css .= '#page_header{background-color:'.Configuration::get('STSN_HEADER_BG_COLOR').';}';
        if (Configuration::get('STSN_HEADER_CON_BG_COLOR'))
			$css .= '#header .wide_container,#top_extra .wide_container{background-color:'.Configuration::get('STSN_HEADER_CON_BG_COLOR').';}';
        if (Configuration::get('STSN_HEADER_BG_PATTERN') && (Configuration::get('STSN_HEADER_BG_IMG')==""))
			$css .= '#page_header{background-image: url(../../patterns/'.Configuration::get('STSN_HEADER_BG_PATTERN').'.png);}';
        if (Configuration::get('STSN_HEADER_BG_IMG'))
			$css .= '#page_header{background-image:url(../../'.Configuration::get('STSN_HEADER_BG_IMG').');}';
		if (Configuration::get('STSN_HEADER_BG_REPEAT')) {
			switch(Configuration::get('STSN_HEADER_BG_REPEAT')) {
				case 1 :
					$repeat_option = 'repeat-x';
					break;
				case 2 :
					$repeat_option = 'repeat-y';
					break;
				case 3 :
					$repeat_option = 'no-repeat';
					break;
				default :
					$repeat_option = 'repeat';
			}
			$css .= '#page_header{background-repeat:'.$repeat_option.';}';
		}
		if (Configuration::get('STSN_HEADER_BG_POSITION')) {
			switch(Configuration::get('STSN_HEADER_BG_POSITION')) {
				case 1 :
					$position_option = 'center top';
					break;
				case 2 :
					$position_option = 'right top';
					break;
				default :
					$position_option = 'left top';
			}
			$css .= '#page_header{background-position: '.$position_option.';}';
		}
        if (Configuration::get('STSN_DISPLAY_BANNER_BG'))
            $css .= 'header .banner{background-color:'.Configuration::get('STSN_DISPLAY_BANNER_BG').';}';

                            
        if (Configuration::get('STSN_F_TOP_BG_PATTERN') && (Configuration::get('STSN_F_TOP_BG_IMG')==""))
			$css .= '#footer-top{background-image: url(../../patterns/'.Configuration::get('STSN_F_TOP_BG_PATTERN').'.png);}';
        if (Configuration::get('STSN_F_TOP_BG_IMG'))
			$css .= '#footer-top{background-image:url(../../'.Configuration::get('STSN_F_TOP_BG_IMG').');}';
		if (Configuration::get('STSN_FOOTER_BG_REPEAT')) {
			switch(Configuration::get('STSN_FOOTER_BG_REPEAT')) {
				case 1 :
					$repeat_option = 'repeat-x';
					break;
				case 2 :
					$repeat_option = 'repeat-y';
					break;
				case 3 :
					$repeat_option = 'no-repeat';
					break;
				default :
					$repeat_option = 'repeat';
			}
			$css .= '#footer-top{background-repeat:'.$repeat_option.';}';
		}
		if (Configuration::get('STSN_F_TOP_BG_PATTERN')) {
			switch(Configuration::get('STSN_F_TOP_BG_PATTERN')) {
				case 1 :
					$position_option = 'center top';
					break;
				case 2 :
					$position_option = 'right top';
					break;
				default :
					$position_option = 'left top';
			}
			$css .= '#footer-top{background-position: '.$position_option.';}';
		}
        if (Configuration::get('STSN_FOOTER_TOP_BORDER_COLOR'))
			$css .= '#footer-top{border-top-color:'.Configuration::get('STSN_FOOTER_TOP_BORDER_COLOR').';}';
        if (Configuration::get('STSN_FOOTER_TOP_BG'))
			$css .= '#footer-top{background-color:'.Configuration::get('STSN_FOOTER_TOP_BG').';}';
        if (Configuration::get('STSN_FOOTER_TOP_CON_BG'))
			$css .= '#footer-top .wide_container{background-color:'.Configuration::get('STSN_FOOTER_TOP_CON_BG').';}';
            
        if (Configuration::get('STSN_FOOTER_BG_PATTERN') && (Configuration::get('STSN_FOOTER_BG_IMG')==""))
			$css .= '#footer-primary{background-image: url(../../patterns/'.Configuration::get('STSN_FOOTER_BG_PATTERN').'.png);}';
        if (Configuration::get('STSN_FOOTER_BG_IMG'))
			$css .= '#footer-primary{background-image:url(../../'.Configuration::get('STSN_FOOTER_BG_IMG').');}';
		if (Configuration::get('STSN_FOOTER_BG_REPEAT')) {
			switch(Configuration::get('STSN_FOOTER_BG_REPEAT')) {
				case 1 :
					$repeat_option = 'repeat-x';
					break;
				case 2 :
					$repeat_option = 'repeat-y';
					break;
				case 3 :
					$repeat_option = 'no-repeat';
					break;
				default :
					$repeat_option = 'repeat';
			}
			$css .= '#footer-primary{background-repeat:'.$repeat_option.';}';
		}
		if (Configuration::get('STSN_FOOTER_BG_POSITION')) {
			switch(Configuration::get('STSN_FOOTER_BG_POSITION')) {
				case 1 :
					$position_option = 'center top';
					break;
				case 2 :
					$position_option = 'right top';
					break;
				default :
					$position_option = 'left top';
			}
			$css .= '#footer-primary{background-position: '.$position_option.';}';
		}
        if (Configuration::get('STSN_FOOTER_BG_COLOR'))
			$css .= '#footer-primary{background-color:'.Configuration::get('STSN_FOOTER_BG_COLOR').';}';
        if (Configuration::get('STSN_FOOTER_CON_BG_COLOR'))
			$css .= '#footer-primary .wide_container{background-color:'.Configuration::get('STSN_FOOTER_CON_BG_COLOR').';}';
            
        if (Configuration::get('STSN_F_SECONDARY_BG_PATTERN') && (Configuration::get('STSN_F_SECONDARY_BG_IMG')==""))
			$css .= '#footer-secondary{background-image: url(../../patterns/'.Configuration::get('STSN_F_SECONDARY_BG_PATTERN').'.png);}';
        if (Configuration::get('STSN_F_SECONDARY_BG_IMG'))
			$css .= '#footer-secondary{background-image:url(../../'.Configuration::get('STSN_F_SECONDARY_BG_IMG').');}';
		if (Configuration::get('STSN_F_SECONDARY_BG_REPEAT')) {
			switch(Configuration::get('STSN_F_SECONDARY_BG_REPEAT')) {
				case 1 :
					$repeat_option = 'repeat-x';
					break;
				case 2 :
					$repeat_option = 'repeat-y';
					break;
				case 3 :
					$repeat_option = 'no-repeat';
					break;
				default :
					$repeat_option = 'repeat';
			}
			$css .= '#footer-secondary{background-repeat:'.$repeat_option.';}';
		}
		if (Configuration::get('STSN_F_SECONDARY_BG_POSITION')) {
			switch(Configuration::get('STSN_F_SECONDARY_BG_POSITION')) {
				case 1 :
					$position_option = 'center top';
					break;
				case 2 :
					$position_option = 'right top';
					break;
				default :
					$position_option = 'left top';
			}
			$css .= '#footer-secondary{background-position: '.$position_option.';}';
		}
        if (Configuration::get('STSN_FOOTER_SECONDARY_BG'))
			$css .= '#footer-secondary{background-color:'.Configuration::get('STSN_FOOTER_SECONDARY_BG').';}';
        if (Configuration::get('STSN_FOOTER_SECONDARY_CON_BG'))
			$css .= '#footer-secondary .wide_container{background-color:'.Configuration::get('STSN_FOOTER_SECONDARY_CON_BG').';}';
            
                        
        if (Configuration::get('STSN_F_INFO_BG_PATTERN') && (Configuration::get('STSN_F_INFO_BG_IMG')==""))
			$css .= '.footer-container #footer_info{background-image: url(../../patterns/'.Configuration::get('STSN_F_INFO_BG_PATTERN').'.png);}';
        if (Configuration::get('STSN_F_INFO_BG_IMG'))
			$css .= '.footer-container #footer_info{background-image:url(../../'.Configuration::get('STSN_F_INFO_BG_IMG').');}';
		if (Configuration::get('STSN_F_INFO_BG_REPEAT')) {
			switch(Configuration::get('STSN_F_INFO_BG_REPEAT')) {
				case 1 :
					$repeat_option = 'repeat-x';
					break;
				case 2 :
					$repeat_option = 'repeat-y';
					break;
				case 3 :
					$repeat_option = 'no-repeat';
					break;
				default :
					$repeat_option = 'repeat';
			}
			$css .= '.footer-container #footer_info{background-repeat:'.$repeat_option.';}';
		}
		if (Configuration::get('STSN_F_INFO_BG_POSITION')) {
			switch(Configuration::get('STSN_F_INFO_BG_POSITION')) {
				case 1 :
					$position_option = 'center top';
					break;
				case 2 :
					$position_option = 'right top';
					break;
				default :
					$position_option = 'left top';
			}
			$css .= '.footer-container #footer_info{background-position: '.$position_option.';}';
		}
        if (Configuration::get('STSN_FOOTER_INFO_BG'))
			$css .= '.footer-container #footer_info{background-color:'.Configuration::get('STSN_FOOTER_INFO_BG').';}';
        if (Configuration::get('STSN_FOOTER_INFO_CON_BG'))
			$css .= '.footer-container #footer_info .wide_container{background-color:'.Configuration::get('STSN_FOOTER_INFO_CON_BG').';}';
        
        if(!Configuration::get('STSN_RESPONSIVE'))    
			$css .= '#body_wrapper{min-width:960px;margin-right:auto;margin-left:auto;}';
        
        if(Configuration::get('STSN_NEW_COLOR'))
            $css .='span.new i{color: '.Configuration::get('STSN_NEW_COLOR').';}';
        $new_style = (int)Configuration::get('STSN_NEW_STYLE');
		if($new_style==1)
        {
            $css .= 'span.new{border:none;width:40px;height:40px;line-height:40px;top:0;-webkit-border-radius: 500px;-moz-border-radius: 500px;border-radius: 500px;}span.new i{position:static;left:auto;}';
            if(!Configuration::get('STSN_NEW_BG_IMG'))
                $css .= 'span.new{-webkit-border-radius: 500px;-moz-border-radius: 500px;border-radius: 500px;}';
        }                    
        $new_bg_color = Configuration::get('STSN_NEW_BG_COLOR');
        if($new_bg_color)
        {
            if($new_style==1)
                $css .= 'span.new{background-color:'.$new_bg_color.';}';
            $css .='span.new{color: '.$new_bg_color.';border-color:'.$new_bg_color.';border-left-color:transparent;}';
        }  
        elseif(!$new_bg_color && $new_style==1) 
            $css .= 'span.new{background-color:#00A161;}';
        
        if($new_stickers_width = Configuration::get('STSN_NEW_STICKERS_WIDTH'))
        {
            if($new_style==1)
                $css .= 'span.new{width:'.$new_stickers_width.'px;height:'.$new_stickers_width.'px;line-height:'.$new_stickers_width.'px;}';
            else
                $css .= 'span.new{border-right-width:'.$new_stickers_width.'px;}';
        }
		if(Configuration::get('STSN_NEW_STICKERS_TOP')!==false)
			$css .= 'span.new{top:'.(int)Configuration::get('STSN_NEW_STICKERS_TOP').'px;}';
		if(Configuration::get('STSN_NEW_STICKERS_RIGHT')!==false)
			$css .= 'span.new{right:'.(int)Configuration::get('STSN_NEW_STICKERS_RIGHT').'px;}';
		if($new_style==1 && Configuration::get('STSN_NEW_BG_IMG'))
			$css .= 'span.new{background:url(../../'.Configuration::get('STSN_NEW_BG_IMG').') no-repeat center center transparent;}span.new i{display:none;}';
            
        if(Configuration::get('STSN_SALE_COLOR'))
            $css .='span.on_sale i{color: '.Configuration::get('STSN_SALE_COLOR').';}';
        $sale_style = (int)Configuration::get('STSN_SALE_STYLE');
        if($sale_style==1)  
        {
            $css .= 'span.on_sale{border:none;width:40px;height:40px;line-height:40px;top:0;-webkit-border-radius: 500px;-moz-border-radius: 500px;border-radius: 500px;}span.on_sale i{position:static;left:auto;}';
            if(!Configuration::get('STSN_SALE_BG_IMG'))
                $css .= 'span.on_sale{-webkit-border-radius: 500px;-moz-border-radius: 500px;border-radius: 500px;}';
        }       
        $sale_bg_color = Configuration::get('STSN_SALE_BG_COLOR');
        if($sale_bg_color)
        {
            if($sale_style==1)
                $css .= 'span.on_sale{background-color:'.$sale_bg_color.';}';
            $css .='span.on_sale{color: '.$sale_bg_color.';border-color: '.$sale_bg_color.';border-right-color:transparent;}';
        }
        elseif(!$sale_bg_color && $sale_style==1)
            $css .= 'span.on_sale{background-color:#ff8a00;}';        

		if($sale_stickers_width = Configuration::get('STSN_SALE_STICKERS_WIDTH'))
        {
            if($sale_style==1)
                $css .= 'span.on_sale{width:'.$sale_stickers_width.'px;height:'.$sale_stickers_width.'px;line-height:'.$sale_stickers_width.'px;}';
            else
            {
    			$css .= 'span.on_sale{border-left-width:'.$sale_stickers_width.'px;}';
    			$css .= 'span.on_sale i{left:-'.($sale_stickers_width-7).'px;}';
            }
        }
		if(Configuration::get('STSN_SALE_STICKERS_TOP')!==false)
			$css .= 'span.on_sale{top:'.(int)Configuration::get('STSN_SALE_STICKERS_TOP').'px;}';
		if(Configuration::get('STSN_SALE_STICKERS_LEFT')!==false)
			$css .= 'span.on_sale{left:'.(int)Configuration::get('STSN_SALE_STICKERS_LEFT').'px;}';
		if($sale_style==1 && Configuration::get('STSN_SALE_BG_IMG'))
			$css .= 'span.on_sale{background:url(../../'.Configuration::get('STSN_SALE_BG_IMG').') no-repeat center center transparent;}span.on_sale i{display:none;}';
             
        if(Configuration::get('STSN_PRICE_DROP_COLOR'))
    	    $css .= 'span.sale_percentage_sticker{color: '.Configuration::get('STSN_PRICE_DROP_COLOR').';}';
        if(Configuration::get('STSN_PRICE_DROP_BORDER_COLOR'))
    	    $css .= 'span.sale_percentage_sticker{border-color: '.Configuration::get('STSN_PRICE_DROP_BORDER_COLOR').';}';
        if(Configuration::get('STSN_PRICE_DROP_BG_COLOR'))
    	    $css .= 'span.sale_percentage_sticker{background-color: '.Configuration::get('STSN_PRICE_DROP_BG_COLOR').';}';
        if(Configuration::get('STSN_PRICE_DROP_BOTTOM')!==false)
    	    $css .= 'span.sale_percentage_sticker{bottom: '.(int)Configuration::get('STSN_PRICE_DROP_BOTTOM').'px;}';
        if(Configuration::get('STSN_PRICE_DROP_RIGHT')!==false)
    	    $css .= 'span.sale_percentage_sticker{right: '.(int)Configuration::get('STSN_PRICE_DROP_RIGHT').'px;}';
        $price_drop_width = (int)Configuration::get('STSN_PRICE_DROP_WIDTH');
        if($price_drop_width>28)
        {
            $price_drop_padding = round(($price_drop_width-28)/2,3);
    	    $css .= 'span.sale_percentage_sticker{width: '.$price_drop_width.'px;height: '.$price_drop_width.'px;padding:'.$price_drop_padding.'px 0;}';
        }

        if(Configuration::get('STSN_LOGO_POSITION') && Configuration::get('STSN_LOGO_HEIGHT'))
    	    $css .= '.logo_center #header_left,.logo_center #logo_wrapper,.logo_center #header_right{height: '.(int)Configuration::get('STSN_LOGO_HEIGHT').'px;}';   
        if($megamenu_position = Configuration::get('STSN_MEGAMENU_POSITION'))
    	    $css .= '#st_mega_menu{text-align: '.($megamenu_position==1 ? 'center' : 'right').';}.sttlevel0{float:none;display:inline-block;vertical-align:middle;}';   
            
        if(Configuration::get('STSN_CART_ICON'))
            $css .= '.icon-basket:before{ content: "\e83c"; }';
        if(Configuration::get('STSN_WISHLIST_ICON'))
            $css .= '.icon-heart:before{ content: "\e800"; }';
        if(Configuration::get('STSN_COMPARE_ICON'))
            $css .= '.icon-ajust:before{ content: "\e808"; }';
            
        if(Configuration::get('STSN_PRO_TAB_COLOR'))  
            $css .= '#more_info_tabs a{ color: '.Configuration::get('STSN_PRO_TAB_COLOR').'; }';
        if(Configuration::get('STSN_PRO_TAB_ACTIVE_COLOR'))  
            $css .= '#more_info_tabs a.selected,#more_info_tabs a:hover{ color: '.Configuration::get('STSN_PRO_TAB_ACTIVE_COLOR').'; }';
        if(Configuration::get('STSN_PRO_TAB_BG'))  
            $css .= '#more_info_tabs a{ background-color: '.Configuration::get('STSN_PRO_TAB_BG').'; }';
        if(Configuration::get('STSN_PRO_TAB_ACTIVE_BG'))  
            $css .= '#more_info_tabs a.selected{ background-color: '.Configuration::get('STSN_PRO_TAB_ACTIVE_BG').'; }';
        if(Configuration::get('STSN_PRO_TAB_CONTENT_BG'))  
            $css .= '#more_info_sheets{ background-color: '.Configuration::get('STSN_PRO_TAB_CONTENT_BG').'; }';
        
        if (Configuration::get('STSN_CUSTOM_CSS') != "")
			$css .= Configuration::get('STSN_CUSTOM_CSS');
        
        if (Shop::getContext() == Shop::CONTEXT_SHOP)
        {
            $cssFile = $this->local_path."views/css/customer-s".(int)$this->context->shop->getContextShopID().".css";
    		$write_fd = fopen($cssFile, 'w') or die('can\'t open file "'.$cssFile.'"');
    		fwrite($write_fd, $css);
    		fclose($write_fd);
        }
        if (Configuration::get('STSN_CUSTOM_JS') != "")
		{
		    $jsFile = $this->local_path."views/js/customer".$id_shop.".js";
    		$write_fd = fopen($jsFile, 'w') or die('can\'t open file "'.$jsFile.'"');
    		fwrite($write_fd, Configuration::get('STSN_CUSTOM_JS'));
    		fclose($write_fd);
		}
        else
            if(file_exists($this->local_path.'views/js/customer'.$id_shop.'.js'))
                unlink($this->local_path.'views/js/customer'.$id_shop.'.js');
    }
    
    public static function hex2rgb($hex) {
       $hex = str_replace("#", "", $hex);
    
       if(strlen($hex) == 3) {
          $r = hexdec(substr($hex,0,1).substr($hex,0,1));
          $g = hexdec(substr($hex,1,1).substr($hex,1,1));
          $b = hexdec(substr($hex,2,1).substr($hex,2,1));
       } else {
          $r = hexdec(substr($hex,0,2));
          $g = hexdec(substr($hex,2,2));
          $b = hexdec(substr($hex,4,2));
       }
       $rgb = array($r, $g, $b);
       return $rgb;
    }
    
	public function hookActionShopDataDuplication($params)
	{
	    $this->_useDefault(false,shop::getGroupFromShop($params['new_id_shop']),$params['new_id_shop']);
	}
    public function hookHeader($params)
	{
        $id_shop = (int)Shop::getContextShopID();
        $googleFontLinks = '';
	    $theme_font = array();
    	$theme_font[] = Configuration::get('STSN_FONT_TEXT');
        $theme_font[] = Configuration::get('STSN_FONT_HEADING');
        $theme_font[] = Configuration::get('STSN_FONT_PRICE');
        $theme_font[] = Configuration::get('STSN_FONT_MENU');
    	$theme_font[] = Configuration::get('STSN_FONT_CART_BTN');
    	$theme_font[] = Configuration::get('STSN_FONT_TITLE');
        
        $theme_font = array_unique($theme_font);
        $fonts = $this->systemFonts;
        $theme_font = array_diff($theme_font,$fonts);
        
        $font_latin_support = Configuration::get('STSN_FONT_LATIN_SUPPORT');
        $font_cyrillic_support = Configuration::get('STSN_FONT_CYRILLIC_SUPPORT');
        $font_vietnamese = Configuration::get('STSN_FONT_VIETNAMESE');
        $font_greek_support = Configuration::get('STSN_FONT_GREEK_SUPPORT');
        $font_support = ($font_latin_support || $font_cyrillic_support || $font_vietnamese || $font_greek_support) ? '&subset=' : '';
        $font_latin_support && $font_support .= 'latin,latin-ext,';
        $font_cyrillic_support && $font_support .= 'cyrillic,cyrillic-ext,';
        $font_vietnamese && $font_support .= 'vietnamese,';
        $font_greek_support && $font_support .= 'greek,greek-ext,';
        if(is_array($theme_font) && count($theme_font))
            foreach($theme_font as $v)
            {
                if(!$v)
                    continue;
    	        $googleFontLinks .="<link href='//fonts.googleapis.com/css?family=".str_replace(' ', '+', $v).($font_support ? rtrim($font_support,',') : '')."' rel='stylesheet' type='text/css'>";
            }
	    $footer_img_src = '';
	    if(Configuration::get('STSN_FOOTER_IMG') !='' )
	       $footer_img_src = (Configuration::get('STSN_FOOTER_IMG')==$this->defaults["footer_img"] ? _MODULE_DIR_.$this->name.'/' : _THEME_PROD_PIC_DIR_).Configuration::get('STSN_FOOTER_IMG');

        $mobile_detect = $this->context->getMobileDetect();
        $mobile_device = $mobile_detect->isMobile() || $mobile_detect->isTablet();
        
        $enabled_version_swithing = Configuration::get('STSN_VERSION_SWITCHING') && $mobile_device;
        $version_switching = isset($this->context->cookie->version_switching) ? (int)$this->context->cookie->version_switching : 0;
        
        $is_responsive = Configuration::get('STSN_RESPONSIVE');
	    $theme_settings = array(
            'boxstyle' => (int)Configuration::get('STSN_BOXSTYLE'),
            'footer_img_src' => $footer_img_src, 
            'copyright_text' => Configuration::get('STSN_COPYRIGHT_TEXT', $this->context->language->id),
            'search_label' => Configuration::get('STSN_SEARCH_LABEL', $this->context->language->id),
            'newsletter_label' => Configuration::get('STSN_NEWSLETTER_LABEL', $this->context->language->id),
            'icon_iphone_57' => Configuration::get('STSN_ICON_IPHONE_57') ? $this->_path.Configuration::get('STSN_ICON_IPHONE_57') : '',
            'icon_iphone_72' => Configuration::get('STSN_ICON_IPHONE_72') ? $this->_path.Configuration::get('STSN_ICON_IPHONE_72') : '',
            'icon_iphone_114' => Configuration::get('STSN_ICON_IPHONE_114') ? $this->_path.Configuration::get('STSN_ICON_IPHONE_114') : '',
            'icon_iphone_144' => Configuration::get('STSN_ICON_IPHONE_144') ? $this->_path.Configuration::get('STSN_ICON_IPHONE_144') : '',
            'google_font_links'  => $googleFontLinks,
            'show_cate_header' => Configuration::get('STSN_SHOW_CATE_HEADER'),
            'responsive' => $is_responsive,
            'enabled_version_swithing' => $enabled_version_swithing,
            'version_switching' => $version_switching,
            'responsive_max' => Configuration::get('STSN_RESPONSIVE_MAX'),
            'scroll_to_top' => Configuration::get('STSN_SCROLL_TO_TOP'),
            'addtocart_animation' => Configuration::get('STSN_ADDTOCART_ANIMATION'),
            'google_rich_snippets' => Configuration::get('STSN_GOOGLE_RICH_SNIPPETS'),
            'display_tax_label' => Configuration::get('STSN_DISPLAY_TAX_LABEL'),
            'discount_percentage' => Configuration::get('STSN_DISCOUNT_PERCENTAGE'),
            'flyout_buttons' => Configuration::get('STSN_FLYOUT_BUTTONS'),
            'length_of_product_name' => Configuration::get('STSN_LENGTH_OF_PRODUCT_NAME'),
            'logo_position' => Configuration::get('STSN_LOGO_POSITION'),
            'body_has_background' => (Configuration::get('STSN_BODY_BG_COLOR') || Configuration::get('STSN_BODY_BG_PATTERN') || Configuration::get('STSN_BODY_BG_IMG')),
            'tracking_code' =>  Configuration::get('STSN_TRACKING_CODE'),
            'display_cate_desc_full' => Configuration::get('STSN_DISPLAY_CATE_DESC_FULL'), 
            //'category_show_all_btn' => Configuration::get('STSN_CATEGORY_SHOW_ALL_BTN'), 
            'display_pro_tags' => Configuration::get('STSN_DISPLAY_PRO_TAGS'), 
            'zoom_type' => Configuration::get('STSN_ZOOM_TYPE'), 
            'sticky_menu' => Configuration::get('STSN_STICKY_MENU'), 
            'is_rtl' => $this->context->language->is_rtl, 
            'categories_per_lg' => Configuration::get('STSN_CATEGORIES_PER_LG_0'),
            'categories_per_md' => Configuration::get('STSN_CATEGORIES_PER_MD_0'),
            'categories_per_sm' => Configuration::get('STSN_CATEGORIES_PER_SM_0'),
            'categories_per_xs' => Configuration::get('STSN_CATEGORIES_PER_XS_0'),
            'categories_per_xxs' => Configuration::get('STSN_CATEGORIES_PER_XXS_0'),
            'product_big_image' => Configuration::get('STSN_PRODUCT_BIG_IMAGE'), 
            'breadcrumb_width' => Configuration::get('STSN_BREADCRUMB_WIDTH'), 
            'welcome' => ($this->context->customer->isLogged() ? Configuration::get('STSN_WELCOME_LOGGED', $this->context->language->id) : Configuration::get('STSN_WELCOME', $this->context->language->id)),
            'welcome_link' => Configuration::get('STSN_WELCOME_LINK', $this->context->language->id),
        );
        $this->context->controller->addJS($this->_path.'views/js/global.js');
        if(file_exists($this->local_path.'views/js/customer'.$id_shop.'.js'))
		  $theme_settings['custom_js'] = $this->_path.'views/js/customer'.$id_shop.'.js';
        
        if (Shop::getContext() == Shop::CONTEXT_SHOP)
        {
            if(!file_exists($this->local_path.'views/css/customer-s'.$this->context->shop->getContextShopID().'.css'))
                $this->writeCss();
            $theme_settings['custom_css'] = $this->_path.'views/css/customer-s'.$this->context->shop->getContextShopID().'.css';
        }
        $theme_settings['custom_css_media'] = 'all';

		if($is_responsive && (!$enabled_version_swithing || $version_switching==0))
        {
            $this->context->controller->addCSS(_THEME_CSS_DIR_.'responsive.css', 'all');
            if ($this->context->language->is_rtl)
                $this->context->controller->addCSS(_THEME_CSS_DIR_.'rtl-responsive.css', 'all');

            if(Configuration::get('STSN_RESPONSIVE_MAX'))
                $this->context->controller->addCSS(_THEME_CSS_DIR_.'responsive-md.css', 'all');
            else
                $this->context->controller->addCSS(_THEME_CSS_DIR_.'responsive-md-max.css', 'all');
        }else{
            $this->context->controller->addCSS(_THEME_CSS_DIR_.'responsiveness.css', 'all');
        }

        if($is_responsive && (!$enabled_version_swithing || $version_switching==0))
        {
            if(Configuration::get('STSN_RESPONSIVE_MAX'))
            {
                for($i=1;$i<=Configuration::get('STSN_RESPONSIVE_MAX');$i++)
                    $this->context->controller->addCSS(_THEME_CSS_DIR_.'responsive-'.$i.'.css', 'all');
            }
        }
        //
        $this->context->controller->addCSS($this->_path.'views/css/animate.min.css', 'all');
        $this->context->controller->addJqueryPlugin('hoverIntent');
        $this->context->controller->addJqueryPlugin('fancybox');
		$this->context->smarty->assign('sttheme', $theme_settings);

		return $this->display(__FILE__, 'stthemeeditor-header.tpl');
	}
    
    public function getProductRatingAverage($id_product)
    {
        if(Configuration::get('STSN_DISPLAY_COMMENT_RATING') && Module::isInstalled('productcomments') && Module::isEnabled('productcomments'))
        {
            include_once(_PS_MODULE_DIR_.'productcomments/ProductComment.php');
            $averageGrade = ProductComment::getAverageGrade($id_product);
            if(Configuration::get('STSN_DISPLAY_COMMENT_RATING')==1 && !$averageGrade['grade'])
                return ;
    	    $this->context->smarty->assign('ratingAverage',round($averageGrade['grade']));
            return $this->display(__FILE__, 'product_rating_average.tpl');
        }
        return false;
    }
    public function getProductAttributes($id_product)
    {
        if(!$show_pro_attr = Configuration::get('STSN_DISPLAY_PRO_ATTR'))
            return false;
        $product = new Product($id_product);
		if (!isset($product) || !Validate::isLoadedObject($product))
            return false;
		$groups = array();
		$attributes_groups = $product->getAttributesGroups($this->context->language->id);
        if (is_array($attributes_groups) && $attributes_groups)
		{
            foreach ($attributes_groups as $k => $row)
			{
			     if (!isset($groups[$row['id_attribute_group']]))
					$groups[$row['id_attribute_group']] = array(
						'name' => $row['public_group_name'],
						'group_type' => $row['group_type'],
						'default' => -1,
					);
                $groups[$row['id_attribute_group']]['attributes'][$row['id_attribute']] = $row['attribute_name'];
				if (!isset($groups[$row['id_attribute_group']]['attributes_quantity'][$row['id_attribute']]))
					$groups[$row['id_attribute_group']]['attributes_quantity'][$row['id_attribute']] = 0;
				$groups[$row['id_attribute_group']]['attributes_quantity'][$row['id_attribute']] += (int)$row['quantity'];
			}
            $this->context->smarty->assign(array(
				'groups' => $groups,
                'show_pro_attr' => $show_pro_attr,
            ));
            return $this->display(__FILE__, 'product_attributes.tpl');
        }
        return false;
    }
    public function getAddToWhishlistButton($id_product,$show_icon)
    {
        if(Module::isInstalled('blockwishlist') && Module::isEnabled('blockwishlist'))
        {
    	    $this->context->smarty->assign(array(
                'id_product' => $id_product,
                'show_icon' => $show_icon,
            ));
            return $this->display(__FILE__, 'product_add_to_wishlist.tpl');
        }
    }
    public function getManufacturerLink($id_manufacturer)
    {
	    if (!$this->isCached('manufacturer_link.tpl', $this->stGetCacheId($id_manufacturer,'manufacturer_link')))
        {
		  	$this->context->smarty->assign(array(
              'product_manufacturer' => new Manufacturer((int)$id_manufacturer, $this->context->language->id),
            ));
        }
         
        return $this->display(__FILE__, 'manufacturer_link.tpl',$this->stGetCacheId($id_manufacturer,'manufacturer_link'));
    }
    public function getCarouselJavascript($identify)
    {
	    if (!$this->isCached('carousel_javascript.tpl', $this->stGetCacheId($identify)))
        {
            if($identify=='crossselling')
                $pre = 'STSN_CS';
            else if($identify=='accessories')
                $pre = 'STSN_AC';
            else if($identify=='productscategory')
                $pre = 'STSN_PC';
            if(!isset($pre))
                return false;
            $this->context->smarty->assign(array(
                'identify' => $identify,
                'easing' => self::$easing[Configuration::get($pre.'_EASING')]['name'],
                'slideshow' => Configuration::get($pre.'_SLIDESHOW'),
                's_speed' => Configuration::get($pre.'_S_SPEED'),
                'a_speed' => Configuration::get($pre.'_A_SPEED'),
                'pause_on_hover' => Configuration::get($pre.'_PAUSE_ON_HOVER'),
                'loop' => Configuration::get($pre.'_LOOP'),
                'move' => Configuration::get($pre.'_MOVE'),
                'pro_per_lg'       => (int)Configuration::get($pre.'_PER_LG_0'),
                'pro_per_md'       => (int)Configuration::get($pre.'_PER_MD_0'),
                'pro_per_sm'       => (int)Configuration::get($pre.'_PER_SM_0'),
                'pro_per_xs'       => (int)Configuration::get($pre.'_PER_XS_0'),
                'pro_per_xxs'       => (int)Configuration::get($pre.'_PER_XXS_0'),
            ));
        }
        return $this->display(__FILE__, 'carousel_javascript.tpl',$this->stGetCacheId($identify));
    }
    
	protected function stGetCacheId($key,$name = null)
	{
		$cache_id = parent::getCacheId($name);
		return $cache_id.'_'.$key;
	}
    
    public function hookDisplayAnywhere($params)
    {
	    if(!isset($params['caller']) || $params['caller']!=$this->name)
            return false;
        if(isset($params['function']) && method_exists($this,$params['function']))
        {
            if($params['function']=='getProductRatingAverage')
                return call_user_func_array(array($this,$params['function']),array($params['id_product']));
            elseif($params['function']=='getAddToWhishlistButton')
                return call_user_func_array(array($this,$params['function']),array($params['id_product'],$params['show_icon']));
            elseif($params['function']=='getCarouselJavascript')
                return call_user_func_array(array($this,$params['function']),array($params['identify']));
            elseif($params['function']=='getProductAttributes')
                return call_user_func_array(array($this,$params['function']),array($params['id_product']));
            elseif($params['function']=='getManufacturerLink')
                return call_user_func_array(array($this,$params['function']),array($params['id_manufacturer']));
            elseif($params['function']=='getFlyoutButtonsClass')
                return call_user_func(array($this,$params['function']));
            elseif($params['function']=='getProductNameClass')
                return call_user_func(array($this,$params['function']));
            elseif($params['function']=='getSaleStyleFlag')
                return call_user_func_array(array($this,$params['function']),array($params['percentage_amount'],$params['reduction'],$params['price_without_reduction'],$params['price']));
            elseif($params['function']=='getSaleStyleCircle')
                return call_user_func_array(array($this,$params['function']),array($params['percentage_amount'],$params['reduction'],$params['price_without_reduction'],$params['price']));
            elseif($params['function']=='getLengthOfProductName')
                return call_user_func_array(array($this,$params['function']),array($params['product_name']));
            elseif($params['function']=='getProductsPerRow')
                return call_user_func_array(array($this,$params['function']),array($params['for_w'], $params['devices']));
            elseif($params['function']=='setColumnsNbr')
                return call_user_func_array(array($this,$params['function']),array($params['columns_nbr'], $params['page_name']));
            elseif($params['function']=='getShortDescOnGrid')
                return call_user_func(array($this,$params['function']));
            elseif($params['function']=='getDisplayColorList')
                return call_user_func(array($this,$params['function']));
            elseif($params['function']=='getCategoryDefaultView')
                return call_user_func(array($this,$params['function']));
            else
                return false;
        }
        return false;
    }
    public function hookDisplayRightColumnProduct($params)
    {        
	    if(!Module::isInstalled('blockviewed') || !Module::isEnabled('blockviewed'))
            return false;
            
		$id_product = (int)Tools::getValue('id_product');
        if(!$id_product)
            return false;
            
		$productsViewed = (isset($params['cookie']->viewed) && !empty($params['cookie']->viewed)) ? array_slice(array_reverse(explode(',', $params['cookie']->viewed)), 0, Configuration::get('PRODUCTS_VIEWED_NBR')) : array();

		if ($id_product && !in_array($id_product, $productsViewed))
		{
			if(isset($params['cookie']->viewed) && !empty($params['cookie']->viewed))
		  		$params['cookie']->viewed .= ',' . (int)$id_product;
			else
		  		$params['cookie']->viewed = (int)$id_product;
		}
        return false;
    }
    public function getFlyoutButtonsClass()
    {
        return Configuration::get('STSN_FLYOUT_BUTTONS') ? ' hover_fly_static ' : '';
    }
    
    public function getProductNameClass()
    {
        return Configuration::get('STSN_LENGTH_OF_PRODUCT_NAME') ? ' nohidden ' : '';
    }
    
    public function getSaleStyleFlag($percentage_amount,$reduction,$price_without_reduction,$price)
    {
        if(Configuration::get('STSN_DISCOUNT_PERCENTAGE')!=1)
            return false;
        $this->context->smarty->assign(array(
            'percentage_amount'  => $percentage_amount,
            'reduction'  => $reduction,
            'price_without_reduction'  => $price_without_reduction,
			'price' => $price,
        ));    
		return $this->display(__FILE__, 'sale_style_flag.tpl');
    }
    public function getSaleStyleCircle($percentage_amount,$reduction,$price_without_reduction,$price)
    {
        if(Configuration::get('STSN_DISCOUNT_PERCENTAGE')!=2)
            return false;
        $this->context->smarty->assign(array(
            'percentage_amount'  => $percentage_amount,
            'reduction'  => $reduction,
            'price_without_reduction'  => $price_without_reduction,
			'price' => $price,
        ));    
		return $this->display(__FILE__, 'sale_style_circle.tpl');
    }
    public function getLengthOfProductName($product_name)
    {
        $length_of_product_name = Configuration::get('STSN_LENGTH_OF_PRODUCT_NAME');
        $this->context->smarty->assign(array(
            'product_name_full' => $length_of_product_name==2,
            'length_of_product_name'  => ($length_of_product_name==1 ? 70 : 35),
			'product_name' => $product_name,
        ));    
		return $this->display(__FILE__, 'lenght_of_product_name.tpl');
    }
    public function initTab()
    {
        $html = '<div class="sidebar col-xs-12 col-lg-2"><ul class="nav nav-tabs">';
        foreach(self::$tabs AS $tab)
            $html .= '<li class="nav-item"><a href="javascript:;" title="'.$this->l($tab['name']).'" data-fieldset="'.$tab['id'].'">'.$this->l($tab['name']).'</a></li>';
        $html .= '</ul></div>';
        return $html;
    }
    public function initToolbarBtn()
    {
        $token = Tools::getAdminTokenLite('AdminModules');
        $toolbar_btn = array(
            'delete' => array(
                'desc' => $this->l('Reset Change'),
                'class' => 'icon-plus-sign',
                'js' => 'if (confirm(\''.$this->l('Reset all options, are you sure?').'\')){return true;}else{event.preventDefault();}',
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&reset'.$this->name.'&token='.$token,
            ),
            'green' => array(
                'desc' => $this->l('Import Green'),
                'class' => 'icon-plus-sign',
                'js' => 'if (confirm(\''.$this->l('Importing Green color scheme, are your sure?').'\')){return true;}else{event.preventDefault();}',
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&predefinedcolor'.$this->name.'=green&token='.$token,
            ),
            'gray' => array(
                'desc' => $this->l('Import Gray'),
                'class' => 'icon-plus-sign',
                'js' => 'if (confirm(\''.$this->l('Importing Gray color scheme, are your sure?').'\')){return true;}else{event.preventDefault();}',
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&predefinedcolor'.$this->name.'=gray&token='.$token,
            ),
            'red' => array(
                'desc' => $this->l('Import Red'),
                'class' => 'icon-plus-sign',
                'js' => 'if (confirm(\''.$this->l('Importing Red color scheme, are your sure?').'\')){return true;}else{event.preventDefault();}',
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&predefinedcolor'.$this->name.'=red&token='.$token,
            ),
            'blue' => array(
                'desc' => $this->l('Import Blue'),
                'class' => 'icon-plus-sign',
                'js' => 'if (confirm(\''.$this->l('Importing Blue color scheme, are your sure?').'\')){return true;}else{event.preventDefault();}',
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&predefinedcolor'.$this->name.'=blue&token='.$token,
            ),
            'brown' => array(
                'desc' => $this->l('Import Brown'),
                'class' => 'icon-plus-sign',
                'js' => 'if (confirm(\''.$this->l('Importing Brown color scheme, are your sure?').'\')){return true;}else{event.preventDefault();}',
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&predefinedcolor'.$this->name.'=brown&token='.$token,
            ),
            'yellow' => array(
                'desc' => $this->l('Import Yellow'),
                'class' => 'icon-plus-sign',
                'js' => 'if (confirm(\''.$this->l('Importing Yellow color scheme, are your sure?').'\')){return true;}else{event.preventDefault();}',
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&predefinedcolor'.$this->name.'=yellow&token='.$token,
            ),
        );
        $html = '<div class="panel st_toolbtn clearfix">';
        foreach($toolbar_btn AS $k => $btn)
        {
            $html .= '
            <a id="desc-configuration-'.$k.'" class="boolbtn-'.$k.' btn btn-default" onclick="'.$btn['js'].'" href="'.$btn['href'].'" title="'.$btn['desc'].'">
            <span>
            <i class="'.$btn['class'].'"></i>'.$btn['desc'].'</span></a>';
        }
        $html .= '</div>';
        return $html;
    }
    private function getConfigFieldsValues()
    {
        $fields_values = array();
        foreach($this->defaults as $k=>$v)
            $fields_values[$k] = Configuration::get('STSN_'.strtoupper($k));
        
        $languages = Language::getLanguages(false);    
		foreach ($languages as $language)
        {
            $fields_values['welcome'][$language['id_lang']] = Configuration::get('STSN_WELCOME', $language['id_lang']);
            $fields_values['welcome_logged'][$language['id_lang']] = Configuration::get('STSN_WELCOME_LOGGED', $language['id_lang']);
            $fields_values['welcome_link'][$language['id_lang']] = Configuration::get('STSN_WELCOME_LINK', $language['id_lang']);
            $fields_values['copyright_text'][$language['id_lang']] = Configuration::get('STSN_COPYRIGHT_TEXT', $language['id_lang']);
            $fields_values['search_label'][$language['id_lang']] = Configuration::get('STSN_SEARCH_LABEL', $language['id_lang']);
            $fields_values['newsletter_label'][$language['id_lang']] = Configuration::get('STSN_NEWSLETTER_LABEL', $language['id_lang']);
        }
        
        foreach ($this->getConfigurableModules() as $module)
			$fields_values[$module['name']] = $module['value'];
        
        return $fields_values;
    }

    public function getShortDescOnGrid()
    {  
        return Configuration::get('STSN_SHOW_SHORT_DESC_ON_GRID') ? 'display_sd' : '';
    }
    public function getDisplayColorList()
    {
        return Configuration::get('STSN_DISPLAY_COLOR_LIST') ? '' : 'hidden';
    }
    public function getCategoryDefaultView()
    {
        return Configuration::get('STSN_PRODUCT_VIEW')=='list_view' ? 'list' : 'grid';
    }
    public function getProductsPerRow($for_w, $devices)
    {
        switch ($for_w) {
            case 'category':
            case 'prices-drop':
            case 'best-sales':
            case 'manufacturer':
            case 'supplier':
            case 'new-products':
            case 'search':
                $columns_nbr = $this->context->cookie->st_category_columns_nbr;
                $nbr = Configuration::get('STSN_CATEGORY_PRO_PER_'.strtoupper($devices).'_'.$columns_nbr);
                break;  
            case 'hometab':
                $nbr = Configuration::get('STSN_'.strtoupper($for_w).'_PRO_PER_'.strtoupper($devices).'_0');
                break;           
            case 'packitems':
                $nbr = Configuration::get('STSN_'.strtoupper($for_w).'_PRO_PER_'.strtoupper($devices).'_0');
                break;       
            case 'homenew':
                $nbr = Configuration::get('STSN_'.strtoupper($for_w).'_PRO_PER_'.strtoupper($devices).'_0');
                break;  
            case 'featured':
                $nbr = Configuration::get('STSN_'.strtoupper($for_w).'_PRO_PER_'.strtoupper($devices).'_0');
                break;  
            case 'special':
                $nbr = Configuration::get('STSN_'.strtoupper($for_w).'_PRO_PER_'.strtoupper($devices).'_0');
                break;  
            case 'pro_cate':
                $nbr = Configuration::get('STSN_'.strtoupper($for_w).'_PRO_PER_'.strtoupper($devices).'_0');
                break; 
            case 'sellers':
                $nbr = Configuration::get('STSN_'.strtoupper($for_w).'_PRO_PER_'.strtoupper($devices).'_0');
                break;        
            default:
                $nbr = 3;
                break;
        }
        return $nbr ? $nbr : 3;
    }
    public function setColumnsNbr($columns_nbr, $page_name)
    {
        $this->context->cookie->st_category_columns_nbr = (int)$columns_nbr;
    }
    public function BuildDropListGroup($group)
    {
        if(!is_array($group) || !count($group))
            return false;

        $html = '<div class="row">';
        foreach($group AS $key => $k)
        {
             if($key==3)
                 $html .= '</div><div class="row">';

             $html .= '<div class="col-xs-4 col-sm-3"><label '.(isset($k['tooltip']) ? ' data-html="true" data-toggle="tooltip" class="label-tooltip" data-original-title="'.$k['tooltip'].'" ':'').'>'.$k['label'].'</label>'.
             '<select name="'.$k['id'].'" 
             id="'.$k['id'].'" 
             class="'.(isset($k['class']) ? $k['class'] : 'fixed-width-md').'"'.
             (isset($k['onchange']) ? ' onchange="'.$k['onchange'].'"':'').' >';
            
            for ($i=1; $i < 7; $i++){
                $html .= '<option value="'.$i.'" '.(Configuration::get('STSN_'.strtoupper($k['id'])) == $i ? ' selected="selected"':'').'>'.$i.'</option>';
            }
                                
            $html .= '</select></div>';
        }
        return $html.'</div>';
    }
    public function findCateProPer($k=null)
    {
        $proper = array(
            1 => array(
                array(
                    'id' => 'category_pro_per_lg_1',
                    'label' => $this->l('Large devices'),
                    'tooltip' => $this->l('Desktops (>1200px)'),
                ),
                array(
                    'id' => 'category_pro_per_md_1',
                    'label' => $this->l('Medium devices'),
                    'tooltip' => $this->l('Desktops (>992px)'),
                ),
                array(
                    'id' => 'category_pro_per_sm_1',
                    'label' => $this->l('Small devices'),
                    'tooltip' => $this->l('Tablets (>768px)'),
                ),
                array(
                    'id' => 'category_pro_per_xs_1',
                    'label' => $this->l('Extra small devices'),
                    'tooltip' => $this->l('Phones (>480px)'),
                ),
                array(
                    'id' => 'category_pro_per_xxs_1',
                    'label' => $this->l('Extra extra small devices'),
                    'tooltip' => $this->l('Phones (<480px)'),
                ),
            ),
            2 => array(
                array(
                    'id' => 'category_pro_per_lg_2',
                    'label' => $this->l('Large devices'),
                    'tooltip' => $this->l('Desktops (>1200px)'),
                ),
                array(
                    'id' => 'category_pro_per_md_2',
                    'label' => $this->l('Medium devices'),
                    'tooltip' => $this->l('Desktops (>992px)'),
                ),
                array(
                    'id' => 'category_pro_per_sm_2',
                    'label' => $this->l('Small devices'),
                    'tooltip' => $this->l('Tablets (>768px)'),
                ),
                array(
                    'id' => 'category_pro_per_xs_2',
                    'label' => $this->l('Extra small devices'),
                    'tooltip' => $this->l('Phones (>480px)'),
                ),
                array(
                    'id' => 'category_pro_per_xxs_2',
                    'label' => $this->l('Extra extra small devices'),
                    'tooltip' => $this->l('Phones (<480px)'),
                ),
            ),
            3 => array(
                array(
                    'id' => 'category_pro_per_lg_3',
                    'label' => $this->l('Large devices'),
                    'tooltip' => $this->l('Desktops (>1200px)'),
                ),
                array(
                    'id' => 'category_pro_per_md_3',
                    'label' => $this->l('Medium devices'),
                    'tooltip' => $this->l('Desktops (>992px)'),
                ),
                array(
                    'id' => 'category_pro_per_sm_3',
                    'label' => $this->l('Small devices'),
                    'tooltip' => $this->l('Tablets (>768px)'),
                ),
                array(
                    'id' => 'category_pro_per_xs_3',
                    'label' => $this->l('Extra small devices'),
                    'tooltip' => $this->l('Phones (>480px)'),
                ),
                array(
                    'id' => 'category_pro_per_xxs_3',
                    'label' => $this->l('Extra extra small devices'),
                    'tooltip' => $this->l('Phones (<480px)'),
                ),
            ),
            4 => array(
                array(
                    'id' => 'hometab_pro_per_lg_0',
                    'label' => $this->l('Large devices'),
                    'tooltip' => $this->l('Desktops (>1200px)'),
                ),
                array(
                    'id' => 'hometab_pro_per_md_0',
                    'label' => $this->l('Medium devices'),
                    'tooltip' => $this->l('Desktops (>992px)'),
                ),
                array(
                    'id' => 'hometab_pro_per_sm_0',
                    'label' => $this->l('Small devices'),
                    'tooltip' => $this->l('Tablets (>768px)'),
                ),
                array(
                    'id' => 'hometab_pro_per_xs_0',
                    'label' => $this->l('Extra small devices'),
                    'tooltip' => $this->l('Phones (>480px)'),
                ),
                array(
                    'id' => 'hometab_pro_per_xxs_0',
                    'label' => $this->l('Extra extra small devices'),
                    'tooltip' => $this->l('Phones (<480px)'),
                ),
            ),
            5 => array(
                array(
                    'id' => 'packitems_pro_per_lg_0',
                    'label' => $this->l('Large devices'),
                    'tooltip' => $this->l('Desktops (>1200px)'),
                ),
                array(
                    'id' => 'packitems_pro_per_md_0',
                    'label' => $this->l('Medium devices'),
                    'tooltip' => $this->l('Desktops (>992px)'),
                ),
                array(
                    'id' => 'packitems_pro_per_sm_0',
                    'label' => $this->l('Small devices'),
                    'tooltip' => $this->l('Tablets (>768px)'),
                ),
                array(
                    'id' => 'packitems_pro_per_xs_0',
                    'label' => $this->l('Extra small devices'),
                    'tooltip' => $this->l('Phones (<768px)'),
                ),
                array(
                    'id' => 'packitems_pro_per_xxs_0',
                    'label' => $this->l('Extra extra small devices'),
                    'tooltip' => $this->l('Phones (>480px)'),
                ),
            ),
            6 => array(
                array(
                    'id' => 'categories_per_lg_0',
                    'label' => $this->l('Large devices'),
                    'tooltip' => $this->l('Desktops (>1200px)'),
                ),
                array(
                    'id' => 'categories_per_md_0',
                    'label' => $this->l('Medium devices'),
                    'tooltip' => $this->l('Desktops (>992px)'),
                ),
                array(
                    'id' => 'categories_per_sm_0',
                    'label' => $this->l('Small devices'),
                    'tooltip' => $this->l('Tablets (>768px)'),
                ),
                array(
                    'id' => 'categories_per_xs_0',
                    'label' => $this->l('Extra small devices'),
                    'tooltip' => $this->l('Phones (<768px)'),
                ),
                array(
                    'id' => 'categories_per_xxs_0',
                    'label' => $this->l('Extra extra small devices'),
                    'tooltip' => $this->l('Phones (>480px)'),
                ),
            ),
            7 => array(
                array(
                    'id' => 'cs_per_lg_0',
                    'label' => $this->l('Large devices'),
                    'tooltip' => $this->l('Desktops (>1200px)'),
                ),
                array(
                    'id' => 'cs_per_md_0',
                    'label' => $this->l('Medium devices'),
                    'tooltip' => $this->l('Desktops (>992px)'),
                ),
                array(
                    'id' => 'cs_per_sm_0',
                    'label' => $this->l('Small devices'),
                    'tooltip' => $this->l('Tablets (>768px)'),
                ),
                array(
                    'id' => 'cs_per_xs_0',
                    'label' => $this->l('Extra small devices'),
                    'tooltip' => $this->l('Phones (<768px)'),
                ),
                array(
                    'id' => 'cs_per_xxs_0',
                    'label' => $this->l('Extra extra small devices'),
                    'tooltip' => $this->l('Phones (>480px)'),
                ),
            ),
            8 => array(
                array(
                    'id' => 'pc_per_lg_0',
                    'label' => $this->l('Large devices'),
                    'tooltip' => $this->l('Desktops (>1200px)'),
                ),
                array(
                    'id' => 'pc_per_md_0',
                    'label' => $this->l('Medium devices'),
                    'tooltip' => $this->l('Desktops (>992px)'),
                ),
                array(
                    'id' => 'pc_per_sm_0',
                    'label' => $this->l('Small devices'),
                    'tooltip' => $this->l('Tablets (>768px)'),
                ),
                array(
                    'id' => 'pc_per_xs_0',
                    'label' => $this->l('Extra small devices'),
                    'tooltip' => $this->l('Phones (<768px)'),
                ),
                array(
                    'id' => 'pc_per_xxs_0',
                    'label' => $this->l('Extra extra small devices'),
                    'tooltip' => $this->l('Phones (>480px)'),
                ),
            ),
            9 => array(
                array(
                    'id' => 'ac_per_lg_0',
                    'label' => $this->l('Large devices'),
                    'tooltip' => $this->l('Desktops (>1200px)'),
                ),
                array(
                    'id' => 'ac_per_md_0',
                    'label' => $this->l('Medium devices'),
                    'tooltip' => $this->l('Desktops (>992px)'),
                ),
                array(
                    'id' => 'ac_per_sm_0',
                    'label' => $this->l('Small devices'),
                    'tooltip' => $this->l('Tablets (>768px)'),
                ),
                array(
                    'id' => 'ac_per_xs_0',
                    'label' => $this->l('Extra small devices'),
                    'tooltip' => $this->l('Phones (<768px)'),
                ),
                array(
                    'id' => 'ac_per_xxs_0',
                    'label' => $this->l('Extra extra small devices'),
                    'tooltip' => $this->l('Phones (>480px)'),
                ),
            ),
        );
        return ($k!==null && isset($proper[$k])) ? $proper[$k] : $proper;
    }
    
    protected function updateConfigurableModules()
    {
        foreach ($this->getConfigurableModules() as $module)
		{
			if (!isset($module['is_module']) || !$module['is_module'] || !Validate::isModuleName($module['name']) || !Tools::isSubmit($module['name']))
				continue;

			$module_instance = Module::getInstanceByName($module['name']);
			if ($module_instance === false || !is_object($module_instance))
				continue;

			$is_installed = (int)Validate::isLoadedObject($module_instance);
			if ($is_installed)
			{
				if (($active = (int)Tools::getValue($module['name'])) == $module_instance->active)
					continue;

				if ($active)
					$module_instance->enable();
				else
					$module_instance->disable();
			}
			else
				if ((int)Tools::getValue($module['name']))
					$module_instance->install();
		}
        Configuration::updateValue('PS_QUICK_VIEW', (int)Tools::getValue('quick_view'));
    }
    
    protected function getConfigurableModules()
	{
		return array(
            array(
                'label' => $this->l('Hover image'),
                'name' => 'sthoverimage',
                'value' => (int)Validate::isLoadedObject($module = Module::getInstanceByName('sthoverimage')) && $module->isEnabledForShopContext(),
                'is_module' => true,
                'desc' => $this->l('Dispaly second product image on mouse hover.') 
            ),
			array(
				'label' => $this->l('Add this button'),
				'name' => 'staddthisbutton',
				'value' => (int)Validate::isLoadedObject($module = Module::getInstanceByName('staddthisbutton')) && $module->isEnabledForShopContext(),
				'is_module' => true,
                'desc' => $this->l('Display add this button on product page, article page.')
			),
            array(
                'label' => $this->l('Enable quick view'),
                'name' => 'quick_view',
                'value' => (int)Tools::getValue('PS_QUICK_VIEW', Configuration::get('PS_QUICK_VIEW'))
            ),
            array(
                'label' => $this->l('Products Comparison'),
                'name' => 'stcompare',
                'value' => (int)Validate::isLoadedObject($module = Module::getInstanceByName('stcompare')) && $module->isEnabledForShopContext(),
                'is_module' => true,
                'desc' => $this->l('Display products comparison button on right bar')
            ),
			array(
				'label' => $this->l('Facebook Like Box'),
				'name' => 'stfblikebox',
				'value' => (int)Validate::isLoadedObject($module = Module::getInstanceByName('stfblikebox')) && $module->isEnabledForShopContext(),
				'is_module' => true,
                'desc' => $this->l('Dispaly facebook like box on page footer') 
			),
            array(
                'label' => $this->l('Twitter Embedded Timelines'),
                'name' => 'sttwitterembeddedtimelines',
                'value' => (int)Validate::isLoadedObject($module = Module::getInstanceByName('sttwitterembeddedtimelines')) && $module->isEnabledForShopContext(),
                'is_module' => true,
                'desc' => $this->l('Enable twitter embedded timelines')
            ),
			array(
				'label' => $this->l('Right bar cart block'),
				'name' => 'strightbarcart',
				'value' => (int)Validate::isLoadedObject($module = Module::getInstanceByName('strightbarcart')) && $module->isEnabledForShopContext(),
				'is_module' => true,
                'desc' => $this->l('Display cart button on page right bar.')
			),
			array(
				'label' => $this->l('Social networking block'),
				'name' => 'stsocial',
				'value' => (int)Validate::isLoadedObject($module = Module::getInstanceByName('stsocial')) && $module->isEnabledForShopContext(),
				'is_module' => true,
                'desc' => 'Display links to your store\'s social accounts (Twitter, Facebook, etc.)'
			),
            array(
				'label' => $this->l('Display social sharing buttons on the products page'),
				'name' => 'socialsharing',
				'value' => (int)Validate::isLoadedObject($module = Module::getInstanceByName('socialsharing')) && $module->isEnabledForShopContext(),
				'is_module' => true,
			),
			array(
				'label' => $this->l('Enable top banner'),
				'name' => 'blockbanner',
				'value' => (int)Validate::isLoadedObject($module = Module::getInstanceByName('blockbanner')) && $module->isEnabledForShopContext(),
				'is_module' => true,
			),
			array(
				'label' => $this->l('Display your product payment logos'),
				'name' => 'productpaymentlogos',
				'value' => (int)Validate::isLoadedObject($module = Module::getInstanceByName('productpaymentlogos')) && $module->isEnabledForShopContext(),
				'is_module' => true,
			),
            array(
                'label' => $this->l('Next and previous links on product'),
                'name' => 'stproductlinknav',
                'value' => (int)Validate::isLoadedObject($module = Module::getInstanceByName('stproductlinknav')) && $module->isEnabledForShopContext(),
                'is_module' => true,
                'desc' => $this->l('Dispaly next and previous links on product page') 
            ),
            array(
                'label' => $this->l('Next and previous links on blog'),
                'name' => 'stbloglinknav',
                'value' => (int)Validate::isLoadedObject($module = Module::getInstanceByName('stbloglinknav')) && $module->isEnabledForShopContext(),
                'is_module' => true,
                'desc' => $this->l('Dispaly next and previous links on blog article page') 
            ),
		);
	}
    
    public function getImageHtml($src, $id)
    {
        $html = '';
        if ($src && $id)
            $html .= '
			<img src="'.$src.'" class="img_preview">
            <p>
                <a id="'.$id.'" href="javascript:;" class="btn btn-default st_delete_image"><i class="icon-trash"></i> Delete</a>
			</p>
            ';
        return $html;    
    }
}
