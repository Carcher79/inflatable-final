<?php
class FrontController extends FrontControllerCore
{
    public function initContent()
    {
        parent::initContent();
            
        $this->context->smarty->assign(array(
            'HOOK_TOP_LEFT' => Hook::exec('displayTopLeft'),
            'HOOK_TOP_SECONDARY' => Hook::exec('displayTopSecondary'),
            'HOOK_FOOTER_TOP' => Hook::exec('displayFooterTop'),
            'HOOK_FOOTER_SECONDARY' => Hook::exec('displayFooterSecondary'),
            'isIntalledBlockWishlist'=> Module::isInstalled('blockwishlist'),
        ));
        
        $this->addJqueryPlugin('hoverIntent');
    }
}