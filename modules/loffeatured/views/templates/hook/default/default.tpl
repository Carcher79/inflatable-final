<div class="clearfix clear clr"></div>
<div class="lof-featured {$config_values.module_theme}">
	<section class="featured-widget">
		<header>
			<h3 class="featured-title">{l s='Featured Products' mod='loffeatured'}</h3>
		</header>
		<div class="list-featured responsive">
			<ul id="loffeatured-{$moduleId}" class="featured-news clearfix">
				{foreach from=$listFeature item=item}    
				<li>
					<article>
						<div class="featured-item box-hover clearfix">
							<div class="entry-content">
								<div class="video-thumb">
									<a href="{$item.link}" title="{$item.name}" class="product_image" target="{$target}">
									<img class="responsive-img" src="{$item.mainImge}" alt="{$item.name}"/>
									</a>
								</div>
								{if $config_values.show_title eq '1'}
								<h4 class="entry-title">
									<a href="{$item.link}" target="{$target}" title="{$item.name}">{$item.name}</a>
								</h4>
								{/if}
								{if !$PS_CATALOG_MODE && is_array($item.specific_prices) AND ($config_values.price_special  eq '1') AND !isset($restricted_country_mode) && $item.show_price}
								<div class="entry-price entry-price-discount"><strike>{displayWtPrice p=$item.price_without_reduction}</strike></div>
								{/if}
								{if !$PS_CATALOG_MODE && $config_values.show_price eq '1' AND !isset($restricted_country_mode) && $item.show_price}<div class="entry-price">{$item.price}</div>{/if}
								{if $config_values.show_desc eq '1'}<p>{$item.description}</p>{/if}

                                                                {if ($item.id_product_attribute == 0 || (isset($add_prod_display) && ($add_prod_display == 1))) && $item.available_for_order && !isset($restricted_country_mode) && $item.minimal_quantity <= 1 && $item.customizable != 2 && !$PS_CATALOG_MODE}
                                                                    {if ($item.allow_oosp || $item.quantity > 0)}
                                                                            {if isset($static_token)}
                                                                                    <a class="button ajax_add_to_cart_button btn btn-default" href="{$link->getPageLink('cart',false, NULL, "add=1&amp;id_product={$item.id_product|intval}&amp;token={$static_token}", false)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Add to cart'}" data-id-product="{$item.id_product|intval}">
                                                                                            <span>{l s='Add to cart'}</span>
                                                                                    </a>
                                                                            {else}
                                                                                    <a class="button ajax_add_to_cart_button btn btn-default" href="{$link->getPageLink('cart',false, NULL, 'add=1&amp;id_product={$item.id_product|intval}', false)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Add to cart'}" data-id-product="{$item.id_product|intval}">
                                                                                            <span>{l s='Add to cart'}</span>
                                                                                    </a>
                                                                            {/if}						
                                                                    {else}
                                                                            <span class="button ajax_add_to_cart_button btn btn-default disabled">
                                                                                    <span>{l s='Add to cart'}</span>
                                                                            </span>
                                                                    {/if}
                                                            {/if}
							</div>
						</div>
					</article>				
				</li>
				{/foreach}
			</ul>
			{if $config_values.show_button eq '1'}
			<div class="featured-nav">
				<a id="loffprev-{$moduleId}" class="prev" href="#">&nbsp;</a>
				<a id="loffnext-{$moduleId}" class="next" href="#">&nbsp;</a>
			</div>
			{/if}
			{if $config_values.show_pager eq '1'}<div id="loffpager-{$moduleId}" class="lof-pager"></div>{/if}
		</div>
	</section>
</div>
 
  <script type="text/javascript">
// <![CDATA[
			$('#loffeatured-{$moduleId}').carouFredSel({ldelim}
				responsive:true,
				prev: '#loffprev-{$moduleId}',
				next: '#loffnext-{$moduleId}',
				pagination: "#loffpager-{$moduleId}",
				auto: {$config_values.auto_play},
				width: '{$config_values.slide_width}',
				height: '{$config_values.slide_height}',
				scroll: {$config_values.scroll_items},
				items:{ldelim}
					width:200,
					visible:{ldelim}
						min:1,
						max:{$config_values.limit_cols}
					{rdelim}
				{rdelim}
			{rdelim});	
		 
// ]]>

</script>
