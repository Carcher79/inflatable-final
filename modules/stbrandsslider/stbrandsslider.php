<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

include_once(dirname(__FILE__).'/StBrandsSliderClass.php');
class StBrandsSlider extends Module
{
    private $_html = '';
    public $fields_form;
    public $fields_value;
    public $validation_errors = array();
    public static $easing = array(
		array('id' => 0, 'name' => 'swing'),
		array('id' => 1, 'name' => 'easeInQuad'),
		array('id' => 2, 'name' => 'easeOutQuad'),
		array('id' => 3, 'name' => 'easeInOutQuad'),
		array('id' => 4, 'name' => 'easeInCubic'),
		array('id' => 5, 'name' => 'easeOutCubic'),
		array('id' => 6, 'name' => 'easeInOutCubic'),
		array('id' => 7, 'name' => 'easeInQuart'),
		array('id' => 8, 'name' => 'easeOutQuart'),
		array('id' => 9, 'name' => 'easeInOutQuart'),
		array('id' => 10, 'name' => 'easeInQuint'),
		array('id' => 11, 'name' => 'easeOutQuint'),
		array('id' => 12, 'name' => 'easeInOutQuint'),
		array('id' => 13, 'name' => 'easeInSine'),
		array('id' => 14, 'name' => 'easeOutSine'),
		array('id' => 15, 'name' => 'easeInOutSine'),
		array('id' => 16, 'name' => 'easeInExpo'),
		array('id' => 17, 'name' => 'easeOutExpo'),
		array('id' => 18, 'name' => 'easeInOutExpo'),
		array('id' => 19, 'name' => 'easeInCirc'),
		array('id' => 20, 'name' => 'easeOutCirc'),
		array('id' => 21, 'name' => 'easeInOutCirc'),
		array('id' => 22, 'name' => 'easeInElastic'),
		array('id' => 23, 'name' => 'easeOutElastic'),
		array('id' => 24, 'name' => 'easeInOutElastic'),
		array('id' => 25, 'name' => 'easeInBack'),
		array('id' => 26, 'name' => 'easeOutBack'),
		array('id' => 27, 'name' => 'easeInOutBack'),
		array('id' => 28, 'name' => 'easeInBounce'),
		array('id' => 29, 'name' => 'easeOutBounce'),
		array('id' => 30, 'name' => 'easeInOutBounce'),
	);
    public static $sort_by = array(
        1 => array('id' =>1 , 'name' => 'Product Name: A to Z'),
        2 => array('id' =>2 , 'name' => 'Product Name: Z to A'),
        3 => array('id' =>3 , 'name' => 'Random'),
    );
    public static $items = array(
		array('id' => 1, 'name' => '1'),
		array('id' => 2, 'name' => '2'),
		array('id' => 3, 'name' => '3'),
		array('id' => 4, 'name' => '4'),
		array('id' => 5, 'name' => '5'),
		array('id' => 6, 'name' => '6'),
		array('id' => 7, 'name' => '7'),
    );
	public function __construct()
	{
		$this->name          = 'stbrandsslider';
		$this->tab           = 'front_office_features';
		$this->version       = '1.3.4';
		$this->author        = 'SUNNYTOO.COM';
		$this->need_instance = 0;
		$this->bootstrap 	 = true;
		parent::__construct();
		
		$this->displayName = $this->l('Brand Slider');
		$this->description = $this->l('Brand slider on the home page.');
	}

	public function install()
	{
		if (!parent::install() 
            || !$this->installDB()
            || !$this->initData()
			|| !$this->registerHook('displayHomeBottom')
			|| !$this->registerHook('actionObjectManufacturerDeleteAfter')
			|| !$this->registerHook('actionObjectManufacturerUpdateAfter')
            || !Configuration::updateValue('BRANDS_SLIDER_NBR', 10)
            || !Configuration::updateValue('BRANDS_SLIDER_ORDER', 1)

            || !Configuration::updateValue('STSN_BRANDS_PRO_PER_LG_0', 6)
            || !Configuration::updateValue('STSN_BRANDS_PRO_PER_MD_0', 5)
            || !Configuration::updateValue('STSN_BRANDS_PRO_PER_SM_0', 4)
            || !Configuration::updateValue('STSN_BRANDS_PRO_PER_XS_0', 3)
            || !Configuration::updateValue('STSN_BRANDS_PRO_PER_XXS_0', 2)
            || !Configuration::updateValue('BRANDS_SLIDER_SLIDESHOW', 0)
            || !Configuration::updateValue('BRANDS_SLIDER_S_SPEED', 7000)
            || !Configuration::updateValue('BRANDS_SLIDER_A_SPEED', 400)
            || !Configuration::updateValue('BRANDS_SLIDER_PAUSE_ON_HOVER', 1)
            || !Configuration::updateValue('BRANDS_SLIDER_EASING', 0)
            || !Configuration::updateValue('BRANDS_SLIDER_LOOP', 0)
            || !Configuration::updateValue('BRANDS_SLIDER_MOVE', 0)

            || !Configuration::updateValue('BRANDS_S_ITEMS_COL', 2)
            || !Configuration::updateValue('BRANDS_S_SLIDESHOW_COL', 0)
            || !Configuration::updateValue('BRANDS_S_S_SPEED_COL', 7000)
            || !Configuration::updateValue('BRANDS_S_A_SPEED_COL', 400)
            || !Configuration::updateValue('BRANDS_S_PAUSE_ON_HOVER_COL', 1)
            || !Configuration::updateValue('BRANDS_S_EASING_COL', 0)
            || !Configuration::updateValue('BRANDS_S_LOOP_COL', 0)
            )
			return false;
        $this->clearBrandsSliderCache();
		return true;
	}

	public function uninstall()
	{
        $this->clearBrandsSliderCache();
		if (!parent::uninstall() 
            || !$this->uninstallDB()
        )
			return false;
		return true;
	}
    private function installDB()
	{
		return Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'st_brands_slider` (
                 `id_manufacturer` int(10) NOT NULL,  
                 `id_shop` int(11) NOT NULL,                   
                PRIMARY KEY (`id_manufacturer`,`id_shop`),    
                KEY `id_shop` (`id_shop`)       
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');
	}
	private function uninstallDB()
	{
		return Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'st_brands_slider`');
	}  
    private function initData()
    {
        $res = true;
        $manufacturers = Manufacturer::getManufacturers(false, (int)$this->context->language->id);
        if (is_array($manufacturers) && count($manufacturers))
        {
            foreach($manufacturers as $k => $v)
                if($k<10)
                    $res &= Db::getInstance()->insert('st_brands_slider', array(
        					'id_manufacturer' => (int)$v['id_manufacturer'],
        					'id_shop' => (int)$this->context->shop->id,
        				));
                else
                    break;
        }
        return $res;
    }
    public function getContent()
	{
	    $this->initFieldsForm();
		if (isset($_POST['savestbrandsslider']))
		{
		    StBrandsSliderClass::deleteByShop((int)$this->context->shop->id);
    		$manufacturers = Manufacturer::getManufacturers(false, (int)$this->context->language->id);
            $res = true;
            if (is_array($manufacturers) && count($manufacturers))
            {
                foreach ($manufacturers as $v)
    				if (isset($_POST['manufacturers_'.$v['id_manufacturer']]) && $_POST['manufacturers_'.$v['id_manufacturer']])
   					{
   					    $res &= Db::getInstance()->insert('st_brands_slider', array(
        					'id_manufacturer' => (int)$v['id_manufacturer'],
        					'id_shop' => (int)$this->context->shop->id,
        				));
   					}
            }  
            if($res)
            {
                foreach($this->fields_form as $form)
                    foreach($form['form']['input'] as $field)
                        if(isset($field['validation']))
                        {
                            $errors = array();       
                            $value = Tools::getValue($field['name']);
                            if (isset($field['required']) && $field['required'] && $value==false && (string)$value != '0')
            						$errors[] = sprintf(Tools::displayError('Field "%s" is required.'), $field['label']);
                            elseif($value)
                            {
            					if (!Validate::$field['validation']($value))
            						$errors[] = sprintf(Tools::displayError('Field "%s" is invalid.'), $field['label']);
                            }
            				// Set default value
            				if ($value === false && isset($field['default_value']))
            					$value = $field['default_value'];
                                
                            if(count($errors))
                            {
                                $this->validation_errors = array_merge($this->validation_errors, $errors);
                            }
                            elseif($value==false)
                            {
                                switch($field['validation'])
                                {
                                    case 'isUnsignedId':
                                    case 'isUnsignedInt':
                                    case 'isInt':
                                    case 'isBool':
                                        $value = 0;
                                    break;
                                    default:
                                        $value = '';
                                    break;
                                }
                                Configuration::updateValue('BRANDS_'.strtoupper($field['name']), $value);
                            }
                            else
                                Configuration::updateValue('BRANDS_'.strtoupper($field['name']), $value);
                        }
            	$this->updateCatePerRow();
                if(count($this->validation_errors))
                    $this->_html .= $this->displayError(implode('<br/>',$this->validation_errors));
                else 
                    $this->_html .= $this->displayConfirmation($this->l('Settings updated'));
            }
            else
                $this->_html .= $this->displayError($this->l('Cannot update settings'));
                
            $this->clearBrandsSliderCache();            
        }
        $this->fields_form[1]['form']['input']['brands_pro_per_0']['name'] = $this->BuildDropListGroup($this->findCateProPer());
		$helper = $this->initForm();                    
		return $this->_html.$helper->generateForm($this->fields_form);
	}

    public function updateCatePerRow() {
        $arr = $this->findCateProPer();
        foreach ($arr as $v)
            if($gv = Tools::getValue($v['id']))
                Configuration::updateValue('STSN_'.strtoupper($v['id']), (int)$gv);
    }

    public function initFieldsForm()
    {
		$this->fields_form[0]['form'] = array(
			'legend' => array(
				'title' => $this->displayName,
                'icon' => 'icon-cogs'
			),
			'input' => array(
                array(
					'type' => 'text',
					'label' => $this->l('Define the number of brands to be displayed:'),
					'name' => 'slider_nbr',
                    'required' => true,
                    'desc' => $this->l('Define the number of brands that you would like to display on homepage (default: 8).'),
                    'validation' => 'isUnsignedInt',
                    'class' => 'fixed-width-sm'
				),
                array(
                    'type' => 'select',
    				'label' => $this->l('Sort order:'),
    				'name' => 'slider_order',
    				'options' => array(
    					'query' => self::$sort_by,
        				'id' => 'id',
        				'name' => 'name',
    				),
                    'validation' => 'isUnsignedInt',
                    //'desc' => $this->l('If you choose "RANDOM" smarty cache will be disabled'),
                ),
				array(
					'type' => 'checkbox',
					'label' => $this->l('Brands/Manufacturers:'),
					'name' => 'manufacturers',
					'lang' => true,
					'values' => array(
						'query' => Manufacturer::getManufacturers(false, (int)$this->context->language->id),
						'id' => 'id_manufacturer',
						'name' => 'name'
					)
				),
			),
			'submit' => array(
				'title' => $this->l('   Save all  ')
			),
		);
        $this->fields_form[1]['form'] = array(
			'legend' => array(
				'title' => $this->l('Slider on homepage'),
			),
			'input' => array(
                'brands_pro_per_0' => array(
                    'type' => 'html',
                    'id' => 'brands_pro_per_0',
                    'label'=> $this->l('The number of columns'),
                    'name' => '',
                ),
                array(
					'type' => 'switch',
					'label' => $this->l('Autoplay:'),
					'name' => 'slider_slideshow',
					'is_bool' => true,
                    'default_value' => 1,
					'values' => array(
						array(
							'id' => 'slide_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'slide_off',
							'value' => 0,
							'label' => $this->l('No')),
					),
                    'validation' => 'isBool',
				), 
                array(
					'type' => 'text',
					'label' => $this->l('Time:'),
					'name' => 'slider_s_speed',
                    'default_value' => 7000,
                    'desc' => $this->l('The period, in milliseconds, between the end of a transition effect and the start of the next one.'),
                    'validation' => 'isUnsignedInt',
                    'class' => 'fixed-width-sm'
				),
                array(
					'type' => 'text',
					'label' => $this->l('Transition period:'),
					'name' => 'slider_a_speed',
                    'default_value' => 400,
                    'desc' => $this->l('The period, in milliseconds, of the transition effect.'),
                    'validation' => 'isUnsignedInt',
                    'class' => 'fixed-width-sm'
				),
                array(
					'type' => 'switch',
					'label' => $this->l('Pause On Hover:'),
					'name' => 'slider_pause_on_hover',
                    'default_value' => 1,
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'pause_on_hover_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'pause_on_hover_off',
							'value' => 0,
							'label' => $this->l('No')),
					),
                    'validation' => 'isBool',
				),
                array(
					'type' => 'select',
        			'label' => $this->l('Easing method:'),
        			'name' => 'slider_easing',
                    'options' => array(
        				'query' => self::$easing,
        				'id' => 'id',
        				'name' => 'name',
        			),
                    'desc' => $this->l('The type of easing applied to the transition animation'),
                    'validation' => 'isUnsignedInt',
				), 
                array(
					'type' => 'switch',
					'label' => $this->l('Loop:'),
					'name' => 'slider_loop',
                    'default_value' => 0,
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'loop_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'loop_off',
							'value' => 0,
							'label' => $this->l('No')),
					),
                    'desc' => $this->l('"No" if you want to perform the animation once; "Yes" to loop the animation'),
                    'validation' => 'isBool',
				),
                array(
					'type' => 'radio',
					'label' => $this->l('Move:'),
					'name' => 'slider_move',
                    'default_value' => 0,
					'values' => array(
						array(
							'id' => 'move_on',
							'value' => 1,
							'label' => $this->l('1 item')),
						array(
							'id' => 'move_off',
							'value' => 0,
							'label' => $this->l('All visible items')),
					),
                    'validation' => 'isBool',
				),
			),
			'submit' => array(
				'title' => $this->l('   Save all  ')
			),
		);
        $this->fields_form[2]['form'] = array(
			'legend' => array(
				'title' => $this->l('Slide on left column/right column'),
			),
			'input' => array(
                array(
					'type' => 'select',
        			'label' => $this->l('The number of columns:'),
        			'name' => 's_items_col',
                    'options' => array(
        				'query' => self::$items,
        				'id' => 'id',
        				'name' => 'name',
        			),
                    'desc' => array(
                        $this->l('Set number of columns for default screen resolution(980px).'),
                    ),
                    'validation' => 'isUnsignedInt',
                    'class' => 'fixed-width-sm'
				), 
                array(
					'type' => 'switch',
					'label' => $this->l('Autoplay:'),
					'name' => 's_slideshow_col',
					'is_bool' => true,
                    'default_value' => 1,
					'values' => array(
						array(
							'id' => 'slide_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'slide_off',
							'value' => 0,
							'label' => $this->l('No')),
					),
                    'validation' => 'isBool',
				), 
                array(
					'type' => 'text',
					'label' => $this->l('Time:'),
					'name' => 's_s_speed_col',
                    'default_value' => 7000,
                    'desc' => $this->l('The period, in milliseconds, between the end of a transition effect and the start of the next one.'),
                    'validation' => 'isUnsignedInt',
                    'class' => 'fixed-width-sm'
				),
                array(
					'type' => 'text',
					'label' => $this->l('Transition period:'),
					'name' => 's_a_speed_col',
                    'default_value' => 400,
                    'desc' => $this->l('The period, in milliseconds, of the transition effect.'),
                    'validation' => 'isUnsignedInt',
                    'class' => 'fixed-width-sm'
				),
                array(
					'type' => 'switch',
					'label' => $this->l('Pause On Hover:'),
					'name' => 's_pause_on_hover_col',
                    'default_value' => 1,
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'pause_on_hover_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'pause_on_hover_off',
							'value' => 0,
							'label' => $this->l('No')),
					),
                    'validation' => 'isBool',
				),
                array(
					'type' => 'select',
        			'label' => $this->l('Easing method:'),
        			'name' => 's_easing_col',
                    'options' => array(
        				'query' => self::$easing,
        				'id' => 'id',
        				'name' => 'name',
        			),
                    'desc' => $this->l('The type of easing applied to the transition animation'),
                    'validation' => 'isUnsignedInt',
				), 
                array(
					'type' => 'switch',
					'label' => $this->l('Loop:'),
					'name' => 's_loop_col',
                    'default_value' => 0,
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'loop_on',
							'value' => 1,
							'label' => $this->l('Yes')),
						array(
							'id' => 'loop_off',
							'value' => 0,
							'label' => $this->l('No')),
					),
                    'desc' => $this->l('"No" if you want to perform the animation once; "Yes" to loop the animation'),
                    'validation' => 'isBool',
				),
			),
			'submit' => array(
				'title' => $this->l('   Save all  ')
			),
		);
    }
    protected function initForm()
	{
	    $helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table =  $this->table;
        $helper->module = $this;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;

		$helper->identifier = $this->identifier;
		$helper->submit_action = 'savestbrandsslider';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
		return $helper;
	}
	public function hookDisplayHome($params, $flag=0)
	{
		if (Configuration::get('BRANDS_SLIDER_ORDER')==3 || !$this->isCached('stbrandsslider.tpl', $this->stGetCacheId($flag)))
            $this->_prepareHook(0);
        $this->smarty->assign(array(
            'homeverybottom'         => ($flag==2 ? true : false),
        ));
        if(Configuration::get('BRANDS_SLIDER_ORDER')==3)
            return $this->display(__FILE__, 'stbrandsslider.tpl');
        else
		    return $this->display(__FILE__, 'stbrandsslider.tpl', $this->stGetCacheId($flag));
	}
    public function hookDisplayHomeBottom($params)
    {
        return $this->hookDisplayHome($params);
    }
    public function hookDisplayHomeTop($params)
    {
        return $this->hookDisplayHome($params);
    }

    public function hookDisplayHomeVeryBottom($params)
    {
        return $this->hookDisplayHome($params,2);
    }

    public function hookDisplayHomeTertiaryLeft($params)
    {
        return $this->hookDisplayHome($params);
    }
    public function hookDisplayHomeTertiaryRight($params)
    {
        return $this->hookDisplayHome($params);
    }
    

    public function hookDisplayHomeSecondaryRight($params)
    {
        return $this->hookDisplayLeftColumn($params);
    }
    public function hookDisplayRightColumn($params)
    {
        return $this->hookDisplayLeftColumn($params);
    }
    public function hookDisplayLeftColumn($params)
    {
		if (Configuration::get('BRANDS_SLIDER_ORDER')==3 || !$this->isCached('stbrandsslider-column.tpl'))
            $this->_prepareHook(1);
        if(Configuration::get('BRANDS_SLIDER_ORDER')==3)
            return $this->display(__FILE__, 'stbrandsslider-column.tpl');
        else
		    return $this->display(__FILE__, 'stbrandsslider-column.tpl');
    }
	public function hookDisplayProductSecondaryColumn($params)
	{
        return $this->hookDisplayLeftColumn($params);
	}
    
    public function hookDisplayCategoryFooter($params)
    {
        return $this->hookDisplayHome($params);
    }
    
    public function hookDisplayFooterProduct($params)
    {
        return $this->hookDisplayHome($params);
    }
    
    public function hookDisplayFooterTop($params)
    {
        $this->smarty->assign('brandsslider_footer',true);
        return $this->hookDisplayHome($params);
    }
    public function hookDisplayFooter($params)
    {
        $this->smarty->assign('brandsslider_footer',true);
        return $this->hookDisplayHome($params);
    }
    public function hookDisplayFooterSecondary($params)
    {
        $this->smarty->assign('brandsslider_footer',true);
        return $this->hookDisplayHome($params);
    }
    
    private function _prepareHook($col=0)
    {
        $brands = $this->getManufacturers(Configuration::get('BRANDS_SLIDER_NBR'), $this->context->shop->id,$this->context->language->id);

        $pre = $col ? 'S' : 'SLIDER';
        $ext = $col ? '_COL' : '';

		$this->smarty->assign(array(
            'brands' => $brands,
			'manufacturerSize' => Image::getSize(ImageType::getFormatedName('manufacturer')),

            'brand_slider_easing' => self::$easing[Configuration::get('BRANDS_'.$pre.'_EASING'.$ext)]['name'],
            'brand_slider_slideshow' => Configuration::get('BRANDS_'.$pre.'_SLIDESHOW'.$ext),
            'brand_slider_s_speed' => Configuration::get('BRANDS_'.$pre.'_S_SPEED'.$ext),
            'brand_slider_a_speed' => Configuration::get('BRANDS_'.$pre.'_A_SPEED'.$ext),
            'brand_slider_pause_on_hover' => Configuration::get('BRANDS_'.$pre.'_PAUSE_ON_HOVER'.$ext),
            'brand_slider_loop' => Configuration::get('BRANDS_'.$pre.'_LOOP'.$ext),

            'brand_slider_move' => Configuration::get('BRANDS_SLIDER_MOVE'),
            'brand_slider_items' => Configuration::get('BRANDS_S_ITEMS_COL'),

            'pro_per_lg'       => (int)Configuration::get('STSN_BRANDS_PRO_PER_LG_0'),
            'pro_per_md'       => (int)Configuration::get('STSN_BRANDS_PRO_PER_MD_0'),
            'pro_per_sm'       => (int)Configuration::get('STSN_BRANDS_PRO_PER_SM_0'),
            'pro_per_xs'       => (int)Configuration::get('STSN_BRANDS_PRO_PER_XS_0'),
            'pro_per_xxs'       => (int)Configuration::get('STSN_BRANDS_PRO_PER_XXS_0'),
        ));
    }
    public function hookActionObjectManufacturerDeleteAfter($params)
    {
        $this->clearBrandsSliderCache();
    }
    public function hookActionObjectManufacturerUpdateAfter($params)
    {
        $this->clearBrandsSliderCache();
    }
	private function clearBrandsSliderCache()
	{
        $this->_clearCache('stbrandsslider.tpl');
        $this->_clearCache('stbrandsslider.tpl', $this->stGetCacheId(0));
        $this->_clearCache('stbrandsslider.tpl', $this->stGetCacheId(1));
		$this->_clearCache('stbrandsslider.tpl', $this->stGetCacheId(2));
		$this->_clearCache('stbrandsslider-column.tpl');
	}
    
    protected function stGetCacheId($key,$name = null)
    {
        $cache_id = parent::getCacheId($name);
        return $cache_id.'_'.$key;
    }

	public function getManufacturers($nbr, $id_shop , $id_lang = 0 )
	{
		if (!$id_lang)
			$id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
		if (!$id_shop)
			$id_shop = (int)$this->context->shop->id;

		$sql = 'SELECT m.*, ml.`description`, ml.`short_description`
			FROM `'._DB_PREFIX_.'st_brands_slider` sbs
            LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON m.`id_manufacturer` = sbs.`id_manufacturer` 
			LEFT JOIN `'._DB_PREFIX_.'manufacturer_lang` ml ON (
				m.`id_manufacturer` = ml.`id_manufacturer`
				AND ml.`id_lang` = '.(int)$id_lang.'
			)
			'.Shop::addSqlAssociation('manufacturer', 'm');
			$sql .= ' WHERE sbs.`id_shop` = '.$id_shop.' AND m.`active` = 1 
            GROUP BY m.id_manufacturer ';
            switch(Configuration::get('BRANDS_SLIDER_ORDER'))
            {
                case 1:
                    $sql .= ' ORDER BY m.`name` ASC ';
                break;
                case 2:
                    $sql .= ' ORDER BY m.`name` DESC ';
                break;
                case 3:
                    $sql .= ' ORDER BY RAND() ';
                break;
                default:
                    $sql .= ' ORDER BY m.`name` ASC ';
                break;
            }
            $sql .= ($nbr ? ' LIMIT '.$nbr : '');

		$manufacturers = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
		if ($manufacturers === false)
			return false;

		$total_manufacturers = count($manufacturers);
		$rewrite_settings = (int)Configuration::get('PS_REWRITING_SETTINGS');

		for ($i = 0; $i < $total_manufacturers; $i++)
			if ($rewrite_settings)
				$manufacturers[$i]['link_rewrite'] = Tools::link_rewrite($manufacturers[$i]['name']);
			else
				$manufacturers[$i]['link_rewrite'] = 0;
		return $manufacturers;
	}
    private function getConfigFieldsValues()
    {
        $fields_values = array(
            'slider_nbr' => Configuration::get('BRANDS_SLIDER_NBR'),
            'slider_order' => Configuration::get('BRANDS_SLIDER_ORDER'),

            'slider_easing' => Configuration::get('BRANDS_SLIDER_EASING'),
            'slider_slideshow' => Configuration::get('BRANDS_SLIDER_SLIDESHOW'),
            'slider_s_speed' => Configuration::get('BRANDS_SLIDER_S_SPEED'),
            'slider_a_speed' => Configuration::get('BRANDS_SLIDER_A_SPEED'),
            'slider_pause_on_hover' => Configuration::get('BRANDS_SLIDER_PAUSE_ON_HOVER'),
            'slider_loop' => Configuration::get('BRANDS_SLIDER_LOOP'),

            'slider_move' => Configuration::get('BRANDS_SLIDER_MOVE'),

            's_easing_col' => Configuration::get('BRANDS_S_EASING_COL'),
            's_slideshow_col' => Configuration::get('BRANDS_S_SLIDESHOW_COL'),
            's_s_speed_col' => Configuration::get('BRANDS_S_S_SPEED_COL'),
            's_a_speed_col' => Configuration::get('BRANDS_S_A_SPEED_COL'),
            's_pause_on_hover_col' => Configuration::get('BRANDS_S_PAUSE_ON_HOVER_COL'),
            's_loop_col' => Configuration::get('BRANDS_S_LOOP_COL'),
            's_items_col' => Configuration::get('BRANDS_S_ITEMS_COL'),  
        );
        $manufacturers_checked = StBrandsSliderClass::getByShop((int)$this->context->shop->id);
            if($manufacturers_checked)
                foreach($manufacturers_checked as $v)
                    $fields_values['manufacturers_'.$v['id_manufacturer']] = 1; 
        return $fields_values;
    }

    public function BuildDropListGroup($group)
    {
        if(!is_array($group) || !count($group))
            return false;

        $html = '<div class="row">';
        foreach($group AS $key => $k)
        {
             if($key==3)
                 $html .= '</div><div class="row">';

             $html .= '<div class="col-xs-4 col-sm-3"><label '.(isset($k['tooltip']) ? ' data-html="true" data-toggle="tooltip" class="label-tooltip" data-original-title="'.$k['tooltip'].'" ':'').'>'.$k['label'].'</label>'.
             '<select name="'.$k['id'].'" 
             id="'.$k['id'].'" 
             class="'.(isset($k['class']) ? $k['class'] : 'fixed-width-md').'"'.
             (isset($k['onchange']) ? ' onchange="'.$k['onchange'].'"':'').' >';
            
            for ($i=1; $i < 8; $i++){
                $html .= '<option value="'.$i.'" '.(Configuration::get('STSN_'.strtoupper($k['id'])) == $i ? ' selected="selected"':'').'>'.$i.'</option>';
            }
                                
            $html .= '</select></div>';
        }
        return $html.'</div>';
    }
    public function findCateProPer()
    {
        return array(
            array(
                'id' => 'brands_pro_per_lg_0',
                'label' => $this->l('Large devices'),
                'tooltip' => $this->l('Desktops (>1200px)'),
            ),
            array(
                'id' => 'brands_pro_per_md_0',
                'label' => $this->l('Medium devices'),
                'tooltip' => $this->l('Desktops (>992px)'),
            ),
            array(
                'id' => 'brands_pro_per_sm_0',
                'label' => $this->l('Small devices'),
                'tooltip' => $this->l('Tablets (>768px)'),
            ),
            array(
                'id' => 'brands_pro_per_xs_0',
                'label' => $this->l('Extra small devices'),
                'tooltip' => $this->l('Phones (>480px)'),
            ),
            array(
                'id' => 'brands_pro_per_xxs_0',
                'label' => $this->l('Extra extra small devices'),
                'tooltip' => $this->l('Phones (<480px)'),
            ),
        );
    }
}