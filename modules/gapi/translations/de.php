<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{gapi}prestashop>gapi_69ee9bf9cf3d83a8468278c44959caf0'] = 'Google Analytics API';
$_MODULE['<{gapi}prestashop>gapi_0851f7a0844553fa1168718de0f87262'] = 'Sie sind nicht berechtigt, externe URLs (allow_url_fopen)  zu öffnen';
$_MODULE['<{gapi}prestashop>gapi_6401593f1412a6b385c8e645d1f056ac'] = 'cURL ist nicht aktiviert';
$_MODULE['<{gapi}prestashop>gapi_f8b94463fa8b5591e5edbbb8021e8038'] = 'OpenSSL ist nicht aktiviert';
$_MODULE['<{gapi}prestashop>gapi_6e4c3e76dd29876e6d33ce8c89e5fc5f'] = 'Keine Verbindung zu Google (Überprüfen Sie Ihre Firewall)';
$_MODULE['<{gapi}prestashop>gapi_a1ed99ed6aaac91d7c3b127f032abf2d'] = 'Aktuell testen Sie Ihren Shop nur lokal. Um sämtliche Funktionen nutzen zu können, müssen Sie Ihren Shop online stellen.';
$_MODULE['<{gapi}prestashop>gapi_2ccf68a6aec8eda73156a7ef54b03351'] = 'Welche Google Analytics API möchten Sie verwenden?';
$_MODULE['<{gapi}prestashop>gapi_0caf30452ef28d761ae80a407b64bd9b'] = 'Version 1.3: leicht einzurichten, aber veraltet und unsicher';
$_MODULE['<{gapi}prestashop>gapi_949617ff3314c7cf2d88d356a953bd67'] = 'Version 3.0 mit OAuth, 2.0: sehr mächtig und aktuell';
$_MODULE['<{gapi}prestashop>gapi_f1f4f41c5cab767032db832ec7bd5b64'] = 'Speichern und einrichten';
$_MODULE['<{gapi}prestashop>gapi_00e9b476102174b72bce85f57ef4f251'] = 'Ihr Shop hatte gestern %d Besucher und insgesamt %d Seitenaufrufe.';
$_MODULE['<{gapi}prestashop>gapi_8c3d59ec958045d2138efbf189fc0b3d'] = 'Rufen Sie https://code.google.com/apis/console auf und klicken Sie auf den Button "Create project ..."';
$_MODULE['<{gapi}prestashop>gapi_d1e3e532408766651e81db385118d7e9'] = 'Rufen Sie im Menü "Services" die Analytics API auf';
$_MODULE['<{gapi}prestashop>gapi_8075da7f240d2fa5cb5df57aa199ad13'] = 'Sie werden aufgefordert, den AGB der Google APIs zuzustimmen';
$_MODULE['<{gapi}prestashop>gapi_2cafb0c89afb28b57e86d6020fdd20e8'] = 'und auch den Allgemeinen Geschäftsbedingungen der Google Analytics API';
$_MODULE['<{gapi}prestashop>gapi_6489ed26701b74c0fb139a3368804121'] = 'Danach sollte da etwas stehen wie';
$_MODULE['<{gapi}prestashop>gapi_55fdafff0d00bf5ce524ff4239126a57'] = 'Im Fenster "API Access" jetzt bitte den großen blauen "Create an OAuth 2.0 client ID..."-Button anklicken';
$_MODULE['<{gapi}prestashop>gapi_f68e4897a344f746740672c299f9c38d'] = 'Geben Sie nun den Namen Ihres Shops, die URLs von Logo und Shop ein und Klicken Sie auf "Next"';
$_MODULE['<{gapi}prestashop>gapi_3ba0cb1f9445416b1ea908245488f4ec'] = 'Bleiben Sie bei der Auswahl "Web application" und geben Sie im Feld  "Authorized Redirect URIs" die folgende URL ein: %s (eventuell müssen Sie auf "more options" klicken). Dann bestätigen Sie durch Anklicken des "Create client ID"-Buttons.';
$_MODULE['<{gapi}prestashop>gapi_6e1e99918b40cf3f46166fae1e642b73'] = 'Jetzt werden Sie eine Seite weiter geleitet. Kopieren Sie "Client ID" und "Client secret" und fügen Sie beide in das folgende Formular ein,';
$_MODULE['<{gapi}prestashop>gapi_3360d865f7470f7e584792548dc8b87a'] = 'Sie benötigen nun die ID des Analytics-Profils, mit dem Sie sich verbinden möchten. Um die Profil-ID zu ermitteln, rufen Sie das Analytics-Dashboard auf und schauen sich die URL in der Adressleiste Ihres Browsers an. IhrProfil-ID ist die Nummer nach dem "p", wie rot unterstrichen im Screenshot zu sehen.';
$_MODULE['<{gapi}prestashop>gapi_b18cb8e83113953f96bbe47bd90ab69c'] = 'Google Analytics API  Version 3.0 ';
$_MODULE['<{gapi}prestashop>gapi_76525f0f34b48475e5ca33f71d296f3b'] = 'Client-ID';
$_MODULE['<{gapi}prestashop>gapi_734082edf44417dd19cc65943aa65c36'] = 'Client Sicherheitsschlüssel';
$_MODULE['<{gapi}prestashop>gapi_cce99c598cfdb9773ab041d54c3d973a'] = 'Rolle';
$_MODULE['<{gapi}prestashop>gapi_b1a026d322c634ca9e88525070e012fd'] = 'Sichern und Authentifizieren';
$_MODULE['<{gapi}prestashop>gapi_d4e6d6c42bf3eb807b8778255a4ce415'] = 'Authentifizierung fehlgeschlagen';
$_MODULE['<{gapi}prestashop>gapi_a670b4cdb42644e4b46fa857d3f73d9e'] = 'Google Analytics API  Version 1.3 ';
$_MODULE['<{gapi}prestashop>gapi_ce8ae9da5b7cd6c3df2929543a9af92d'] = 'E-Mail';
$_MODULE['<{gapi}prestashop>gapi_dc647eb65e6711e155375218212b3964'] = 'Passwort';
$_MODULE['<{gapi}prestashop>gapi_970a710b7344f8639b6a86d1f081b660'] = 'Ihre Profil-ID wird an die URL angehängt, wenn Sie den Analytics-Report aufrufen,';
$_MODULE['<{gapi}prestashop>gapi_e33d3b3409f8a0fcc326596c918c4961'] = 'Bei der ALTEN Version von Google Analytics finden Sie die profile ID im Parameter "id" der URL (etwa "&id=xxxxxxxx"):';
$_MODULE['<{gapi}prestashop>gapi_c78fedea48082c7a437773e31b418f96'] = 'Bei der NEUEN Version von Google Analytics finden Sie die profile ID am Ende der URL, eine Nummer, die mit "p" beginnt:';


return $_MODULE;
