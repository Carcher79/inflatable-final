<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{gapi}prestashop>gapi_69ee9bf9cf3d83a8468278c44959caf0'] = 'Google Analytics API';
$_MODULE['<{gapi}prestashop>gapi_0851f7a0844553fa1168718de0f87262'] = 'Non hai i permessi per aprire gli URL esterni (allow_url_fopen)';
$_MODULE['<{gapi}prestashop>gapi_6401593f1412a6b385c8e645d1f056ac'] = 'cURL non è abilitato';
$_MODULE['<{gapi}prestashop>gapi_f8b94463fa8b5591e5edbbb8021e8038'] = 'OpenSSL non è abilitato';
$_MODULE['<{gapi}prestashop>gapi_6e4c3e76dd29876e6d33ce8c89e5fc5f'] = 'Google non è raggiungibile (verifica il tuo firewall)';
$_MODULE['<{gapi}prestashop>gapi_a1ed99ed6aaac91d7c3b127f032abf2d'] = 'Stai testando il tuo negozio su un server locale. Per poter usufruire di tutte le caratteristiche, hai bisogno di mettere il tuo negozio su un server online.';
$_MODULE['<{gapi}prestashop>gapi_2ccf68a6aec8eda73156a7ef54b03351'] = 'Quale versione delle API di Google Analytics vuoi usare?';
$_MODULE['<{gapi}prestashop>gapi_0caf30452ef28d761ae80a407b64bd9b'] = 'v.1.3: facile da configurare ma obsoleta e poco sicura';
$_MODULE['<{gapi}prestashop>gapi_949617ff3314c7cf2d88d356a953bd67'] = 'v.3.0 con OAuth 2.0: versione più potente e aggiornata';
$_MODULE['<{gapi}prestashop>gapi_f1f4f41c5cab767032db832ec7bd5b64'] = 'Salva e configura';
$_MODULE['<{gapi}prestashop>gapi_00e9b476102174b72bce85f57ef4f251'] = 'Ieri, il tuo negozio ha ricevuto la visita di %d persone per un totale di %d pagine uniche visualizzate.';
$_MODULE['<{gapi}prestashop>gapi_8c3d59ec958045d2138efbf189fc0b3d'] = 'Vai su https://code.google.com/apis/console e clicca sul bottone "Create project..."';
$_MODULE['<{gapi}prestashop>gapi_d1e3e532408766651e81db385118d7e9'] = 'Nel tab "Services", attiva le API Analytics';
$_MODULE['<{gapi}prestashop>gapi_8075da7f240d2fa5cb5df57aa199ad13'] = 'Ti verrà chiesto di accettare i Termini di Servizio delle API di Google';
$_MODULE['<{gapi}prestashop>gapi_2cafb0c89afb28b57e86d6020fdd20e8'] = 'E i Termini di Servizio  delle API di Analytics';
$_MODULE['<{gapi}prestashop>gapi_6489ed26701b74c0fb139a3368804121'] = 'Dovresti avere qualcosa di simile a questo';
$_MODULE['<{gapi}prestashop>gapi_55fdafff0d00bf5ce524ff4239126a57'] = 'Nel tab "API Access", clicca sul tasto blue. "Create an OAuth 2.0 client ID... "';
$_MODULE['<{gapi}prestashop>gapi_f68e4897a344f746740672c299f9c38d'] = 'Compila il modulo con il nome del tuo negozio, la URL del logo e la URL del tuo negozio, quindi clicca su "Avanti"';
$_MODULE['<{gapi}prestashop>gapi_3ba0cb1f9445416b1ea908245488f4ec'] = 'Lascia "Web application" selezionata e compila il campo "Authorized Redirect URIs" con il seguente URL: %s (potresti dover cliccare sul link "more options"). Quindi conferma cliccando il pulsante "Create client ID"';
$_MODULE['<{gapi}prestashop>gapi_6e1e99918b40cf3f46166fae1e642b73'] = 'Dovresti avere la schermata seguente. Copia e incolla il  "Client ID" e il "Client secret" nel modulo sottostante';
$_MODULE['<{gapi}prestashop>gapi_3360d865f7470f7e584792548dc8b87a'] = 'Adesso hai bisongo dell\'ID del Profilo Analitycs a cui ti vuoi connettere. Per trovare il l\'ID del tuo profilo, devi connetterti alla dashboard di Analutics, guarda l\'URL nella barra degli indirizzi. Il tuo ID Profilo è il numero che segue una "p", come quello  sottolineato in rosso nello screeenshot';
$_MODULE['<{gapi}prestashop>gapi_b18cb8e83113953f96bbe47bd90ab69c'] = 'Google Analytics API v3.0';
$_MODULE['<{gapi}prestashop>gapi_76525f0f34b48475e5ca33f71d296f3b'] = 'Client ID';
$_MODULE['<{gapi}prestashop>gapi_734082edf44417dd19cc65943aa65c36'] = 'Client Secret';
$_MODULE['<{gapi}prestashop>gapi_cce99c598cfdb9773ab041d54c3d973a'] = 'Profilo';
$_MODULE['<{gapi}prestashop>gapi_b1a026d322c634ca9e88525070e012fd'] = 'Salva e autentica';
$_MODULE['<{gapi}prestashop>gapi_d4e6d6c42bf3eb807b8778255a4ce415'] = 'Autenticazione fallita';
$_MODULE['<{gapi}prestashop>gapi_a670b4cdb42644e4b46fa857d3f73d9e'] = 'Google Analytics API v1.3';
$_MODULE['<{gapi}prestashop>gapi_ce8ae9da5b7cd6c3df2929543a9af92d'] = 'E-mail';
$_MODULE['<{gapi}prestashop>gapi_dc647eb65e6711e155375218212b3964'] = 'Password';
$_MODULE['<{gapi}prestashop>gapi_970a710b7344f8639b6a86d1f081b660'] = 'Puoi trovare il tuo ID profilo, nella barra degli indirizzi del tuo browser mente accedi ai report di Analytics.';
$_MODULE['<{gapi}prestashop>gapi_e33d3b3409f8a0fcc326596c918c4961'] = 'Per la VECCHIA VERSIONE di Google Analytics, l\'ID profilo è nel parametro "id" dell\'URL (vedi "&id=xxxxxxxx"):';
$_MODULE['<{gapi}prestashop>gapi_c78fedea48082c7a437773e31b418f96'] = 'Per la NUOVA VERSIONE di Google Analytics, l\'ID profilo è il numero alla fine dell\'URL, che comincia con p:';


return $_MODULE;
