<?php
if (!defined('_PS_VERSION_'))
	exit;

class regenerateCatTree extends Module
{	
	function __construct()
	{
		$this->name = 'regenerateCatTree';
		$this->tab = 'administration';
		$this->version = '1.0';
		$this->author = 'HA!*!*Y';
		$this->need_instance = 0;

		parent::__construct();

		$this->displayName = $this->l('Regenerate Category Tree');
		$this->description = $this->l('Regenerate Category Tree for your shop.');
	}
	
    function install()
    {
        if (!parent::install())
			return false;
		return true;
    }
	
	public function getContent()
	{
		$output = '<h2>Regenerate Category Tree</h2>';
		if (Tools::isSubmit('submitNewTrees'))
		{
			Category::regenerateEntireNtree();

			$output .= '
			<div class="conf confirm">
				<img src="../img/admin/ok.gif" alt="" title="" />Category Tree\'s DONE</div>';
		}
		return $output.$this->displayForm();
	}

	public function displayForm()
	{
		$output = '
		<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post">
			<fieldset class="width2">
				<legend><img src="../img/admin/cog.gif" alt="" class="middle" />'.$this->l('Run').'</legend>
				<center><input type="submit" name="submitNewTrees" value="'.$this->l('Rebuild Category Table').'" class="button" /></center>
			</fieldset>
		</form>';
		
		return $output;
	}
}
