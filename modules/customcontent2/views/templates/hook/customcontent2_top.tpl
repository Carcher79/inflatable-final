{if $page_name == 'index'}
{foreach from=$xml->item item=itemPos name=items}
  {if $itemPos->hook == 'top'}{assign var='TopHook' value='true'}{/if}
{/foreach}
{if isset($TopHook) && $TopHook=='true'}
<div id="customcontent2_top" class="clearfix">
<ul class="row">
{foreach from=$xml->item item=item name=items}
{if $item->hook == 'top'}
  <li class="item-{$smarty.foreach.items.iteration} span4">
    {if $item->url}<a href="{$item->url}">{/if}
      {if $item->img}<img src="{$img_path}{$item->img}" alt=""/>{/if}
      {if $item->html->$html_lang}<div class="item_html">{$item->html->$html_lang}</div>{/if}
    {if $item->url}</a>{/if}
  </li>
{/if}
{/foreach}
</ul>
</div>
{/if}
{/if}