

    <link rel="stylesheet" type="text/css" href="{$css_dir}contact_form.css">
	<link rel="stylesheet" type="text/css" href="{$css_dir}cat_req.css">
    
<script src="{$js_dir}ga.js" async="" type="text/javascript"></script><script type="text/javascript" src="{$js_dir}jquery-1.js"></script>
<script type="text/javascript" src="{$js_dir}mglobal.js"></script>
<script type="text/javascript" src="{$js_dir}form_validation.js"></script>
<script type="text/javascript" src="{$js_dir}aviary.js"></script>
<script type="text/javascript" src="{$js_dir}jquery_002.js"></script>
<script type="text/ecmascript">
$(function(){
	$('#login_form').slidinglabels({
		/* these are all optional */
		className    : 'slider', // the class you're wrapping the label & input with -> default = slider
		//topPosition  : '3px',  // how far down you want each label to start
		//leftPosition : '10px',  // how far left you want each label to start
		axis         : 'x',    // can take 'x' or 'y' for slide direction
		speed        : 'slow'  // can take 'fast', 'slow', or a numeric value
	});
	$('#pwreq_form').slidinglabels({
		/* these are all optional */
		className    : 'slider', // the class you're wrapping the label & input with -> default = slider
		//topPosition  : '3px',  // how far down you want each label to start
		//leftPosition : '10px',  // how far left you want each label to start
		axis         : 'x',    // can take 'x' or 'y' for slide direction
		speed        : 'slow'  // can take 'fast', 'slow', or a numeric value
	});
	
	// allow automatic hiding of login window when clicked outside of (assuming it is already showing of course)
	$("#overlay").bind('click', function(e) {  
		var $clicked = $(e.target);
		if ( $clicked.parents().hasClass("loginwindow") )
		{
			// do nothing, we've clicked in the login window
		}
		else if( !$(".loginwindow").is(":hidden") )
		{
			// hide and reset login window as we've clicked out of it
			hide_overlay(); 
			login_box_reset();
		}
	});
});
</script>

    
    <script type="text/javascript" src="{$js_dir}jquery.js"></script>
	<script type="text/javascript">
		function preload(arrayOfImages) {
			$(arrayOfImages).each(function(){
				$('<img/>')[0].src = this;
				// Alternatively you could use:
				// (new Image()).src = this;
			});
		}
		
		function request_option_change() {
			if($("#request-quote").is(":checked")) $.cookies.set("request-quote",true);
			else $.cookies.set("request-quote",false);
			
			if($("#request-phone").is(":checked")) $.cookies.set("request-phone",true);
			else $.cookies.set("request-phone",false);
			
			if($("#request-booklet").is(":checked")) $.cookies.set("request-booklet",true);
			else $.cookies.set("request-booklet",false);
			
			if(	!$("#request-quote").is(":checked") &&
				!$("#request-phone").is(":checked") &&
				!$("#request-booklet").is(":checked"))
			{
				$("#form-wrapper").hide("fast");
			}
			else $("#form-wrapper").show("fast");
			
			if($("#request-quote").is(":checked")) $("#form-right").show("fast");
			else $("#form-right").hide("fast");
			
			if($("#request-booklet").is(":checked")) {
				$("#address").parent().show('fast');
				$("#address2").parent().show('fast');
				$("#city").parent().show('fast');
				$("#state").parent().show('fast');
				$("#zip").parent().show('fast');
				$("#country").parent().show('fast');
				$("#address").attr('required', "required");
				$("#city").attr('required', "required");
				$("#state").attr('required', "required");
				$("#zip").attr('required', "required");
				$("#country").attr('required', "required");
			}
			else {
				$("#address").parent().hide('fast');
				$("#address2").parent().hide('fast');
				$("#city").parent().hide('fast');
				$("#state").parent().hide('fast');
				$("#zip").parent().hide('fast');
				$("#country").parent().hide('fast');
				$("#address").removeAttr('required');
				$("#city").removeAttr('required');
				$("#state").removeAttr('required');
				$("#zip").removeAttr('required');
				$("#country").removeAttr('required');
			}
		}
		
		// enable form submit through javascript
		$(function(){
						
			$('#submit_button').attr('disabled', false);
			
			preload(['{$img_dir}request/request-booklet-on.gif','{$img_dir}request/request-quote-on.gif','{$img_dir}request/request-phone-on.gif']);
			$("#request-options input").click(function(){
				request_option_change();
			});
			
			$("#country").change(function(){
				if($(this).val() == "US") {
					$("#state").show();
					$("#province").hide();
				}
				else {
					$("#state").hide();
					$("#province").show();
				}
			});
			
			$("#howyouheard").change(function(){
				if($(this).val() == "Other") $("#howyouheard_other").show();
				else $("#howyouheard_other").hide();
			});
			
			$("#form-wrapper input:not(.nocookie)").cookieBind();
			$("#form-wrapper textarea").cookieBind();
			$("#form-wrapper select").cookieBind();
			
			var num_phones = 1;
			if($.cookies.get('bill_numbers') != null) num_phones = parseInt($.cookies.get('bill_numbers'));
			for(var i = 1; i <= num_phones; i++)
			{
				if($.cookies.get('bill_cn_'+i) != null) {
					$.cookies.set('bill_cn_'+i, $.cookies.get('bill_cn_'+i));
					$.cookies.set('bill_ct_'+i, $.cookies.get('bill_ct_'+i));
					if(num_phones > i)
						add_phone();
				}
			}
			
			var num_prod_types = 1;
			if($.cookies.get('num_prod_types') != null) num_prod_types = parseInt($.cookies.get('num_prod_types'));
			for(var i = 1; i <= num_prod_types; i++)
			{
				if(parseInt($.cookies.get('prodqty_'+i)) != 0) {
					$.cookies.set('product_'+i, $.cookies.get('product_'+i));
					$.cookies.set('prodqty_'+i, $.cookies.get('prodqty_'+i));
					if(num_prod_types > i)
					   add_product();
				}
			}
			
			if($.cookies.get("request-quote") == true) $("#request-quote").attr('checked','checked');
			if($.cookies.get("request-phone") === true) $("#request-phone").attr('checked','checked');
			if($.cookies.get("request-booklet") === true) $("#request-booklet").attr('checked','checked');
			request_option_change();
		});
		
		var numProducts = 1;
		function add_product() {
			numProducts++;
			var html = '<tr>\
<td>\
<select name="product_'+numProducts+'" id="product_'+numProducts+'" class="product">\
<option value="">Please Select Product</option>\
	<optgroup label="OBSTACLE COURSES"><option value="299">32 Obstacle Course 32\'x11\'x11\'</option><option value="173">35 Obstacle Course 35\'x11\'x8\'</option><option value="93">40 Obstacle Course 40\'x11\'x12\'</option><option value="379">50 Obstacle Course 50\'x11\'x16\'</option><option value="409">Double Slide OC 62\'x11\'x16\'</option><option value="253">Obstacle Course 360 24\'x16\'x11\'</option><option value="490">Zip Thru Obstacle (R) 32\'x11\'x16\'</option><option value="488">Zip Thru Obstacle (L) 32\'x11\'x16\'</option><option value="491">Safari Adventure 40\'x26\'x18\'</option><option value="168">Extreme Rush 40\'x26\'x13.5\'</option><option value="410">Obstacle Island 20\'x20\'x14\'</option><option value="258">Obstacle Island 22\'x22\'x15\'</option><option value="92">Obstacle Combo 25\'x17\'x16\'</option><option value="481">Castle Course (50) 50\'x11\'x16\'</option><option value="482">Sports Course (50) 50\'x11\'x16\'</option><option value="487">Tropical Course (50) 50\'x11\'x16\'</option><option value="440">Sports Course (60) 60\'x11\'x20\'</option><option value="486">Tropical Course (60) 60\'x11\'x20\'</option><option value="203">Castle Course (60) 60\'x11\'x20\'</option><option value="174">65 Obstacle Course 65\'x11\'x16\'</option><option value="397">Wacky Wild Course 45\'x11\'x11\'</option><option value="201">Street Racer 40\'x12\'x14\'</option></optgroup><optgroup label="SPORTS"><option value="489">Wrecking Ball 20\'x20\'x17\'</option><option value="233">Free Throw Shootout 9\'x9\'x8\'</option><option value="177">Basketball 6\'x6\'x8\'</option><option value="459">Basketball Shootout 15\'x13\'x13\'</option><option value="442">Slam Dunk 22\'x15\'x13\'</option><option value="194">Sports Shootout 20\'x15\'x14\'</option><option value="451">Gaga Pit 26\'x22\'x48"</option><option value="438">Gaga Pit 18\'x16\'x48"</option><option value="190">Sport Cage 15\'x12\'x10\'</option><option value="232">Home Run Derby 12\'x12\'x8\'</option><option value="236">Ball Blaster 12\'x12\'x8\'</option><option value="396">Floating Balls 12\'x5\'x3\'</option><option value="140">Joust Arena 21\'x21\'x8\'</option><option value="64">Ultimate Boxing 17\'x17\'x10\'</option><option value="276">Sports Fusion 38\'x19\'x15\'</option><option value="271">Coliseum 33\'x30\'x16\'</option><option value="205">Batting Cage 40\'x18\'x15\'</option><option value="89">Velcro Wall 19\'x11\'x12\'</option><option value="395">Climb the Ladder 22\'x17\'x9\'</option><option value="443">Rock Wall 23\'x23\'x24\'</option><option value="192">Rat Race 75\'x47\'x12\'</option><option value="296">The BLOB 35\'x35\'x8\'</option><option value="239">Jr Twister 12\'x12\'x3\'</option><option value="237">Mr Twister 20\'x20\'x3\'</option><option value="479">Tumble Air Track 20\'x9\'x2\'</option><option value="480">Tumble Air Catcher 10\'x9\'x6\'</option><option value="376">Soccer Field 50\'x26\'x9\'</option><option value="191">Soccer Goal 18\'x7\'x10\'</option><option value="195">Dodge Ball Arena 30\'x16\'x12\'</option><option value="221">Football Helmet Tunnel 27\'x14.5\'x12\'</option></optgroup><optgroup label="WATER SLIDES"><option value="466">18 Velocity 27\'x12\'x18\'</option><option value="496">Dual Pool Splash 16\'x30\'x15\'</option><option value="495">Volcano Lava Slide 42\'x18\'x25\'</option><option value="494">Sharko Water Slide 20\'x9\'x10\'</option><option value="400">Slippity Slide (Wet/Dry) 18\'x10\'x12\'</option><option value="457">15 Slippity Slide (Wet/Dry) 23\'x11\'x15\'</option><option value="458">19 Slippity Slide (Wet/Dry) 30\'x11\'x19\'</option><option value="493">19 Flammin Slippity Slide (Wet/Dry) 30\'x11\'x19\'</option><option value="492">19 Rockin Slippity Slide (Wet/Dry) 30\'x11\'x19\'</option><option value="468">22 Velocity 33\'x15\'x22\'</option><option value="95">Water Slide 33\'x9\'x14\'</option><option value="170">Dolphin Water Slide 33\'x9\'x14\'</option><option value="303">Jr Wave 27\'x10\'x15\'</option><option value="447">Big Wave SL 32\'x11\'x19\'</option><option value="484">Flamin Surf-A-Curve 30\'x19\'x18\'</option><option value="485">Rockin Surf-A-Curve 30\'x19\'x18\'</option><option value="431">Surf-A-Curve 30\'x19\'x18\'</option><option value="367">Big Wave 32\'x15\'x18\'</option><option value="448">Giant Wave SL 54\'x13\'x20\'</option><option value="224">Giant Wave 54\'x16\'x20\'</option><option value="483">Flamin Super Giant Wave 67\'x13\'x23\'</option><option value="372">Super Giant Wave 67\'x13\'x23\'</option><option value="467">37 North Shore 95\'x30\'x37\'</option><option value="96">Tropical Water Slide 33\'x9\'x14\'</option><option value="302">Jr Tropical Wave 27\'x11\'x15\'</option><option value="428">Tropical Dual Water Slide 33\'x11\'x16\'</option><option value="446">Big Tropical Wave SL 32\'x11\'x19\'</option><option value="368">Big Tropical Wave 32\'x15\'x19\'</option><option value="449">Giant Tropical Wave SL 54\'x13\'x21\'</option><option value="366">Giant Tropical Wave 54\'x16\'x21\'</option><option value="244">Infinity Water Slide 37\'x22\'x18\'</option><option value="98">Slide N Splash 31\'x8\'x8\'</option><option value="99">Dual Slide N Splash 35\'x13\'x8\'</option><option value="354">Tropical Dual Slide N Splash 35\'x13\'x8\'</option><option value="100">Crocodile Slide N Splash 46\'x12\'x10\'</option><option value="433">Summer Splash 20\'x13\'x15\'</option><option value="432">Summer Splash 14\'x12\'x10\'</option><option value="333">Water Slide Swimming Pool 18\'x11\'x17\'</option></optgroup><optgroup label="SLIDES"><option value="365">15 Slide 20\'x11\'x15\'</option><option value="198">18 Single Lane Slide 24\'x13\'x18\'</option><option value="272">18 Double Lane Slide 24\'x18\'x18\'</option><option value="171">20 Double Lane Slide 26\'x18\'x20\'</option><option value="317">20 Wavy Double Lane Slide 26\'x18\'x20\'</option><option value="350">20 Double Slide II 24\'x15\'x20\'</option><option value="197">22 Double Lane Slide 30\'x18\'x22\'</option><option value="196">24 Double Lane Slide 36\'x18\'x24\'</option><option value="87">Fire Truck 30\'x13\'x19\'</option><option value="189">X Slide 56\'x30\'x23\'</option><option value="398">Cirque Slide 30\'x30\'x15\'</option></optgroup><optgroup label="COMBO UNITS"><option value="363">EZ Prince Combo 21\'x13\'x14\'</option><option value="463">EZ Princess Wet or Dry 28\'x13\'x15\'</option><option value="460">EZ Castle Wet or Dry 28\'x13\'x15\'</option><option value="462">EZ Sports Wet or Dry 28\'x13\'x14\'</option><option value="461">EZ Tropical Wet or Dry 28\'x13\'x14\'</option><option value="338">EZ Castle Combo 21\'x13\'x15\'</option><option value="364">EZ Princess Combo 21\'x13\'x15\'</option><option value="342">EZ Tropical Combo 21\'x13\'x14\'</option><option value="454">EZ Jungle Book Combo 21\'x13\'x13\'</option><option value="343">EZ Sports Combo 21\'x13\'x14\'</option><option value="346">EZ Hot Air Combo 21\'x13\'x16\'</option><option value="408">EZ Fun House Combo 21\'x13\'x13\'</option><option value="369">EZ Dalmatian Combo 21\'x13\'x15\'</option><option value="169">Dual Prince Combo 23\'x15\'x15\'</option><option value="315">Dual Castle Combo 23\'x15\'x15\'</option><option value="345">Dual Princess Castle 23\'x15\'x15\'</option><option value="331">Dual Tropical Combo 23\'x15\'x13\'</option><option value="254">Dual Sports Combo 23\'x15\'x14\'</option><option value="344">Dual Hot Air Combo 23\'x15\'x16\'</option><option value="370">Dual Dalmatian Combo 23\'x15\'x14.5\'</option><option value="434">Dual Medieval Castle Combo 23\'x15\'x13\'</option><option value="77">Fun Tropical Combo 19\'x15\'x13\'</option><option value="215">Fun Castle Combo 19\'x15\'x13.5\'</option><option value="74">Fun Sports Combo 19\'x15\'x11\'</option><option value="435">Fun Prince Combo 19\'x15\'x13\'</option><option value="73">Fun Pink Castle Combo 19\'x15\'x13\'</option><option value="76">Fun Hot Air Combo 19\'x15\'x14\'</option><option value="132">Fun House Combo 19\'x15\'x12\'</option><option value="211">Fun Indoor Combo 17\'x15\'x8.5\'</option><option value="391">5in1 Tropical Combo 19\'x18\'x16\'</option><option value="380">5in1 Castle Combo 19\'x18\'x16\'</option><option value="382">5in1 Sports Combo 19\'x18\'x16\'</option><option value="83">Large Tropical Combo 30\'x11\'x14\'</option><option value="81">Large Castle Combo 30\'x11\'x14\'</option><option value="82">Large Sports Combo 30\'x11\'x15\'</option><option value="413">Funtopia 30\'x20\'x13\'</option><option value="381">Thrill Zone 26\'x20\'x11\'</option><option value="335">Giant Castle Combo 30\'x19\'x13\'</option><option value="353">Fun Combo X 2 25\'x19\'x9.5\'</option><option value="88">Pirate Ship 31\'x14\'x16\'</option><option value="429">Toddler Town 30\'x13\'x12\' w/Slide</option><option value="430">Toddler Town 23\'x13\'x12\'</option><option value="411">Toddler Combo 14\'x14\'x8\'</option><option value="70">Toddler Combo 17\'x17\'x8\'</option><option value="392">1-2-3 Bounce 20\'x20\'x8\'</option><option value="301">Inflatable Zoo 17\'x17\'x10\'</option><option value="248">Fire Truck Combo 30\'x11\'x9.5\'</option><option value="274">Kids Gran Turismo 27\'x16\'x10\'</option></optgroup><optgroup label="BOUNCE HOUSES"><option value="478">Ball Pit 13\'x13\'x8\'</option><option value="12">Castle 15\'x15\'x14\'</option><option value="10">Castle 10\'x10\'x12\'</option><option value="11">Castle 13\'x13\'x14\'</option><option value="50">Princess Castle 15\'x15\'x14\'</option><option value="49">Princess Castle 13\'x13\'x14\'</option><option value="52">Palm Tree 15\'x15\'x14\'</option><option value="51">Palm Tree 13\'x13\'x14\'</option><option value="455">The Jungle Book 13\'x13\'x12\'</option><option value="456">The Jungle Book 15\'x15\'x12\'</option><option value="60">Sport Arena 15\'x15\'x13.5\'</option><option value="59">Sport Arena 13\'x13\'x13.5\'</option><option value="30">Hot Air Balloon 15\'x15\'x15\'</option><option value="159">Hot Air Balloon 13\'x13\'x15\'</option><option value="2">Arched Castle 15\'x15\'x14\'</option><option value="1">Arched Castle 13\'x13\'x14\'</option><option value="3">Arched Pink Castle 13\'x13\'x14\'</option><option value="4">Arched Pink Castle 15\'x15\'x14\'</option><option value="24">Fun House 13\'x13\'x12\'</option><option value="23">Fun House 10\'x10\'x10.5\'</option><option value="25">Fun House 15\'x15\'x12\'</option><option value="19">Doll House 13\'x13\'x12\'</option><option value="20">Doll House 15\'x15\'x12\'</option><option value="225">Fun House Panel 13\'x13\'x12\'</option><option value="226">Fun House Panel 15\'x15\'x12\'</option><option value="68">Under the Sea 15\'x15\'x14.5\'</option><option value="67">Under the Sea 13\'x13\'x14.5\'</option><option value="41">Ocean Castle 15\'x15\'x13\'</option><option value="40">Ocean Castle 13\'x13\'x13\'</option><option value="161">Prince Castle 13\'x13\'x14\'</option><option value="219">Prince Castle 15\'x15\'x14\'</option><option value="228">Pirate 15\'x15\'x14\'</option><option value="227">Pirate 13\'x13\'x14\'</option><option value="38">Medieval Castle 13\'x13\'x14\'</option><option value="39">Medieval Castle 15\'x15\'x14\'</option><option value="44">Pink Castle 10\'x10\'x12\'</option><option value="46">Pink Castle 15\'x15\'x14\'</option><option value="45">Pink Castle 13\'x13\'x14\'</option><option value="43">Pink Cake 15\'x15\'x14\'</option><option value="42">Pink Cake 13\'x13\'x14\'</option><option value="9">Cake 15\'x15\'x14\'</option><option value="8">Cake 13\'x13\'x14\'</option><option value="57">Spaceship 13\'x13\'x13\'</option><option value="58">Spaceship 15\'x15\'x13\'</option><option value="63">UFO 14\'x14\'x12\'</option><option value="212">Juke Box 15\'x15\'x15\'</option><option value="54">Race Car 15\'x15\'x14.5\'</option><option value="53">Race Car 13\'x13\'x14.5\'</option><option value="55">Robo Ranger 13\'x13\'x12\'</option><option value="56">Robo Ranger 15\'x15\'x12\'</option><option value="62">Train 15\'x15\'x16\'</option><option value="61">Train 13\'x13\'x16\'</option><option value="15">Dalmatian 13\'x13\'x14.5\'</option><option value="16">Dalmatian 15\'x15\'x14.5\'</option><option value="126">Tiger 15\'x15\'x15\'</option><option value="125">Tiger 13\'x13\'x15\'</option><option value="13">Clown 13\'x13\'x16\'</option><option value="14">Clown 15\'x15\'x16\'</option><option value="18">Dinosaur 15\'x15\'x16.5\'</option><option value="17">Dinosaur 13\'x13\'x16.5\'</option><option value="21">Dragon 13\'x13\'x15\'</option><option value="22">Dragon 15\'x15\'x15\'</option><option value="66">Unicorn 15\'x15\'x15\'</option><option value="65">Unicorn 13\'x13\'x15\'</option><option value="178">Giraffe 13\'x13\'x16\'</option><option value="179">Giraffe 15\'x15\'x16\'</option><option value="27">Horse 15\'x15\'x15\'</option><option value="26">Horse 13\'x13\'x15\'</option><option value="6">Boxing Ring 13\'x13\'x10\'</option><option value="7">Boxing Ring 15\'x15\'x10\'</option><option value="33">Indoor Unit 10\'x10\'x8\'</option><option value="307">Indoor Unit 15\'x15\'x8\'</option><option value="34">Indoor Unit 13\'x13\'x8\'</option><option value="5">Ball Pit 8\'x8\'x8\'</option><option value="160">Ball Pit 10\'x10\'x8\'</option><option value="452">Mini Bouncer 9\'x9\'x6\'</option><option value="407">Ball Pool 12\'x12\'x3\'</option><option value="336">Mini Bouncer 6\'x6\'x6\'</option><option value="172">Jumbo Castle 20\'x20\'x14\'</option><option value="352">Jumbo Fun House 20\'x20\'x15\'</option><option value="450">Jumbo Corn Bounce 20\'x20\'x12.5\'</option><option value="234">Magic Bounce 20\'x20\'x10\'</option></optgroup><optgroup label="INDOOR PLAY CENTER"><option value="444">IPC 15 Slide II 20\'x15\'x13\'</option><option value="287">IPC 18 Single 24\'x13\'x15\'</option><option value="476">IPC 18 Dual Slide II 24\'x15\'x15\'</option><option value="347">IPC 18 Double 24\'x18\'x15\'</option><option value="475">IPC 20 Double Slide II 24\'x15\'x17\'</option><option value="415">IPC 32 Obstacle Course 32\'x11\'x11\'</option><option value="472">IPC Toddler Combo 14\'x14\'x8\'</option><option value="471">IPC Toddler Combo 17\'x17\'x8\'</option><option value="474">IPC Toddler Town 23\'x13\'x12\'</option><option value="473">IPC Toddler Town 30\'x13\'x12\' w/Slide</option><option value="470">IPC Funtopia 30\'x20\'x13\'</option><option value="387">IPC Thrill Zone 26\'x20\'x11\'</option><option value="292">IPC Kids Turismo 27\'x16\'x10\'</option><option value="389">IPC Fun Combo X 2 25\'x19\'x9.5\'</option><option value="388">IPC Giant Castle Combo 30\'x19\'x13\'</option><option value="414">IPC 40 Obstacle Course 40\'x11\'x12\'</option><option value="416">IPC 50 Obstacle Course 50\'x11\'x16\'</option><option value="289">IPC Castle Course 60\'x11\'x15\'</option><option value="385">IPC Obstacle Course 360 24\'x16\'x11\'</option><option value="386">IPC Obstacle Combo 25\'x17\'x16\'</option><option value="384">IPC Obstacle Island 22\'x22\'x15\'</option><option value="291">IPC Extreme Rush 40\'x26\'x13.5\'</option><option value="288">IPC Bounce Fusion 35\'x26\'x12\'</option><option value="293">IPC 20 Double 26\'x18\'x17\'</option><option value="295">IPC X Slide 30\'x56\'x23\'</option><option value="290">IPC Coliseum 33\'x30\'x16\'</option><option value="229">Prince Throne 4\'x4\'x6\'</option><option value="230">Princess Throne 4\'x4\'x6\'</option><option value="324">Green Throne 4\'x4\'x6\'</option></optgroup><optgroup label="CUSTOM MADE"><option value="453">Giant Snow Globe 16\'x17\'x15\'</option><option value="275">Custom Movie Unit 20\'x20\'x14\'</option><option value="403">Inflatable Snow Globe 25\'x15\'x15\'</option><option value="355">Medieval Castle Movie 15\'x15\'x13\'</option><option value="394">Mushroom Bounce 15\'x15\'x13\'</option><option value="373">Taj Mahal 40\'x15\'x18\'</option><option value="200">U.S.S. Peacemaker 80\'x20\'x20\'</option><option value="269">Custom Extreme Rush 2 40\'x29\'x16\'</option><option value="204">Custom Extreme Rush 40\'x29\'x16\'</option><option value="270">Custom Bounce Fusion 35\'x26\'x12\'</option><option value="202">Interceptor 40\'x12\'x14\'</option><option value="405">Wrestling Ring 25\'x25\'x9\'</option><option value="263">Custom Slide Away 50\'x13\'x17\'</option><option value="297">Big Bounce 35\'x25\'x19\'</option><option value="175">X-Large Combo Series 40\'x13\'x16\'</option><option value="218">Enclosed Bounce N Slide 22\'x11\'x16\'</option><option value="220">Enclosed Bounce N Slide 26\'x11\'x16\'</option><option value="94">Magic Truck 27\'x11\'x18\'</option><option value="268">Custom Kids Gran Turismo 27\'x16\'x10\'</option><option value="262">Custom Kids Combo 15\'x15\'x6.5\'</option><option value="158">Shark Attack 30\'x11\'x17\'</option><option value="85">Medium Combo Series 20\'x16\'x13\'</option><option value="255">Custom Boxing Ring 13\'x13\'x10\'</option><option value="250">Custom Princess Castle 13\'x13\'x14\'</option><option value="402">Dual Tiger Combo 23\'x15\'x14\'</option><option value="210">Custom Pink Castle 13\'x13\'x14\'</option><option value="261">Custom Bounce House 10\'x8\'x7.5\'</option><option value="249">Inflatable Lagoon 20\'x20\'x3\'</option><option value="404">Long Slide N Splash 120\'x10\'x8\'</option><option value="375">Pool of Balls</option><option value="445">Surfboard Mattress 20\'x20\'x10\'</option><option value="207">Custom Bull Ride Mattress 17\'x17\'</option><option value="383">Bowling Pin 4\'x15\'</option><option value="252">Show Booth 30\'x30\'x24\'</option><option value="265">Snowball Doorway N/A</option><option value="371">Inflatable Screen 19\'x10\'x15\'</option><option value="257">Movie Screen 10\'x10\'x8\'</option><option value="193">Sports Bar 13\'x6\'x9.5\'</option><option value="267">Tire Replica Height: 20\'</option><option value="401">Inflatable Archway 5.5\'x3\'x14.5\'</option><option value="264">Ad Pole Height: 3\'</option><option value="209">Bull Run Checkpoint Height: 10\'</option><option value="349">Inflatable Guard Rails</option><option value="377">Construction Piece 24\'x24\'x5\'</option><option value="374">Snowball Sofa 9\'x9\'x3\'</option></optgroup><optgroup label="ACCESSORIES"><option value="436">Blower-Koala 1/2 hp</option><option value="247">Blower-Koala 1hp</option><option value="502">Blower-Kodiak 0.5hp 0.5hp</option><option value="437">Blower-Kodiak Eco 1hp 1hp</option><option value="306">Blower-Kodiak 1hp</option><option value="246">Blower-Kodiak 1.5hp</option><option value="399">Blower-Kodiak 2hp</option><option value="213">Super Deflator Round</option><option value="327">Magic Air Deluxe DIDA-2</option><option value="406">Foam Step 40"x20"x12"</option><option value="465">Cushion Pad 14\'x4\'x2.5"</option><option value="412">Cushion Pad 4\'x3\'x2.5"</option><option value="464">Safety Mat 11\'x6\'x2.5"</option><option value="166">Stakes</option><option value="102">Repair Kit</option><option value="122">Helmet</option><option value="106">Jumbo Boxing Gloves Jumbo</option><option value="121">Joust Poles</option><option value="320">Velcro Suits S,M,L sizes</option><option value="101">Inflatable Storage Bag</option><option value="107">Sand Bag</option><option value="348">Sand Bag Deluxe</option><option value="330">Sliding Mat</option><option value="418">Slide Lining 15 Slide</option><option value="325">Slide Lining 24 Double</option><option value="323">Slide Lining 22 Double</option><option value="417">Slide Lining 18 Double</option><option value="322">Slide Lining 20 Double</option><option value="321">Slide Lining 18 Single</option><option value="427">Obstacle Lining Obstacle Combo</option><option value="424">Obstacle Lining Extreme RL</option><option value="422">Obstacle Lining 65 Obstacle</option><option value="426">Obstacle Lining Obstacle Island</option><option value="421">Obstacle Lining 50 Obstacle</option><option value="425">Obstacle Lining Obstacle 360</option><option value="423">Obstacle Lining Extreme M</option><option value="420">Obstacle Lining 40 Obstacle</option><option value="499">Combo Lining (Fun Combo)</option><option value="501">Combo Lining (Large Combos)</option><option value="498">Combo Lining (Dual Combo)</option><option value="500">Combo Lining (5in1 Combo)</option><option value="497">Combo Lining (EZ Combo)</option><option value="503">Straps 6\' long</option><option value="238">Banner 19"x69"</option><option value="477">Tarp HD 18\'x24\'</option><option value="104">Tarp HD 16\'x20\'</option><option value="241">Vinyl Tarp 16\'x16\'</option><option value="393">Dunk Tank Cover</option><option value="167">Weight Mover Dolly</option><option value="441">Medium Duty Dolly</option></optgroup>\
</select>\
</td>\
<td>\
<input type="text" name="prodqty_'+numProducts+'" id="prodqty_'+numProducts+'" value="1" class="prodqty" onkeyup="javascript:set_total_products()" />\
</td>\
</tr>';
			$("#selectedProdsTable tr:last").before(html);
			$("#product_"+numProducts).cookieBind();
			$("#prodqty_"+numProducts).cookieBind();
			$("#num_prod_types").val(numProducts);
			$.cookies.set('num_prod_types', numProducts);
			set_total_products()
		}
		
		function set_total_products()
		{
			var total = 0;
			var n = 0;
			for(i = 1; i <= numProducts; i++) {
				n = $("#prodqty_"+i).val();
				if(n == '' || isNaN(n)) {
					n = 0;
					$("#prodqty_"+i).val(n);
				}
				total += parseInt(n);
			}
			$("#total_prods").html(total);
		}
		
		var numPhones = 1;
		function add_phone()
		{
			numPhones++;
			var html = '<li>\
        <label for="bill_ct_'+numPhones+'">Phone '+numPhones+'</label>\
        <select name="bill_ct_'+numPhones+'" id="bill_ct_'+numPhones+'" class="phone_type" >\
        <option value="">Select</option>\
        <option value="home">Home</option>\
        <option value="cell">Cell</option>\
        <option value="work">Work</option>\
        <option value="fax">Fax</option>\
        </select>\
        <input type="text" name="bill_cn_'+numPhones+'" id="bill_cn_'+numPhones+'" class="phone"  placeholder="555-555-5555" />\
    </li>';
			$("#add_phone_li").before(html);
			$("#bill_ct_"+numPhones).cookieBind();
			$("#bill_cn_"+numPhones).cookieBind();
			$("#bill_numbers").val(numPhones);
			$.cookies.set('bill_numbers', numPhones);
		}
    </script>

    <link rel="stylesheet" type="text/css" href="{$css_dir}FriendlyErrors.css"><script language="javascript" src="{$js_dir}FriendlyErrors.js"></script><!-- base href="cat_req.php" --></head>

<div style="position:relative; width:870px; margin-left:auto; margin-right:auto; padding:0px; display:none ;">
   
      
       
   
    <script type="text/javascript">
		function float(n) {
			// stop the balloon floating after all 3 have been floated in succession
			if(n > 3) return;
			// make sure all balloons other than n are returned to start position prior to floating called balloon
			var balloon;
			var balloon_pos;
			var balloon_top;
			for(x=1; x<=3; x++) {
				if(x == 1) balloon_top = 245;
				else if(x == 2) balloon_top = 495;
				else if(x == 3) balloon_top = 745;
				if(x != n) {
					balloon = $('#balloon_'+x);
					balloon_pos = balloon.position();
					if(balloon_pos.left != -15) {
						$('#balloon_'+x).clearQueue()
						.animate(
						{
							'top': balloon_top,
							'left': -15
						}, 800)
					}
				}
			}
			balloon = $('#balloon_'+n);
			balloon_pos = balloon.position();
			//verify we're at starting position in order to animate to float position
			if(balloon_pos.left == -15) {
				$('#balloon_'+n).animate(
				{
					'top': balloon_pos.top - 200,
					'left': balloon_pos.left - 185
				}, 800)
				.delay(5000)
				.animate(
				{
					'top': balloon_pos.top,
					'left': balloon_pos.left
				}, 800, null, function() { float(n+1); })
			}
		}
	</script>
    </div>


	
    	
        <td class="page_bg1" valign="top">
        	<table border="0" cellpadding="0" cellspacing="0">
                <tbody><tr>
                	<td valign="top">
                    	
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tbody><tr>
                                <td class="page_padding_type3" align="justify" valign="top">

						<center>
						<form name="info_form" id="info_form" action="main_pages.php" method="post">

						
						<p>&nbsp;</p>
						<p>
							</p><div id="request-options">
							<input checked="checked" name="request-phone" value="1" id="request-phone" type="checkbox"><label for="request-phone"></label>
							&nbsp; &nbsp; &nbsp; 
							<input checked="checked" name="request-booklet" value="1" id="request-booklet" type="checkbox"><label for="request-booklet"></label>
							&nbsp; &nbsp; &nbsp; 
							<input checked="checked" name="request-quote" value="1" id="request-quote" type="checkbox"><label for="request-quote"></label>
							</div>
						<p></p>
							
							<a name="form_msg"></a><br>


<div id="form_errors_msg" class="size14 msg_error bold"></div>
<div id="form-wrapper" style="display: block;">
    <div id="form-left">
        <div class="contact_form">
            <ul>
                <li>
                    <label for="fname">First Name</label>
                    <input name="fname" placeholder="Jane" required="required" type="text" >
                </li>
                <li>
                    <label for="lname">Last Name</label>
                    <input name="lname" placeholder="Doe" required="required" type="text">
                </li>
                <li>
                    <label for="company">Company</label>
                    <input name="company" placeholder="Jane Doe, Inc." type="text">
                </li>
                <li>
                    <label for="email">Email</label>
                    <input name="email" placeholder="jane.doe@mydomain.com" required="required" type="email">
                    <span class="form_hint">Proper format "jane.doe@mydomain.com"</span>
                </li>
                <li>
                    <label for="website">Website</label>
                    <input name="website" placeholder="mydomain.com" type="text">
                </li>
                <li>
                    <label for="bill_ct_1">Phone</label>
                    <select name="bill_ct_1" id="bill_ct_1" class="phone_type" required="required">
                    <option selected="selected" value="">Select</option>
                    <option value="home">Home</option>
                    <option value="cell">Cell</option>
                    <option value="work">Work</option>
                    <option value="fax">Fax</option>
                    </select>
                    <input name="bill_cn_1" class="phone" placeholder="555-555-5555" required="required" type="text">
                </li>
                <li id="add_phone_li">
                    <input name="bill_numbers" id="bill_numbers" class="nocookie" value="2" type="hidden">
                    &nbsp; &nbsp; &nbsp; &nbsp; 
                    <span class="baby_blue3 bold underline"><a onclick="javascript:add_phone();">+add contact number</a></span>
                </li>
                <li style="display: list-item;">
                    <label for="country">Country</label>
                    <select required="" name="country" id="country" class="menu1">
                                                <option selected="selected" value="US">United States</option>
                                                <option value="AF">Afghanistan</option>
                                                <option value="AL">Albania</option>
                                                <option value="DZ">Algeria</option>
                                                <option value="AS">American Samoa</option>
                                                <option value="AD">Andorra</option>
                                                <option value="AO">Angola</option>
                                                <option value="AI">Anguilla</option>
                                                <option value="AQ">Antarctica</option>
                                                <option value="AG">Antigua and Barbuda</option>
                                                <option value="AR">Argentina</option>
                                                <option value="AM">Armenia</option>
                                                <option value="AW">Aruba</option>
                                                <option value="AU">Australia</option>
                                                <option value="AT">Austria</option>
                                                <option value="AZ">Azerbaijan</option>
                                                <option value="BS">Bahamas</option>
                                                <option value="BH">Bahrain</option>
                                                <option value="BD">Bangladesh</option>
                                                <option value="BB">Barbados</option>
                                                <option value="BY">Belarus</option>
                                                <option value="BE">Belgium</option>
                                                <option value="BZ">Belize</option>
                                                <option value="BJ">Benin</option>
                                                <option value="BM">Bermuda</option>
                                                <option value="BT">Bhutan</option>
                                                <option value="BO">Bolivia</option>
                                                <option value="BA">Bosnia and Herzegovina</option>
                                                <option value="BW">Botswana</option>
                                                <option value="BV">Bouvet Island</option>
                                                <option value="BR">Brazil</option>
                                                <option value="IO">British Indian Ocean Territory</option>
                                                <option value="BN">Brunei Darussalam</option>
                                                <option value="BG">Bulgaria</option>
                                                <option value="BF">Burkina Faso</option>
                                                <option value="BI">Burundi</option>
                                                <option value="KH">Cambodia</option>
                                                <option value="CM">Cameroon</option>
                                                <option value="CA">Canada</option>
                                                <option value="CV">Cape Verde</option>
                                                <option value="KY">Cayman Islands</option>
                                                <option value="CF">Central African Republic</option>
                                                <option value="TD">Chad</option>
                                                <option value="CL">Chile</option>
                                                <option value="CN">China</option>
                                                <option value="CX">Christmas Island</option>
                                                <option value="CC">Cocos (Keeling) Islands</option>
                                                <option value="CO">Colombia</option>
                                                <option value="KM">Comoros</option>
                                                <option value="CG">Congo</option>
                                                <option value="CD">Congo, the Democratic Republic of the</option>
                                                <option value="CK">Cook Islands</option>
                                                <option value="CR">Costa Rica</option>
                                                <option value="CI">Cote D'Ivoire</option>
                                                <option value="HR">Croatia</option>
                                                <option value="CU">Cuba</option>
                                                <option value="CY">Cyprus</option>
                                                <option value="CZ">Czech Republic</option>
                                                <option value="DK">Denmark</option>
                                                <option value="DJ">Djibouti</option>
                                                <option value="DM">Dominica</option>
                                                <option value="DO">Dominican Republic</option>
                                                <option value="EC">Ecuador</option>
                                                <option value="EG">Egypt</option>
                                                <option value="SV">El Salvador</option>
                                                <option value="GQ">Equatorial Guinea</option>
                                                <option value="ER">Eritrea</option>
                                                <option value="EE">Estonia</option>
                                                <option value="ET">Ethiopia</option>
                                                <option value="FK">Falkland Islands (Malvinas)</option>
                                                <option value="FO">Faroe Islands</option>
                                                <option value="FJ">Fiji</option>
                                                <option value="FI">Finland</option>
                                                <option value="FR">France</option>
                                                <option value="GF">French Guiana</option>
                                                <option value="PF">French Polynesia</option>
                                                <option value="TF">French Southern Territories</option>
                                                <option value="GA">Gabon</option>
                                                <option value="GM">Gambia</option>
                                                <option value="GE">Georgia</option>
                                                <option value="DE">Germany</option>
                                                <option value="GH">Ghana</option>
                                                <option value="GI">Gibraltar</option>
                                                <option value="GR">Greece</option>
                                                <option value="GL">Greenland</option>
                                                <option value="GD">Grenada</option>
                                                <option value="GP">Guadeloupe</option>
                                                <option value="GU">Guam</option>
                                                <option value="GT">Guatemala</option>
                                                <option value="GN">Guinea</option>
                                                <option value="GW">Guinea-Bissau</option>
                                                <option value="GY">Guyana</option>
                                                <option value="HT">Haiti</option>
                                                <option value="HM">Heard Island and Mcdonald Islands</option>
                                                <option value="VA">Holy See (Vatican City State)</option>
                                                <option value="HN">Honduras</option>
                                                <option value="HK">Hong Kong</option>
                                                <option value="HU">Hungary</option>
                                                <option value="IS">Iceland</option>
                                                <option value="IN">India</option>
                                                <option value="ID">Indonesia</option>
                                                <option value="IR">Iran, Islamic Republic of</option>
                                                <option value="IQ">Iraq</option>
                                                <option value="IE">Ireland</option>
                                                <option value="IL">Israel</option>
                                                <option value="IT">Italy</option>
                                                <option value="JM">Jamaica</option>
                                                <option value="JP">Japan</option>
                                                <option value="JO">Jordan</option>
                                                <option value="KZ">Kazakhstan</option>
                                                <option value="KE">Kenya</option>
                                                <option value="KI">Kiribati</option>
                                                <option value="KP">Korea, Democratic People's Republic of</option>
                                                <option value="KR">Korea, Republic of</option>
                                                <option value="KW">Kuwait</option>
                                                <option value="KG">Kyrgyzstan</option>
                                                <option value="LA">Lao People's Democratic Republic</option>
                                                <option value="LV">Latvia</option>
                                                <option value="LB">Lebanon</option>
                                                <option value="LS">Lesotho</option>
                                                <option value="LR">Liberia</option>
                                                <option value="LY">Libyan Arab Jamahiriya</option>
                                                <option value="LI">Liechtenstein</option>
                                                <option value="LT">Lithuania</option>
                                                <option value="LU">Luxembourg</option>
                                                <option value="MO">Macao</option>
                                                <option value="MK">Macedonia, the Former Yugoslav Republic of</option>
                                                <option value="MG">Madagascar</option>
                                                <option value="MW">Malawi</option>
                                                <option value="MY">Malaysia</option>
                                                <option value="MV">Maldives</option>
                                                <option value="ML">Mali</option>
                                                <option value="MT">Malta</option>
                                                <option value="MH">Marshall Islands</option>
                                                <option value="MQ">Martinique</option>
                                                <option value="MR">Mauritania</option>
                                                <option value="MU">Mauritius</option>
                                                <option value="YT">Mayotte</option>
                                                <option value="MX">Mexico</option>
                                                <option value="FM">Micronesia, Federated States of</option>
                                                <option value="MD">Moldova, Republic of</option>
                                                <option value="MC">Monaco</option>
                                                <option value="MN">Mongolia</option>
                                                <option value="MS">Montserrat</option>
                                                <option value="MA">Morocco</option>
                                                <option value="MZ">Mozambique</option>
                                                <option value="MM">Myanmar</option>
                                                <option value="NA">Namibia</option>
                                                <option value="NR">Nauru</option>
                                                <option value="NP">Nepal</option>
                                                <option value="NL">Netherlands</option>
                                                <option value="AN">Netherlands Antilles</option>
                                                <option value="NC">New Caledonia</option>
                                                <option value="NZ">New Zealand</option>
                                                <option value="NI">Nicaragua</option>
                                                <option value="NE">Niger</option>
                                                <option value="NG">Nigeria</option>
                                                <option value="NU">Niue</option>
                                                <option value="NF">Norfolk Island</option>
                                                <option value="MP">Northern Mariana Islands</option>
                                                <option value="NO">Norway</option>
                                                <option value="OM">Oman</option>
                                                <option value="PK">Pakistan</option>
                                                <option value="PW">Palau</option>
                                                <option value="PS">Palestinian Territory, Occupied</option>
                                                <option value="PA">Panama</option>
                                                <option value="PG">Papua New Guinea</option>
                                                <option value="PY">Paraguay</option>
                                                <option value="PE">Peru</option>
                                                <option value="PH">Philippines</option>
                                                <option value="PN">Pitcairn</option>
                                                <option value="PL">Poland</option>
                                                <option value="PT">Portugal</option>
                                                <option value="PR">Puerto Rico</option>
                                                <option value="QA">Qatar</option>
                                                <option value="RE">Reunion</option>
                                                <option value="RO">Romania</option>
                                                <option value="RU">Russian Federation</option>
                                                <option value="RW">Rwanda</option>
                                                <option value="SH">Saint Helena</option>
                                                <option value="KN">Saint Kitts and Nevis</option>
                                                <option value="LC">Saint Lucia</option>
                                                <option value="PM">Saint Pierre and Miquelon</option>
                                                <option value="VC">Saint Vincent and the Grenadines</option>
                                                <option value="WS">Samoa</option>
                                                <option value="SM">San Marino</option>
                                                <option value="ST">Sao Tome and Principe</option>
                                                <option value="SA">Saudi Arabia</option>
                                                <option value="SN">Senegal</option>
                                                <option value="CS">Serbia and Montenegro</option>
                                                <option value="SC">Seychelles</option>
                                                <option value="SL">Sierra Leone</option>
                                                <option value="SG">Singapore</option>
                                                <option value="SK">Slovakia</option>
                                                <option value="SI">Slovenia</option>
                                                <option value="SB">Solomon Islands</option>
                                                <option value="SO">Somalia</option>
                                                <option value="ZA">South Africa</option>
                                                <option value="GS">South Georgia and the South Sandwich Islands</option>
                                                <option value="ES">Spain</option>
                                                <option value="LK">Sri Lanka</option>
                                                <option value="SD">Sudan</option>
                                                <option value="SR">Suriname</option>
                                                <option value="SJ">Svalbard and Jan Mayen</option>
                                                <option value="SZ">Swaziland</option>
                                                <option value="SE">Sweden</option>
                                                <option value="CH">Switzerland</option>
                                                <option value="SY">Syrian Arab Republic</option>
                                                <option value="TW">Taiwan, Province of China</option>
                                                <option value="TJ">Tajikistan</option>
                                                <option value="TZ">Tanzania, United Republic of</option>
                                                <option value="TH">Thailand</option>
                                                <option value="TL">Timor-Leste</option>
                                                <option value="TG">Togo</option>
                                                <option value="TK">Tokelau</option>
                                                <option value="TO">Tonga</option>
                                                <option value="TT">Trinidad and Tobago</option>
                                                <option value="TN">Tunisia</option>
                                                <option value="TR">Turkey</option>
                                                <option value="TM">Turkmenistan</option>
                                                <option value="TC">Turks and Caicos Islands</option>
                                                <option value="TV">Tuvalu</option>
                                                <option value="UG">Uganda</option>
                                                <option value="UA">Ukraine</option>
                                                <option value="AE">United Arab Emirates</option>
                                                <option value="GB">United Kingdom</option>
                                                <option value="US">United States</option>
                                                <option value="UM">United States Minor Outlying Islands</option>
                                                <option value="UY">Uruguay</option>
                                                <option value="UZ">Uzbekistan</option>
                                                <option value="VU">Vanuatu</option>
                                                <option value="VE">Venezuela</option>
                                                <option value="VN">Viet Nam</option>
                                                <option value="VG">Virgin Islands, British</option>
                                                <option value="VI">Virgin Islands, U.S.</option>
                                                <option value="WF">Wallis and Futuna</option>
                                                <option value="EH">Western Sahara</option>
                                                <option value="YE">Yemen</option>
                                                <option value="ZM">Zambia</option>
                                                <option value="ZW">Zimbabwe</option>
                                            </select>
                </li>
                <li style="display: list-item;">
                    <label for="address">Address</label>
                    <input required="" name="address" id="address" placeholder="123 Main St." type="text">
                </li>
                <li style="display: list-item;">
                    <label for="address2">Address 2</label>
                    <input name="address2" id="address2" placeholder="Suite A" type="text">
                </li>
                <li style="display: list-item;">
                    <label for="city">City</label>
                    <input required="" name="city" id="city" placeholder="Los Angeles" type="text">
                </li>
                <li style="display: list-item;">
                    <label for="state">State/Province</label>
                                        <select required="" name="state" id="state" class="menu1" onchange="vAddress(this, 3);" onkeyup="vAddress(this, 3);" onblur="vAddress(this, 3);">
                        <option selected="selected" value="">Select a State...</option>
                                                <option value="AK">Alaska</option>
                                                <option value="AL">Alabama</option>
                                                <option value="AS">American Samoa</option>
                                                <option value="AZ">Arizona</option>
                                                <option value="AR">Arkansas</option>
                                                <option value="CA">California</option>
                                                <option value="CO">Colorado</option>
                                                <option value="CT">Connecticut</option>
                                                <option value="DE">Delaware</option>
                                                <option value="DC">District of Columbia</option>
                                                <option value="FL">Florida</option>
                                                <option value="GA">Georgia</option>
                                                <option value="GU">Guam</option>
                                                <option value="HI">Hawaii</option>
                                                <option value="ID">Idaho</option>
                                                <option value="IL">Illinois</option>
                                                <option value="IN">Indiana</option>
                                                <option value="IA">Iowa</option>
                                                <option value="KS">Kansas</option>
                                                <option value="KY">Kentucky</option>
                                                <option value="LA">Louisiana</option>
                                                <option value="ME">Maine</option>
                                                <option value="MH">Marshall Islands</option>
                                                <option value="MD">Maryland</option>
                                                <option value="MA">Massachusetts</option>
                                                <option value="MI">Michigan</option>
                                                <option value="MN">Minnesota</option>
                                                <option value="MS">Mississippi</option>
                                                <option value="MO">Missouri</option>
                                                <option value="MT">Montana</option>
                                                <option value="NE">Nebraska</option>
                                                <option value="NV">Nevada</option>
                                                <option value="NH">New Hampshire</option>
                                                <option value="NJ">New Jersey</option>
                                                <option value="NM">New Mexico</option>
                                                <option value="NY">New York</option>
                                                <option value="NC">North Carolina</option>
                                                <option value="ND">North Dakota</option>
                                                <option value="OH">Ohio</option>
                                                <option value="OK">Oklahoma</option>
                                                <option value="OR">Oregon</option>
                                                <option value="PW">Palau</option>
                                                <option value="PA">Pennsylvania</option>
                                                <option value="PR">Puerto Rico</option>
                                                <option value="RI">Rhode Island</option>
                                                <option value="SC">South Carolina</option>
                                                <option value="SD">South Dakota</option>
                                                <option value="TN">Tennessee</option>
                                                <option value="TX">Texas</option>
                                                <option value="UT">Utah</option>
                                                <option value="VT">Vermont</option>
                                                <option value="VI">Virgin Islands</option>
                                                <option value="VA">Virginia</option>
                                                <option value="WA">Washington</option>
                                                <option value="WV">West Virginia</option>
                                                <option value="WI">Wisconsin</option>
                                                <option value="WY">Wyoming</option>
                                            </select>
                    <input name="province" id="province" onkeyup="javascript:vAddress(this, 3);" onblur="javascript:vAddress(this, 3);" style="display: none;" type="text">
                </li>
                <li style="display: list-item;">
                    <label for="zip">Zip</label>
                    <input required="" name="zip" id="zip" placeholder="12345" type="text">
                </li>
                <li>
                    <label for="howyouheard">How you found us?</label>
					<select name="howyouheard" id="howyouheard" class="" required="required">
						<option value="" selected="selected">Please Select</option>
						<option value="Google search">Google search</option>
						<option value="Bing search">Bing search</option>
						<option value="Yahoo search">Yahoo search</option>
						<option value="Other Search">Other Search</option>
						<option value="Returning client">Returning client</option>
						<option value="Friend/Relative referred">Friend/Relative referred</option>
						<option value="Magic Jump newsletter">Magic Jump newsletter</option>
						<option value="Trade Show">Trade Show</option>
						<option value="eBay">eBay</option>
						<option value="Moonwalk Forum">Moonwalk Forum</option>
						<option value="Other">Other (please specify)</option>
					</select>
				<br>
                    <textarea name="howyouheard_other" id="howyouheard_other" style="float:left; margin-left:140px; margin-top:10px; display:none;"></textarea>
                    <div style="clear:both;"></div>
                </li>
                <li>
                    <label for="interest">Interested in?</label>
				<select name="interest" id="interest" class="" required="required">
					<option value="" selected="selected">Please Select</option>
					<option value="FEC / Indoor Center">FEC / Indoor Center</option>
					<option value="Party Rental">Party Rental</option>
					<option value="Custom Project">Custom Project</option>
					<option value="Personal Use">Personal Use</option>
				</select>
                </li>
                <li>
                    <label for="comments">Comments</label>
                    <textarea name="comments" cols="20" rows="3"></textarea>
                </li>
            </ul>
        </div>
	</div>
    <div style="display: block;" id="form-right">
        <table id="selectedProdsTable" width="100%">
            <tbody><tr>
                <td style="padding-top:0px;">
                    <label for="product_1" class="grey3">Products I'm Interested In</label>
                    <select name="product_1" id="product_1" class="product">
                        <option selected="selected" value="">Please Select Product</option><optgroup label="OBSTACLE COURSES"><option value="299">32 Obstacle Course 32'x11'x11'</option><option value="173">35 Obstacle Course 35'x11'x8'</option><option value="93">40 Obstacle Course 40'x11'x12'</option><option value="379">50 Obstacle Course 50'x11'x16'</option></optgroup></select>
                </td>
                <td style="padding-top:0px;">
                    <label for="prodqty" class="grey3">Qty</label>
                    <input name="prodqty_1" id="prodqty_1" value="1" class="prodqty" onkeyup="javascript:set_total_products()" type="text">
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding-top:0px;">
                    <input name="num_prod_types" id="num_prod_types" class="nocookie" value="1" type="hidden">
                    <span class="baby_blue3 bold underline"><a onclick="javascript:add_product();">+add another product</a></span>
                </td>
            </tr>
        </tbody></table>
        <div style="border-top:1px solid #999; padding:10px 0 0 0; margin-top:10px;">
            <table width="100%">
                <tbody><tr>
                    <td class="grey3" style="padding-top:0px;">Total Products:</td>
                    <td class="grey3" id="total_prods" style="padding-top:0px;">1</td>
                </tr>
            </tbody></table>
        </div>
    </div>
    <div style="clear:both;">
        <div class="grey3" style="margin:10px 0 20px 10px; text-align:left;">
        <input name="mailinglist" id="mailinglist" value="1" checked="checked" class="nocookie" type="checkbox"> 
        I would like to receive exclusive promotions news and updates <br> &nbsp; &nbsp; &nbsp; (we promise the emails will only be magical, not spam!)
        </div>
		
        <table border="0" cellpadding="0" cellspacing="0">
            <tbody><tr>
                <td colspan="2" class="grey3" align="left" valign="middle">
                  For verification, please type the characters displayed in the image below.
                  <br>
                  <br>
                  <input name="cap_code" id="cap_code" value="104871" type="hidden">
                  <img src="captcha.php" align="absmiddle" height="35" width="157">
                  <div class="contact_form">
                    <br>
					
					<input name="captcha" id="captcha" type="text" class="warranty_input2 nocookie" placeholder="Verification Code" required="required" type="text">
					<?php echo $cap; ?>
                  </div>
                  <br>
                  <button class="submit" type="submit">Submit Form</button>
                </td>
               
                </tr>
				</tbody></table>
				</div>
			</div>
		</form> 
				
			

						<br><br>
		</center>
    </td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
            </tr>
        </tbody></table>
        </div>
        
        </div>
	</div>
</div>