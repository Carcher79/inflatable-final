{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7 " lang="{$lang_iso}"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7" lang="{$lang_iso}"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8" lang="{$lang_iso}"><![endif]-->
<!--[if gt IE 8]> <html class="no-js ie9" lang="{$lang_iso}"><![endif]-->
<html lang="{$lang_iso}">
	<head>
		<meta charset="utf-8" />
		<title>{$meta_title|escape:'html':'UTF-8'}</title>
{if isset($meta_description) AND $meta_description}
		<meta name="description" content="{$meta_description|escape:'html':'UTF-8'}" />
{/if}
{if isset($meta_keywords) AND $meta_keywords}
		<meta name="keywords" content="{$meta_keywords|escape:'html':'UTF-8'}" />
{/if}
		<meta name="robots" content="{if isset($nobots)}no{/if}index,{if isset($nofollow) && $nofollow}no{/if}follow" />
		{if isset($sttheme.responsive) && $sttheme.responsive && (!$sttheme.enabled_version_swithing || $sttheme.version_switching==0)}
		<meta name="viewport" content="width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0" /> 
        {/if}
		<meta name="apple-mobile-web-app-capable" content="yes" /> 
		<link rel="icon" type="image/vnd.microsoft.icon" href="{$favicon_url}?{$img_update_time}" />
		<link rel="shortcut icon" type="image/x-icon" href="{$favicon_url}?{$img_update_time}" />
		{if isset($sttheme.icon_iphone_57) && $sttheme.icon_iphone_57}
        <link rel="apple-touch-icon" sizes="57x57" href="{$sttheme.icon_iphone_57}" />
        {/if}
        {if isset($sttheme.icon_iphone_72) && $sttheme.icon_iphone_72}
        <link rel="apple-touch-icon" sizes="72x72" href="{$sttheme.icon_iphone_72}" />
        {/if}
        {if isset($sttheme.icon_iphone_114) && $sttheme.icon_iphone_114}
        <link rel="apple-touch-icon" sizes="114x114" href="{$sttheme.icon_iphone_114}" />
        {/if}
        {if isset($sttheme.icon_iphone_144) && $sttheme.icon_iphone_144}
        <link rel="apple-touch-icon" sizes="144x144" href="{$sttheme.icon_iphone_144}" />
        {/if}
{if isset($css_files)}
	{foreach from=$css_files key=css_uri item=media}
		<link rel="stylesheet" href="{$css_uri}" type="text/css" media="{$media}" />
	{/foreach}
{/if}
{if isset($sttheme.custom_css) && $sttheme.custom_css}
	<link href="{$sttheme.custom_css}" rel="stylesheet" type="text/css" media="{$sttheme.custom_css_media}" />
{/if}
		{$HOOK_HEADER}
		{if isset($sttheme.google_font_links)}{$sttheme.google_font_links}{/if}
		<!--[if IE 8]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body{if isset($page_name)} id="{$page_name|escape:'html':'UTF-8'}"{/if} class="{if isset($page_name)}{$page_name|escape:'html':'UTF-8'}{/if}{if isset($body_classes) && $body_classes|@count} {implode value=$body_classes separator=' '}{/if}{if $hide_left_column} hide-left-column{/if}{if $hide_right_column} hide-right-column{/if}{if isset($content_only) && $content_only} content_only{/if} lang_{$lang_iso} 
	{foreach from=$languages key=k item=language name="languages"}
		{if $language.iso_code == $lang_iso && $language.is_rtl}
			is_rtl
		{/if}
	{/foreach}
	">
	{if !isset($content_only) || !$content_only}
		{if isset($restricted_country_mode) && $restricted_country_mode}
			<div id="restricted-country">
				<p>{l s='You cannot place a new order from your country.'} <span class="bold">{$geolocation_country}</span></p>
			</div>
		{/if}
		<div id="body_wrapper">
			{if isset($sttheme.boxstyle) && $sttheme.boxstyle==2}<div id="page_wrapper">{/if}
			<header id="page_header">
				{capture name="displayBanner"}{hook h="displayBanner"}{/capture}
				{if isset($smarty.capture.displayBanner) && $smarty.capture.displayBanner|trim}
				<div class="banner">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
								{$smarty.capture.displayBanner}
							</div>
						</div>
					</div>
				</div>
				{/if}
				{capture name="displayNav"}{hook h="displayNav"}{/capture}
				{if isset($smarty.capture.displayNav) && $smarty.capture.displayNav|trim}
				<div id="top_bar" class="nav">
					<div class="container">
						<div class="row">
							<nav class="clearfix">{$smarty.capture.displayNav}</nav>
						</div>
					</div>
				</div>
				{/if}
				{if isset($sttheme.logo_position) && $sttheme.logo_position}
				    {assign var="logo_left_center" value=1}
				{else}
				    {assign var="logo_left_center" value=0}
				{/if}
				<section id="header" class="{if $logo_left_center} logo_center {/if}">
				    <div class="wide_container">
					    <div class="container">
					        <div class="row">
					            {if $logo_left_center}
								<div id="header_left" class="col-xs-12 col-sm-3 col-md-3 posi_rel">
					                <div id="header_left_inner" class="clearfix">{if isset($HOOK_TOP_LEFT) && $HOOK_TOP_LEFT|trim}{$HOOK_TOP_LEFT}{/if}</div>
					            </div>
					            {/if}
					            <div id="logo_wrapper" class="col-xs-12 {if $logo_left_center} col-sm-6 col-md-6 {else} col-sm-4 col-md-4 {/if}">
					            <div id="header_logo_inner">
								<a id="header_logo" href="{$base_dir}" title="{$shop_name|escape:'html':'UTF-8'}">
									<img class="logo" src="{$logo_url}" alt="{$shop_name|escape:'html':'UTF-8'}"{if isset($logo_image_width) && $logo_image_width} width="{$logo_image_width}"{/if}{if isset($logo_image_height) && $logo_image_height} height="{$logo_image_height}"{/if}/>
								</a>
					            </div>
					            </div>
								<div id="header_right" class="col-xs-12 {if $logo_left_center} col-sm-3 col-md-3 {else} col-sm-8 col-md-8 {/if} text-right">
					                <div id="header_right_inner" class="clearfix">{$HOOK_TOP}</div>
					            </div>
					        </div>
					    </div>
				    </div>
				</section>
	            {if isset($HOOK_TOP_SECONDARY) && $HOOK_TOP_SECONDARY}
	            <section id="top_extra">
	                {$HOOK_TOP_SECONDARY}
	            </section>
	            {/if}
	            
				<!-- Breadcrumb -->         
	            {if $page_name != 'index' 
	            && $page_name != 'pagenotfound'
	            && $page_name != 'module-stblog-default'
	            }
	            <div id="breadcrumb_wrapper" class="{if isset($sttheme.breadcrumb_width) && $sttheme.breadcrumb_width} wide_container {/if}"><div class="container"><div class="row">
	                <div class="col-xs-12 col-sm-12 col-md-12 clearfix">
	                	{include file="$tpl_dir./breadcrumb.tpl"}
	                </div>
	            </div></div></div>
	            {/if}
				<!--/ Breadcrumb -->
			</header>
			<div class="main_content_area">
			<div class="main_content_area_top"><div class="wide_container"></div></div>
			<!-- Main slideshow -->
            {if $page_name == 'index'}
	            {hook h='displayAnywhere' function="displayMainSlide" mod='stcameraslideshow' caller='stcameraslideshow'}
	            {hook h='displayAnywhere' function="displayMainSlide" mod='stiosslider' caller='stiosslider'}
	            {hook h='displayAnywhere' function="displayMainSlide" mod='revsliderprestashop' caller='revsliderprestashop'}
            {/if}
            {if $page_name == 'module-stblog-default'}
	            {hook h='displayAnywhere' function="displayBlogMainSlide" mod='stcameraslideshow' caller='stcameraslideshow'}
	            {hook h='displayAnywhere' function="displayBlogMainSlide" mod='stiosslider' caller='stiosslider'}
	            {hook h='displayAnywhere' function="displayBlogMainSlide" mod='revsliderprestashop' caller='revsliderprestashop'}
            {/if}
			<!--/ Main slideshow -->
			<div class="columns-container wide_container">
				<div id="columns" class="container">
					{capture name="displayTopColumn "}{hook h="displayTopColumn"}{/capture}
					{if isset($smarty.capture.displayTopColumn) && $smarty.capture.displayTopColumn|trim}
					<div class="row">
						<div id="top_column" class="center_column clearfix col-xs-12 col-sm-12 col-md-12">{$smarty.capture.displayTopColumn}</div>
					</div>
					{/if}
					<div class="row">
						{if isset($left_column_size) && !empty($left_column_size)}
						<div id="left_column" class="column col-xxs-8 col-xs-6 col-sm-{$left_column_size|intval} col-md-{$left_column_size|intval}">{$HOOK_LEFT_COLUMN}</div>
						{/if}
						{if isset($left_column_size) && isset($right_column_size)}{assign var='cols' value=(12 - $left_column_size - $right_column_size)}{else}{assign var='cols' value=12}{/if}
						<div id="center_column" class="center_column col-xs-12 col-sm-{$cols|intval} col-md-{$cols|intval}">
	{/if}