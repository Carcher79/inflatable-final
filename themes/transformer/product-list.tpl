{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if $page_name == 'category' || $page_name == 'prices-drop' || $page_name == 'best-sales' || $page_name == 'manufacturer' || $page_name == 'supplier' || $page_name == 'new-products' || $page_name == 'search'}
{if isset($HOOK_RIGHT_COLUMN) || isset($HOOK_LEFT_COLUMN) }
	{assign var='st_columns_nbr' value=1}
	{if isset($HOOK_LEFT_COLUMN) && $HOOK_LEFT_COLUMN|trim}{$st_columns_nbr=$st_columns_nbr+1}{/if}
	{if isset($HOOK_RIGHT_COLUMN) && $HOOK_RIGHT_COLUMN|trim}{$st_columns_nbr=$st_columns_nbr+1}{/if}
	{hook h='displayAnywhere' function='setColumnsNbr' columns_nbr=$st_columns_nbr page_name=$page_name mod='stthemeeditor' caller='stthemeeditor'}
	{capture name="st_columns_nbr"}{$st_columns_nbr}{/capture}
{/if}
{/if}

{if isset($products) && $products}
	{capture name="home_default-bbh_width"}{getWidthSize type='home_default-bbh'}{/capture}
	{capture name="home_default-bbh_height"}{getHeightSize type='home_default-bbh'}{/capture}
	{capture name="display_sd"}{hook h='displayAnywhere' function="getShortDescOnGrid" mod='stthemeeditor' caller='stthemeeditor'}{/capture}
	{*define numbers of product per line in other page for desktop*}

	{assign var='for_w' value='category'}
	{if isset($for_f) && $for_f}
		{$for_w=$for_f}
	{/if}

	{capture name="display_color_list"}{if $for_w=='category'}{hook h='displayAnywhere' function="getDisplayColorList"  mod='stthemeeditor' caller='stthemeeditor'}{else}hidden{/if}{/capture}

	{capture name="nbItemsPerLineDesktop"}{hook h='displayAnywhere' function='getProductsPerRow' for_w=$for_w devices='lg' mod='stthemeeditor' caller='stthemeeditor'}{/capture}
	{capture name="nbItemsPerLine"}{hook h='displayAnywhere' function='getProductsPerRow' for_w=$for_w devices='md' mod='stthemeeditor' caller='stthemeeditor'}{/capture}
	{capture name="nbItemsPerLineTablet"}{hook h='displayAnywhere' function='getProductsPerRow' for_w=$for_w devices='sm' mod='stthemeeditor' caller='stthemeeditor'}{/capture}
	{capture name="nbItemsPerLineMobile"}{hook h='displayAnywhere' function='getProductsPerRow' for_w=$for_w devices='xs' mod='stthemeeditor' caller='stthemeeditor'}{/capture}
	{capture name="nbItemsPerLinePortrait"}{hook h='displayAnywhere' function='getProductsPerRow' for_w=$for_w devices='xxs' mod='stthemeeditor' caller='stthemeeditor'}{/capture}

	{*define numbers of product per line in other page for tablet*}

	{assign var='nbLi' value=$products|@count}
	{math equation="nbLi/nbItemsPerLineDesktop" nbLi=$nbLi nbItemsPerLineDesktop=$smarty.capture.nbItemsPerLineDesktop assign=nbLinesDesktop}
	{math equation="nbLi/nbItemsPerLine" nbLi=$nbLi nbItemsPerLine=$smarty.capture.nbItemsPerLine assign=nbLines}
	{math equation="nbLi/nbItemsPerLineTablet" nbLi=$nbLi nbItemsPerLineTablet=$smarty.capture.nbItemsPerLineTablet assign=nbLinesTablet}
	{math equation="nbLi/nbItemsPerLineMobile" nbLi=$nbLi nbItemsPerLineMobile=$smarty.capture.nbItemsPerLineMobile assign=nbLinesMobile}
	{math equation="nbLi/nbItemsPerLinePortrait" nbLi=$nbLi nbItemsPerLinePortrait=$smarty.capture.nbItemsPerLinePortrait assign=nbLinesPortrait}

	<!-- Products list -->
	<ul{if isset($id) && $id} id="{$id}"{/if} class="product_list grid row{if isset($class) && $class} {$class}{/if}{if isset($active) && $active == 1} active{/if} " data-classnames="col-lg-{(12/$smarty.capture.nbItemsPerLineDesktop)|replace:'.':'-'} col-md-{(12/$smarty.capture.nbItemsPerLine)|replace:'.':'-'} col-sm-{(12/$smarty.capture.nbItemsPerLineTablet)|replace:'.':'-'} col-xs-{(12/$smarty.capture.nbItemsPerLineMobile)|replace:'.':'-'} col-xxs-{(12/$smarty.capture.nbItemsPerLinePortrait)|replace:'.':'-'}" data-default-view="{if $for_w=='category'}{hook h='displayAnywhere' function="getCategoryDefaultView" mod='stthemeeditor' caller='stthemeeditor'}{/if}">
	{foreach from=$products item=product name=products}
		{math equation="(total%perLine)" total=$smarty.foreach.products.total perLine=$smarty.capture.nbItemsPerLineDesktop assign=totModuloDesktop}
		{math equation="(total%perLine)" total=$smarty.foreach.products.total perLine=$smarty.capture.nbItemsPerLine assign=totModulo}
		{math equation="(total%perLine)" total=$smarty.foreach.products.total perLine=$smarty.capture.nbItemsPerLineTablet assign=totModuloTablet}
		{math equation="(total%perLine)" total=$smarty.foreach.products.total perLine=$smarty.capture.nbItemsPerLineMobile assign=totModuloMobile}
		{math equation="(total%perLine)" total=$smarty.foreach.products.total perLine=$smarty.capture.nbItemsPerLinePortrait assign=totModuloPortrait}
		{if $totModuloDesktop == 0}{assign var='totModuloDesktop' value=$smarty.capture.nbItemsPerLineDesktop}{/if}
		{if $totModulo == 0}{assign var='totModulo' value=$smarty.capture.nbItemsPerLine}{/if}
		{if $totModuloTablet == 0}{assign var='totModuloTablet' value=$smarty.capture.nbItemsPerLineTablet}{/if}
		{if $totModuloMobile == 0}{assign var='totModuloMobile' value=$smarty.capture.nbItemsPerLineMobile}{/if}
		{if $totModuloPortrait == 0}{assign var='totModuloPortrait' value=$smarty.capture.nbItemsPerLinePortrait}{/if}
		<li class="ajax_block_product col-lg-{(12/$smarty.capture.nbItemsPerLineDesktop)|replace:'.':'-'} col-md-{(12/$smarty.capture.nbItemsPerLine)|replace:'.':'-'} col-sm-{(12/$smarty.capture.nbItemsPerLineTablet)|replace:'.':'-'} col-xs-{(12/$smarty.capture.nbItemsPerLineMobile)|replace:'.':'-'} col-xxs-{(12/$smarty.capture.nbItemsPerLinePortrait)|replace:'.':'-'}
		{if $smarty.foreach.products.iteration%$smarty.capture.nbItemsPerLineDesktop == 0} last-item-of-desktop-line{elseif $smarty.foreach.products.iteration%$smarty.capture.nbItemsPerLineDesktop == 1} first-item-of-desktop-line{/if}{if $smarty.foreach.products.iteration > ($smarty.foreach.products.total - $totModuloDesktop)} last-desktop-line{/if}{if $smarty.foreach.products.index < $smarty.capture.nbItemsPerLineDesktop} first-desktop-line{/if}
		{if $smarty.foreach.products.iteration%$smarty.capture.nbItemsPerLine == 0} last-in-line{elseif $smarty.foreach.products.iteration%$smarty.capture.nbItemsPerLine == 1} first-in-line{/if}{if $smarty.foreach.products.iteration > ($smarty.foreach.products.total - $totModulo)} last-line{/if}{if $smarty.foreach.products.index < $smarty.capture.nbItemsPerLine} first-line{/if}
		{if $smarty.foreach.products.iteration%$smarty.capture.nbItemsPerLineTablet == 0} last-item-of-tablet-line{elseif $smarty.foreach.products.iteration%$smarty.capture.nbItemsPerLineTablet == 1} first-item-of-tablet-line{/if}{if $smarty.foreach.products.iteration > ($smarty.foreach.products.total - $totModuloTablet)} last-tablet-line{/if}{if $smarty.foreach.products.index < $smarty.capture.nbItemsPerLineTablet} first-tablet-line{/if}
		{if $smarty.foreach.products.iteration%$smarty.capture.nbItemsPerLineMobile == 0} last-item-of-mobile-line{elseif $smarty.foreach.products.iteration%$smarty.capture.nbItemsPerLineMobile == 1} first-item-of-mobile-line{/if}{if $smarty.foreach.products.iteration > ($smarty.foreach.products.total - $totModuloMobile)} last-mobile-line{/if}{if $smarty.foreach.products.index < $smarty.capture.nbItemsPerLineMobile} first-mobile-line{/if}
		{if $smarty.foreach.products.iteration%$smarty.capture.nbItemsPerLinePortrait == 0} last-item-of-portrait-line{elseif $smarty.foreach.products.iteration%$smarty.capture.nbItemsPerLinePortrait == 1} first-item-of-portrait-line{/if}{if $smarty.foreach.products.iteration > ($smarty.foreach.products.total - $totModuloPortrait)} last-portrait-line{/if}{if $smarty.foreach.products.index < $smarty.capture.nbItemsPerLinePortrait} first-portrait-line{/if}">
			<div class="product-container" itemscope itemtype="http://schema.org/Product">
				<div class="pro_first_box">
					<a class="product_img_link"	href="{$product.link|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" itemprop="url">
						<img class="replace-2x img-responsive front-image" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default-bbh')|escape:'html':'UTF-8'}" alt="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" title="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" width="{$smarty.capture.home_default-bbh_width}" height="{$smarty.capture.home_default-bbh_height}" itemprop="image" />
						{hook h='displayAnywhere' function='getHoverImage' id_product=$product.id_product product_link_rewrite=$product.link_rewrite home_default-bbh_height=$smarty.capture.home_default-bbh_height home_default-bbh_width=$smarty.capture.home_default-bbh_width product_name=$product.name mod='sthoverimage' caller='sthoverimage'}
						{if isset($product.new) && $product.new == 1}<span class="new"><i>{l s='New'}</i></span>{/if}{if isset($product.on_sale) && $product.on_sale && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}<span class="on_sale"><i>{l s='Sale'}</i></span>{/if}						
	                    {if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
							{if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}
								{if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
		                            {if $product.specific_prices && $product.specific_prices.reduction_type=='percentage'}
		                            	{hook h='displayAnywhere' function='getSaleStyleCircle' percentage_amount='percentage' reduction=$product.specific_prices.reduction price_without_reduction=$product.price_without_reduction price=$product.price mod='stthemeeditor' caller='stthemeeditor'}
		                            {elseif $product.specific_prices && $product.specific_prices.reduction_type=='amount' && $product.specific_prices.reduction|intval !=0}
		                            	{hook h='displayAnywhere' function='getSaleStyleCircle' percentage_amount='amount' reduction=$product.specific_prices.reduction price_without_reduction=$product.price_without_reduction price=$product.price mod='stthemeeditor' caller='stthemeeditor'}
		                            {/if}
		                        {/if}
	                        {/if}
	                    {/if}
					</a>
	                

	                {capture name="pro_a_compare"}
	                    {if isset($comparator_max_item) && $comparator_max_item}
							<a class="add_to_compare" href="{$product.link|escape:'html':'UTF-8'}" data-id-product="{$product.id_product}" rel="nofollow" data-product-cover="{$link->getImageLink($product.link_rewrite, $product.id_image, 'thumb_default')|escape:'html':'UTF-8'}" data-product-name="{$product.name|escape:'html':'UTF-8'}"><div><i class="icon-ajust icon-0x icon-mar-lr2"></i><span>{l s='Add to compare'}</span></div></a>
	        			{/if} 
	                {/capture}
	                {capture name="pro_a_wishlist"}
	                    {hook h='displayAnywhere' function="getAddToWhishlistButton" id_product=$product.id_product show_icon=0 mod='stthemeeditor' caller='stthemeeditor'}    
	                {/capture}
	                {capture name="pro_quick_view"}
	                    {if isset($quick_view) && $quick_view}
	                        <a class="quick-view" href="{$product.link|escape:'html':'UTF-8'}" rel="{$product.link|escape:'html':'UTF-8'}"><div><i class="icon-search-1 icon-0x icon-mar-lr2"></i><span>{l s='Quick view'}</span></div></a>
	                    {else}
	                        <a href="{$product.link|escape:'html':'UTF-8'}" class="pro_more_info" rel="nofollow" title="{l s='More info'}"><div><i class="icon-link icon-0x icon-mar-lr2"></i><span>{l s='More info'}</span></div></a>
	                    {/if}
	                {/capture}
	                {assign var="fly_i" value=1}
	                {if trim($smarty.capture.pro_a_cart)}{assign var="fly_i" value=$fly_i+1}{/if}
	                {if trim($smarty.capture.pro_a_compare)}{assign var="fly_i" value=$fly_i+1}{/if}
	                {if trim($smarty.capture.pro_a_wishlist)}{assign var="fly_i" value=$fly_i+1}{/if}
	                <div class="hover_fly {hook h='displayAnywhere' function='getFlyoutButtonsClass' mod='stthemeeditor' caller='stthemeeditor'} fly_{$fly_i} clearfix">
	                    {$smarty.capture.pro_a_cart}
	                    {if trim($smarty.capture.pro_quick_view)}
	                        {$smarty.capture.pro_quick_view}
	                    {else}
	                        <a itemprop="url" href="{$product.link|escape:'html':'UTF-8'}" class="pro_more_info" rel="nofollow" title="{l s='More info'}"><div><i class="icon-link icon-0x icon-mar-lr2"></i><span>{l s='More info'}</span></div></a>
	                    {/if}
	                    {$smarty.capture.pro_a_compare}
	                    {$smarty.capture.pro_a_wishlist}
	                </div>
				</div>
	        	<div class="pro_second_box">
	        		<h5 itemprop="name" class="s_title_block {hook h='displayAnywhere' function='getProductNameClass' mod='stthemeeditor' caller='stthemeeditor'}">{if isset($product.pack_quantity) && $product.pack_quantity}{$product.pack_quantity|intval|cat:' x '}{/if}<a class="product-name" href="{$product.link|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" itemprop="url" >{$product.reference} | {hook h='displayAnywhere' function='getLengthOfProductName' product_name=$product.name mod='stthemeeditor' caller='stthemeeditor'}</a></h5>
					{if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
					<div class="price_container" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
						{if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}<span itemprop="price" class="price product-price">{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}</span>
						<meta itemprop="priceCurrency" content="{$currency->iso_code}" />
	                    {if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
	                        <span class="old-price product-price">{displayWtPrice p=$product.price_without_reduction}</span>
	                        {if $product.specific_prices && $product.specific_prices.reduction_type=='percentage'}
	                        	{hook h='displayAnywhere' function='getSaleStyleFlag' percentage_amount='percentage' reduction=$product.specific_prices.reduction price_without_reduction=$product.price_without_reduction price=$product.price mod='stthemeeditor' caller='stthemeeditor'}
	                        {elseif $product.specific_prices && $product.specific_prices.reduction_type=='amount' && $product.specific_prices.reduction|intval !=0}
	                        	{hook h='displayAnywhere' function='getSaleStyleFlag' percentage_amount='amount' reduction=$product.specific_prices.reduction price_without_reduction=$product.price_without_reduction price=$product.price mod='stthemeeditor' caller='stthemeeditor'}
	                        {/if}
	                    {/if}
	                    {/if}
					</div>
					{if isset($product.quantity) && $product.quantity > 0}
                	<p style="background: #00a161; color: #FFF; padding: 3px;">IN STOCK</p>
                    {/if}
				    {if isset($product.online_only) && $product.online_only}<div class="mar_b6 product_online_only_flags"><span class="online_only sm_lable">{l s='Online only'}</span></div>{/if}
					{/if}					
					{if isset($product.on_sale) && $product.on_sale && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}
					{elseif isset($product.reduction) && $product.reduction && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}
						<div class="mar_b6 product_discount_flags"><span class="discount sm_lable">{l s='Reduced price!'}</span></div>
					{/if}
					{if (!$PS_CATALOG_MODE && $PS_STOCK_MANAGEMENT && ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
						{if isset($product.available_for_order) && $product.available_for_order && !isset($restricted_country_mode)}
							<div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="availability product_stock_info mar_b6">
								{if ($product.allow_oosp || $product.quantity > 0)}
									<span class="{if $product.quantity <= 0}out-of-stock{else}available-now{/if} hidden sm_lable">
										<link itemprop="availability" href="http://schema.org/InStock" />{if $product.quantity <= 0}{if $product.allow_oosp}{$product.available_later}{else}{l s='Out of stock'}{/if}{else}{if isset($product.available_now) && $product.available_now}{$product.available_now}{else}{l s='In Stock'}{/if}{/if}
									</span>
								{elseif (isset($product.quantity_all_versions) && $product.quantity_all_versions > 0)}
									<span class="available-dif sm_lable">
										<link itemprop="availability" href="http://schema.org/LimitedAvailability" />{l s='Product available with different options'}
									</span>
								{else}
									<span class="out-of-stock sm_lable">
										<link itemprop="availability" href="http://schema.org/OutOfStock" />{l s='Out of stock'}
									</span>
								{/if}
							</div>
						{/if}
					{/if}
					{if isset($product.color_list)}
						<div class="color-list-container {$smarty.capture.display_color_list}">{$product.color_list} </div>
					{/if}
	                {hook h='displayAnywhere' function="getProductRatingAverage" id_product=$product.id_product mod='stthemeeditor' caller='stthemeeditor'}
	                {if $for_w=='category'}{hook h='displayAnywhere' function="getProductAttributes" id_product=$product.id_product mod='stthemeeditor' caller='stthemeeditor'}{/if}
	                <p class="product-desc {$smarty.capture.display_sd} " itemprop="description">{$product.description_short|strip_tags:'UTF-8'|truncate:360:'...'}</p>
	                <div class="act_box">
	                    {$smarty.capture.pro_a_cart}
	                    <div class="act_box_inner">
	                    {$smarty.capture.pro_a_compare}
	                    {$smarty.capture.pro_a_wishlist}
	                    {if trim($smarty.capture.pro_quick_view)}
	                        {$smarty.capture.pro_quick_view}
	                    {/if}
	                    </div>
	                </div>
	        	</div>
	        </div>
		</li>
	{/foreach}
	</ul>
{addJsDefL name=min_item}{l s='Please select at least one product' js=1}{/addJsDefL}
{addJsDefL name=max_item}{l s='You cannot add more than %d product(s) to the product comparison' sprintf=$comparator_max_item js=1}{/addJsDefL}
{/if}