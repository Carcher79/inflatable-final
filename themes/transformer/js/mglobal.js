// JavaScript Document
if( document.getElementById ) {
	getElemById = function( id ) {
		return document.getElementById( id );
	}
} else if( document.all ) {
	getElemById = function( id ) {
		return document.all[ id ];
	}
} else if( document.layers ) {
	getElemById = function( id ) {
		return document.layers[ id ];
	}
}

function clickclear(thisfield, defaulttext) {
	if (thisfield.value == defaulttext) {
		thisfield.value = "";
	}
}

function clickrecall(thisfield, defaulttext) {
	if (thisfield.value == "") {
		thisfield.value = defaulttext;
	}
}

function showhide_swap(id) {
	if ( getElemById(id).style.display == 'none' ) 
		getElemById(id).style.display = '';
	else
		getElemById(id).style.display = 'none';
}

function hide_elem(id) {
	if(getElemById(id).style.display == '')
	{
		getElemById(id).style.display = 'none';
		return true;
	}
	else return false;
}

function show_elem(id) {
	if(getElemById(id).style.display == 'none')
	{
		getElemById(id).style.display = '';
		return true;
	}
	else return false;
}

function showhide_swap_class(elem_class) {
	if( $("."+elem_class).is(":visible") )
		$("."+elem_class).hide();
	else
		$("."+elem_class).hide();
}

function fade_in(id, speed) {
	$("#"+id).fadeIn(speed);
}

function fade_out(id, speed) {
	$("#"+id).fadeOut(speed);
}

function fade_toggle(id, speed) {
	if( $("#"+id).is(":visible") )
		$("#"+id).fadeOut(speed);
	else
		$("#"+id).fadeIn(speed);
}

function changeVal( id, id_val) {
	getElemById( id ).value = id_val;
}

function printDateTime() {
	var now = new Date();
	var month = now.getMonth() + 1;
	var day = now.getDate();
	var year = now.getFullYear();
	var hour = now.getHours();
	var minute = now.getMinutes();
	var second = now.getSeconds();
	var ampm = "am";
	// build date time string in format mm/dd/yyyy hh:ii:ss am/pm
	var date = "";
	if( month < 10 ) date += "0";
	date += month.toString() + "/";
	if( day < 10 ) date += "0";
	date += day.toString() + "/";
	date += year.toString() + " ";
	// hours we'll convert to 12 hour am/pm format
	if( hour > 12 ) {
		hour -= 12;
		ampm = "pm";
	}
	date += hour.toString() + ":";
	if( minute < 10 ) date += "0";
	date += minute.toString() + ":";
	if( second < 10 ) date += "0";
	date += second.toString();
	date += ampm;
	// return date string
	return date;
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/* overlay pop up / fade in div for login and password recovery and registration js */
function show_overlay() {
	$("#overlay").fadeIn(700);
}
function hide_overlay() {
	$("#overlay").fadeOut(500);
}
function show_ship_overlay(shipto_id, page)
{
	$.ajax({
		type: "POST",
		url: "/components/shipping_window.cmp.php",
		cache: false,
		data: "shipto_id="+shipto_id+"&page="+page+"&live=187&",
		error: function() 
		{
			alert('error loading shipping address');
		},
		success: function(ret_val)
		{
			$("#ship_address_popup").html(ret_val); // update window
			$("#overlay2").fadeIn(700); // fade in window
		}
	});
}
function hide_ship_overlay() {
	$("#overlay2").fadeOut(500);
}
function show_cvv2_overlay() {
	$("#overlay3").fadeIn(700);
}
function hide_cvv2_overlay() {
	$("#overlay3").fadeOut(500);
}
function show_dynamic_overlay(id) {
	$("#"+id).fadeIn(700);
}
function hide_dynamic_overlay(id) {
	$("#"+id).fadeOut(500);
}
/* overlay pop up / fade in div for login and password recovery and registration etc. */

function countrySelectUpdate(objList, type)
{
	if(type == null)
	{
		if (objList.value == 'US')
		{
			document.getElementById("state_div").style.display = "";
			document.getElementById("province_div").style.display = "none";
		}
		else
		{
			document.getElementById("state_div").style.display = "none";
			document.getElementById("province_div").style.display = "";
		}
	}
	else if(type == "ship")
	{
		if (objList.value == 'US')
		{
			document.getElementById("ship_state_div").style.display = "";
			document.getElementById("ship_province_div").style.display = "none";
		}
		else
		{
			document.getElementById("ship_state_div").style.display = "none";
			document.getElementById("ship_province_div").style.display = "";
		}
	}
	else
	{
		if (objList.value == 'US')
		{
			document.getElementById(type+"_state_div").style.display = "";
			document.getElementById(type+"_province_div").style.display = "none";
		}
		else
		{
			document.getElementById(type+"_state_div").style.display = "none";
			document.getElementById(type+"_province_div").style.display = "";
		}
	}
}

function setRegLink(url)
{
	//alert(url);
	getElemById('reglink').href = url;
}

// function to add additional numbers to billing address and shipping address forms
// requires standard form format to work correctly in all pages... review /components/acct_edit_billing.cmp.php for example
function add_number(type)
{
	var count = parseInt ( $("input#"+type+"_numbers").val() );
	var container = getElemById("new_"+type+"_numbers");
	var new_num = document.createElement("div");
	new_num.style.padding = "0px 0px 10px 0px";
	var html = '<select name="'+type+'_ct_'+count+'" id="'+type+'_ct_'+count+'" class="warranty_select">';
	html += '<option value="">Select</option>';
	html += '<option value="home">Home</option>';
	html += '<option value="cell">Cell</option>';
	html += '<option value="work">Work</option>';
	html += '<option value="fax">Fax</option>';
	html += '</select> &nbsp; ';
	html += '<input type="text" name="'+type+'_cn_'+count+'" id="'+type+'_cn_'+count+'"  value="" class="warranty_input2" />';
	new_num.innerHTML = html;
	container.appendChild(new_num);
	count++;
	getElemById(type+"_numbers").value = count;
}

// improvements made and versatility added to the above function
// to see an example check out /office/components/clients/details.cmp.php
function add_number_v2(optional_prefix, class1, class2, class3)
{
	var count = parseInt ( $("input#"+optional_prefix+"numbers").val() );
	var html = $("#"+optional_prefix+"new_numbers").html();
	html += '<div class="'+class1+'">';
	html += '<select name="'+optional_prefix+'ct_'+count+'" id="'+optional_prefix+'ct_'+count+'" class="'+class2+'">';
	html += '<option value="">Select</option>';
	html += '<option value="home">Home</option>';
	html += '<option value="cell">Cell</option>';
	html += '<option value="work">Work</option>';
	html += '<option value="fax">Fax</option>';
	html += '</select> ';
	html += '<input type="text" name="'+optional_prefix+'cn_'+count+'" id="'+optional_prefix+'cn_'+count+'"  value="" class="'+class3+'" />';
	html += '</div>';
	// replace with additional content
	$("#"+optional_prefix+"new_numbers").html(html);
	// update count
	$("input#"+optional_prefix+"numbers").val(count+1);
}

function addtocart(prod_id, item_number) {
	$("img#acart_loading").css("display",""); // turn on loading img
	$("img#buy_button").css("display","none"); // turn off button
	
	$.ajax({
		type: "GET",
		url: "/gloinc/mj_global.inc.php",
		cache: false,
		data: "id="+prod_id+"&i="+item_number+"&xaddtocart=187",
		error: function() 
		{
			//alert('unexpected error');
			$("img#acart_loading").css("display","none"); // turn off loading img
			$("img#buy_button").css("display",""); // turn on buy button
		},
		success: function(ret_val)
		{
			//alert(ret_val);
			$("img#acart_loading").css("display","none"); // turn off loading img
			$("img#viewc_button").css("display",""); // turn on view cart button
			// increment cart count
			c = parseInt( $("input#cart_count_box").val() ) + 1;
			//alert(c);
			$("input#cart_count_box").val( c );
		}
	});
}

function returnRadioValue(radio)
{
	var radioLength = radio.length;
	if(radioLength == undefined){
		if(radio.checked){
			//alert(radio.value);
			return radio.value;
		}
		else{
			return null;
		}
	}
	for(var i = 0; i < radioLength; i++){
		if(radio[i].checked) {
			//alert(radio[i].value);
			return radio[i].value;
		}
	}
	return null;
}

function validateCreditCard(s) 
{
	// remove non-numerics
	var v = "0123456789";
	var w = "";
	for (i=0; i < s.length; i++) {
		x = s.charAt(i);
		if (v.indexOf(x,0) != -1)
			w += x;
	}
	// validate number
	j = w.length / 2;
	if (j < 6.5 || j > 8 || j == 7) return false;
	k = Math.floor(j);
	m = Math.ceil(j) - k;
	c = 0;
	for (i=0; i<k; i++) {
		a = w.charAt(i*2+m) * 2;
		c += a > 9 ? Math.floor(a/10 + a%10) : a;
	}
	for (i=0; i<k+m; i++) 
		c += w.charAt(i*2+1-m) * 1;
	return (c%10 == 0);
}

function check_terms_accepted()
{
	if( getElemById("agreetoterms").checked )
		return true;
	else
	{
		alert("Please read and agree to the terms and conditions.");
		return false;
	}
}

// function used to highlight selected shipping address during checkout step 2
// uses add_count to track the number of shipping addresses so we can turn on / off highlighting
function highlightCOS(highlight_id) {
	count = parseInt( $("input#add_count").val() );
	
	for( i=0; i<count; i++) {
		getElemById( "hc1_"+i ).style.backgroundColor = "";
		getElemById( "hc2_"+i ).style.backgroundColor = "";
	}
	getElemById( "hc1_"+highlight_id ).style.backgroundColor = "#dceef0";
	getElemById( "hc2_"+highlight_id ).style.backgroundColor = "#dceef0";
}
function highlightSMETH(method_id) {
	$("#ship_method_bg").removeClass("bg_error");
	for( i=1; i<=4; i++) {
		if( getElemById( "ship"+i ) )
			getElemById( "ship"+i ).style.backgroundColor = "";
	}
	getElemById( "ship"+method_id ).style.backgroundColor = "#dceef0";
}
function highlightAMOUNT(method_id) {
	for( i=1; i<=2; i++) {
		getElemById( "amount"+i ).style.backgroundColor = "";
	}
	getElemById( "amount"+method_id ).style.backgroundColor = "#dceef0";
}
function highlightPAY(method_id) {
	for( i=1; i<=4; i++) {
		getElemById( "pay"+i ).style.backgroundColor = "";
		getElemById( "paydetail"+i ).style.display = "none";
	}
	getElemById( "pay"+method_id ).style.backgroundColor = "#dceef0";
	getElemById( "paydetail"+method_id ).style.display = "";
}

// check all function that clicking an input checkbox will go through the form and check the rest of them 
// based on the id of the check all checkbox
function check_all(form, checkall_id)
{ 
	for (var i=1; i<form.elements.length; i++)
		form.elements[i].checked = getElemById(checkall_id).checked;
} 
// shows / hides non-distributor and distributor div fields
function office_update_user_form_display()
{
	var sel = getElemById("userlevel").value;
	if(sel == "4")
	{
		hide_elem("non_distributor_fields");
		show_elem("distributor_fields1");
		show_elem("distributor_fields2");
	}
	else
	{
		hide_elem("distributor_fields1");
		hide_elem("distributor_fields2");
		show_elem("non_distributor_fields");
	}
}

// javascript live login validation with jquery
function live_login() {
	var username = escape( $("input#username").val() );
	var password = escape( $("input#password").val() );
	var page = $("input#forward_to").val();
	$("#login_err_msg").text("");
	
	$.ajax({
		type: "POST",
		url: "/gloinc/mj_global.inc.php",
		cache: false,
		data: "username="+username+"&password="+password+"&xlogin=1",
		error: function() 
		{
				$("#login_err_msg").text("Login attempt failed");
		},
		success: function(ret_val)
		{
			if(ret_val == "success") {
				// login successful, close login box and forward to correct page or reload current page
				hide_overlay();
				window.location = page;
			}
			else {
				// login failed, display error
				$("#login_err_msg").text("Login credentials invalid");
			}
		}
	});
}


// live logout feature fo sho
function live_logout() {
	$.ajax({
		type: "POST",
		url: "/gloinc/mj_global.inc.php",
		cache: false,
		data: "xlogout=1",
		success: function(ret_val)
		{
			// shouldnt really ever fail... just clears session... just reload the page or send to home page i guess
			window.location = "http://www.magicjump.com";
		}
	});
}

// live password request feature fo sho
function live_pwreq() {
	var username_email = escape( $("input#username_email").val() );
	$("#password_err_msg").text("");
	$("img#pwreq_loading").css("display",""); // turn on loading img
	$("img#pwreq_button").css("display","none"); // turn off button
	$.ajax({
		type: "POST",
		url: "/gloinc/mj_global.inc.php",
		cache: false,
		data: "username_email="+username_email+"&xpasswordreq=1",
		error: function() 
		{
			//alert('unexpected error');
			$("#password_err_msg").text("unexpected error");
			$("img#pwreq_loading").css("display","none"); // turn off loading img
			$("img#pwreq_button").css("display",""); // turn on button
		},
		success: function(ret_val)
		{
			$("img#pwreq_loading").css("display","none"); // turn off loading img
			$("img#pwreq_button").css("display",""); // turn on button
			// could fail with mail send not working or email not existing in database, account for both errors
			if(ret_val == "fail") {
				//alert('mail error try again later');
				$("#password_err_msg").text("Mail error try again later");
			}
			if(ret_val == "fail-email") {
				//alert('email not in system');
				$("#password_err_msg").text("Email not in system");
			}
			// on a success we should probably just flip mode the login window to a your email has been sent deal, fo sho
			if(ret_val == "success") {
				//alert('password email sent');
				hide_elem('logregpass_box'); 
				show_elem('mailsent_box');
			}
		}
	});
}

// reset login pop up box on load / close
function login_box_reset()
{
	hide_elem('mailsent_box'); 
	show_elem('logregpass_box');
	hide_elem('password_req_box'); 
	show_elem('login_req_box');	
	$("#login_err_msg").text("");
	$("#password_err_msg").text("");
	$("#username").removeClass("input_error");
	$("#password").removeClass("input_error");
	$("#username_email").removeClass("input_error");
}

// user account billing information update
function live_billing_update() {
	var fname = escape( $("input#fname").val() );
	var mname = escape( $("input#mname").val() );
	var lname = escape( $("input#lname").val() );
	var company = escape( $("input#company").val() );
	var website = escape( $("input#website").val() );
	var address = escape( $("input#address").val() );
	var address2 = escape( $("input#address2").val() );
	var city = escape( $("input#city").val() );
	var state = escape( $("select#state").val() );
	var province = escape( $("input#province").val() );
	var zip = escape( $("input#zip").val() );
	var country = escape( $("select#country").val() );
	var residential = escape( $("select#residential").val() );
	var liftgate = escape( $("select#liftgate").val() );
	var count = parseInt( $("input#bill_numbers").val() ) - 1;
	// construct post data for variable number of telephone numbers
	var data_str = "fname="+fname+"&mname="+mname+"&lname="+lname+"&company="+company+"&website="+website+"&address="+address+"&address2="+address2;
	data_str += "&city="+city+"&state="+state+"&province="+province+"&zip="+zip+"&country="+country+"&residential="+residential+"&liftgate="+liftgate;
	data_str += "&xchangebilling=187&count="+count;
	for( i=1; i<=count; i++ )
	{
		data_str += "&ct_"+i+"="+$("select#bill_ct_"+i).val()+"&cn_"+i+"="+$("input#bill_cn_"+i).val();
	}
	//alert(data_str);
	
	$("#bl_success_msg").text("");
	$("img#bl_loading").css("display",""); // turn on loading img
	$("input#bl_button").css("display","none"); // turn off button
	$.ajax({
		type: "POST",
		url: "/gloinc/mj_global.inc.php",
		cache: false,
		data: data_str,
		error: function() 
		{
			$("img#bl_loading").css("display","none"); // turn off loading img
			$("input#bl_button").css("display",""); // turn on button
			// unknown fatal error
		},
		success: function(ret_val)
		{
			$("img#bl_loading").css("display","none"); // turn off loading img
			$("input#bl_button").css("display",""); // turn on button
			//alert(ret_val);
			if(ret_val == "success") {
				// update worked
				$("#bl_success_msg").text("Your Billing Information has been Updated!");
			}
			else if(ret_val == "fail-login") {
				// user not logged in failure
			}
			else if(ret_val == "fail-query") {
				// query failure
			}
		}
	});
}

// user account billing information update
function live_shipping_update() {
	var name = $("input#ship_name").val();
	var company = $("input#ship_company").val();
	var address = $("input#ship_address").val();
	var address2 = $("input#ship_address2").val();
	var city = $("input#ship_city").val();
	var state = $("select#ship_state").val();
	var province = $("input#ship_province").val();
	var zip = $("input#ship_zip").val();
	var country = $("select#ship_country").val();
	var residential = $("select#ship_residential").val();
	var liftgate = $("select#ship_liftgate").val();
	var count = parseInt( $("input#ship_numbers").val() ) - 1;
	var shipto_id = $("input#shipto_id").val();
	// construct post data for variable number of telephone numbers
	var data_str = "name="+name+"&company="+company+"&address="+address+"&address2="+address2+"&city="+city+"&state="+state;
	data_str += "&province="+province+"&zip="+zip+"&country="+country+"&residential="+residential+"&liftgate="+liftgate;
	data_str += "&count="+count+"&shipto_id="+shipto_id;
	for( i=1; i<=count; i++ )
	{
		data_str += "&ct_"+i+"="+$("select#ship_ct_"+i).val()+"&cn_"+i+"="+$("input#ship_cn_"+i).val();
	}
	if(shipto_id == "billing")
		data_str += "&xchangebilling=187";
	else
		data_str += "&xchangeshipping=187";
	//alert(data_str);
	
	$("#sh_success_msg").text("");
	$("img#sh_loading").css("display",""); // turn on loading img
	$("img#sh_button").css("display","none"); // turn off button
	$.ajax({
		type: "POST",
		url: "/gloinc/mj_global.inc.php",
		cache: false,
		data: data_str,
		error: function() 
		{
			$("img#sh_loading").css("display","none"); // turn off loading img
			$("input#sh_button").css("display",""); // turn on button
			// unknown fatal error
		},
		success: function(ret_val)
		{
			$("img#sh_loading").css("display","none"); // turn off loading img
			//alert(ret_val);
			if(ret_val == "success") {
				// update worked
				if( shipto_id == "new" ) {
					$("#sh_success_msg").text("Your Shipping Information has been Added!");
					// we dont turn on the button here because it allows them to keep adding addresses... 
				}
				else {
					$("#sh_success_msg").text("Your Shipping Information has been Updated!");
					$("img#sh_button").css("display",""); // turn on button
				}
			}
			else if(ret_val == "fail-login") {
				// user not logged in failure
			}
			else if(ret_val == "fail-query") {
				// query failure
			}
		}
	});
}

// javascript user account email address update 
function live_email_update() {
	var email = $("input#email").val();
	var mlist = 0;
	if( $('input#mailinglist').attr('checked') )
		mlist = 1;
	$("#em_success_msg").text("");
	$("img#em_loading").css("display",""); // turn on loading img
	$("input#em_button").css("display","none"); // turn off button
	
	$.ajax({
		type: "POST",
		url: "/gloinc/mj_global.inc.php",
		cache: false,
		data: "email="+email+"&mailinglist="+mlist+"&xchangeemail=187",
		error: function() 
		{
			$("img#em_loading").css("display","none"); // turn off loading img
			$("input#em_button").css("display",""); // turn on button
			// unknown fatal error
			$("#email_msg").text("fatal error, contact support");
		},
		success: function(ret_val)
		{
			$("img#em_loading").css("display","none"); // turn off loading img
			$("input#em_button").css("display",""); // turn on button
			//alert(ret_val);
			if(ret_val == "success") {
				// update worked
				$("#em_success_msg").text("Your Account Email has been Updated!");
			}
			else if(ret_val == "fail-email") {
				// email address exists for another user
				$("#email_msg").text("email is already in use");
			}
			else if(ret_val == "fail-login") {
				// user not logged in failure
				$("#email_msg").text("please try again later");
			}
			else if(ret_val == "fail-query") {
				// query failure
				$("#email_msg").text("database failure, please contact support");
			}
		}
	});
}

// javascript user account password update 
function live_pw_update() {
	var old_pw = $("input#password_old").val();
	var new_pw = $("input#password1").val();
	$("#pw_success_msg").text("");
	$("img#pw_loading").css("display",""); // turn on loading img
	$("input#pw_button").css("display","none"); // turn off button
	$.ajax({
		type: "POST",
		url: "/gloinc/mj_global.inc.php",
		cache: false,
		data: "password_old="+old_pw+"&password_new="+new_pw+"&xchangepassword=187",
		error: function() 
		{
			$("img#pw_loading").css("display","none"); // turn off loading img
			$("input#pw_button").css("display",""); // turn on button
			// unknown fatal error
			$("#opw_msg").text("fatal error, contact support");
		},
		success: function(ret_val)
		{
			$("img#pw_loading").css("display","none"); // turn off loading img
			$("input#pw_button").css("display",""); // turn on button
			//alert(ret_val);
			if(ret_val == "success") {
				// update worked
				$("#password_old").val("");
				$("#password1").val("");
				$("#password2").val("");
				$("#pw_success_msg").text("Your Password has been Updated!");
			}
			else if(ret_val == "fail-password") {
				// current password missmatch failure
				$("#password_old_msg").text("current password incorrect");
			}
			else if(ret_val == "fail-login") {
				// user not logged in failure
				$("#password_old_msg").text("please try again later");
			}
			else if(ret_val == "fail-query") {
				// query failure
				$("#password_old_msg").text("database failure, please contact support");
			}
		}
	});
}

// javascript user account mailing address selected update 
function live_mailing_update() {
	var mail = $("#update_mailing input:radio:checked").val();
	$("#ma_success_msg").text("");
	$("img#ma_loading").css("display",""); // turn on loading img
	$("input#ma_button").css("display","none"); // turn off button
	$.ajax({
		type: "POST",
		url: "/gloinc/mj_global.inc.php",
		cache: false,
		data: "mailing_address="+mail+"&xchangemailing=187",
		error: function() 
		{
			$("img#ma_loading").css("display","none"); // turn off loading img
			$("input#ma_button").css("display",""); // turn on button
			// unknown fatal error
			//$("#email_msg").text("fatal error, contact support");
		},
		success: function(ret_val)
		{
			$("img#ma_loading").css("display","none"); // turn off loading img
			$("input#ma_button").css("display",""); // turn on button
			//alert(ret_val);
			if(ret_val == "success") {
				// update worked
				$("#ma_success_msg").text("Your Mailing Address has been Updated!");
			}
			else if(ret_val == "fail-login") {
				// user not logged in failure
				//$("#email_msg").text("please try again later");
			}
			else if(ret_val == "fail-query") {
				// query failure
				//$("#email_msg").text("database failure, please contact support");
			}
		}
	});
}

// refreshes shipping information in user account area
function refresh_shipping_area(type) {
	// meant to refresh the addresses of the mailing area whenever billing or shipping updates are made
	var source_url = "";
	if(type == 1)
		source_url = "/components/account/acct_edit_shipping.cmp.php";
	else
		source_url = "/components/checkout_select_shipping.cmp.php";
	$.ajax({
		type: "POST",
		url: source_url,
		cache: false,
		data: "live=187",
		success: function(ret_val)
		{
			//alert(ret_val);
			$("#edit_shipping").html(ret_val);
			// show shipping area after refresh
			hide_elem('ship_off');
			show_elem('ship_on');
		}
	});
}

// refreshes mailing address information area in user account area
function refresh_mailing_area() {
	// meant to refresh the addresses of the mailing area whenever billing or shipping updates are made
	$.ajax({
		type: "POST",
		url: "/components/account/acct_edit_mailing.cmp.php",
		cache: false,
		data: "live=187",
		success: function(ret_val)
		{
			//alert(ret_val);
			$("#edit_mailing").html(ret_val);
		}
	});
}

// sends account login information notification / reminder email to client supplied (used in checkout and in various office pages)
function send_invoice_client_login_info(client_id, version)
{
	// show in progress
	if( version == "office_invoice" )
	{
		$("#send_email_btn").css("display", "none");
		$("#send_email_ajax").css("display", "inline");
	}
	else if( version == "office_client" )
	{
		$("#client_nav #ajax_loading").css("display", "block");
	}
	$.ajax({
		type: "GET",
		url: "/components/emails/email_account_send.cmp.php",
		cache: false,
		data: "id="+client_id+"&",
		error: function() 
		{
			alert('ajax communication error');
		},
		success: function(ret_val)
		{
			if( version == "office_invoice" )
			{
				// no longer loading
				$("#send_email_ajax").css("display", "none");
				$("#send_email_btn").css("display", "inline");
				// hide the pop up
				hide_dynamic_overlay("overlay_email_acct");
			}
			else if( version == "office_client" )
			{
				$("#client_nav #ajax_loading").css("display", "none");
				// refresh the action log to display email sent notification
				refresh_log_results('action_log');
			}
		}
	});
}

// show tracking information details (and load them)
function show_tracking_info(track_id)
{
	$.ajax({
		type: "GET",
		url: "/components/account/tracking_window.cmp.php",
		cache: false,
		data: "track_id="+track_id+"&",
		error: function() 
		{
			alert('ajax communication error');
		},
		success: function(ret_val)
		{
			$("#acct_tracking_popup").html(ret_val);
			show_dynamic_overlay("overlay_tracking");
		}
	});
}

// FUNCTIONS USED IN THE NEW OFFICE BACKEND (possibly by several pages, which is why they are here and not on the specific page)
// -----------------------------------------------------------------------------------------------------------------------------
// loads and displays shipping address pop up window with proper data based on shipto_id
function office_load_shipping_address(id, shipto_id, calling_page)
{
	$.ajax({
		type: "GET",
		url: "/office/components/clients/address_window.cmp.php",
		cache: false,
		data: "id="+id+"&shipto_id="+shipto_id+"&calling_page="+calling_page+"&",
		error: function() 
		{
			alert('ajax communication error');
		},
		success: function(ret_val)
		{
			$("#shipping_popup").html(ret_val);
			show_dynamic_overlay('overlay_shipadd');
		}
	});
}
// depending on the page supplied (client details or invoice details) the data_str will be submitted to 1 of 2 pages components to handle the updates.
// the action variable can be either save, delete or refresh (refresh is only used on the client details page so far)
// if shipto_id ends up being empty basically the component files will handle this as a shipping address creation
function office_save_shipping(id, shipto_id, action)
{
	// hide buttons and show loading
	$(".save_shipping_btns").css("display", "none");
	$("#save_shipping_ajax").css("display", "inline");
	
	var data_str = "";
	if( action == "save" )
	{
		// set the get variables for passing to 
		data_str += "&name=" + escape( $("#ship_name").val() ); // deal with possible # characters etc.
		data_str += "&company=" + escape( $("#ship_company").val() ); // deal with possible # characters etc.
		data_str += "&address=" + escape( $("#ship_address").val() ); // deal with possible # characters etc.
		data_str += "&address2=" + escape( $("#ship_address2").val() ); // deal with possible # characters etc.
		data_str += "&city=" + escape( $("#ship_city").val() ); // deal with possible # characters etc.
		if( $("#ship_country").val() == "US" )
			data_str += "&state=" + $("#ship_state_div #ship_state").val();
		else
			data_str += "&state=" + escape( $("#ship_province_div #ship_state").val() ); // deal with possible # characters etc.
		data_str += "&zip=" + $("#ship_zip").val();
		data_str += "&country=" + $("#ship_country").val();
		data_str += "&residential=" + $("#ship_residential").val();
		data_str += "&liftgate=" + $("#ship_liftgate").val();
		// phone numbers
		var numbers = $("#ship_numbers").val();
		data_str += "&numbers=" + numbers;
		for(i=1; i<numbers; i++)
		{
			data_str += "&ct_"+i+"=" + $("#ship_ct_"+i).val();
			data_str += "&cn_"+i+"=" + $("#ship_cn_"+i).val();
		}
	}
	else if( action == "refresh" ) // so that the address box remains the same display as currently on refresh
		data_str += "&show_address_info=" + $("#address_info").css("display");
	
	// set url based on page calling script
	var page = $("#calling_page").val();
	if( page == "invoice" ) 
	{
		var url_str = "/office/components/invoices/info_bill_ship.cmp.php";
		// also requires the invoiceid be sent in request, so we've included a hidden input in the file above
		data_str += "&invoiceid=" + $("#invoiceid").val();
	}
	else var url_str = "/office/components/clients/addresses.cmp.php"; // is defaulted because of the possible refresh action on client page
	$.ajax({
		type: "GET",
		url: url_str,
		cache: false,
		data: "id="+id+"&shipto_id="+shipto_id+"&action="+action+data_str+"&",
		error: function() 
		{
			alert('ajax communication error');
		},
		success: function(ret_val)
		{
			if( page == "invoice" )
			{
				$("#invoice_billship_area").html(ret_val);
				hide_dynamic_overlay('overlay_shipadd');
			}
			else // is defaulted because of the possible refresh action on client page
			{
				$("#addresses").html(ret_val);
				hide_dynamic_overlay('overlay_shipadd');
			}
		}
	});	
}
// delete an image uploaded by admin or SEO level office user
function office_delete_image(name)
{
	var msg = "Are you sure you want to delete image \"" + name + "\"?\n\n";
	msg += "If it exists anywhere on the site, it will now be a broken image tag / link. This action cannot be undone.";
	if( confirm(msg) )
	{
		$.ajax({
			type: "GET",
			url: "/office/components/seo/images.cmp.php",
			cache: false,
			data: "image_name="+name+"&action=delete&",
			error: function() 
			{
				alert('ajax communication error');
			},
			success: function(ret_val)
			{
				$("#images_results").html(ret_val);
			}
		});
	}
}
// search images uploaded by admin and SEO level office users for use in custom SEO pages or news articles
function office_image_search(page)
{
	var image_name = $("#image_name").val();
	$("#search_loading").css("display", "inline");
	$.ajax({
		type: "GET",
		url: "/office/components/seo/images.cmp.php",
		cache: false,
		data: "image_name="+image_name+"&p="+page+"&action=search&",
		error: function() 
		{
			alert('ajax communication error');
		},
		success: function(ret_val)
		{
			$("#search_loading").css("display", "none");
			$("#images_results").html(ret_val);
		}
	});
}
// shows preview image on mouseover
function img_preview(id)
{
	if( $("#imgPrev_"+id).is(":visible") )
		$("#imgPrev_"+id).fadeOut(300);
	else
		$("#imgPrev_"+id).fadeIn(300);
}