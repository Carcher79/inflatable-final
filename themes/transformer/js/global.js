/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
//global variables
var responsiveflag = false;

var window_width;
var verifMailREGEX = /^([\w+-]+(?:\.[\w+-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;
var menu_container_top=0;

$(document).ready(function(){
	//highdpiInit();
	//responsiveResize();
	//$(window).resize(responsiveResize);
	//blockHover();
	if (typeof quickView !== 'undefined' && quickView)
		quick_view();
	
    $('.dropdown_wrap').hover(function(){
        if($(this).find('.dropdown_list ul li').size())
            $(this).addClass('open');
    },function(){
        $(this).removeClass('open');
    });

	if (typeof page_name != 'undefined' && !in_array(page_name, ['index', 'product']))
	{
		bindGrid();

 		$(document).on('change', '.selectProductSort', function(e){
			if (typeof request != 'undefined' && request)
				var requestSortProducts = request;
 			var splitData = $(this).val().split(':');
			if (typeof requestSortProducts != 'undefined' && requestSortProducts)
				document.location.href = requestSortProducts + ((requestSortProducts.indexOf('?') < 0) ? '?' : '&') + 'orderby=' + splitData[0] + '&orderway=' + splitData[1];
    	});

		$(document).on('change', 'select[name="n"]', function(){
			$(this.form).submit();
		});

		$(document).on('change', 'select[name="manufacturer_list"], select[name="supplier_list"]', function(){
			if (this.value != '')
				location.href = this.value;
		});

		$(document).on('change', 'select[name="currency_payement"]', function(){
			setCurrency($(this).val());
		});
	}

	$(document).on('click', '.back', function(e){
		e.preventDefault();
		history.back();
	});
	
	jQuery.curCSS = jQuery.css;
	if (!!$.prototype.cluetip)
		$('a.cluetip').cluetip({
			local:true,
			cursor: 'pointer',
			dropShadow: false,
			dropShadowSteps: 0,
			showTitle: false,
			tracking: true,
			sticky: false,
			mouseOutClose: true,
			fx: {             
		    	open:       'fadeIn',
		    	openSpeed:  'fast'
			}
		}).css('opacity', 0.8);

	if (!!$.prototype.fancybox)
		$.extend($.fancybox.defaults.tpl, {
			closeBtn : '<a title="' + FancyboxboxI18nClose + '" class="fancybox-item fancybox-close" href="javascript:;"></a>',
			next     : '<a title="' + FancyboxI18nNext + '" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
			prev     : '<a title="' + FancyboxI18nPrev + '" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
		});
});

function highdpiInit()
{
	if($('.replace-2x').css('font-size') == "1px")
	{		
		var els = $("img.replace-2x").get();
		for(var i = 0; i < els.length; i++)
		{
			src = els[i].src;
			extension = src.substr( (src.lastIndexOf('.') +1) );
			src = src.replace("." + extension, "2x." + extension);
			
			var img = new Image();
			img.src = src;
			img.height != 0 ? els[i].src = src : els[i].src = els[i].src;
		}
	}
}

function responsiveResize()
{
	if ($(document).width() <= 767 && responsiveflag == false)
	{
		accordion('enable');
		responsiveflag = true;	
	}
	else if ($(document).width() >= 768)
	{
		accordion('disable');
	    responsiveflag = false;
	}
}

function blockHover(status)
{
	$(document).off('mouseenter').on('mouseenter', '.product_list.grid li.ajax_block_product .product-container', function(e){

		if ('ontouchstart' in document.documentElement)
			return;
		if ($('body').find('.container').width() == 1170)
		{
			var pcHeight = $(this).parent().outerHeight();
			var pcPHeight = $(this).parent().find('.button-container').outerHeight() + $(this).parent().find('.comments_note').outerHeight() + $(this).parent().find('.functional-buttons').outerHeight();
			$(this).parent().addClass('hovered');
			$(this).parent().css('height', pcHeight + pcPHeight).css('margin-bottom', pcPHeight * (-1));
		}
	});

	$(document).off('mouseleave').on('mouseleave', '.product_list.grid li.ajax_block_product .product-container', function(e){
		if ($('body').find('.container').width() == 1170)
			$(this).parent().removeClass('hovered').removeAttr('style');
	});
}

function quick_view()
{
	$(document).on('click', '.quick-view:visible', function(e) 
	{
		quickViewCaller = this;
		
		e.preventDefault();
		var url = this.rel;
		if (url.indexOf('?') != -1)
			url += '&';
		else
			url += '?';

		if (!!$.prototype.fancybox)
			$.fancybox({
				'padding':  0,
				'width':    1087,
				'height':   610,
				'type':     'iframe',
				'href':     url + 'content_only=1'
			});
	});
}

function bindGrid()
{
	var view = $.totalStorage('display');
	if(!view && $('ul.product_list').length==1)
	{
		var default_view = $('ul.product_list').attr('data-default-view');
		if(default_view=='grid')view='grid';
		if(default_view=='list')view='list';
	}

	if (view && view != 'grid')
		display(view);
	else
		$('.content_sortPagiBar .display').find('li.grid').addClass('selected');
	
	$(document).on('click', '.content_sortPagiBar .display .grid', function(e){
		e.preventDefault();
		display('grid');
	});

	$(document).on('click', '.content_sortPagiBar .display .list', function(e){
		e.preventDefault();
		display('list');
	});
}

function display(view)
{
	if (view == 'list')
	{
		var classnames = $('ul.product_list').removeClass('grid row').addClass('list').attr('data-classnames');
		$('.product_list > li').removeClass(classnames).addClass('col-xs-12 clearfix');
		
		$('.content_sortPagiBar .display').find('li.list').addClass('selected');
		$('.content_sortPagiBar .display').find('li.grid').removeClass('selected');
		$.totalStorage('display', 'list');
	}
	else 
	{
		var classnames = $('ul.product_list').removeClass('list').addClass('grid row').attr('data-classnames');
		$('.product_list > li').removeClass('col-xs-12 clearfix').addClass(classnames);
		
		$('.content_sortPagiBar .display').find('li.grid').addClass('selected');
		$('.content_sortPagiBar .display').find('li.list').removeClass('selected');
		$.totalStorage('display', 'grid');
	}	
}

function accordionFooter(status)
{
	if(status == 'enable')
	{
		$('#footer .footer-block h4').on('click', function(){
			$(this).toggleClass('active').parent().find('.toggle-footer').stop().slideToggle('medium');
		})
		$('#footer').addClass('accordion').find('.toggle-footer').slideUp('fast');
	}
	else
	{
		$('.footer-block h4').removeClass('active').off().parent().find('.toggle-footer').removeAttr('style').slideDown('fast');
		$('#footer').removeClass('accordion');
	}
}

function accordion(status)
{
	leftColumnBlocks = $('#left_column');
	if(status == 'enable')
	{
		$('#right_column .block:not(#layered_block_left) .title_block, #left_column .block:not(#layered_block_left) .title_block, #left_column #newsletter_block_left h4').on('click', function(){
			$(this).toggleClass('active').parent().find('.block_content').stop().slideToggle('medium');
		})
		$('#right_column, #left_column').addClass('accordion').find('.block:not(#layered_block_left) .block_content').slideUp('fast');
	}
	else
	{
		$('#right_column .block:not(#layered_block_left) .title_block, #left_column .block:not(#layered_block_left) .title_block, #left_column #newsletter_block_left h4').removeClass('active').off().parent().find('.block_content').removeAttr('style').slideDown('fast');
		$('#left_column, #right_column').removeClass('accordion');
	}
}
function isPlaceholer(){
    var input = document.createElement('input');
    return "placeholder" in input;
} 

function getFlexSliderSize(per_row) {
	var ww= 'md';
    if(st_responsive)
    {
        var window_width = $(window).innerWidth();
        if(window_width>=1200 && st_responsive_max==1)
            ww = 'lg';
        else if(window_width>=992)
            ww = 'md';
        else if(window_width>=768)
            ww = 'sm';
        else if(window_width>=480)
            ww = 'xs';
        else
            ww = 'xxs';
    }
    return eval('per_row.'+ww);
}

jQuery(function($){
    window_width = $(window).width();
    $('#to_top').click(function() {
    	$('body,html').animate({
    		scrollTop: 0
    	}, 'fast');
		return false;
    }); 
    $('.dropdown_wrap').hover(function(){
        if($(this).find('.dropdown_list ul li').size())
            $(this).addClass('open');
    },function(){
        $(this).removeClass('open');
    });
    $(".sortPagiBar select").selectBox();
    $('#footer .opener,.product_accordion .opener').click(function(){
        $(this).parent().toggleClass('open');
        return false;
    }); 
    
    $('#switch_left_column,#switch_right_column').click(function(){
        var column = $(this).attr('data-column');
        var opened = $('#'+column).hasClass('opened');
        var that_left_column = $('#'+column);
        if(!that_left_column.size())
        {
            $(this).hide();
            return false;
        }
        //
        $('#left_column,#right_column').removeClass('opened');
        $('#switch_left_column i').removeClass('icon-left-open-1').addClass('icon-right-open-1');
        $('#switch_right_column i').removeClass('icon-right-open-1').addClass('icon-left-open-1');
        
        if(!opened){
        	var that_center_column = $('#center_column');
	        if(that_left_column.height()>that_center_column.height())
	            that_center_column.height(that_left_column.height());
	        $('#'+column).addClass('opened');
	        if(column=='left_column')
	            $(this).find('i').removeClass('icon-right-open-1').addClass('icon-left-open-1');
	        else
	            $(this).find('i').removeClass('icon-left-open-1').addClass('icon-right-open-1');
	        var center_column_offset = $('#center_column').offset();
	        //if(center_column_offset.top+that_left_column.height() < $(window).scrollTop())            
	        	$('body,html').animate({
	        		scrollTop: center_column_offset.top-20
	        	}, 'fast');
        }
        else
        {
        	$('#center_column').height('auto');
        }
        
        return false;
    });
    to_top_wrap_master($(window).scrollTop() > 300);
    $('.version_switching').click(function(){
        $.getJSON(baseDir+'modules/stthemeeditor/stthemeeditor-ajax.php?action=version_switching&vs='+($(this).hasClass('vs_desktop') ? 1 : 0)+'&_ts='+new Date().getTime(),
            function(json){
                if(json.r)
                    window.location.reload(true);
            }
        ); 
        return false;
    });
    if($('#st_mega_menu_container').size())
    {
        menu_container_top = $('#st_mega_menu_container').offset().top;
        sticky_menu();
    }
});
$(window).scroll(function() {
    to_top_wrap_master($(this).scrollTop() > 300);
    sticky_menu();
});
function sticky_menu()
{    
    if(!menu_container_top || typeof(st_sticky_menu)=='undefined' || !st_sticky_menu)
        return false;
        
    if(window_width<980)
    {
        $('#st_mega_menu_container').removeClass('sticky');
        return false;
    }

    if ($(window).scrollTop() > menu_container_top) 
        $('#st_mega_menu_container').addClass('sticky');
    else 
        $('#st_mega_menu_container').removeClass('sticky');
    return false;
}

function to_top_wrap_master($statu)
{
    if($statu)
        $('#to_top_wrap .icon_wrap').removeClass('disabled');
    else
       $('#to_top_wrap .icon_wrap').addClass('disabled'); 
}

$(window).resize(function() {
    window_width = $(window).width();
    if(typeof(st_sticky_menu)!='undefined' && st_sticky_menu)
        sticky_menu();
});

function pug() {
	if (!arguments || typeof arguments[0] != 'object') {
		return
	};
	var param = arguments[0],
	url = arguments[1] || window.location.href;
	for (var i in param) {
		if (!i) {
			continue
		};
		var name = i,
		val = param[i];
		var reg = new RegExp('([\\?&])((' + name + '\=)([\\w\-]+))(&?)', 'i');
		var omatch = url.match(reg);
		if (!val && omatch) { (omatch[5] && omatch[2]) ? url = url.replace(omatch[2] + '&', '') : (omatch[1] && omatch[2]) ? url = url.replace(omatch[0], '') : ''
		}
		if (val && !omatch) {
			url.indexOf('?') > -1 ? url += '&' + name + '=' + val: url += '?' + name + '=' + val
		}
		if (val && omatch && val != omatch[4]) {
			url = url.replace(omatch[2], omatch[3] + val)
		}
	};
	if (!arguments[1] && window.location.href != url) {
		if (location.hash != '') {
			url = url.replace(location.hash, '');
			url += location.hash
		}
		window.location.href = url
	} else {
		return url
	}
};