
// used to validate various forms throughout the site
function validate_form(type, element) 
{
	var value = $(element).val();
	var err_div = $(element).attr("id") + "_err";
	// for validation that takes more than one variable (optional)
	var vsid = $(element).attr("id") + "_secondary";
	var value_secondary = $("#"+vsid).val();
	//alert(value_secondary);
	// used to disable / enable form buttons
	var form = element.form;
	var form_id = $(form).attr('id');
	// disable submit button(s) while waiting
	$("#"+form_id+" #sub_form_btn").attr("disabled", "true");
	// turn on loading img	
	$("#"+err_div).html("<img src=\"/img/ajax-loader.gif\" border=\"0\" />"); 
	$.ajax({
		type: "POST",
		url: "/inc/form_validator.inc.php",
		cache: false,
		data: "type="+type+"&value="+value+"&value_secondary="+value_secondary+"&",
		error: function() 
		{
			alert('ajax communication error');
		},
		success: function(ret_val)
		{
			$("#"+err_div).html(""); // turn off loading img
			if(ret_val != "") {
				var err_img = "<img src=\"/img/x-red-icon.gif\" border=\"0\" />";
				var err_msg = "";
				if( ret_val != "err" ) err_msg = " &nbsp; &nbsp; " + ret_val;
				$("#"+err_div).html(err_img + err_msg);
			}
			else {
				$("#"+err_div).html("<img src=\"/img/check-green-icon.gif\" border=\"0\" />");
				// enable submit button(s)
				$("#"+form_id+" #sub_form_btn").removeAttr('disabled');
			}
		}
	});
}

// used to validate various forms throughout the site
function validate_field(type, element, element2, error_msg_div, error_msg)
{
	var success = false;
	var value = $(element).val();
	if( element2 != null ) var value2 = $(element2).val();
	if( type == "not_empty" ) {
		if( $.trim(value) != "" ) success = true;
	}
	else if( type == "L2" ) {
		if( $.trim(value).length >= 2 ) success = true;
	}
	else if( type == "L4" ) {
		if( $.trim(value).length >= 4 ) success = true;
	}
	else if( type == "L8" ) {
		if( $.trim(value).length >= 8 ) success = true;
	}
	else if( type == "L10" ) {
		if( $.trim(value).length >= 10 ) success = true;
	}
	else if( type == "zip" ) {
		var filter = /^[a-zA-Z0-9_-]+$/;
		if( filter.test(value) && $.trim(value).length >= 4 ) success = true;
	}
	else if( type == "email" ) {
		//var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+\.[a-z]{2,4}$/;
		var filter = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		if( filter.test(value) ) success = true;
	}
	else if( type == "verification_code" ) {
		if( $.trim(value) != "" && value != "Verification Code" ) success = true;
	}
	else if( type == "passmatch" ) {
		if( value == value2 ) success = true;
	}
	else if( type == "credit_card" ) {
		if( validateCreditCard(value) ) success = true;
	}
	// update error message area and form input
	if(success) 
	{
		$(element).removeClass("input_error"); // make sure error class is removed
		if( error_msg_div != "" ) $("#"+error_msg_div).html(""); // make sure error message is blank
		if( element2 != null ) {
			$(element2).removeClass("input_error"); // make sure error class is removed
			$("#"+error_msg_div).html(""); // make sure error message is blank
		}
	}
	else 
	{
		$(element).addClass("input_error"); // add error class to input
		if( error_msg_div != "" ) $("#"+error_msg_div).html(error_msg); // update error message
		if( element2 != null ) {
			$(element2).addClass("input_error"); // add error class to input
			$("#"+error_msg_div).html(error_msg); // update error message
		}
	}
	return success;
}

// specific form field checks
function vName(element, field_type) 
{
	var error_msg_div = $(element).attr("id") + "_msg";
	if( field_type == 0 ) return validate_field("not_empty", element, null, error_msg_div, "Enter a valid name");
	else if( field_type == 1 ) return validate_field("not_empty", element, null, error_msg_div, "Enter a valid first name");
	else if( field_type == 2 ) return validate_field("not_empty", element, null, error_msg_div, "Enter a valid last name");
	else return false;
}
function vNumber(element, field_type)
{
	if( field_type == 1 ) return validate_field("not_empty", element, null, "contact_number_msg", "Select a number type");
	else if( field_type == 2 ) return validate_field("L10", element, null, "contact_number_msg", "Enter a valid contact number");
	else return false;
}
function vAddress(element, field_type)
{
	var error_msg_div = $(element).attr("id") + "_msg";
	if( field_type == 1 ) return validate_field("L4", element, null, error_msg_div, "Enter a valid address");
	else if( field_type == 2 ) return validate_field("L2", element, null, error_msg_div, "Enter a valid city");
	else if( field_type == 3 ) return validate_field("L2", element, null, error_msg_div, "Enter a valid state / province");
	else if( field_type == 4 ) return validate_field("zip", element, null, error_msg_div, "Enter a valid zip code");
	else return false;
}
function vEmail(element)
{
	var error_msg_div = $(element).attr("id") + "_msg";
	return validate_field("email", element, null, error_msg_div, "Enter a valid email address");
}
function vSelect(element)
{
	var error_msg_div = $(element).attr("id") + "_msg";
	return validate_field("not_empty", element, null, error_msg_div, "Make a valid selection");
}
function vCode(element)
{
	var error_msg_div = $(element).attr("id") + "_msg";
	return validate_field("verification_code", element, null, error_msg_div, "&nbsp;Enter verification code");
}
function vPassword(element)
{
	var error_msg_div = $(element).attr("id") + "_msg";
	return validate_field("L8", element, null, error_msg_div, "Enter a valid password");
}
function vPasswordMatch(element1, element2)
{
	var error_msg_div = $(element2).attr("id") + "_msg";
	return validate_field("passmatch", element1, element2, error_msg_div, "Passwords do not match");
}
function vGeneric(element)
{
	var error_msg_div = $(element).attr("id") + "_msg";
	if( $(element).attr("id") == "product_name" ) var error_msg = "Enter the product name";
	else if( $(element).attr("id") == "product_item_number" ) var error_msg = "Enter the item number";
	else if( $(element).attr("id") == "product_serial_number" ) var error_msg = "Enter the serial number";
	else if( $(element).attr("id") == "damages" ) var error_msg = "Explain the damages";
	else if( $(element).attr("id") == "photo1" ) var error_msg = "Submit an image of damages";
	else var error_msg = "Field required";
	return validate_field("not_empty", element, null, error_msg_div, error_msg);
}
function vCreditCard(element)
{
	var error_msg_div = $(element).attr("id") + "_msg";
	return validate_field("credit_card", element, null, error_msg_div, "Enter a valid credit card");
}
function vLogin(element, field_type)
{
	if(field_type == 1) return validate_field("email", element, null, "login_err_msg", "Enter a valid email address");
	else if(field_type == 2) return validate_field("L8", element, null, "login_err_msg", "Enter a valid password");
	else if(field_type == 3) return validate_field("email", element, null, "password_err_msg", "Enter a valid email address");
	else return false;
}
function vCheckSum(element1, element2, element_sum)
{
	if( element1.val() == "" ) var v1 = 0.0;
	else var v1 = parseFloat(element1.val());
	if( element2.val() == "" ) var v2 = 0.0;
	else var v2 = parseFloat(element2.val());
	// check sum
	if( parseFloat(v1 + v2) == parseFloat(element_sum.val()) ) return true;
	else 
	{
		$(element1).addClass("input_error");
		$(element2).addClass("input_error");
		return false;
	}
}

// specific form submission checks
function validateSubmitCatReq()
{
	// check all required fields are set
	if( vName($("#fname"), 1) & vName($("#lname"), 2) & 
		vNumber($("#bill_ct_1"), 1) & vNumber($("#bill_cn_1"), 2) & 
		( 
		  ( $("#contact_type:checked").val() == "mail" & vAddress($("#address"), 1) & vAddress($("#city"), 2) & 
		  ( vAddress($("#state"), 3) || vAddress($("#province"), 3) ) & vAddress($("#zip"), 4) ) 
		  || 
		  ($("#contact_type:checked").val() != "mail")
		) & 
		vEmail($("#email")) & 
		vSelect($("#howyouheard")) & vSelect($("#interest")) & 
		vCode($("#human_verify")) ) 
	{
		return true;
	}
	else
	{
		$("#form_errors_msg").text("Please review the following form errors in red before submitting.");
		window.location = "cat_req.php#form_msg";
		return false;
	}
}
function validateSubmitRegister()
{
	// check all required fields are set
	if( vName($("#fname"), 1) & vName($("#lname"), 2) & 
		vNumber($("#bill_ct_1"), 1) & vNumber($("#bill_cn_1"), 2) & 
		vAddress($("#address"), 1) & vAddress($("#city"), 2) & 
		( vAddress($("#state"), 3) || vAddress($("#province"), 3) & 
		vAddress($("#zip"), 4) ) & 
		vEmail($("#email")) & 
		vPassword($("#password1")) & 
		vPassword($("#password2")) & 
		vPasswordMatch($("#password1"), $("#password2")) & 
		vCode($("#human_verify")) ) 
	{
		return true;
	}
	else
	{
		$("#form_errors_msg").text("Please review the following form errors in red before submitting.");
		if( $("#forward_to").val() != "" )
			window.location = "register.php?forward_to="+$("#forward_to").val()+"#form_msg";
		else
			window.location = "register.php#form_msg";
		return false;
	}
}
function validateSubmitWarrantyClaim()
{
	// check all required fields are set
	if( vName($("#fname"), 1) & vName($("#lname"), 2) & 
		vNumber($("#number_type"), 1) & vNumber($("#contact_number"), 2) & 
		vAddress($("#address"), 1) & vAddress($("#city"), 2) & 
		( vAddress($("#state"), 3) || vAddress($("#province"), 3) & 
		vAddress($("#zip"), 4) ) & 
		vEmail($("#email")) & 
		vGeneric($("#product_name")) & vGeneric($("#product_item_number")) & vGeneric($("#product_serial_number")) & 
		vGeneric($("#damages")) & vGeneric($("#photo1")) & 
		vCode($("#human_verify")) ) 
	{
		return true;
	}
	else
	{
		$("#form_errors_msg").text("Please review the following form errors before submitting your warranty claim");
		window.location = "warranty_claim.php#form_msg";
		return false;
	}
}
function validateSubmitEmailUpdate()
{
	// check all required fields are set
	if( vEmail($("#email")) ) return true;
	else return false;
}
function validateSubmitPasswordUpdate()
{
	// check all required fields are set
	if( vPassword($("#password_old")) & 
		vPassword($("#password1")) & 
		vPassword($("#password2")) & 
		vPasswordMatch($("#password1"), $("#password2")) ) return true;
	else return false;
}
function validateSubmitBillingUpdate()
{
	// check all required fields are set
	if( vName($("#fname"), 1) & vName($("#lname"), 2) & 
		vNumber($("#bill_ct_1"), 1) & vNumber($("#bill_cn_1"), 2) & 
		vAddress($("#address"), 1) & vAddress($("#city"), 2) & 
		( vAddress($("#state"), 3) || vAddress($("#province"), 3) & 
		vAddress($("#zip"), 4) ) ) 
	{
		return true;
	}
	else return false;
}
function validateSubmitShippingUpdate()
{
	// check all required fields are set
	if( vName($("#ship_name"), 0) & 
		vNumber($("#ship_ct_1"), 1) & vNumber($("#ship_cn_1"), 2) & 
		vAddress($("#ship_address"), 1) & vAddress($("#ship_city"), 2) & 
		( vAddress($("#ship_state"), 3) || vAddress($("#ship_province"), 3) & 
		vAddress($("#ship_zip"), 4) ) ) 
	{
		return true;
	}
	else return false;
}
function validateSubmitCheckout1()
{
	// check all required fields are set
	if( vName($("#fname"), 1) & vName($("#lname"), 2) & 
		vNumber($("#bill_ct_1"), 1) & vNumber($("#bill_cn_1"), 2) & 
		vAddress($("#address"), 1) & vAddress($("#city"), 2) & 
		( vAddress($("#state"), 3) || vAddress($("#province"), 3) & 
		vAddress($("#zip"), 4) ) & 
		vEmail($("#email")) ) 
	{
		return true;
	}
	else 
	{
		$("#form_errors_msg").text("Please review the following form errors before continuing your checkout process");
		window.location = "checkout1.php#form_msg";
		return false;
	}
}
function validateSubmiteCheckout3()
{
	if( $("input[name='shipping_method']").is(':checked') )
	{
		$("#ship_method_msg").removeClass("msg_error");
		$("#ship_method_bg").removeClass("bg_error");
		return true;
	}
	else
	{
		$("#ship_method_msg").addClass("msg_error");
		$("#ship_method_bg").addClass("bg_error");
		return false;
	}
}
function validateSubmiteCheckout4()
{
	if( $("input[name='payment_type']:checked").val() == "credit_card" )
	{
		if( $("#card_count").val() == "1" )
		{
			if( vGeneric($("#dc_name")) & vCreditCard($("#dc_number")) & vSelect($("#dc_expiration_month")) & 
				vSelect($("#dc_expiration_year")) & vGeneric($("#dc_verification_number")) )
			{
				return true;
			}
			else return false;
		}
		else
		{
			if( vGeneric($("#dc_name")) & vCreditCard($("#dc_number")) & vGeneric($("#dc_expiration_month")) & 
				vGeneric($("#dc_expiration_year")) & vGeneric($("#dc_verification_number")) & vName($("#amount"), 0) & 
				vName($("#dc_name_2"), 0) & vCreditCard($("#dc_number_2")) & vSelect($("#dc_expiration_month_2")) & 
				vSelect($("#dc_expiration_year_2")) & vGeneric($("#dc_verification_number_2")) & vName($("#amount_2"), 0) & 
				vCheckSum($("#amount"), $("#amount_2"), $("#paying")) )
			{
				return true;
			}
			else return false;
		}
	}
	else return true;
}
function validateSubmitLogin()
{
	if( vLogin($("#username"), 1) && vLogin($("#password"), 2) )
		return true;
	else return false;
}
function validateSubmitForgotPW()
{
	if( vLogin($("#username_email"), 3) ) return true;
	else return false;
}