
{if isset($confirmation)}
	<p class="alert alert-success">{l s='Your message has been successfully sent to our team.'}</p>
    <ul class="footer_links clearfix">
        <li class="pull-left">
            <a href="{$link->getPageLink('request', true)|escape:'html':'UTF-8'}" title="{l s='Back to Your Account'}" rel="nofollow">
                <i class="icon-left icon-mar-lr2"></i>{l s='Back to Request Form'}
            </a>
        </li>
        <li class="pull-right">
            <a href="{$base_dir}" title="{l s='Home'}" rel="nofollow">
                <i class="icon-home icon-mar-lr2"></i>{l s='Home'}
            </a>
        </li>
    </ul>
  {else}
	{include file="$tpl_dir./errors.tpl"}          
    <link rel="stylesheet" type="text/css" href="{$css_dir}contact_form.css">
	<link rel="stylesheet" type="text/css" href="{$css_dir}cat_req.css">
    
<script src="{$js_dir}ga.js" async="" type="text/javascript"></script><script type="text/javascript" src="{$js_dir}jquery-1.js"></script>
<script type="text/javascript" src="{$js_dir}mglobal.js"></script>
<script type="text/javascript" src="{$js_dir}form_validation.js"></script>
<script type="text/javascript" src="{$js_dir}aviary.js"></script>
<script type="text/javascript" src="{$js_dir}jquery_002.js"></script>
<script type="text/ecmascript">
$(function(){
	$('#login_form').slidinglabels({
		/* these are all optional */
		className    : 'slider', // the class you're wrapping the label & input with -> default = slider
		//topPosition  : '3px',  // how far down you want each label to start
		//leftPosition : '10px',  // how far left you want each label to start
		axis         : 'x',    // can take 'x' or 'y' for slide direction
		speed        : 'slow'  // can take 'fast', 'slow', or a numeric value
	});
	$('#pwreq_form').slidinglabels({
		/* these are all optional */
		className    : 'slider', // the class you're wrapping the label & input with -> default = slider
		//topPosition  : '3px',  // how far down you want each label to start
		//leftPosition : '10px',  // how far left you want each label to start
		axis         : 'x',    // can take 'x' or 'y' for slide direction
		speed        : 'slow'  // can take 'fast', 'slow', or a numeric value
	});
	
	// allow automatic hiding of login window when clicked outside of (assuming it is already showing of course)
	$("#overlay").bind('click', function(e) {  
		var $clicked = $(e.target);
		if ( $clicked.parents().hasClass("loginwindow") )
		{
			// do nothing, we've clicked in the login window
		}
		else if( !$(".loginwindow").is(":hidden") )
		{
			// hide and reset login window as we've clicked out of it
			hide_overlay(); 
			login_box_reset();
		}
	});
});
</script>

    
    <script type="text/javascript" src="{$js_dir}jquery.js"></script>
	<script type="text/javascript">
		function preload(arrayOfImages) {
			$(arrayOfImages).each(function(){
				$('<img/>')[0].src = this;
				// Alternatively you could use:
				// (new Image()).src = this;
			});
		}
		
		function request_option_change() {
			if($("#request-quote").is(":checked")) $.cookies.set("request-quote",true);
			else $.cookies.set("request-quote",false);
			
			if($("#request-phone").is(":checked")) $.cookies.set("request-phone",true);
			else $.cookies.set("request-phone",false);
			
			if($("#request-booklet").is(":checked")) $.cookies.set("request-booklet",true);
			else $.cookies.set("request-booklet",false);
			
			if(	!$("#request-quote").is(":checked") &&
				!$("#request-phone").is(":checked") &&
				!$("#request-booklet").is(":checked"))
			{
				$("#form-wrapper").hide("fast");
			}
			else $("#form-wrapper").show("fast");
			
			if($("#request-quote").is(":checked")) $("#form-right").show("fast");
			else $("#form-right").hide("fast");
			
			if($("#request-booklet").is(":checked")) {
				$("#address").parent().show('fast');
				$("#address2").parent().show('fast');
				$("#city").parent().show('fast');
				$("#state").parent().show('fast');
				$("#zip").parent().show('fast');
				$("#country").parent().show('fast');
				$("#address").attr('required', "required");
				$("#city").attr('required', "required");
				$("#state").attr('required', "required");
				$("#zip").attr('required', "required");
				$("#country").attr('required', "required");
			}
			else {
				$("#address").parent().hide('fast');
				$("#address2").parent().hide('fast');
				$("#city").parent().hide('fast');
				$("#state").parent().hide('fast');
				$("#zip").parent().hide('fast');
				$("#country").parent().hide('fast');
				$("#address").removeAttr('required');
				$("#city").removeAttr('required');
				$("#state").removeAttr('required');
				$("#zip").removeAttr('required');
				$("#country").removeAttr('required');
			}
		}
		
		// enable form submit through javascript
		$(function(){
						
			$('#submit_button').attr('disabled', false);
			
			preload(['{$img_dir}request/request-booklet-on.gif','{$img_dir}request/request-quote-on.gif','{$img_dir}request/request-phone-on.gif']);
			$("#request-options input").click(function(){
				request_option_change();
			});
			
			$("#country").change(function(){
				if($(this).val() == "US") {
					$("#state").show();
					$("#province").hide();
				}
				else {
					$("#state").hide();
					$("#province").show();
				}
			});
			
			$("#howyouheard").change(function(){
				if($(this).val() == "Other") $("#howyouheard_other").show();
				else $("#howyouheard_other").hide();
			});
			
			$("#form-wrapper input:not(.nocookie)").cookieBind();
			$("#form-wrapper textarea").cookieBind();
			$("#form-wrapper select").cookieBind();
			
			var num_phones = 1;
			if($.cookies.get('bill_numbers') != null) num_phones = parseInt($.cookies.get('bill_numbers'));
			for(var i = 1; i <= num_phones; i++)
			{
				if($.cookies.get('bill_cn_'+i) != null) {
					$.cookies.set('bill_cn_'+i, $.cookies.get('bill_cn_'+i));
					$.cookies.set('bill_ct_'+i, $.cookies.get('bill_ct_'+i));
					if(num_phones > i)
						add_phone();
				}
			}
			
			var num_prod_types = 1;
			if($.cookies.get('num_prod_types') != null) num_prod_types = parseInt($.cookies.get('num_prod_types'));
			for(var i = 1; i <= num_prod_types; i++)
			{
				if(parseInt($.cookies.get('prodqty_'+i)) != 0) {
					$.cookies.set('product_'+i, $.cookies.get('product_'+i));
					$.cookies.set('prodqty_'+i, $.cookies.get('prodqty_'+i));
					if(num_prod_types > i)
					   add_product();
				}
			}
			
			if($.cookies.get("request-quote") == true) $("#request-quote").attr('checked','checked');
			if($.cookies.get("request-phone") === true) $("#request-phone").attr('checked','checked');
			if($.cookies.get("request-booklet") === true) $("#request-booklet").attr('checked','checked');
			request_option_change();
		});
		
		var numProducts = 1;
		function add_product() {
			numProducts++;
			
                       
			
    var html = '<tr><td><select name=product_'+numProducts+'" id="product_'+numProducts+'" class="product">'+$('#hdnDropdown').html()+'</select></td><td><input type="text" name="prodqty_'+numProducts+'" id="prodqty_'+numProducts+'" value="1" class="prodqty" onkeyup="javascript:set_total_products()" /></td></tr>';
                       
                        $("#selectedProdsTable tr:last").before(html);
			$("#product_"+numProducts).cookieBind();
			$("#prodqty_"+numProducts).cookieBind();
			$("#num_prod_types").val(numProducts);
			$.cookies.set('num_prod_types', numProducts);
			set_total_products()
		}
		
		function set_total_products()
		{
			var total = 0;
			var n = 0;
			for(i = 1; i <= numProducts; i++) {
				n = $("#prodqty_"+i).val();
				if(n == '' || isNaN(n)) {
					n = 0;
					$("#prodqty_"+i).val(n);
				}
				total += parseInt(n);
			}
			$("#total_prods").html(total);
		}
		
		var numPhones = 1;
		function add_phone()
		{
			numPhones++;
			var html = '<li>\
        <label for="bill_ct_'+numPhones+'">Phone '+numPhones+'</label>\
        <select name="bill_ct_'+numPhones+'" id="bill_ct_'+numPhones+'" class="phone_type" >\
        <option value="">Select</option>\
        <option value="home">Home</option>\
        <option value="cell">Cell</option>\
        <option value="work">Work</option>\
        <option value="fax">Fax</option>\
        </select>\
        <input type="text" name="bill_cn_'+numPhones+'" id="bill_cn_'+numPhones+'" class="phone"  placeholder="555-555-5555" />\
    </li>';
			$("#add_phone_li").before(html);
			$("#bill_ct_"+numPhones).cookieBind();
			$("#bill_cn_"+numPhones).cookieBind();
			$("#bill_numbers").val(numPhones);
			$.cookies.set('bill_numbers', numPhones);
		}
    </script>

    <link rel="stylesheet" type="text/css" href="{$css_dir}FriendlyErrors.css"><script language="javascript" src="{$js_dir}FriendlyErrors.js"></script><!-- base href="cat_req.php" --></head>
<div style="display:none;" id="hdnDropdown">{$productdropdown}</div>
<div style="position:relative; width:870px; margin-left:auto; margin-right:auto; padding:0px; display:none ;">
   
      
       
   
    <script type="text/javascript">
		function float(n) {
			// stop the balloon floating after all 3 have been floated in succession
			if(n > 3) return;
			// make sure all balloons other than n are returned to start position prior to floating called balloon
			var balloon;
			var balloon_pos;
			var balloon_top;
			for(x=1; x<=3; x++) {
				if(x == 1) balloon_top = 245;
				else if(x == 2) balloon_top = 495;
				else if(x == 3) balloon_top = 745;
				if(x != n) {
					balloon = $('#balloon_'+x);
					balloon_pos = balloon.position();
					if(balloon_pos.left != -15) {
						$('#balloon_'+x).clearQueue()
						.animate(
						{
							'top': balloon_top,
							'left': -15
						}, 800)
					}
				}
			}
			balloon = $('#balloon_'+n);
			balloon_pos = balloon.position();
			//verify we're at starting position in order to animate to float position
			if(balloon_pos.left == -15) {
				$('#balloon_'+n).animate(
				{
					'top': balloon_pos.top - 200,
					'left': balloon_pos.left - 185
				}, 800)
				.delay(5000)
				.animate(
				{
					'top': balloon_pos.top,
					'left': balloon_pos.left
				}, 800, null, function() { float(n+1); })
			}
		}
	</script>
    </div>


	
    	
        <td class="page_bg1" valign="top">
        	<table border="0" cellpadding="0" cellspacing="0">
                <tbody><tr>
                	<td valign="top">
                    	
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tbody><tr>
                                <td class="page_padding_type3" align="justify" valign="top">

						<center>
						
<form action="{$request_uri|escape:'html':'UTF-8'}" method="post" name="info_form" id="info_form"  class="contact-form-box" enctype="multipart/form-data">
	
						
						<p>&nbsp;</p>
						<p>
							</p><div id="request-options">
							<input checked="checked" name="request-phone" value="1" id="request-phone" type="checkbox"><label for="request-phone"></label>
							&nbsp; &nbsp; &nbsp; 
							<input checked="checked" name="request-booklet" value="1" id="request-booklet" type="checkbox"><label for="request-booklet"></label>
							&nbsp; &nbsp; &nbsp; 
							<input checked="checked" name="request-quote" value="1" id="request-quote" type="checkbox"><label for="request-quote"></label>
							</div>
						<p></p>
							
							<a name="form_msg"></a><br>


<div id="form_errors_msg" class="size14 msg_error bold"></div>
<div id="form-wrapper" style="display: block;">
    <div id="form-left">
        <div class="contact_form">
            <ul>
                <li>
                    <label for="fname">First Name</label>
                    <input name="fname" placeholder="Jane" required="required" type="text" >
                </li>
                <li>
                    <label for="lname">Last Name</label>
                    <input name="lname" placeholder="Doe" required="required" type="text">
                </li>
                <li>
                    <label for="company">Company</label>
                    <input name="company" placeholder="Jane Doe, Inc." type="text">
                </li>
                <li>
                    <label for="email">Email</label>
                    <input name="email" placeholder="jane.doe@mydomain.com" required="required" type="email">
                    <span class="form_hint">Proper format "jane.doe@mydomain.com"</span>
                </li>
                <li>
                    <label for="website">Website</label>
                    <input name="website" placeholder="mydomain.com" type="text">
                </li>
                <li>
                    <label for="bill_ct_1">Phone</label>
                    <select name="bill_ct_1" id="bill_ct_1" class="phone_type" required="required">
                    <option selected="selected" value="">Select</option>
                    <option value="home">Home</option>
                    <option value="cell">Cell</option>
                    <option value="work">Work</option>
                    <option value="fax">Fax</option>
                    </select>
                    <input name="bill_cn_no" class="phone" placeholder="555-555-5555" required="required" type="text">
                </li>
               
                <li style="display: list-item;">
                    <label for="country">Country</label>
                    <select required="" name="country" id="country" class="menu1">
                                                <option selected="selected" value="US">United States</option>
                                                <option value="AF">Afghanistan</option>
                                                <option value="AL">Albania</option>
                                                <option value="DZ">Algeria</option>
                                                <option value="AS">American Samoa</option>
                                                <option value="AD">Andorra</option>
                                                <option value="AO">Angola</option>
                                                <option value="AI">Anguilla</option>
                                                <option value="AQ">Antarctica</option>
                                                <option value="AG">Antigua and Barbuda</option>
                                                <option value="AR">Argentina</option>
                                                <option value="AM">Armenia</option>
                                                <option value="AW">Aruba</option>
                                                <option value="AU">Australia</option>
                                                <option value="AT">Austria</option>
                                                <option value="AZ">Azerbaijan</option>
                                                <option value="BS">Bahamas</option>
                                                <option value="BH">Bahrain</option>
                                                <option value="BD">Bangladesh</option>
                                                <option value="BB">Barbados</option>
                                                <option value="BY">Belarus</option>
                                                <option value="BE">Belgium</option>
                                                <option value="BZ">Belize</option>
                                                <option value="BJ">Benin</option>
                                                <option value="BM">Bermuda</option>
                                                <option value="BT">Bhutan</option>
                                                <option value="BO">Bolivia</option>
                                                <option value="BA">Bosnia and Herzegovina</option>
                                                <option value="BW">Botswana</option>
                                                <option value="BV">Bouvet Island</option>
                                                <option value="BR">Brazil</option>
                                                <option value="IO">British Indian Ocean Territory</option>
                                                <option value="BN">Brunei Darussalam</option>
                                                <option value="BG">Bulgaria</option>
                                                <option value="BF">Burkina Faso</option>
                                                <option value="BI">Burundi</option>
                                                <option value="KH">Cambodia</option>
                                                <option value="CM">Cameroon</option>
                                                <option value="CA">Canada</option>
                                                <option value="CV">Cape Verde</option>
                                                <option value="KY">Cayman Islands</option>
                                                <option value="CF">Central African Republic</option>
                                                <option value="TD">Chad</option>
                                                <option value="CL">Chile</option>
                                                <option value="CN">China</option>
                                                <option value="CX">Christmas Island</option>
                                                <option value="CC">Cocos (Keeling) Islands</option>
                                                <option value="CO">Colombia</option>
                                                <option value="KM">Comoros</option>
                                                <option value="CG">Congo</option>
                                                <option value="CD">Congo, the Democratic Republic of the</option>
                                                <option value="CK">Cook Islands</option>
                                                <option value="CR">Costa Rica</option>
                                                <option value="CI">Cote D'Ivoire</option>
                                                <option value="HR">Croatia</option>
                                                <option value="CU">Cuba</option>
                                                <option value="CY">Cyprus</option>
                                                <option value="CZ">Czech Republic</option>
                                                <option value="DK">Denmark</option>
                                                <option value="DJ">Djibouti</option>
                                                <option value="DM">Dominica</option>
                                                <option value="DO">Dominican Republic</option>
                                                <option value="EC">Ecuador</option>
                                                <option value="EG">Egypt</option>
                                                <option value="SV">El Salvador</option>
                                                <option value="GQ">Equatorial Guinea</option>
                                                <option value="ER">Eritrea</option>
                                                <option value="EE">Estonia</option>
                                                <option value="ET">Ethiopia</option>
                                                <option value="FK">Falkland Islands (Malvinas)</option>
                                                <option value="FO">Faroe Islands</option>
                                                <option value="FJ">Fiji</option>
                                                <option value="FI">Finland</option>
                                                <option value="FR">France</option>
                                                <option value="GF">French Guiana</option>
                                                <option value="PF">French Polynesia</option>
                                                <option value="TF">French Southern Territories</option>
                                                <option value="GA">Gabon</option>
                                                <option value="GM">Gambia</option>
                                                <option value="GE">Georgia</option>
                                                <option value="DE">Germany</option>
                                                <option value="GH">Ghana</option>
                                                <option value="GI">Gibraltar</option>
                                                <option value="GR">Greece</option>
                                                <option value="GL">Greenland</option>
                                                <option value="GD">Grenada</option>
                                                <option value="GP">Guadeloupe</option>
                                                <option value="GU">Guam</option>
                                                <option value="GT">Guatemala</option>
                                                <option value="GN">Guinea</option>
                                                <option value="GW">Guinea-Bissau</option>
                                                <option value="GY">Guyana</option>
                                                <option value="HT">Haiti</option>
                                                <option value="HM">Heard Island and Mcdonald Islands</option>
                                                <option value="VA">Holy See (Vatican City State)</option>
                                                <option value="HN">Honduras</option>
                                                <option value="HK">Hong Kong</option>
                                                <option value="HU">Hungary</option>
                                                <option value="IS">Iceland</option>
                                                <option value="IN">India</option>
                                                <option value="ID">Indonesia</option>
                                                <option value="IR">Iran, Islamic Republic of</option>
                                                <option value="IQ">Iraq</option>
                                                <option value="IE">Ireland</option>
                                                <option value="IL">Israel</option>
                                                <option value="IT">Italy</option>
                                                <option value="JM">Jamaica</option>
                                                <option value="JP">Japan</option>
                                                <option value="JO">Jordan</option>
                                                <option value="KZ">Kazakhstan</option>
                                                <option value="KE">Kenya</option>
                                                <option value="KI">Kiribati</option>
                                                <option value="KP">Korea, Democratic People's Republic of</option>
                                                <option value="KR">Korea, Republic of</option>
                                                <option value="KW">Kuwait</option>
                                                <option value="KG">Kyrgyzstan</option>
                                                <option value="LA">Lao People's Democratic Republic</option>
                                                <option value="LV">Latvia</option>
                                                <option value="LB">Lebanon</option>
                                                <option value="LS">Lesotho</option>
                                                <option value="LR">Liberia</option>
                                                <option value="LY">Libyan Arab Jamahiriya</option>
                                                <option value="LI">Liechtenstein</option>
                                                <option value="LT">Lithuania</option>
                                                <option value="LU">Luxembourg</option>
                                                <option value="MO">Macao</option>
                                                <option value="MK">Macedonia, the Former Yugoslav Republic of</option>
                                                <option value="MG">Madagascar</option>
                                                <option value="MW">Malawi</option>
                                                <option value="MY">Malaysia</option>
                                                <option value="MV">Maldives</option>
                                                <option value="ML">Mali</option>
                                                <option value="MT">Malta</option>
                                                <option value="MH">Marshall Islands</option>
                                                <option value="MQ">Martinique</option>
                                                <option value="MR">Mauritania</option>
                                                <option value="MU">Mauritius</option>
                                                <option value="YT">Mayotte</option>
                                                <option value="MX">Mexico</option>
                                                <option value="FM">Micronesia, Federated States of</option>
                                                <option value="MD">Moldova, Republic of</option>
                                                <option value="MC">Monaco</option>
                                                <option value="MN">Mongolia</option>
                                                <option value="MS">Montserrat</option>
                                                <option value="MA">Morocco</option>
                                                <option value="MZ">Mozambique</option>
                                                <option value="MM">Myanmar</option>
                                                <option value="NA">Namibia</option>
                                                <option value="NR">Nauru</option>
                                                <option value="NP">Nepal</option>
                                                <option value="NL">Netherlands</option>
                                                <option value="AN">Netherlands Antilles</option>
                                                <option value="NC">New Caledonia</option>
                                                <option value="NZ">New Zealand</option>
                                                <option value="NI">Nicaragua</option>
                                                <option value="NE">Niger</option>
                                                <option value="NG">Nigeria</option>
                                                <option value="NU">Niue</option>
                                                <option value="NF">Norfolk Island</option>
                                                <option value="MP">Northern Mariana Islands</option>
                                                <option value="NO">Norway</option>
                                                <option value="OM">Oman</option>
                                                <option value="PK">Pakistan</option>
                                                <option value="PW">Palau</option>
                                                <option value="PS">Palestinian Territory, Occupied</option>
                                                <option value="PA">Panama</option>
                                                <option value="PG">Papua New Guinea</option>
                                                <option value="PY">Paraguay</option>
                                                <option value="PE">Peru</option>
                                                <option value="PH">Philippines</option>
                                                <option value="PN">Pitcairn</option>
                                                <option value="PL">Poland</option>
                                                <option value="PT">Portugal</option>
                                                <option value="PR">Puerto Rico</option>
                                                <option value="QA">Qatar</option>
                                                <option value="RE">Reunion</option>
                                                <option value="RO">Romania</option>
                                                <option value="RU">Russian Federation</option>
                                                <option value="RW">Rwanda</option>
                                                <option value="SH">Saint Helena</option>
                                                <option value="KN">Saint Kitts and Nevis</option>
                                                <option value="LC">Saint Lucia</option>
                                                <option value="PM">Saint Pierre and Miquelon</option>
                                                <option value="VC">Saint Vincent and the Grenadines</option>
                                                <option value="WS">Samoa</option>
                                                <option value="SM">San Marino</option>
                                                <option value="ST">Sao Tome and Principe</option>
                                                <option value="SA">Saudi Arabia</option>
                                                <option value="SN">Senegal</option>
                                                <option value="CS">Serbia and Montenegro</option>
                                                <option value="SC">Seychelles</option>
                                                <option value="SL">Sierra Leone</option>
                                                <option value="SG">Singapore</option>
                                                <option value="SK">Slovakia</option>
                                                <option value="SI">Slovenia</option>
                                                <option value="SB">Solomon Islands</option>
                                                <option value="SO">Somalia</option>
                                                <option value="ZA">South Africa</option>
                                                <option value="GS">South Georgia and the South Sandwich Islands</option>
                                                <option value="ES">Spain</option>
                                                <option value="LK">Sri Lanka</option>
                                                <option value="SD">Sudan</option>
                                                <option value="SR">Suriname</option>
                                                <option value="SJ">Svalbard and Jan Mayen</option>
                                                <option value="SZ">Swaziland</option>
                                                <option value="SE">Sweden</option>
                                                <option value="CH">Switzerland</option>
                                                <option value="SY">Syrian Arab Republic</option>
                                                <option value="TW">Taiwan, Province of China</option>
                                                <option value="TJ">Tajikistan</option>
                                                <option value="TZ">Tanzania, United Republic of</option>
                                                <option value="TH">Thailand</option>
                                                <option value="TL">Timor-Leste</option>
                                                <option value="TG">Togo</option>
                                                <option value="TK">Tokelau</option>
                                                <option value="TO">Tonga</option>
                                                <option value="TT">Trinidad and Tobago</option>
                                                <option value="TN">Tunisia</option>
                                                <option value="TR">Turkey</option>
                                                <option value="TM">Turkmenistan</option>
                                                <option value="TC">Turks and Caicos Islands</option>
                                                <option value="TV">Tuvalu</option>
                                                <option value="UG">Uganda</option>
                                                <option value="UA">Ukraine</option>
                                                <option value="AE">United Arab Emirates</option>
                                                <option value="GB">United Kingdom</option>
                                                <option value="US">United States</option>
                                                <option value="UM">United States Minor Outlying Islands</option>
                                                <option value="UY">Uruguay</option>
                                                <option value="UZ">Uzbekistan</option>
                                                <option value="VU">Vanuatu</option>
                                                <option value="VE">Venezuela</option>
                                                <option value="VN">Viet Nam</option>
                                                <option value="VG">Virgin Islands, British</option>
                                                <option value="VI">Virgin Islands, U.S.</option>
                                                <option value="WF">Wallis and Futuna</option>
                                                <option value="EH">Western Sahara</option>
                                                <option value="YE">Yemen</option>
                                                <option value="ZM">Zambia</option>
                                                <option value="ZW">Zimbabwe</option>
                                            </select>
                </li>
                <li style="display: list-item;">
                    <label for="address">Address</label>
                    <input required="" name="address" id="address" placeholder="123 Main St." type="text">
                </li>
                <li style="display: list-item;">
                    <label for="address2">Address 2</label>
                    <input name="address2" id="address2" placeholder="Suite A" type="text">
                </li>
                <li style="display: list-item;">
                    <label for="city">City</label>
                    <input required="" name="city" id="city" placeholder="Los Angeles" type="text">
                </li>
                <li style="display: list-item;">
                    <label for="state">State/Province</label>
                                        <select required="" name="state" id="state" class="menu1" onchange="vAddress(this, 3);" onkeyup="vAddress(this, 3);" onblur="vAddress(this, 3);">
                        <option selected="selected" value="">Select a State...</option>
                                                <option value="AK">Alaska</option>
                                                <option value="AL">Alabama</option>
                                                <option value="AS">American Samoa</option>
                                                <option value="AZ">Arizona</option>
                                                <option value="AR">Arkansas</option>
                                                <option value="CA">California</option>
                                                <option value="CO">Colorado</option>
                                                <option value="CT">Connecticut</option>
                                                <option value="DE">Delaware</option>
                                                <option value="DC">District of Columbia</option>
                                                <option value="FL">Florida</option>
                                                <option value="GA">Georgia</option>
                                                <option value="GU">Guam</option>
                                                <option value="HI">Hawaii</option>
                                                <option value="ID">Idaho</option>
                                                <option value="IL">Illinois</option>
                                                <option value="IN">Indiana</option>
                                                <option value="IA">Iowa</option>
                                                <option value="KS">Kansas</option>
                                                <option value="KY">Kentucky</option>
                                                <option value="LA">Louisiana</option>
                                                <option value="ME">Maine</option>
                                                <option value="MH">Marshall Islands</option>
                                                <option value="MD">Maryland</option>
                                                <option value="MA">Massachusetts</option>
                                                <option value="MI">Michigan</option>
                                                <option value="MN">Minnesota</option>
                                                <option value="MS">Mississippi</option>
                                                <option value="MO">Missouri</option>
                                                <option value="MT">Montana</option>
                                                <option value="NE">Nebraska</option>
                                                <option value="NV">Nevada</option>
                                                <option value="NH">New Hampshire</option>
                                                <option value="NJ">New Jersey</option>
                                                <option value="NM">New Mexico</option>
                                                <option value="NY">New York</option>
                                                <option value="NC">North Carolina</option>
                                                <option value="ND">North Dakota</option>
                                                <option value="OH">Ohio</option>
                                                <option value="OK">Oklahoma</option>
                                                <option value="OR">Oregon</option>
                                                <option value="PW">Palau</option>
                                                <option value="PA">Pennsylvania</option>
                                                <option value="PR">Puerto Rico</option>
                                                <option value="RI">Rhode Island</option>
                                                <option value="SC">South Carolina</option>
                                                <option value="SD">South Dakota</option>
                                                <option value="TN">Tennessee</option>
                                                <option value="TX">Texas</option>
                                                <option value="UT">Utah</option>
                                                <option value="VT">Vermont</option>
                                                <option value="VI">Virgin Islands</option>
                                                <option value="VA">Virginia</option>
                                                <option value="WA">Washington</option>
                                                <option value="WV">West Virginia</option>
                                                <option value="WI">Wisconsin</option>
                                                <option value="WY">Wyoming</option>
                                            </select>
                    <input name="province" id="province" onkeyup="javascript:vAddress(this, 3);" onblur="javascript:vAddress(this, 3);" style="display: none;" type="text">
                </li>
                <li style="display: list-item;">
                    <label for="zip">Zip</label>
                    <input required="" name="zip" id="zip" placeholder="12345" type="text">
                </li>
                <li>
                    <label for="howyouheard">How you found us?</label>
					<select name="howyouheard" id="howyouheard" class="" required="required">
						<option value="" selected="selected">Please Select</option>
						<option value="Google search">Google search</option>
						<option value="Bing search">Bing search</option>
						<option value="Yahoo search">Yahoo search</option>
						<option value="Other Search">Other Search</option>
						<option value="Returning client">Returning client</option>
						<option value="Friend/Relative referred">Friend/Relative referred</option>
						<option value="Magic Jump newsletter">Magic Jump newsletter</option>
						<option value="Trade Show">Trade Show</option>
						<option value="eBay">eBay</option>
						<option value="Moonwalk Forum">Moonwalk Forum</option>
						<option value="Other">Other (please specify)</option>
					</select>
				<br>
                    <textarea name="howyouheard_other" id="howyouheard_other" style="float:left; margin-left:140px; margin-top:10px; display:none;"></textarea>
                    <div style="clear:both;"></div>
                </li>
                <li>
                    <label for="interest">Interested in?</label>
				<select name="interest" id="interest" class="" required="required">
					<option value="" selected="selected">Please Select</option>
					<option value="FEC / Indoor Center">FEC / Indoor Center</option>
					<option value="Party Rental">Party Rental</option>
					<option value="Custom Project">Custom Project</option>
					<option value="Personal Use">Personal Use</option>
				</select>
                </li>
                <li>
                    <label for="comments">Comments</label>
                    <textarea name="comments" cols="20" rows="3"></textarea>
                </li>
            </ul>
        </div>
	</div>
    <div style="display: block;" id="form-right">
        <table id="selectedProdsTable" width="100%">
            <tbody><tr>
                <td style="padding-top:0px;">
                    <label for="product_1" class="grey3">Products I'm Interested In</label>
                    <select name="product_1" id="product_1" class="product">
                
                    {$productdropdown}
                    </select>
                </td>
                <td style="padding-top:0px;">
                    <label for="prodqty" class="grey3">Qty</label>
                    <input name="prodqty_1" id="prodqty_1" value="1" class="prodqty" type="text">
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding-top:0px;">
                    <input name="num_prod_types" id="num_prod_types" class="nocookie" value="1" type="hidden">
                    <span class="baby_blue3 bold underline"><a onclick="javascript:add_product();">+add another product</a></span>
                </td>
            </tr>
        </tbody></table>
        <div style="border-top:1px solid #999; padding:10px 0 0 0; margin-top:10px;">
            <table width="100%">
                <tbody><tr>
                    <td class="grey3" style="padding-top:0px;">Total Products:</td>
                    <td class="grey3" id="total_prods" style="padding-top:0px;">1</td>
                </tr>
            </tbody></table>
        </div>
    </div>
    <div style="clear:both;">
        <div class="grey3" style="margin:10px 0 20px 10px; text-align:left;">
        <input name="mailinglist" id="mailinglist" value="1" checked="checked" class="nocookie" type="checkbox"> 
        I would like to receive exclusive promotions news and updates <br> &nbsp; &nbsp; &nbsp; (we promise the emails will only be magical, not spam!)
        </div>
		
        <table border="0" cellpadding="0" cellspacing="0">
            <tbody><tr>
                <td colspan="2" class="grey3" align="left" valign="middle">
                  For verification, please type the characters displayed in the image below.
                  <br>
                  <br>
                  <input name="cap_code" id="cap_code" value="104871" type="hidden">
                  <img src="captcha.php" align="absmiddle" height="35" width="157">
                  <div class="contact_form">
                    <br>
					
					<input name="captcha" id="captcha" type="text" class="warranty_input2 nocookie" placeholder="Verification Code" required="required" type="text">
					<?php echo $cap; ?>
                  </div>
                  <br>
                 <!-- <button class="submit" name="requestinfo" type="submit">Submit Form</button> -->
                   <div class="submit">
                       <button type="submit" name="requestinfo" id="requestinfo" class="btn btn-default" value="1">{l s='Send'}</button>
		</div>
                </td>
               
                </tr>
				</tbody></table>
				</div>
			</div>
		</form> 
				
			

						<br><br>
		</center>
    </td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
            </tr>
        </tbody></table>
        </div>
        
        </div>
	</div>
</div>
{/if}