<!-- Block user information module NAV  -->
<div id="header_user_info" class="header_user_info clearfix">
	{if $is_logged}
		{if isset($sttheme.welcome_logged) && trim($sttheme.welcome_logged)}{if $sttheme.welcome_link}<a href="{$sttheme.welcome_link}" class="welcome" rel="nofollow">{else}<span class="welcome">{/if}{$sttheme.welcome_logged}{if $sttheme.welcome_link}</a>{else}</span>{/if}{/if}
		<a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" title="{l s='View my customer account' mod='blockuserinfo'}" class="account" rel="nofollow">{$cookie->customer_firstname} {$cookie->customer_lastname}</a>
		<a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" title="{l s='View my customer account' mod='blockuserinfo'}" class="my_account_link" rel="nofollow">{l s='My Account' mod='blockuserinfo'}</a>
		<a class="logout" href="{$link->getPageLink('index', true, NULL, "mylogout")|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log me out' mod='blockuserinfo'}">
			{l s='Sign out' mod='blockuserinfo'}
		</a>
	{else}
		{if isset($sttheme.welcome) && trim($sttheme.welcome)}{if $sttheme.welcome_link}<a href="{$sttheme.welcome_link}" class="welcome" rel="nofollow">{else}<span class="welcome">{/if}{$sttheme.welcome}{if $sttheme.welcome_link}</a>{else}</span>{/if}{/if}
		<a class="login" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Login to your customer account' mod='blockuserinfo'}">
			{l s='Login' mod='blockuserinfo'}
		</a>
		<a class="sing_up" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Login to your customer account' mod='blockuserinfo'}">
			{l s='Sign Up' mod='blockuserinfo'}
		</a>
	{/if}
</div>
<!-- /Block usmodule NAV -->