{if $MENU != ''}
<script type="text/javascript">
{literal}
(function($) {
    $.fn.splitWords = function(index) {
        return this.each(function() {
            var el = $(this),
                i, first, words = el.text().split(/\s/);
            if (typeof index === 'number') {
                i = (index > 0) ? index : words.length + index;
            }
            else {
                i = Math.floor(words.length / 2);
            }
            first = words.splice(0, i);
            el.empty().
                append(makeWrapElem(1, first)).
                append(makeWrapElem(2, words));
        });
    };
    function makeWrapElem(i, wordList) {
        return $('<span class="wrap-' + i + '">' + wordList.join(' ') + ' </span>');
    }
}(jQuery));
$(function() {
    $('#menu-custom > li > a').splitWords();
});
{/literal}
</script>
<div id="menu-wrap" class="clearfix desktop">
	<div id="menu-trigger">{l s='Categories' mod='blocktopmenu'}<i class="menu-icon icon-plus-sign-alt"></i></div>
	<ul id="menu-custom">
			{$MENU}
			{if $MENU_SEARCH}
				<li class="search">
					<form id="searchbox" action="{$link->getPageLink('search')}" method="get">
								<input type="hidden" name="controller" value="search" />
						<input type="hidden" value="position" name="orderby"/>
						<input type="hidden" value="desc" name="orderway"/>
						<input type="text" name="search_query" value="{if isset($smarty.get.search_query)}{$smarty.get.search_query|escape:'htmlall':'UTF-8'}{/if}" placeholder="Search..." />
					</form>
				</li>
			{/if}
		</ul>
	</div>
{/if}