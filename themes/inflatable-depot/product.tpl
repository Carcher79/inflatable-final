{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{include file="$tpl_dir./errors.tpl"}
{if $errors|@count == 0}
	{if !isset($priceDisplayPrecision)}
		{assign var='priceDisplayPrecision' value=2}
	{/if} 
	{if !$priceDisplay || $priceDisplay == 2}
		{assign var='productPrice' value=$product->getPrice(true, $smarty.const.NULL, $priceDisplayPrecision)}
		{assign var='productPriceWithoutReduction' value=$product->getPriceWithoutReduct(false, $smarty.const.NULL)}
	{elseif $priceDisplay == 1}
		{assign var='productPrice' value=$product->getPrice(false, $smarty.const.NULL, $priceDisplayPrecision)}
		{assign var='productPriceWithoutReduction' value=$product->getPriceWithoutReduct(true, $smarty.const.NULL)}
	{/if}
	<div id="center_column">
	<div class="primary_block row" itemscope itemtype="http://schema.org/Product">
		{if !$content_only}
			<div class="container">
				<!-- <div class="top-hr"></div> -->
			</div>
		{/if}
		{if isset($adminActionDisplay) && $adminActionDisplay}
		
			<div id="admin-action">
				<p>{l s='This product is not visible to your customers.'}
					<input type="hidden" id="admin-action-product-id" value="{$product->id}" />
					<input type="submit" value="{l s='Publish'}" name="publish_button" class="exclusive" />
					<input type="submit" value="{l s='Back'}" name="lnk_view" class="exclusive" />
				</p>
				<p id="admin-action-result"></p>
			</div>
		{/if}
		{if isset($confirmation) && $confirmation}
			<p class="confirmation">
				{$confirmation}
			</p>
		{/if}
		<!-- left infos-->  
		<div class="pb-left-column col-xs-12 col-sm-4 col-md-5">
			<!-- product img-->        
			<div id="image-block" class="clearfix">
				<img class="product-image-left" src="../themes/inflatable-depot/img/product-image-left.png" />
				<img class="product-image-bottom" src="../themes/inflatable-depot/img/product-image-bottom.png" />
				{if $product->new}
					<!-- <span class="new-box">
						<span class="new-label">{l s='New'}</span>
					</span> -->
				{/if}
				{if $product->on_sale}
					<span class="sale-box no-print">
						<span class="sale-label">{l s='Sale!'}</span>
					</span>
				{elseif $product->specificPrice && $product->specificPrice.reduction && $productPriceWithoutReduction > $productPrice}
					<span class="discount">{l s='Reduced price!'}</span>
				{/if}
				{if $have_image}
					<span id="view_full_size">
						{if $jqZoomEnabled && $have_image && !$content_only}
							<a class="jqzoom" title="{if !empty($cover.legend)}{$cover.legend|escape:'html':'UTF-8'}{else}{$product->name|escape:'html':'UTF-8'}{/if}" rel="gal1" href="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'thickbox_default')|escape:'html':'UTF-8'}" itemprop="url">
								<img itemprop="image" src="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'large_default')|escape:'html':'UTF-8'}" title="{if !empty($cover.legend)}{$cover.legend|escape:'html':'UTF-8'}{else}{$product->name|escape:'html':'UTF-8'}{/if}" alt="{if !empty($cover.legend)}{$cover.legend|escape:'html':'UTF-8'}{else}{$product->name|escape:'html':'UTF-8'}{/if}"/>
							</a>
						{else}
							<img id="bigpic" itemprop="image" src="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'large_default')|escape:'html':'UTF-8'}" title="{if !empty($cover.legend)}{$cover.legend|escape:'html':'UTF-8'}{else}{$product->name|escape:'html':'UTF-8'}{/if}" alt="{if !empty($cover.legend)}{$cover.legend|escape:'html':'UTF-8'}{else}{$product->name|escape:'html':'UTF-8'}{/if}" width="{$largeSize.width}" height="{$largeSize.height}"/>
							{if !$content_only}
								<span class="span_link no-print">{l s='Zoom'}</span>
							{/if}
						{/if}
					</span>
				{else}
					<span id="view_full_size">
						<img itemprop="image" src="{$img_prod_dir}{$lang_iso}-default-large_default.jpg" id="bigpic" alt="" title="{$product->name|escape:'html':'UTF-8'}" width="{$largeSize.width}" height="{$largeSize.height}"/>
						{if !$content_only}
							<span class="span_link">
								{l s='Zoom'}
							</span>
						{/if}
					</span>
				{/if}
			</div> <!-- end image-block -->
			{if isset($images) && count($images) > 0}
				<!-- thumbnails -->
				<div id="views_block" class="clearfix {if isset($images) && count($images) < 2}hidden{/if}">
					{if isset($images) && count($images) > 4}
						<span class="view_scroll_spacer">
							<a id="view_scroll_left" class="" title="{l s='Other views'}" href="javascript:{ldelim}{rdelim}">
								{l s='Previous'}
							</a>
						</span>
					{/if}
					<div id="thumbs_list">
						<ul id="thumbs_list_frame">
						{if isset($images)}
							{foreach from=$images item=image name=thumbnails}
								{assign var=imageIds value="`$product->id`-`$image.id_image`"}
								{if !empty($image.legend)}
									{assign var=imageTitle value=$image.legend|escape:'html':'UTF-8'}
								{else}
									{assign var=imageTitle value=$product->name|escape:'html':'UTF-8'}
								{/if}
								<li id="thumbnail_{$image.id_image}"{if $smarty.foreach.thumbnails.last} class="last"{/if}>
									<a 
										{if $jqZoomEnabled && $have_image && !$content_only}
											href="javascript:void(0);"
											rel="{literal}{{/literal}gallery: 'gal1', smallimage: '{$link->getImageLink($product->link_rewrite, $imageIds, 'large_default')|escape:'html':'UTF-8'}',largeimage: '{$link->getImageLink($product->link_rewrite, $imageIds, 'thickbox_default')|escape:'html':'UTF-8'}'{literal}}{/literal}"
										{else}
											href="{$link->getImageLink($product->link_rewrite, $imageIds, 'thickbox_default')|escape:'html':'UTF-8'}"
											data-fancybox-group="other-views"
											class="fancybox{if $image.id_image == $cover.id_image} shown{/if}"
										{/if}
										title="{$imageTitle}">
										<img class="img-responsive" id="thumb_{$image.id_image}" src="{$link->getImageLink($product->link_rewrite, $imageIds, 'cart_default')|escape:'html':'UTF-8'}" alt="{$imageTitle}" title="{$imageTitle}" height="{$cartSize.height}" width="{$cartSize.width}" itemprop="image" />
									</a>
								</li>
							{/foreach}
						{/if}
						</ul>
					</div> <!-- end thumbs_list -->
					{if isset($images) && count($images) > 4}
						<a id="view_scroll_right" title="{l s='Other views'}" href="javascript:{ldelim}{rdelim}">
							{l s='Next'}
						</a>
					{/if}
				</div> <!-- end views-block -->
				<!-- end thumbnails -->
			{/if}
			{if isset($images) && count($images) > 1}
				<p class="resetimg clear no-print">
					<span id="wrapResetImages" style="display: none;">
						<a href="{$link->getProductLink($product)|escape:'html':'UTF-8'}" name="resetImages">
							<i class="icon-repeat"></i>
							{l s='Display all pictures'}
						</a>
					</span>
				</p>
			{/if}
		</div> <!-- end pb-left-column -->
		<!-- end left infos--> 
		
		<!-- pb-right-column-->
		<div class="pb-right-column col-xs-12 col-sm-4 col-md-3">

			<!--Added for custom layout-->
			{if $product->online_only}
				<p class="online_only">{l s='Online only'}</p>
			{/if}
	
			<h1 class="product-title" itemprop="name">{if !isset($groups)}{$product->name|escape:'html':'UTF-8'}</h1><div class="product-number">{$product->reference|escape:'html':'UTF-8'}</div>{/if}
			<!-- <p id="product_reference"{if empty($product->reference) || !$product->reference} style="display: none;"{/if}>
				<label>{l s='Model'} </label>
				<span class="editable" itemprop="sku">{if !isset($groups)}{$product->reference|escape:'html':'UTF-8'}{/if}</span>
			</p> -->
			<!-- {capture name=condition}
				{if $product->condition == 'new'}{l s='New'}
				{elseif $product->condition == 'used'}{l s='Used'}
				{elseif $product->condition == 'refurbished'}{l s='Refurbished'}
				{/if}
			{/capture}
			<p id="product_condition"{if !$product->condition} style="display: none;"{/if}>
				<label>{l s='Condition'} </label>
				<span class="editable" itemprop="condition">{$smarty.capture.condition}</span>
			</p> -->
			{if $product->description_short || $packItems|@count > 0}
				<div id="short_description_block">
					
		
					{if $product->description}
						<!-- <div class="info-block">
							<p class="buttons_bottom_block">
								<a href="/contact-us?product={$product->reference|escape:'html':'UTF-8'} | {$product->name|escape:'html':'UTF-8'}" class="button blue-button">
									{l s='Request More Info'}
								</a>
							</p>
						</div> -->
					{/if}

					<!--{if $packItems|@count > 0}
						<div class="short_description_pack">
						<h3>{l s='Pack content'}</h3>
							{foreach from=$packItems item=packItem}
							
							<div class="pack_content">
								{$packItem.pack_quantity} x <a href="{$link->getProductLink($packItem.id_product, $packItem.link_rewrite, $packItem.category)|escape:'html':'UTF-8'}">{$packItem.name|escape:'html':'UTF-8'}</a>
								<p>{$packItem.description_short}</p>
							</div>
							{/foreach}
						</div>
					{/if}-->
				</div> <!-- end short_description_block -->
			{/if}
			
			<div class="info-block">
							<p class="buttons_bottom_block">
								<a href="/contact-us?product={$product->reference|escape:'html':'UTF-8'} | {$product->name|escape:'html':'UTF-8'}" class="button blue-button">
									{l s='Request More Info'}
								</a>
							</p>
						</div>

			{if ($display_qties == 1 && !$PS_CATALOG_MODE && $PS_STOCK_MANAGEMENT && $product->available_for_order)}
				<!-- number of item in stock -->
				<p id="pQuantityAvailable"{if $product->quantity <= 0} style="display: none;"{/if}>
					<span id="quantityAvailable">{$product->quantity|intval}</span>
					<span {if $product->quantity > 1} style="display: none;"{/if} id="quantityAvailableTxt">{l s='Item'}</span>
					<span {if $product->quantity == 1} style="display: none;"{/if} id="quantityAvailableTxtMultiple">{l s='Items'}</span>
				</p>
			{/if}

			{if $PS_STOCK_MANAGEMENT}
				<!-- availability -->
				<p id="availability_statut"{if ($product->quantity <= 0 && !$product->available_later && $allow_oosp) || ($product->quantity > 0 && !$product->available_now) || !$product->available_for_order || $PS_CATALOG_MODE} style="display: none;"{/if}>
					{*<span id="availability_label">{l s='Availability:'}</span>*}
					<span id="availability_value"{if $product->quantity <= 0} class="warning_inline"{/if}>{if $product->quantity <= 0}{if $allow_oosp}{$product->available_later}{else}{l s='This product is no longer in stock'}{/if}{else}{$product->available_now}{/if}</span>				
				</p>
				<p class="warning_inline" id="last_quantities"{if ($product->quantity > $last_qties || $product->quantity <= 0) || $allow_oosp || !$product->available_for_order || $PS_CATALOG_MODE} style="display: none"{/if} >{l s='Warning: Last items in stock!'}</p>
			{/if}
			<p id="availability_date"{if ($product->quantity > 0) || !$product->available_for_order || $PS_CATALOG_MODE || !isset($product->available_date) || $product->available_date < $smarty.now|date_format:'%Y-%m-%d'} style="display: none;"{/if}>
				<span id="availability_date_label">{l s='Availability date:'}</span>
				<span id="availability_date_value">{dateFormat date=$product->available_date full=false}</span>
			</p>

			<!-- Out of stock hook -->
			<div id="oosHook"{if $product->quantity > 0} style="display: none;"{/if}>
				{$HOOK_PRODUCT_OOS}
			</div>
	
			{if isset($HOOK_EXTRA_RIGHT) && $HOOK_EXTRA_RIGHT}{$HOOK_EXTRA_RIGHT}{/if}
			
			<!-- <div class="relative-icons">
				<img src="../themes/inflatable-depot/img/relative-icon.png" />
			</div> -->
			<!--End of custom layout-->
			
			<div class="product-logo">
				<img src="#" />
			</div>

			{if ($product->show_price && !isset($restricted_country_mode)) || isset($groups) || $product->reference || (isset($HOOK_PRODUCT_ACTIONS) && $HOOK_PRODUCT_ACTIONS)}
			<!-- add to cart form-->
			<form id="buy_block"{if $PS_CATALOG_MODE && !isset($groups) && $product->quantity > 0} class="hidden"{/if} action="{$link->getPageLink('cart')|escape:'html':'UTF-8'}" method="post">
				<!-- hidden datas -->
				<p class="hidden">
					<input type="hidden" name="token" value="{$static_token}" />
					<input type="hidden" name="id_product" value="{$product->id|intval}" id="product_page_product_id" />
					<input type="hidden" name="add" value="1" />
					<input type="hidden" name="id_product_attribute" id="idCombination" value="" />
				</p>
				<div class="box-info-product">
					<div class="content_prices clearfix">
						{if $product->show_price && !isset($restricted_country_mode) && !$PS_CATALOG_MODE}
							<!-- prices -->
							<div class="price">
								<p class="our_price_display" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
									{if $product->quantity > 0}<link itemprop="availability" href="http://schema.org/InStock"/>{/if}
									{if $priceDisplay >= 0 && $priceDisplay <= 2}
										<span id="our_price_display" itemprop="price">{convertPrice price=$productPrice}</span>
										<!--{if $tax_enabled  && ((isset($display_tax_label) && $display_tax_label == 1) || !isset($display_tax_label))}
											{if $priceDisplay == 1}{l s='tax excl.'}{else}{l s='tax incl.'}{/if}
										{/if}-->
										<meta itemprop="priceCurrency" content="{$currency->iso_code}" />
									{/if}
								</p>
								<p id="reduction_percent" {if !$product->specificPrice || $product->specificPrice.reduction_type != 'percentage'} style="display:none;"{/if}>
									<span id="reduction_percent_display">
										{if $product->specificPrice && $product->specificPrice.reduction_type == 'percentage'}-{$product->specificPrice.reduction*100}%{/if}
									</span>
								</p>
								<p id="old_price"{if (!$product->specificPrice || !$product->specificPrice.reduction) && $group_reduction == 0} class="hidden"{/if}>
									{if $priceDisplay >= 0 && $priceDisplay <= 2}
										<span id="old_price_display">{if $productPriceWithoutReduction > $productPrice}{convertPrice price=$productPriceWithoutReduction}{/if}</span>
										<!-- {if $tax_enabled && $display_tax_label == 1}{if $priceDisplay == 1}{l s='tax excl.'}{else}{l s='tax incl.'}{/if}{/if} -->
									{/if}
								</p>
								{if $priceDisplay == 2}
									<br />
									<span id="pretaxe_price">
										<span id="pretaxe_price_display">{convertPrice price=$product->getPrice(false, $smarty.const.NULL)}</span>
										{l s='tax excl.'}
									</span>
								{/if}
							</div> <!-- end prices -->
							<p id="reduction_amount" {if !$product->specificPrice || $product->specificPrice.reduction_type != 'amount' || $product->specificPrice.reduction|floatval ==0} style="display:none"{/if}>
								<span id="reduction_amount_display">
								{if $product->specificPrice && $product->specificPrice.reduction_type == 'amount' && $product->specificPrice.reduction|intval !=0}
									-{convertPrice price=$productPriceWithoutReduction-$productPrice|floatval}
								{/if}
								</span>
							</p>
							{if $packItems|@count && $productPrice < $product->getNoPackPrice()}
								<p class="pack_price">{l s='Instead of'} <span style="text-decoration: line-through;">{convertPrice price=$product->getNoPackPrice()}</span></p>
							{/if}
							{if $product->ecotax != 0}
								<p class="price-ecotax">{l s='Include'} <span id="ecotax_price_display">{if $priceDisplay == 2}{$ecotax_tax_exc|convertAndFormatPrice}{else}{$ecotax_tax_inc|convertAndFormatPrice}{/if}</span> {l s='For green tax'}
									{if $product->specificPrice && $product->specificPrice.reduction}
									<br />{l s='(not impacted by the discount)'}
									{/if}
								</p>
							{/if}
							{if !empty($product->unity) && $product->unit_price_ratio > 0.000000}
								{math equation="pprice / punit_price"  pprice=$productPrice  punit_price=$product->unit_price_ratio assign=unit_price}
								<p class="unit-price"><span id="unit_price_display">{convertPrice price=$unit_price}</span> {l s='per'} {$product->unity|escape:'html':'UTF-8'}</p>
							{/if}
						{/if} {*close if for show price*}
						<div class="clear"></div>
					</div> <!-- end content_prices -->
					<div class="product_attributes clearfix">
						<!-- quantity wanted -->
						{if !$PS_CATALOG_MODE}
						<p id="quantity_wanted_p"{if (!$allow_oosp && $product->quantity <= 0) || !$product->available_for_order || $PS_CATALOG_MODE} style="display: none;"{/if}>
							<label>{l s='Quantity:'}</label>
							<input type="text" name="qty" id="quantity_wanted" class="text" value="{if isset($quantityBackup)}{$quantityBackup|intval}{else}{if $product->minimal_quantity > 1}{$product->minimal_quantity}{else}1{/if}{/if}" />
							<a href="#" data-field-qty="qty" class="btn btn-default button-minus product_quantity_down">
								<span><i class="icon-minus"></i></span>
							</a>
							<a href="#" data-field-qty="qty" class="btn btn-default button-plus product_quantity_up ">
								<span><i class="icon-plus"></i></span>
							</a>
							<span class="clearfix"></span>
						</p>
						{/if}
						<!-- minimal quantity wanted -->
						<p id="minimal_quantity_wanted_p"{if $product->minimal_quantity <= 1 || !$product->available_for_order || $PS_CATALOG_MODE} style="display: none;"{/if}>
							{l s='This product is not sold individually. You must select at least'} <b id="minimal_quantity_label">{$product->minimal_quantity}</b> {l s='quantity for this product.'}
						</p>
						{if isset($groups)}
							<!-- attributes -->
							<div id="attributes">
								<div class="clearfix"></div>
								{foreach from=$groups key=id_attribute_group item=group}
									{if $group.attributes|@count}
										<fieldset class="attribute_fieldset">
											<label class="attribute_label" {if $group.group_type != 'color' && $group.group_type != 'radio'}for="group_{$id_attribute_group|intval}"{/if}>{$group.name|escape:'html':'UTF-8'} :&nbsp;</label>
											{assign var="groupName" value="group_$id_attribute_group"}
											<div class="attribute_list">
												{if ($group.group_type == 'select')}
													<select name="{$groupName}" id="group_{$id_attribute_group|intval}" class="form-control attribute_select no-print">
														{foreach from=$group.attributes key=id_attribute item=group_attribute}
															<option value="{$id_attribute|intval}"{if (isset($smarty.get.$groupName) && $smarty.get.$groupName|intval == $id_attribute) || $group.default == $id_attribute} selected="selected"{/if} title="{$group_attribute|escape:'html':'UTF-8'}">{$group_attribute|escape:'html':'UTF-8'}</option>
														{/foreach}
													</select>
												{elseif ($group.group_type == 'color')}
													<ul id="color_to_pick_list" class="clearfix">
														{assign var="default_colorpicker" value=""}
														{foreach from=$group.attributes key=id_attribute item=group_attribute}
															<li{if $group.default == $id_attribute} class="selected"{/if}>
																<a href="{$link->getProductLink($product)|escape:'html':'UTF-8'}" id="color_{$id_attribute|intval}" name="{$colors.$id_attribute.name|escape:'html':'UTF-8'}" class="color_pick{if ($group.default == $id_attribute)} selected{/if}" style="background: {$colors.$id_attribute.value|escape:'html':'UTF-8'};" title="{$colors.$id_attribute.name|escape:'html':'UTF-8'}">
																	{if file_exists($col_img_dir|cat:$id_attribute|cat:'.jpg')}
																		<img src="{$img_col_dir}{$id_attribute|intval}.jpg" alt="{$colors.$id_attribute.name|escape:'html':'UTF-8'}" width="20" height="20" />
																	{/if}
																</a>
															</li>
															{if ($group.default == $id_attribute)}
																{$default_colorpicker = $id_attribute}
															{/if}
														{/foreach}
													</ul>
													<input type="hidden" class="color_pick_hidden" name="{$groupName|escape:'html':'UTF-8'}" value="{$default_colorpicker|intval}" />
												{elseif ($group.group_type == 'radio')}
													<ul>
														{foreach from=$group.attributes key=id_attribute item=group_attribute}
															<li>
																<input type="radio" class="attribute_radio" name="{$groupName|escape:'html':'UTF-8'}" value="{$id_attribute}" {if ($group.default == $id_attribute)} checked="checked"{/if} />
																<span>{$group_attribute|escape:'html':'UTF-8'}</span>
															</li>
														{/foreach}
													</ul>
												{/if}
											</div> <!-- end attribute_list -->
										</fieldset>
									{/if}
								{/foreach}
							</div> <!-- end attributes -->
						{/if}
					</div> <!-- end product_attributes -->
					<div class="box-cart-bottom">
						<div{if (!$allow_oosp && $product->quantity <= 0) || !$product->available_for_order || (isset($restricted_country_mode) && $restricted_country_mode) || $PS_CATALOG_MODE} class="unvisible"{/if}>
							<p id="add_to_cart" class="buttons_bottom_block no-print">
								<button type="submit" name="Submit" class="exclusive">
									<span>{l s='Add to cart'}</span>
								</button>
							</p>
						</div>
						{if isset($HOOK_PRODUCT_ACTIONS) && $HOOK_PRODUCT_ACTIONS}{$HOOK_PRODUCT_ACTIONS}{/if}<strong></strong>
					</div> <!-- end box-cart-bottom -->
				</div> <!-- end box-info-product -->
			</form>
			{/if}
			{if $product->description_short}
				<div id="short_description_content" class="align_justify" itemprop="description">{$product->description_short}</div>
			{/if}
		</div> <!-- end pb-right-column-->

		<!-- center infos -->
		<div class="pb-center-column col-xs-12 col-sm-4">
			
		</div>
		<!-- end center infos-->

		
	</div> <!-- end primary_block -->
	{if !$content_only}
{if (isset($quantity_discounts) && count($quantity_discounts) > 0)}
			<!-- quantity discount -->
			<section class="page-product-box">
				<h3 class="page-product-heading">{l s='Volume discounts'}</h3>
				<div id="quantityDiscount">
					<table class="std table-product-discounts">
						<thead>
							<tr>
								<th>{l s='Quantity'}</th>
								<th>{if $display_discount_price}{l s='Price'}{else}{l s='Discount'}{/if}</th>
								<th>{l s='You Save'}</th>
							</tr>
						</thead>
						<tbody>
							{foreach from=$quantity_discounts item='quantity_discount' name='quantity_discounts'}
							<tr id="quantityDiscount_{$quantity_discount.id_product_attribute}" class="quantityDiscount_{$quantity_discount.id_product_attribute}" data-discount-type="{$quantity_discount.reduction_type}" data-discount="{$quantity_discount.real_value|floatval}" data-discount-quantity="{$quantity_discount.quantity|intval}">
								<td>
									{$quantity_discount.quantity|intval}
								</td>
								<td>
									{if $quantity_discount.price >= 0 || $quantity_discount.reduction_type == 'amount'}
										{if $display_discount_price}
											{convertPrice price=$productPrice-$quantity_discount.real_value|floatval}
										{else}
											{convertPrice price=$quantity_discount.real_value|floatval}
										{/if}
									{else}
										{if $display_discount_price}
											{convertPrice price = $productPrice-($productPrice*$quantity_discount.reduction)|floatval}
										{else}
											{$quantity_discount.real_value|floatval}%
										{/if}
									{/if}
								</td>
								<td>
									<span>{l s='Up to'}</span>
									{if $quantity_discount.price >= 0 || $quantity_discount.reduction_type == 'amount'}
										{$discountPrice=$productPrice-$quantity_discount.real_value|floatval}
									{else}
										{$discountPrice=$productPrice-($productPrice*$quantity_discount.reduction)|floatval}
									{/if}
									{$discountPrice=$discountPrice*$quantity_discount.quantity}
									{$qtyProductPrice = $productPrice*$quantity_discount.quantity}
									{convertPrice price=$qtyProductPrice-$discountPrice}
								</td>
							</tr>
							{/foreach}
						</tbody>
					</table>
				</div>
			</section>
		{/if}
		<div class="product-details">
			<p class="subtitle">Product Details</p>
		{if isset($features) && $features}
			<!-- Data sheet -->
			<section class="page-product-box" style="display:none;">
				<h3 class="page-product-heading">{l s='Data sheet'}</h3>
				<table class="table-data-sheet">			
					{foreach from=$features item=feature}
					<tr class="{cycle values="odd,even"}">
						{if isset($feature.value)}	
						
						<!--Store featured values-->	

						{if $feature.name|escape:'html':'UTF-8' == 'WEB-Length-feet'}
							{$web_length_feet = $feature.value}
						{/if}

						{if $feature.name|escape:'html':'UTF-8' == 'WEB-Length-mts'}
							{$web_length_mts = $feature.value}
						{/if}

						{if $feature.name|escape:'html':'UTF-8' == 'WEB-Width-feet'}
							{$web_width_feet = $feature.value}
						{/if}

						{if $feature.name|escape:'html':'UTF-8' == 'WEB-Width-mts'}
							{$web_width_mts = $feature.value}
						{/if}

						{if $feature.name|escape:'html':'UTF-8' == 'WEB-Height-feet'}
							{$web_height_feet = $feature.value}
						{/if}

						{if $feature.name|escape:'html':'UTF-8' == 'WEB-Height-mts'}
							{$web_height_mts = $feature.value}
						{/if}

						{if $feature.name|escape:'html':'UTF-8' == 'Slide-sitting-height-feet'}
							{$slide_sitting_height_feet = $feature.value}
						{/if}

						{if $feature.name|escape:'html':'UTF-8' == 'Slide-sitting-height-mts'}
							{$slide_sitting_height_mts = $feature.value}
						{/if}

						{if $feature.name|escape:'html':'UTF-8' == 'WEB-Weight-Lbs'}
							{$web_weight_lbs = $feature.value}
						{/if}

						{if $feature.name|escape:'html':'UTF-8' == 'WEB-Weight-Kgs'}
							{$web_weight_kgs = $feature.value}
						{/if}

						{if $feature.name|escape:'html':'UTF-8' == 'Ship-length-inch'}
							{$ship_length_inch= $feature.value}
						{/if}

						{if $feature.name|escape:'html':'UTF-8' == 'Ship-length-cms'}
							{$ship_length_cms = $feature.value}
						{/if}

						{if $feature.name|escape:'html':'UTF-8' == 'Ship-width-inch'}
							{$ship_width_inch = $feature.value}
						{/if}

						{if $feature.name|escape:'html':'UTF-8' == 'Ship-width-cms'}
							{$ship_width_cms = $feature.value}
						{/if}

						{if $feature.name|escape:'html':'UTF-8' == 'Ship-height-inch'}
							{$ship_height_inch = $feature.value}
						{/if}

						{if $feature.name|escape:'html':'UTF-8' == 'Ship-height-cms'}
							{$ship_height_cms = $feature.value}
						{/if}

						{if $feature.name|escape:'html':'UTF-8' == 'Blowers'}
							{$blowers = $feature.value}
						{/if}

						{if $feature.name|escape:'html':'UTF-8' == 'Pieces'}
							{$pieces = $feature.value}
						{/if}
						<!--End storing featured values-->

						<td>{$feature.name|escape:'html':'UTF-8'}</td>
						<td>{$feature.value|escape:'html':'UTF-8'}</td>
						{/if}
					</tr>
					{/foreach}
				</table>
			</section>
			<!--end Data sheet -->
		{/if}

		{if $product->description}
			<!-- More info -->
			<section class="page-product-box details-box">
				<!-- <h3 class="page-product-heading">{l s='More info'}</h3> -->{/if}

				<!-- SELECT * FROM ps_detail_product WHERE id_product = 459 -->

				{if isset($product) && $product->description}
					<!-- full description -->
					<div class="pb-center-column">
						<div class="product-description">{$product->description}</div>
						<div class="back-to-top"><a href="#">Back to Top</a></div>
					</div>
					<div class="pb-center-column">
						<div class="pb-left-column">
						<div class="blue-prints">
							<p class="orange-subtitle">Blue Prints</p>
							<img src="#" />
						</div>
						</div>
						<div class="pb-right-column">
						<div class="safety-first">
							<p class="orange-subtitle">Safety First</p>
							<div class="safety-first-content">
								<p>-18oz Heavy Duty, Flame Retardant, Lead Free</p>
								<p>-Special Reinforcements in high transit areas.</p>
								<p>-Customization &amp; Design Assistance</p>
							</div>
							<img src="/themes/inflatable-depot/img/safety-icon-1.png" alt="" width="50" height="56">
							<img src="/themes/inflatable-depot/img/safety-icon-2.png" alt="" width="50" height="56">
							<img src="/themes/inflatable-depot/img/safety-icon-3.png" alt="" width="50" height="56">
							<img src="/themes/inflatable-depot/img/safety-icon-4.png" alt="" width="50" height="56">
						</div>
						</div>
					</div>
					<div class="additional-info-box">
						<img id="facts-bg" src="/themes/inflatable-depot/img/facts-bg.png" alt="" width="50" height="56">
						<div class="row-top">
							<div class="additional-info-column-left">
								<p class="orange-subtitle">Facts</p>
								<div class="facts-info">
									<p>
										<span class="dimension-label">Length:</span>
										{if isset($web_length_feet)}{$web_length_feet}&nbsp;/&nbsp;{else}{l s='N/A'}{/if}
										{if isset($web_length_mts)}{$web_length_mts}&nbsp;mts{else}{l s='N/A'}{/if}
										<span class="dimension-label">Width:</span>
										{if isset($web_width_feet)}{$web_width_feet}&nbsp;/&nbsp;{else}{l s='N/A'}{/if}
										{if isset($web_width_mts)}{$web_width_mts}&nbsp;mts{else}{l s='N/A'}{/if}
										<span class="dimension-label">Height:</span>
										{if isset($web_height_feet)}{$web_height_feet}&nbsp;/&nbsp;{else}{l s='N/A'}{/if}
										{if isset($web_height_mts)}{$web_height_mts}&nbsp;mts{else}{l s='N/A'}{/if}
									</p>
								</div>
								<p class="orange-subtitle">Shipping Dimensions</p>
								<div class="shipping-dimensions-info">
									<p>
										<span class="dimension-label">Length:</span>
										{if isset($ship_length_inch)}{$ship_length_inch}&nbsp;/&nbsp;{else}{l s='N/A'}{/if}
										{if isset($ship_length_cms)}{$ship_length_cms}&nbsp;cm{else}{l s='N/A'}{/if}
										<span class="dimension-label">Width:</span>
										{if isset($ship_width_inch)}{$ship_width_inch}&nbsp;/&nbsp;{else}{l s='N/A'}{/if}
										{if isset($ship_width_cms)}{$ship_width_cms}&nbsp;cm{else}{l s='N/A'}{/if}
										<span class="dimension-label">Height:</span>
										{if isset($ship_height_inch)}{$ship_height_inch}&nbsp;/&nbsp;{else}{l s='N/A'}{/if}
										{if isset($ship_height_cms)}{$ship_height_cms}&nbsp;cm{else}{l s='N/A'}{/if}
									</p>
								</div>
							</div>
							<div class="additional-info-column-right">
								<p class="orange-subtitle">Additional Info</p>
								<div class="additional-info-info">
									<div>
										<img src="/themes/inflatable-depot/img/weight-icon.jpg" alt="" width="" height="" />
										<div class="addtional-info-output">
											{if isset($web_weight_lbs) && isset($web_weight_kgs)}
												{$web_weight_lbs} lbs / {$web_weight_kgs} kgs
											{/if}
											{if isset($web_weight_lbs) && !isset($web_weight_kgs)}
												{$web_weight_lbs} lbs
											{/if}
											{if !isset($web_weight_lbs) && isset($web_weight_kgs)}
												{$web_weight_kgs} kgs
											{/if}
											{if !isset($web_weight_lbs) && !isset($web_weight_kgs)}
												{l s='N/A'}
											{/if}
											</div>
									</div>
									<div>
										<img src="/themes/inflatable-depot/img/slide-icon.png" alt="" width="" height="" />
										<div class="addtional-info-output">
											{if isset($slide_sitting_height_feet) && isset($slide_sitting_height_mts)}
												{$slide_sitting_height_feet}' / {$slide_sitting_height_mts} mts
											{/if}
											{if isset($slide_sitting_height_feet) && !isset($slide_sitting_height_mts)}
												{$slide_sitting_height_feet}'
											{/if}
											{if !isset($slide_sitting_height_feet) && isset($slide_sitting_height_mts)}
												{$slide_sitting_height_mts} mts
											{/if}
											{if !isset($slide_sitting_height_feet) && !isset($slide_sitting_height_mts)}
												{l s='N/A'}
											{/if}
										</div>
									</div>
									<div>
										<img src="/themes/inflatable-depot/img/blow-icon.jpg" alt="" width="" height="" />
										<div class="addtional-info-output">
											{if isset($blowers)}
												{if ($blowers==1)}{$blowers} Blower{/if}
												{if ($blowers>1)}{$blowers} Blowers{/if}
											{/if}
											{if !isset($blowers)}
												{l s='N/A'}
											{/if}
										</div>
									</div>
									<div>
										<img src="/themes/inflatable-depot/img/pieces-icon.png" alt="" width="" height="" />
										<div class="addtional-info-output">
											{if isset($pieces)}
												{if ($pieces==1)}{$pieces} Piece{/if}
												{if ($pieces>1)}{$pieces} Pieces{/if}
											{/if}
											{if !isset($pieces)}
												{l s='N/A'}
											{/if}
											</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row-bottom">
							<p class="orange-subtitle">Product Features</p>
							<div class="product-features-info"></div>
						</div>
					</div>
					<div class="big-image-box">
						<div class="big-image-info">
							<div class="big-image-header"></div>
							<p class="big-image-text maintext"></p>
							<img src="#" />
						</div>
					</div>
					<div class="virtual-tour-box">
						<div class="virtual-tour-column-left">
							<p class="big-image-header">Video Overview</p>
							<div class="virtual-tour-text"></div>
						</div>
						<div class="virtual-tour-column-right">
							<img src="/themes/inflatable-depot/img/video-bg.png" alt="" width="" height="" />
							<div class="virtual-tour-video"></div>
						</div>
					</div>
					<div class="product-slides-box">
						<div class="product-slides-container">
							<div class="product-slides-header">Totally Customizable</div>
							<div class="product-slides-info"></div>
							{hook h="displaySlidersPro" slider="product_slider"}
						</div>
					</div>
					<div class="product-slides-box-extra-slides">
						<div class="set1">
							<img src="" style=""/>
							<img src="" style=""/>
							<img src="" style=""/>
						</div>
						<div class="set2">
							<img src="" style=""/>
							<img src="" style=""/>
							<img src="" style=""/>
						</div>
						<div class="set3">
							<img src="" style=""/>
							<img src="" style=""/>
							<img src="" style=""/>
						</div>
						<div class="set4">
							<img src="" style=""/>
							<img src="" style=""/>
							<img src="" style=""/>
						</div>
					</div>
					<div class="product-slides-more">
					</div>
					<div class="product-reviews">
						<p class="title">Product Reviews</p>
						<div class="review-content"></div>
					</div>
			</section>
			<!--end  More info -->
		{/if}
		<!--HOOK_PRODUCT_TAB -->
		<section class="page-product-box">
			{$HOOK_PRODUCT_TAB}
			{if isset($HOOK_PRODUCT_TAB_CONTENT) && $HOOK_PRODUCT_TAB_CONTENT}{$HOOK_PRODUCT_TAB_CONTENT}{/if}
		</section> 
		<!--end HOOK_PRODUCT_TAB -->
		{if isset($accessories) && $accessories}
			<!--Accessories -->
			<section class="page-product-box">
				<h3 class="page-product-heading">{l s='Accessories'}</h3>
				<div class="block products_block accessories-block clearfix">
					<div class="block_content">
						<ul id="bxslider" class="bxslider clearfix">
							{foreach from=$accessories item=accessory name=accessories_list}
								{if ($accessory.allow_oosp || $accessory.quantity_all_versions > 0 || $accessory.quantity > 0) && $accessory.available_for_order && !isset($restricted_country_mode)}
									{assign var='accessoryLink' value=$link->getProductLink($accessory.id_product, $accessory.link_rewrite, $accessory.category)}
									<li class="item product-box ajax_block_product{if $smarty.foreach.accessories_list.first} first_item{elseif $smarty.foreach.accessories_list.last} last_item{else} item{/if} product_accessories_description">
										<div class="product_desc">
											<a href="{$accessoryLink|escape:'html':'UTF-8'}" title="{$accessory.legend|escape:'html':'UTF-8'}" class="product-image product_image">
												<img class="lazyOwl" src="{$link->getImageLink($accessory.link_rewrite, $accessory.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="{$accessory.legend|escape:'html':'UTF-8'}" width="{$homeSize.width}" height="{$homeSize.height}"/>
											</a>
											<div class="block_description">
												<a href="{$accessoryLink|escape:'html':'UTF-8'}" title="{l s='More'}" class="product_description">
													{$accessory.description_short|strip_tags|truncate:25:'...'}
												</a>
											</div>
										</div>
										<div class="s_title_block">
											<h5 class="product-name">
												<a href="{$accessoryLink|escape:'html':'UTF-8'}">
													{$accessory.name|truncate:20:'...':true|escape:'html':'UTF-8'}
												</a>
											</h5>
											{if $accessory.show_price && !isset($restricted_country_mode) && !$PS_CATALOG_MODE}
											<span class="price">
												{if $priceDisplay != 1}
												{displayWtPrice p=$accessory.price}{else}{displayWtPrice p=$accessory.price_tax_exc}
												{/if}
											</span>
											{/if}
										</div>
										<div class="clearfix" style="margin-top:5px">
											{if !$PS_CATALOG_MODE && ($accessory.allow_oosp || $accessory.quantity > 0)}
												<div class="no-print">
													<a class="exclusive button ajax_add_to_cart_button" href="{$link->getPageLink('cart', true, NULL, "qty=1&amp;id_product={$accessory.id_product|intval}&amp;token={$static_token}&amp;add")|escape:'html':'UTF-8'}" data-id-product="{$accessory.id_product|intval}" title="{l s='Add to cart'}">
														<span>{l s='Add to cart'}</span>
													</a>
												</div>
											{/if}
										</div>
									</li>
								{/if}
							{/foreach}
						</ul>
					</div>
				</div>	
			</section>
			<!--end Accessories -->
		{/if}
		{if isset($HOOK_PRODUCT_FOOTER) && $HOOK_PRODUCT_FOOTER}{$HOOK_PRODUCT_FOOTER}{/if}
		<!-- description & features -->
		{if (isset($product) && $product->description) || (isset($features) && $features) || (isset($accessories) && $accessories) || (isset($HOOK_PRODUCT_TAB) && $HOOK_PRODUCT_TAB) || (isset($attachments) && $attachments) || isset($product) && $product->customizable}
			{if isset($attachments) && $attachments}
			<!--Download -->
			<section class="page-product-box">
				<h3 class="page-product-heading">{l s='Download'}</h3>
				{foreach from=$attachments item=attachment name=attachements}
					{if $smarty.foreach.attachements.iteration %3 == 1}<div class="row">{/if}
						<div class="col-lg-4">
							<h4><a href="{$link->getPageLink('attachment', true, NULL, "id_attachment={$attachment.id_attachment}")|escape:'html':'UTF-8'}">{$attachment.name|escape:'html':'UTF-8'}</a></h4>
							<p class="text-muted">{$attachment.description|escape:'html':'UTF-8'}</p>
							<a class="btn btn-default btn-block" href="{$link->getPageLink('attachment', true, NULL, "id_attachment={$attachment.id_attachment}")|escape:'html':'UTF-8'}">
								<i class="icon-download"></i>
								{l s="Download"} ({Tools::formatBytes($attachment.file_size, 2)})
							</a>
							<hr>
						</div>
					{if $smarty.foreach.attachements.iteration %3 == 0 || $smarty.foreach.attachements.last}</div>{/if}
				{/foreach}
			</section>
			<!--end Download -->
			{/if}
			{if isset($product) && $product->customizable}
			<!--Customization -->
			<section class="page-product-box">
				<h3 class="page-product-heading">{l s='Product customization'}</h3>
				<!-- Customizable products -->
				<form method="post" action="{$customizationFormTarget}" enctype="multipart/form-data" id="customizationForm" class="clearfix">
					<p class="infoCustomizable">
						{l s='After saving your customized product, remember to add it to your cart.'}
						{if $product->uploadable_files}
						<br />
						{l s='Allowed file formats are: GIF, JPG, PNG'}{/if}
					</p>
					{if $product->uploadable_files|intval}
						<div class="customizableProductsFile">
							<h5 class="product-heading-h5">{l s='Pictures'}</h5>
							<ul id="uploadable_files" class="clearfix">
								{counter start=0 assign='customizationField'}
								{foreach from=$customizationFields item='field' name='customizationFields'}
									{if $field.type == 0}
										<li class="customizationUploadLine{if $field.required} required{/if}">{assign var='key' value='pictures_'|cat:$product->id|cat:'_'|cat:$field.id_customization_field}
											{if isset($pictures.$key)}
												<div class="customizationUploadBrowse">
													<img src="{$pic_dir}{$pictures.$key}_small" alt="" />
														<a href="{$link->getProductDeletePictureLink($product, $field.id_customization_field)|escape:'html':'UTF-8'}" title="{l s='Delete'}" >
															<img src="{$img_dir}icon/delete.gif" alt="{l s='Delete'}" class="customization_delete_icon" width="11" height="13" />
														</a>
												</div>
											{/if}
											<div class="customizationUploadBrowse form-group">
												<label class="customizationUploadBrowseDescription">
													{if !empty($field.name)}
														{$field.name}
													{else}
														{l s='Please select an image file from your computer'}
													{/if}
													{if $field.required}<sup>*</sup>{/if}
												</label>
												<input type="file" name="file{$field.id_customization_field}" id="img{$customizationField}" class="form-control customization_block_input {if isset($pictures.$key)}filled{/if}" />
											</div>
										</li>
										{counter}
									{/if}
								{/foreach}
							</ul>
						</div>
					{/if}
					{if $product->text_fields|intval}
						<div class="customizableProductsText">
							<h5 class="product-heading-h5">{l s='Text'}</h5>
							<ul id="text_fields">
							{counter start=0 assign='customizationField'}
							{foreach from=$customizationFields item='field' name='customizationFields'}
								{if $field.type == 1}
									<li class="customizationUploadLine{if $field.required} required{/if}">
										<label for ="textField{$customizationField}">
											{assign var='key' value='textFields_'|cat:$product->id|cat:'_'|cat:$field.id_customization_field}
											{if !empty($field.name)}
												{$field.name}
											{/if}
											{if $field.required}<sup>*</sup>{/if}
										</label>
										<textarea name="textField{$field.id_customization_field}" class="form-control customization_block_input" id="textField{$customizationField}" rows="3" cols="20">{strip}
											{if isset($textFields.$key)}
												{$textFields.$key|stripslashes}
											{/if}
										{/strip}</textarea>
									</li>
									{counter}
								{/if}
							{/foreach}
							</ul>
						</div>
					{/if}
					<p id="customizedDatas">
						<input type="hidden" name="quantityBackup" id="quantityBackup" value="" />
						<input type="hidden" name="submitCustomizedDatas" value="1" />
						<button class="button btn btn-default button button-small" name="saveCustomization">
							<span>{l s='Save'}</span>
						</button>
						<span id="ajax-loader" class="unvisible">
							<img src="{$img_ps_dir}loader.gif" alt="loader" />
						</span>
					</p>
				</form>
				<p class="clear required"><sup>*</sup> {l s='required fields'}</p>	
			</section>
			<!--end Customization -->
			{/if}
		{/if}
		{if isset($packItems) && $packItems|@count > 0}
		<section id="blockpack">
			<h3 class="page-product-heading">{l s='Pack content'}</h3>
			{include file="$tpl_dir./product-list.tpl" products=$packItems}
		</section>
	</div>
		{/if}
	{/if}
{strip}
{strip}
{if isset($smarty.get.ad) && $smarty.get.ad}
{addJsDefL name=ad}{$base_dir|cat:$smarty.get.ad|escape:'html':'UTF-8'}{/addJsDefL}
{/if}
{if isset($smarty.get.adtoken) && $smarty.get.adtoken}
{addJsDefL name=adtoken}{$smarty.get.adtoken|escape:'html':'UTF-8'}{/addJsDefL}
{/if}
{addJsDef allowBuyWhenOutOfStock=$allow_oosp|boolval}
{addJsDef availableNowValue=$product->available_now|escape:'quotes':'UTF-8'}
{addJsDef availableLaterValue=$product->available_later|escape:'quotes':'UTF-8'}
{addJsDef attribute_anchor_separator=$attribute_anchor_separator|addslashes}
{addJsDef attributesCombinations=$attributesCombinations}
{addJsDef currencySign=$currencySign|html_entity_decode:2:"UTF-8"}
{addJsDef currencyRate=$currencyRate|floatval}
{addJsDef currencyFormat=$currencyFormat|intval}
{addJsDef currencyBlank=$currencyBlank|intval}
{addJsDef currentDate=$smarty.now|date_format:'%Y-%m-%d %H:%M:%S'}
{if isset($combinations) && $combinations}
	{addJsDef combinations=$combinations}
	{addJsDef combinationsFromController=$combinations}
	{addJsDef displayDiscountPrice=$display_discount_price}
	{addJsDefL name='upToTxt'}{l s='Up to' js=1}{/addJsDefL}
{/if}
{if isset($combinationImages) && $combinationImages}
	{addJsDef combinationImages=$combinationImages}
{/if}
{addJsDef customizationFields=$customizationFields}
{addJsDef default_eco_tax=$product->ecotax|floatval}
{addJsDef displayPrice=$priceDisplay|intval}
{addJsDef ecotaxTax_rate=$ecotaxTax_rate|floatval}
{addJsDef group_reduction=$group_reduction}
{if isset($cover.id_image_only)}
	{addJsDef idDefaultImage=$cover.id_image_only|intval}
{else}
	{addJsDef idDefaultImage=0}
{/if}
{addJsDef img_ps_dir=$img_ps_dir}
{addJsDef img_prod_dir=$img_prod_dir}
{addJsDef id_product=$product->id|intval}
{addJsDef jqZoomEnabled=$jqZoomEnabled|boolval}
{addJsDef maxQuantityToAllowDisplayOfLastQuantityMessage=$last_qties|intval}
{addJsDef minimalQuantity=$product->minimal_quantity|intval}
{addJsDef noTaxForThisProduct=$no_tax|boolval}
{addJsDef oosHookJsCodeFunctions=Array()}
{addJsDef productHasAttributes=isset($groups)|boolval}
{addJsDef productPriceTaxExcluded=($product->getPriceWithoutReduct(true)|default:'null' - $product->ecotax)|floatval}
{addJsDef productBasePriceTaxExcluded=($product->base_price - $product->ecotax)|floatval}
{addJsDef productReference=$product->reference|escape:'html':'UTF-8'}
{addJsDef productAvailableForOrder=$product->available_for_order|boolval}
{addJsDef productPriceWithoutReduction=$productPriceWithoutReduction|floatval}
{addJsDef productPrice=$productPrice|floatval}
{addJsDef productUnitPriceRatio=$product->unit_price_ratio|floatval}
{addJsDef productShowPrice=(!$PS_CATALOG_MODE && $product->show_price)|boolval}
{addJsDef PS_CATALOG_MODE=$PS_CATALOG_MODE}
{if $product->specificPrice && $product->specificPrice|@count}
	{addJsDef product_specific_price=$product->specificPrice}
{else}
	{addJsDef product_specific_price=array()}
{/if}
{if $display_qties == 1 && $product->quantity}
	{addJsDef quantityAvailable=$product->quantity}
{else}
	{addJsDef quantityAvailable=0}
{/if}
{addJsDef quantitiesDisplayAllowed=$display_qties|boolval}
{if $product->specificPrice && $product->specificPrice.reduction && $product->specificPrice.reduction_type == 'percentage'}
	{addJsDef reduction_percent=$product->specificPrice.reduction*100|floatval}
{else}
	{addJsDef reduction_percent=0}
{/if}
{if $product->specificPrice && $product->specificPrice.reduction && $product->specificPrice.reduction_type == 'amount'}
	{addJsDef reduction_price=$product->specificPrice.reduction|floatval}
{else}
	{addJsDef reduction_price=0}
{/if}
{if $product->specificPrice && $product->specificPrice.price}
	{addJsDef specific_price=$product->specificPrice.price|floatval}
{else}
	{addJsDef specific_price=0}
{/if}
{addJsDef specific_currency=($product->specificPrice && $product->specificPrice.id_currency)|boolval} {* TODO: remove if always false *}
{addJsDef stock_management=$stock_management|intval}
{addJsDef taxRate=$tax_rate|floatval}
{addJsDefL name=doesntExist}{l s='This combination does not exist for this product. Please select another combination.' js=1}{/addJsDefL}
{addJsDefL name=doesntExistNoMore}{l s='This product is no longer in stock' js=1}{/addJsDefL}
{addJsDefL name=doesntExistNoMoreBut}{l s='with those attributes but is available with others.' js=1}{/addJsDefL}
{addJsDefL name=fieldRequired}{l s='Please fill in all the required fields before saving your customization.' js=1}{/addJsDefL}
{addJsDefL name=uploading_in_progress}{l s='Uploading in progress, please be patient.' js=1}{/addJsDefL}
{/strip}
{/if}
</div>
<!--Extra Tabs Customization-->
{literal}
<script type="text/javascript">
	$(document).ready(function(){

		//Move tab contents to appropriate position

		//Blue Prints tab

		var blue_print = [];
		$('#extraTab_2 img').each(function(){
			blue_print.push($(this).attr('src'));
		});
		
		if (blue_print.length>=1){
			for (x=0; x<blue_print.length; x++){
				$('.blue-prints img').attr('src', blue_print[x]);
			}
		}
		else{
			$('.blue-prints img').remove();
			$('.blue-prints').append('<p>N/A</p>');
		}

		//Safety First tab
		// var safety_first_content = $('#extraTab_1').html();
		// $('.safety-first-content').html(safety_first_content); 

		//Facts tab
		//var facts = $('#extraTab_3').html();
		//$('.facts-info').html(facts);

		//Shipping dimensions tab
		//var shipping_dimensions = $('#extraTab_4').html();
		//$('.shipping-dimensions-info').html(shipping_dimensions);

		//Additional info tab
		//var additional_info = $('#extraTab_5').html();
		//$('.additional-info-info').html(additional_info);

		//Product features tab
		var product_features = $('#extraTab_6').html();
		$('.product-features-info').html(product_features);

		//Apply product logo
		if ($('#extraTab_10 img')){
			var logo = $('#extraTab_10 img').attr('src');
			$('.product-logo img').attr('src', logo);
		}
		else{
			$('.product-logo').remove();
		}

		//Big image tab
		function big_image_content(){
			var big_image_header = $('#extraTab_7 h2').html();
			var big_image_text = $('#extraTab_7 p').html();
			var big_image_img = $('#extraTab_7 img').attr('src');
			$('.big-image-text').html(big_image_text);
			$('.big-image-box .big-image-header').html('<h2>'+big_image_header+'</h2>');
			$('.big-image-box img').attr('src', big_image_img); 
			var x = 1;
			$('.big-image-box img').each(function(){
				if (x>1){
					$(this).remove();
				}
				x++;
			});

			if ($('.big-image-header h2').html()=="undefined"){
				$('.big-image-header h2').html('Totally Interactive');
			}
		}
 
		function check_big_image_content(){
			if ($('.big-image-box img').attr('src')=="#"){
				$('.big-image-box').remove();
			}
		}

		//Relocate boxes
		$('#page').append($('.big-image-box'));
		$('#page').append($('.virtual-tour-box'));
		$('#page').append($('.product-slides-box'));
		$('#page').append($('.product-slides-box-extra-slides'));
		$('#page').append($('.product-reviews'));
		$('#page').append($('.blockproductscategory'));
		$('#page').append($('.footer-container'));

		//Virtual tour tab
		function virtual_tour_content(){
			var virtual_tour_text = $('#extraTab_8 p').html();
			var virtual_tour_video =  $('#extraTab_8 iframe').html();
			var virtual_tour_video_src = $('#extraTab_8 iframe').attr('src');
			$('.virtual-tour-text').html(virtual_tour_text);
			$('.virtual-tour-video').html('<iframe src="'+virtual_tour_video_src+'">'+virtual_tour_video+'</iframe>'); 
		}

		function check_virtual_tour_content(){
			if ($('.virtual-tour-video iframe').attr('src')=="undefined"){
				$('.virtual-tour-box').remove();
			}
		}

		$.when(big_image_content()).then(check_big_image_content());
		$.when(virtual_tour_content()).then(check_virtual_tour_content());

		//Product slides header
		if ($('#extraTab_9 h2')){
			var product_slides_header = '<h2>' + $('#extraTab_9 h2').html() + '</h2>';
			$('.product-slides-header').html(product_slides_header);
		}

		//Product slides tab
		//Apply text from paragraphs that don't contain images
		//Slide 1
		var product_slide1_text = '';
		var x = 0;
		$('#extraTab_9 #slide-1-text p').each(function(){
			if (x==0){
				product_slide1_text += '<p class="product-slide-title">' + $(this).html() + '</p>';
			}
			if (x==1){
				product_slide1_text += '<p>' + $(this).html() + '</p>';
			}
			x = x + 1;
		});

		var slide_logo_1 = $('#extraTab_9 #slide-1-text .logo img').attr('src');
		product_slide1_text += '<p><img src="' + slide_logo_1 + '" /></p>';

		//Slide 2
		var product_slide2_text = '';
		var x = 0;
		$('#extraTab_9 #slide-2-text p').each(function(){
			if (x==0){
				product_slide2_text += '<p class="product-slide-title">' + $(this).html() + '</p>';
			}
			if (x==1){
				product_slide2_text += '<p>' + $(this).html() + '</p>';
			}
			x = x + 1;
		});

		var slide_logo_2 = $('#extraTab_9 #slide-2-text .logo img').attr('src');
		product_slide2_text += '<p><img src="' + slide_logo_2 + '" /></p>';

		//Slide 3
		var product_slide3_text = '';
		var x = 0;
		$('#extraTab_9 #slide-3-text p').each(function(){
			if (x==0){
				product_slide3_text += '<p class="product-slide-title">' + $(this).html() + '</p>';
			}
			if (x==1){
				product_slide3_text += '<p>' + $(this).html() + '</p>';
			}
			x = x + 1;
		});

		var slide_logo_3 = $('#extraTab_9 #slide-3-text .logo img').attr('src');
		product_slide3_text += '<p><img src="' + slide_logo_3 + '" /></p>';

		//Slide 4
		var product_slide4_text = '';
		var x = 0;
		$('#extraTab_9 #slide-4-text p').each(function(){
			if (x==0){
				product_slide4_text += '<p class="product-slide-title">' + $(this).html() + '</p>';
			}
			if (x==1){
				product_slide4_text += '<p>' + $(this).html() + '</p>';
			}
			x = x + 1;
		});

		var slide_logo_4 = $('#extraTab_9 #slide-4-text .logo img').attr('src');
		product_slide4_text += '<p><img src="' + slide_logo_4 + '" /></p>';

		//Make array for all images
		var product_slides_images = [];
		//Take each image from the slides tab and populate the array
		$('#extraTab_9 #slides img').each(function(){
			product_slides_images.push($(this).attr('src'));
		});
		//Insert each image from array to all available img tags from product slider
		x = 0;

		$('#SEslider_product_slider1 li img').each(function(){
			if (!product_slides_images[x]){
				$(this).closest('li').remove();
			}
			$(this).attr('src', product_slides_images[x]);
			
			x = x + 1;
		});  
		
		//Insert extra images below slider
		//Slide 1
		var x = 0;

		var slide_1_additional_images = [];
		$('#extraTab_9 .slide1-additional-images img').each(function(){
			slide_1_additional_images.push($(this).attr('src'));
		});

		$('.product-slides-box-extra-slides .set1 img').each(function(){
			$(this).attr('src', slide_1_additional_images[x]);
			x = x + 1;
		});

		//Slide 2
		var x = 0;

		var slide_2_additional_images = [];
		$('#extraTab_9 .slide2-additional-images img').each(function(){
			slide_2_additional_images.push($(this).attr('src'));
		});

		$('.product-slides-box-extra-slides .set2 img').each(function(){
			$(this).attr('src', slide_2_additional_images[x]);
			x = x + 1;
		});

		//Slide 3
		var x = 0;

		var slide_3_additional_images = [];
		$('#extraTab_9 .slide3-additional-images img').each(function(){
			slide_3_additional_images.push($(this).attr('src'));
		});

		$('.product-slides-box-extra-slides .set3 img').each(function(){
			$(this).attr('src', slide_3_additional_images[x]);
			x = x + 1;
		});

		//Slide 4
		var x = 0;

		var slide_4_additional_images = [];
		$('#extraTab_9 .slide4-additional-images img').each(function(){
			slide_4_additional_images.push($(this).attr('src'));
		});

		$('.product-slides-box-extra-slides .set4 img').each(function(){
			$(this).attr('src', slide_4_additional_images[x]);
			x = x + 1;
		});

		//Remove arrow from last slide

		cycle = new Boolean();
		cycle = true;

		index = 1;

		function fadeOutSlideText(){
			$('#product_slide_1_text').fadeOut();
			$('#product_slide_2_text').fadeOut();
			$('#product_slide_3_text').fadeOut();
			$('#product_slide_4_text').fadeOut();
		}

		//Check for additional slider images

		var x = 0;
		$('#SEslider_product_slider1 li').each(function(){
			x = x + 1;
		});

		//In only 1 slide, remove next arrow
		if (x==1){
			$('.se-next').hide();	
		}	

		//Original slider code

		// $('a.se-next.mynext').live('click', function(){

		// 	if ($('#SEslider_product_slider1 li:last-child').hasClass('active-slide')){
		// 		$('.se-next').hide();
		// 	}
		// 	if (!$('#SEslider_product_slider1 li:last-child').hasClass('active-slide')){
		// 		$('.se-next').show();
		// 	}
		// 	//Move bottom slides

		// 	var timer = setTimeout(function(){cycle=true;}, 800);

		// 	if (cycle==true){

		// 	$('.product-slides-box-extra-slides .set1').animate({left: '-=100%'}, 500); 

		// 	$('.product-slides-box-extra-slides .set2').animate({left: '-=100%'}, 500); 

		// 	$('.product-slides-box-extra-slides .set3').animate({left: '-=100%'}, 500); 
			
		// 	$('.product-slides-box-extra-slides .set4').animate({left: '-=100%'}, 500); 

			

		// 	index += 1;

		// 	//Swap text
		// 	if (index==1){
		// 		fadeOutSlideText();
		// 		//Fade in text
		// 			if ($(window).width()<=1200){
		// 				$('#product_slide_1_text').css('left', '0%');
		// 				setTimeout(function(){$('#product_slide_1_text').show();}, 500);
		// 			}
		// 			else{
		// 				$('#product_slide_1_text').show();
		// 				$('#product_slide_1_text').animate({left: '-=269px'}, 500);
		// 				$('#product_slide_2_text').animate({left: '-=269px'}, 500);
		// 				$('#product_slide_3_text').animate({left: '-=269px'}, 500);
		// 				$('#product_slide_4_text').animate({left: '-=269px'}, 500);
		// 			}
					
		// 	}
		// 	if (index==2){
		// 		fadeOutSlideText();
		// 		//Fade in text
		// 			if ($(window).width()<=1200){
		// 				$('#product_slide_2_text').css('left', '0%');
		// 				setTimeout(function(){$('#product_slide_2_text').show();}, 500);
		// 			}
		// 			else{
		// 				$('#product_slide_2_text').show();
		// 				$('#product_slide_1_text').animate({left: '-=269px'}, 500);
		// 				$('#product_slide_2_text').animate({left: '-=269px'}, 500);
		// 				$('#product_slide_3_text').animate({left: '-=269px'}, 500);
		// 				$('#product_slide_4_text').animate({left: '-=269px'}, 500);
		// 			}

		// 	}
		// 	if (index==3){
		// 		fadeOutSlideText();
		// 		//Fade in text
		// 			if ($(window).width()<=1200){
		// 				$('#product_slide_3_text').css('left', '0%');
		// 				setTimeout(function(){$('#product_slide_3_text').show();}, 500);
		// 			}
		// 			else{
		// 				$('#product_slide_3_text').show();
		// 				$('#product_slide_1_text').animate({left: '-=269px'}, 500);
		// 				$('#product_slide_2_text').animate({left: '-=269px'}, 500);
		// 				$('#product_slide_3_text').animate({left: '-=269px'}, 500);
		// 				$('#product_slide_4_text').animate({left: '-=269px'}, 500);
		// 			}
		// 	}
		// 	if (index==4){
		// 		fadeOutSlideText();
		// 		//Fade in text;
		// 			if ($(window).width()<=1200){
		// 				$('#product_slide_4_text').css('left', '0%');
		// 				setTimeout(function(){$('#product_slide_4_text').show();}, 500);
		// 			}
		// 			else{
		// 				$('#product_slide_4_text').show();
		// 				$('#product_slide_1_text').animate({left: '-=269px'}, 500);
		// 				$('#product_slide_2_text').animate({left: '-=269px'}, 500);
		// 				$('#product_slide_3_text').animate({left: '-=269px'}, 500);
		// 				$('#product_slide_4_text').animate({left: '-=269px'}, 500);
		// 			}
		// 	}

		// 	}

		// 	cycle = false;

		// });

		// $('a.se-prev').live('click', function(){

		// 	if ($('#SEslider_product_slider1 li:last-child').hasClass('active-slide')){
		// 		$('.se-next').hide();
		// 	}
		// 	if (!$('#SEslider_product_slider1 li:last-child').hasClass('active-slide')){
		// 		$('.se-next').show();
		// 	}

		// 	//Move bottom slides

		// 	var timer = setTimeout(function(){cycle=true;}, 800);

		// 	if (cycle==true){

		// 	$('.product-slides-box-extra-slides .set1').animate({left: '+=100%'}, 500); 
 
		// 	$('.product-slides-box-extra-slides .set2').animate({left: '+=100%'}, 500); 

		// 	$('.product-slides-box-extra-slides .set3').animate({left: '+=100%'}, 500); 
			
		// 	$('.product-slides-box-extra-slides .set4').animate({left: '+=100%'}, 500); 

			

		// 	index -= 1;

		// 	//Swap text
		// 	if (index==1){
		// 		fadeOutSlideText();
		// 		//Fade in text
		// 			if ($(window).width()<=1200){
		// 				$('#product_slide_1_text').css('left', '0%');
		// 				setTimeout(function(){$('#product_slide_1_text').show();}, 500);
		// 			}
		// 			else{
		// 				$('#product_slide_1_text').show();
		// 				$('#product_slide_1_text').animate({left: '+=269px'}, 500);
		// 				$('#product_slide_2_text').animate({left: '+=269px'}, 500);
		// 				$('#product_slide_3_text').animate({left: '+=269px'}, 500);
		// 				$('#product_slide_4_text').animate({left: '+=269px'}, 500);
		// 			}
		// 	}
		// 	if (index==2){
		// 		fadeOutSlideText();
		// 		//Fade in text
		// 			if ($(window).width()<=1200){
		// 				$('#product_slide_2_text').css('left', '0%');
		// 				setTimeout(function(){$('#product_slide_2_text').show();}, 500);
		// 			}
		// 			else{
		// 				$('#product_slide_2_text').show();
		// 				$('#product_slide_1_text').animate({left: '+=269px'}, 500);
		// 				$('#product_slide_2_text').animate({left: '+=269px'}, 500);
		// 				$('#product_slide_3_text').animate({left: '+=269px'}, 500);
		// 				$('#product_slide_4_text').animate({left: '+=269px'}, 500);
		// 			}
		// 	}
		// 	if (index==3){
		// 		fadeOutSlideText();
		// 		//Fade in text
		// 			if ($(window).width()<=1200){
		// 				$('#product_slide_3_text').css('left', '0%');
		// 				setTimeout(function(){$('#product_slide_3_text').show();}, 500);
		// 			}
		// 			else{
		// 				$('#product_slide_3_text').show();
		// 				$('#product_slide_1_text').animate({left: '+=269px'}, 500);
		// 				$('#product_slide_2_text').animate({left: '+=269px'}, 500);
		// 				$('#product_slide_3_text').animate({left: '+=269px'}, 500);
		// 				$('#product_slide_4_text').animate({left: '+=269px'}, 500);
		// 			}
		// 	}
		// 	if (index==4){
		// 		fadeOutSlideText();
		// 		//Fade in text
		// 			if ($(window).width()<=1200){
		// 				$('#product_slide_4_text').css('left', '0%');
		// 				setTimeout(function(){$('#product_slide_4_text').show();}, 500);
		// 			}
		// 			else{
		// 				$('#product_slide_4_text').show();
		// 				$('#product_slide_1_text').animate({left: '+=269px'}, 500);
		// 				$('#product_slide_2_text').animate({left: '+=269px'}, 500);
		// 				$('#product_slide_3_text').animate({left: '+=269px'}, 500);
		// 				$('#product_slide_4_text').animate({left: '+=269px'}, 500);
		// 			}
		// 	}

		// 	}

		// 	cycle = false;

		// });
		
		//Remove slider anchor 
		$('body#product a.SElink').removeAttr('href');

		// var product_slides_img_1 = $('#extraTab_9 img:nth-child(1)').attr('src');
		$('.product-slides-info').html('<div id="product_slide_1_text">'+product_slide1_text+'</div>'+'<div id="product_slide_2_text">'+product_slide2_text+'</div>'+'<div id="product_slide_3_text">'+product_slide3_text+'</div>'+'<div id="product_slide_4_text">'+product_slide4_text+'</div>');
		// $('#SEslider_product_slider1 li img:nth-child(1)').attr('src', product_slides_img_1);
 
		//Reviews tab
		var reviews_content = $('div#idTab5').html();
		$('.product-reviews .review-content').html(reviews_content);

		//Additional features storage

		//Related products minimized
		$('body#product #productscategory_list ul li:nth-child(3)').nextAll().remove();

		//Get all related products
		var related_products = [];
		$('body#product #productscategory_list ul li').each(function(){
			related_products.push('<li><div class="product-container"><div class="left-block"><div class="product-image-container">'+ $(this).html() + '</div></div><div class="right-block"><div class="button-container"><a class="lnk-view"><span>See More Details</span></div></div></div></div></li>');
		});

		$('#productscategory_list').empty();
		$('#productscategory_list').append('<ul class="product_list grid"></ul>');
		for (x=0; x<related_products.length; x++){
			$('#productscategory_list ul').append(related_products[x]);
		}

		//Responsive changes for image slider bottom
		function respondImages(){
			var height = $('.set1').height();
			$('.product-slides-box-extra-slides').css('height', height);
		}

		respondImages();

		function resetSliderTextStyles(){
			console.log(index);
			if ($(window).width()>1200){

			if (index==1){
				$('#product_slide_1_text').attr('style', 'float: left; position: absolute; left: 0px; width: 269px;display: block; ');
				$('#product_slide_2_text').attr('style', 'float: left; position: absolute; left: 269px; width: 269px; display: none;');
				$('#product_slide_3_text').attr('style', 'float: left; position: absolute; left: 548px; width: 269px; display: none;');
				$('#product_slide_4_text').attr('style', 'float: left; position: absolute; left: 807px; width: 269px; display: none;');
			}
			if (index==2){
				$('#product_slide_1_text').attr('style', 'float: left; position: absolute; left: -269px; width: 269px; display: none;');
				$('#product_slide_2_text').attr('style', 'float: left; position: absolute; left: 0px; width: 269px; display: block;');
				$('#product_slide_3_text').attr('style', 'float: left; position: absolute; left: 279px; width: 269px; display: none;');
				$('#product_slide_4_text').attr('style', 'float: left; position: absolute; left: 538px; width: 269px; display: none;');
			}
			if (index==3){
				$('#product_slide_1_text').attr('style', 'float: left; position: absolute; left: -538px; width: 269px; display: none;');
				$('#product_slide_2_text').attr('style', 'float: left; position: absolute; left: -269px; width: 269px; display: none;');
				$('#product_slide_3_text').attr('style', 'float: left; position: absolute; left: 10px; width: 269px; display: block;');
				$('#product_slide_4_text').attr('style', 'float: left; position: absolute; left: 269px; width: 269px; display: none;');
			}
			if (index==4){
				$('#product_slide_1_text').attr('style', 'float: left; position: absolute; left: -807px; width: 269px; display: none;');
				$('#product_slide_2_text').attr('style', 'float: left; position: absolute; left: -538px; width: 269px; display: none;');
				$('#product_slide_3_text').attr('style', 'float: left; position: absolute; left: -259px; width: 269px; display: none;');
				$('#product_slide_4_text').attr('style', 'float: left; position: absolute; left: 0px; width: 269px; display: block;');
			}

			}
			else if ($(window).width()<=600){
				if (index==1){
				$('#product_slide_1_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 345px; position: relative; display: block;');
				$('#product_slide_2_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 345px; position: relative; display: none;');
				$('#product_slide_3_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 345px; position: relative; display: none;');
				$('#product_slide_4_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 345px; position: relative; display: none;');
			}
			if (index==2){
				$('#product_slide_1_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 345px; position: relative; display: none;');
				$('#product_slide_2_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 345px; position: relative; display: block;');
				$('#product_slide_3_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 345px; position: relative; display: none;');
				$('#product_slide_4_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 345px; position: relative; display: none;');
			}
			if (index==3){
				$('#product_slide_1_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 345px; position: relative; display: none;');
				$('#product_slide_2_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 345px; position: relative; display: none;');
				$('#product_slide_3_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 345px; position: relative; display: block;');
				$('#product_slide_4_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 345px; position: relative; display: none;');
			}
			if (index==4){
				$('#product_slide_1_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 345px; position: relative; display: none;');
				$('#product_slide_2_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 345px; position: relative; display: none;');
				$('#product_slide_3_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 345px; position: relative; display: none;');
				$('#product_slide_4_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 345px; position: relative; display: block;');
			}

			//Get viewport size and apply to mobile slides
			var width = $('.se-viewport').width();
			$('body#product .SEslider li').css('width', width);
			}
			else{
				if (index==1){
				$('#product_slide_1_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 300px; position: relative; display: block;');
				$('#product_slide_2_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 300px; position: relative; display: none;');
				$('#product_slide_3_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 300px; position: relative; display: none;');
				$('#product_slide_4_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 300px; position: relative; display: none;');
			}
			if (index==2){
				$('#product_slide_1_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 300px; position: relative; display: none;');
				$('#product_slide_2_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 300px; position: relative; display: block;');
				$('#product_slide_3_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 300px; position: relative; display: none;');
				$('#product_slide_4_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 300px; position: relative; display: none;');
			}
			if (index==3){
				$('#product_slide_1_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 300px; position: relative; display: none;');
				$('#product_slide_2_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 300px; position: relative; display: none;');
				$('#product_slide_3_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 300px; position: relative; display: block;');
				$('#product_slide_4_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 300px; position: relative; display: none;');
			}
			if (index==4){
				$('#product_slide_1_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 300px; position: relative; display: none;');
				$('#product_slide_2_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 300px; position: relative; display: none;');
				$('#product_slide_3_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 300px; position: relative; display: none;');
				$('#product_slide_4_text').attr('style', 'float: left;left: 0px;width: 100%;max-width: 100%;text-align: center;margin-top: 300px; position: relative; display: block;');
			}

			//Get viewport size and apply to mobile slides
			var width = $('.se-viewport').width();
			$('body#product .SEslider li').css('width', width);
			}
		}

		resetSliderTextStyles();

		$(window).resize(function(){
			respondImages();
			resetSliderTextStyles();
		});

		//Remove slider box if no slides
		if ($('ul#SEslider_product_slider1 li')){
			
		}
		if ($('ul#SEslider_product_slider1 li').length<1){
			$('.product-slides-box').remove();
			$('.product-slides-box-extra-slides').remove();
		}

		//Remove product features if empty
		if($('.product-features-info').html()==""){
			$('.product-features-info').html('<p>N/A</p>');
		}
		
		window.onload = function(){ respondImages(); }
		
		//Clear search on focus
		$('#search_query_top').focus(function(){
			$(this).val('');
		});
		
		//Check for empty slide text
		var img = $('#product_slide_1_text p img').attr('src');
		if (img != "undefined"){
			
		}
		else{
			$('.product-slides-info').remove();
			$('.seslider_product_slider1 li img').css('float', 'none');
			$('.seslider_product_slider1 li img').css('margin', 'auto');
		}

	});
</script>
{/literal}
