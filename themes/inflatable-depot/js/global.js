/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
//global variables
var responsiveflag = false;

$(document).ready(function(){
	highdpiInit();
	responsiveResize();
	$(window).resize(responsiveResize);
	if (navigator.userAgent.match(/Android/i))
	{
		var viewport = document.querySelector('meta[name="viewport"]');
		viewport.setAttribute('content', 'initial-scale=1.0,maximum-scale=1.0,user-scalable=0,width=device-width,height=device-height');
		window.scrollTo(0, 1);
	}
	blockHover();
	if (typeof quickView !== 'undefined' && quickView)
		quick_view();
	dropDown();

	if (typeof page_name != 'undefined' && !in_array(page_name, ['index', 'product']))
	{
		bindGrid();

 		$(document).on('change', '.selectProductSort', function(e){
			if (typeof request != 'undefined' && request)
				var requestSortProducts = request;
 			var splitData = $(this).val().split(':');
			if (typeof requestSortProducts != 'undefined' && requestSortProducts)
				document.location.href = requestSortProducts + ((requestSortProducts.indexOf('?') < 0) ? '?' : '&') + 'orderby=' + splitData[0] + '&orderway=' + splitData[1];
    	});

		$(document).on('change', 'select[name="n"]', function(){
			$(this.form).submit();
		});

		$(document).on('change', 'select[name="manufacturer_list"], select[name="supplier_list"]', function() {
			if (this.value != '')
				location.href = this.value;
		});

		$(document).on('change', 'select[name="currency_payement"]', function(){
			setCurrency($(this).val());
		});
	}

	$(document).on('click', '.back', function(e){
		e.preventDefault();
		history.back();
	});
	
	jQuery.curCSS = jQuery.css;
	if (!!$.prototype.cluetip)
		$('a.cluetip').cluetip({
			local:true,
			cursor: 'pointer',
			dropShadow: false,
			dropShadowSteps: 0,
			showTitle: false,
			tracking: true,
			sticky: false,
			mouseOutClose: true,
			fx: {             
		    	open:       'fadeIn',
		    	openSpeed:  'fast'
			}
		}).css('opacity', 0.8);

	if (!!$.prototype.fancybox)
		$.extend($.fancybox.defaults.tpl, {
			closeBtn : '<a title="' + FancyboxI18nClose + '" class="fancybox-item fancybox-close" href="javascript:;"></a>',
			next     : '<a title="' + FancyboxI18nNext + '" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
			prev     : '<a title="' + FancyboxI18nPrev + '" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
		});
	$('#editorial_block_center').after($('#balloonwrapper'));
	
});

function highdpiInit()
{
	if($('.replace-2x').css('font-size') == "1px")
	{		
		var els = $("img.replace-2x").get();
		for(var i = 0; i < els.length; i++)
		{
			src = els[i].src;
			extension = src.substr( (src.lastIndexOf('.') +1) );
			src = src.replace("." + extension, "2x." + extension);
			
			var img = new Image();
			img.src = src;
			img.height != 0 ? els[i].src = src : els[i].src = els[i].src;
		}
	}
}

function responsiveResize()
{
	if ($(document).width() <= 767 && responsiveflag == false)
	{
		accordion('enable');
	    accordionFooter('enable');
		responsiveflag = true;	
	}
	else if ($(document).width() >= 768)
	{
		accordion('disable');
		accordionFooter('disable');
	    responsiveflag = false;
	}
}

function blockHover(status)
{
	$(document).off('mouseenter').on('mouseenter', '.product_list.grid li.ajax_block_product .product-container', function(e){

		if ('ontouchstart' in document.documentElement)
			return;
		if ($('body').find('.container').width() == 1170)
		{
			var pcHeight = $(this).parent().outerHeight();
			var pcPHeight = $(this).parent().find('.button-container').outerHeight() + $(this).parent().find('.comments_note').outerHeight() + $(this).parent().find('.functional-buttons').outerHeight();
			$(this).parent().addClass('hovered');
			$(this).parent().css('height', pcHeight + pcPHeight).css('margin-bottom', pcPHeight * (-1));
		}
	});

	$(document).off('mouseleave').on('mouseleave', '.product_list.grid li.ajax_block_product .product-container', function(e){
		if ($('body').find('.container').width() == 1170)
			$(this).parent().removeClass('hovered').removeAttr('style');
	});
}

function quick_view()
{
	$(document).on('click', '.quick-view:visible', function(e) 
	{
		e.preventDefault();
		var url = this.rel;
		if (url.indexOf('?') != -1)
			url += '&';
		else
			url += '?';

		if (!!$.prototype.fancybox)
			$.fancybox({
				'padding':  0,
				'width':    1087,
				'height':   610,
				'type':     'iframe',
				'href':     url + 'content_only=1'
			});
	});
}

function bindGrid()
{
	var view = $.totalStorage('display');

	if (view && view != 'grid')
		display(view);
	else
		$('.display').find('li#grid').addClass('selected');
	
	$(document).on('click', '#grid', function(e){
		e.preventDefault();
		display('grid');
	});

	$(document).on('click', '#list', function(e){
		e.preventDefault();
		display('list');
	});
}

function display(view)
{
	if (view == 'list')
	{
		$('ul.product_list').removeClass('grid').addClass('list row');
		$('.product_list > li').removeClass('col-xs-12 col-sm-6 col-md-4').addClass('col-xs-12');
		$('.product_list > li').each(function(index, element) {
			html = '';
			html = '<div class="product-container"><div class="row">';
				html += '<div class="left-block col-xs-4 col-xs-5 col-md-4">' + $(element).find('.left-block').html() + '</div>';
				html += '<div class="center-block col-xs-4 col-xs-7 col-md-4">';
					html += '<div class="product-flags">'+ $(element).find('.product-flags').html() + '</div>';
					html += '<h5 itemprop="name">'+ $(element).find('h5').html() + '</h5>';
					var rating = $(element).find('.comments_note').html(); // check : rating
					if (rating != null) { 
						html += '<div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating" class="comments_note">'+ rating + '</div>';
					}
					html += '<p class="product-desc">'+ $(element).find('.product-desc').html() + '</p>';
					var colorList = $(element).find('.color-list-container').html();
					if (colorList != null) {
						html += '<div class="color-list-container">'+ colorList +'</div>';
					}
					var availability = $(element).find('.availability').html();	// check : catalog mode is enabled
					if (availability != null) {
						html += '<span class="availability">'+ availability +'</span>';
					}
				html += '</div>';	
				html += '<div class="right-block col-xs-4 col-xs-12 col-md-4"><div class="right-block-content row">';
					var price = $(element).find('.content_price').html();       // check : catalog mode is enabled
					if (price != null) { 
						html += '<div class="content_price col-xs-5 col-md-12">'+ price + '</div>';
					}
					html += '<div class="button-container col-xs-7 col-md-12">'+ $(element).find('.button-container').html() +'</div>';
					html += '<div class="functional-buttons clearfix col-sm-12">' + $(element).find('.functional-buttons').html() + '</div>';
				html += '</div>';
			html += '</div></div>';
		$(element).html(html);
		});		
		$('.display').find('li#list').addClass('selected');
		$('.display').find('li#grid').removeAttr('class');
		$.totalStorage('display', 'list');
	}
	else 
	{
		$('ul.product_list').removeClass('list').addClass('grid row');
		$('.product_list > li').removeClass('col-xs-12').addClass('col-xs-12 col-sm-6 col-md-4');
		$('.product_list > li').each(function(index, element) {
		html = '';
		html += '<div class="product-container">';
			html += '<div class="left-block">' + $(element).find('.left-block').html() + '</div>';
			html += '<div class="right-block">';
				html += '<div class="product-flags">'+ $(element).find('.product-flags').html() + '</div>';
				html += '<h5 itemprop="name">'+ $(element).find('h5').html() + '</h5>';
				var rating = $(element).find('.comments_note').html(); // check : rating
					if (rating != null) { 
						html += '<div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating" class="comments_note">'+ rating + '</div>';
					}
				html += '<p itemprop="description" class="product-desc">'+ $(element).find('.product-desc').html() + '</p>';
				var price = $(element).find('.content_price').html(); // check : catalog mode is enabled
					if (price != null) { 
						html += '<div class="content_price">'+ price + '</div>';
					}
				html += '<div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="button-container">'+ $(element).find('.button-container').html() +'</div>';
				var colorList = $(element).find('.color-list-container').html();
				if (colorList != null) {
					html += '<div class="color-list-container">'+ colorList +'</div>';
				}
				var availability = $(element).find('.availability').html(); // check : catalog mode is enabled
				if (availability != null) {
					html += '<span class="availability">'+ availability +'</span>';
				}
			html += '</div>';
			html += '<div class="functional-buttons clearfix">' + $(element).find('.functional-buttons').html() + '</div>';
		html += '</div>';		
		$(element).html(html);
		});
		$('.display').find('li#grid').addClass('selected');
		$('.display').find('li#list').removeAttr('class');
		$.totalStorage('display', 'grid');
	}	
}

function dropDown() 
{
	elementClick = '#header .current';
	elementSlide =  'ul.toogle_content';       
	activeClass = 'active';			 

	$(elementClick).on('click', function(e){
		e.stopPropagation();
		var subUl = $(this).next(elementSlide);
		if(subUl.is(':hidden'))
		{
			subUl.slideDown();
			$(this).addClass(activeClass);	
		}
		else
		{
			subUl.slideUp();
			$(this).removeClass(activeClass);
		}
		$(elementClick).not(this).next(elementSlide).slideUp();
		$(elementClick).not(this).removeClass(activeClass);
		e.preventDefault();
	});

	$(elementSlide).on('click', function(e){
		e.stopPropagation();
	});

	$(document).on('click', function(e){
		e.stopPropagation();
		var elementHide = $(elementClick).next(elementSlide);
		$(elementHide).slideUp();
		$(elementClick).removeClass('active');
	});
}

function accordionFooter(status)
{
	if(status == 'enable')
	{
		$('#footer .footer-block h4').on('click', function(){
			$(this).toggleClass('active').parent().find('.toggle-footer').stop().slideToggle('medium');
		})
		$('#footer').addClass('accordion').find('.toggle-footer').slideUp('fast');
	}
	else
	{
		$('.footer-block h4').removeClass('active').off().parent().find('.toggle-footer').removeAttr('style').slideDown('fast');
		$('#footer').removeClass('accordion');
	}
}

function accordion(status)
{
	leftColumnBlocks = $('#left_column');
	if(status == 'enable')
	{
		$('#right_column .block .title_block, #left_column .block .title_block, #left_column #newsletter_block_left h4').on('click', function(){
			$(this).toggleClass('active').parent().find('.block_content').stop().slideToggle('medium');
		})
		$('#right_column, #left_column').addClass('accordion').find('.block .block_content').slideUp('fast');
	}
	else
	{
		$('#right_column .block .title_block, #left_column .block .title_block, #left_column #newsletter_block_left h4').removeClass('active').off().parent().find('.block_content').removeAttr('style').slideDown('fast');
		$('#left_column, #right_column').removeClass('accordion');
	}
}


$(document).ready(function(){

	$('#customers_block').slick({
	dots: true,
	dragable: false,
  infinite: false,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 4,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
	});	

	$('.draggable').attr('style', 'outline: none;');
	$('.slick-slide').attr('style', 'float:left;');
	$('.slick-dots').attr('style', 'display:none !important;');

	// $('ul.sf-menu li:eq(2)').after($('#header_logo'));
	// $('#header_logo').show();

	$('span.register').after($('.header_user_info'));
	$('.email-us').after($('.header_user_info'));

	//$('ul.sf-menu li:eq(2)').after('<li class="placeholder">&nbsp;</li>');
	$('#newsletter_block_left').before($('#block_various_links_footer'));
	
	$('#newsletter_block_left').before($('#block_contact_infos'));

	/////////////////
	//case studies
	/////////////////
	$(".seslider_inflatable_solutions1").addClass('active');
	$(".case-studies-client-list .button").click(function(){
		$(".case-studies-client-list .button").addClass('orange-button');
		$(".case-studies-client-list .button").removeClass('blue-button');
		$(this).removeClass('orange-button');
		$(this).addClass('blue-button');	
	});
	$(".btn-jump").click(function(){
		$(".SEslider").removeClass('active');
		$(".seslider_inflatable_solutions1").addClass('active');	
	});
	$(".btn-kona").click(function(){
		$(".SEslider").removeClass('active');
		$(".seslider_kona_ice2").addClass('active');	
	});
	$(".btn-army").click(function(){
		$(".SEslider").removeClass('active');
		$(".seslider_salvation_army3").addClass('active');	
	});

	//Set default active state for customers
	$("#logoonin").attr('src', '/themes/inflatable-depot/img/jump-on-in-Col.png');
	$("#logoonin").attr('active', 'true');

	//Remove all active states
	function removeActiveStates(){
		$("#logobouncy").removeAttr('active');
		$("#logocity").removeAttr('active');
		$("#logoonin").removeAttr('active');
		$("#logojoes").removeAttr('active');
		$("#logoworld").removeAttr('active');
		$("#logoxplosion").removeAttr('active');
	}

	//Reset all logos to grey
	function logosGrey(){
		$("#logobouncy").attr('src', '/themes/inflatable-depot/img/bouncy-times-BnW.png');
		$("#logocity").attr('src', '/themes/inflatable-depot/img/jump-city-BnW.png');
		$("#logoonin").attr('src', '/themes/inflatable-depot/img/jump-on-in-BnW.png');
		$("#logojoes").attr('src', '/themes/inflatable-depot/img/monkey-joes-BnW.png');
		$("#logoworld").attr('src', '/themes/inflatable-depot/img/world-bounce-BnW.png');
		$("#logoxplosion").attr('src', '/themes/inflatable-depot/img/xplosion-BnW.png');
	}

	//Bouncy Times Logo
	$("#logobouncy").click(function(){
		removeActiveStates();
		logosGrey();
		$(this).attr('src', '/themes/inflatable-depot/img/bouncy-times-Col.png');
		$(this).attr('active', 'true');
		//Apply content
		$('.sideimage img').attr('src', '/themes/inflatable-depot/img/bouncy-times-side.png');
		//Name
		$('.quote .subtitle').html('Bouncy Times');
		//Quote
		$('.quote .quotetext').html('I´ve been a customer of Inflatable Depot for almost 10 years. Excellent design, material quality and craftsmanship. They were always there to make things right. I can always count on them.');
		//Signature
		$('.quote .signature').html('Terpos Ageladelis , Owner');
	});

	$("#logobouncy").hover(function() {
  		$(this).attr('src', '/themes/inflatable-depot/img/bouncy-times-Col.png');
	}, function() {
		if (!$(this).attr('active')){
  			$(this).attr('src', '/themes/inflatable-depot/img/bouncy-times-BnW.png');
  		}
	});

	//Jump City Logo
	$("#logocity").click(function(){
		removeActiveStates();
		logosGrey();
		$(this).attr('src', '/themes/inflatable-depot/img/jump-city-Col.png');
		$(this).attr('active', 'true');
		//Apply content
		$('.sideimage img').attr('src', '/themes/inflatable-depot/img/jump-city-side-shot.png');
		//Name
		$('.quote .subtitle').html('Jump City');
		//Quote
		$('.quote .quotetext').html('Jump City loves working with Inflatable Depot.  The service and products are great!  We have always had a great expierence.');
		//Signature
		$('.quote .signature').html('Samantha Lieberman, Owner');
	}); 

	$("#logocity").hover(function() {
  		$(this).attr('src', '/themes/inflatable-depot/img/jump-city-Col.png');
	}, function() {
		if (!$(this).attr('active')){
  			$(this).attr('src', '/themes/inflatable-depot/img/jump-city-BnW.png');
  		}
	});

	//Jump O In Logo
	$("#logoonin").click(function(){
		removeActiveStates();
		logosGrey();
		$(this).attr('src', '/themes/inflatable-depot/img/jump-on-in-Col.png');
		$(this).attr('active', 'true');
		//Apply content
		$('.sideimage img').attr('src', '/themes/inflatable-depot/img/customers_side.png');
		//Name
		$('.quote .subtitle').html('Jump On In');
		//Quote
		$('.quote .quotetext').html('We first looked into The Inflatable Depot as a supplier for our equipment at Jump On In because we were searching for a high-quality supplier of inflatables that would not require constant repairs to keep them in good working condition. We have been very pleased with our purchases from The Inflatable Depot.  The quality of the equipment has been superb!  We very much appreciate the attention they pay to the details of manufacturing, such as placing reinforcements at the stress points on the equipment.  It really holds up well!');
		//Signature
		$('.quote .signature').html('Kevin Lynch , Corporate CEO');
	});

	$("#logoonin").hover(function() {
  		$(this).attr('src', '/themes/inflatable-depot/img/jump-on-in-Col.png');
	}, function() {
		if (!$(this).attr('active')){
  			$(this).attr('src', '/themes/inflatable-depot/img/jump-on-in-BnW.png');
  		}
	});

	//Monkey Joes Logo
	$("#logojoes").click(function(){
		removeActiveStates();
		logosGrey();
		$(this).attr('src', '/themes/inflatable-depot/img/monkey-joes-Col.png');
		$(this).attr('active', 'true');
		//Apply content
		$('.sideimage img').attr('src', '/themes/inflatable-depot/img/monkey-joes-side-shot.png');
		//Name
		$('.quote .subtitle').html('Monkey Joes');
		//Quote
		$('.quote .quotetext').html('Since I began my business over six years ago, I have had a chance to work with various providers in this industry and I have found Martin and Chris to be fair and honest about their products and more than willing to work with me through the whole process.They were quick to respond and kept me in the loop whenever I have ordered a jump/slide. In some cases time was of the essence and they responded quickly and accurately.Over the course of my business tenure, we have developed a trust working with each other and that has helped whenever there was a problem involving a repair, etc. Each and every time I have had a very pleasant encounter with everyone at Inflatable Depot. That speaks volumes to me as to how they run their business and treat their customers. I would say, give Martin a call...he will take care of you fairly and honestly.');
		//Signature
		$('.quote .signature').html('Brooke Hawkings, Franchisee Owner');
	});

	$("#logojoes").hover(function() {
  		$(this).attr('src', '/themes/inflatable-depot/img/monkey-joes-Col.png');
	}, function() {
		if (!$(this).attr('active')){
  			$(this).attr('src', '/themes/inflatable-depot/img/monkey-joes-BnW.png');
  		}
	});

	//Bounce World Logo
	$("#logoworld").click(function(){
		removeActiveStates();
		logosGrey();
		$(this).attr('src', '/themes/inflatable-depot/img/world-bounce-Col.png');
		$(this).attr('active', 'true');
		//Apply content
		$('.sideimage img').attr('src', '/themes/inflatable-depot/img/world-bounce-side-shot.png');
		//Name
		$('.quote .subtitle').html('World of Bounce');
		//Quote
		$('.quote .quotetext').html('Martin Szwimer and The Inflatable Depot have been an extremely valuable resource and contributor to the success of our Family Entertainment Center.  His industry knowledge helped guide us during the development stage of our business in 2008, and we have relied upon his expertise ever since.  We have found The Inflatable Depot’s products to be among the finest in the industry and very competitively priced. We recommend Martin and The Inflatable Depot for anyone interested in purchasing the best inflatable products on the market.');
		//Signature
		$('.quote .signature').html('Jeff Mc Knight, Co-Owner');
	});

	$("#logoworld").hover(function() {
  		$(this).attr('src', '/themes/inflatable-depot/img/world-bounce-Col.png');
	}, function() {
		if (!$(this).attr('active')){
  			$(this).attr('src', '/themes/inflatable-depot/img/world-bounce-BnW.png');
  		}
	});

	//Xplosion Logo
	$("#logoxplosion").click(function(){
		removeActiveStates();
		logosGrey();
		$(this).attr('src', '/themes/inflatable-depot/img/xplosion-Col.png');
		$(this).attr('active', 'true');
		//Apply content
		$('.sideimage img').attr('src', '/themes/inflatable-depot/img/xplosion-side.png');
		//Name
		$('.quote .subtitle').html('Xplosion');
		//Quote
		$('.quote .quotetext').html('Our amusement parks are determined to provide our customers with the highest quality inflatables, day to day, 365 days a year. In order for this to be accomplished our inflatables must be of the utmost in design, appearance, quality, safety, and durability. We are outdoors amusement park, and therefore out inflatables are exposed to the outdoors enviroment day in and day out. Together with inflatable Depot we have been  able to accomplish our goals and be a proven leader in Mexico with our parks. Inflatable Depot has provided these qualities in our inflatables and for over six years, have been there to provide us with these products, and continue to help us innovate in our businnes.');
		//Signature
		$('.quote .signature').html('Santiago Mazon , Owner');
	});

	$("#logoxplosion").hover(function() {
  		$(this).attr('src', '/themes/inflatable-depot/img/xplosion-Col.png');
	}, function() {
		if (!$(this).attr('active')){
  			$(this).attr('src', '/themes/inflatable-depot/img/xplosion-BnW.png');
  		}
	});

	$('a.top-links').each(function(){
		$(this).append('<span class="spacer">&nbsp;|&nbsp;');
	});

	$('#footerLinks span.item a').each(function(){
		$(this).append('<span class="">&nbsp;|&nbsp;');
	});

	$('a.top-links:last').find('span').remove();

	$('#footerLinks span.item a:last').find('span').remove();

	$('.header_user_info a').each(function(){
		$(this).append('<span class="spacerUserInfo">&nbsp;|&nbsp;');
	});

	$('.header_user_info a:last').find('span').remove();

	$('.seslider_sample1 .SEslider').after($('.seslider_sample1 .se-controls'));

	$('#editorial_block_center').prepend($('#balloonwrapper'));

	$('#stepsBlock').append($('.seslider_inflatable_solutions2'));

	$('#playsolutions p#playtext').after($('.seslider_play_solutions3'));

	$('.cat-title').addClass('mobileNavButton');

	$('.cat-title').after('<div class="mobileSearch mobileNavButton"></div><div class="mobileCart mobileNavButton"></div><div class="mobilePhone mobileNavButton"></div>');
	$('.mobilePhone').wrap('<a href="callto://18667163704"></a>');

	$(window).resize(function(){
		responsive();
	});

	function responsive(){
		if ($(window).width()<=480){
			$('.viewcatalog').attr('style', 'background:url("themes/inflatable-depot/img/catalog_bg.png");background-size:cover;background-position:center;height:260px');
			$('#catalog_bg').remove();
			//Nav button
			$('.cat-title').empty();
			$('#header_logo').show();
			$('header .nav').after($('#header_logo'));
			$('img.logo').css('position', 'relative');
			$('img.logo').css('top', '0px'); 
			$('img.logo').attr('src', '/themes/inflatable-depot/img/logo_mobile.png'); 
		} 
		else if ($(window).width()<=767 && $(window).width()>480){
			$('#catalog_bg').attr('src', '/themes/inflatable-depot/img/catalog_bg_mobile.jpg');
			$('header .nav').after($('#header_logo'));
			$('img.logo').css('position', 'relative');
			$('img.logo').css('top', '0px'); 
			$('img.logo').attr('src', '/themes/inflatable-depot/img/logo_mobile.png'); 
			$('.cat-title').empty();
			//Move Search Bar
			$('#search_block_top').hide();
			$('#block_top_menu').append($('#search_block_top'));
			//Change leaders block image
			$('.leaderblackMain').attr('src', '/themes/inflatable-depot/img/mobile-leaders.png');
			$('.leaderblackMain').css('max-width', '50%');
			//Apply images
			$('.viewcatalog').attr('style', 'background:url("themes/inflatable-depot/img/catalog_bg.png");background-size:cover;background-position:center;height:260px');
			$('#catalog_bg').remove();
			$('#header_logo').show();
			$('.slide_description br').remove();  
		}
		else{
			$('#catalog_bg').attr('src', '/themes/inflatable-depot/img/catalog_bg.png');
			$('img.logo').attr('src', '/themes/inflatable-depot/img/logo.png');
			$('ul.sf-menu li:eq(2)').after($('#header_logo')); 
			//$('img.logo').css('position', 'absolute');
			//$('img.logo').css('top', '-16px'); 
			//Move Search Bar Back to Top
			$('.header_user_info').append($('#search_block_top'));
			$('#search_block_top').show();
			//Change leaders block image back
			$('.leaderblackMain').attr('src', '/themes/inflatable-depot/img/leaders.png');
			$('.leaderblackMain').css('max-width', '100%');
			//Apply images
			if (!$('#catalog_bg')){
				$('.viewcatalog').prepend('<img id="catalog_bg" src="/themes/inflatable-depot/img/catalog_bg_mobile.jpg" alt="">');
				$('.viewcatalog').attr('style', 'background: none !important; height: auto !important;');
			}
			$('.cat-title').html('Categories');
			$('ul.sf-menu li:eq(2)').after($('#header_logo'));
			$('#header_logo').show();
		}
	}

	responsive();

	$(".button-search").hide().fadeIn('fast');

	//Search Button Mobile View
	$('.mobileSearch').toggle(
	function(){
		$('#search_block_top').show();
	},
	function(){
		$('#search_block_top').hide();
	});

	//Swap out desk and mobile sliders

	sample_slider = [];
	inflatable_solutions_slider = [];

	$('ul#SEslider_sample1 li:odd').each(function(){
		var li = $(this).html();
		sample_slider.push(li);
	});

	$('ul#SEslider_inflatable_solutions2 li:odd').each(function(){
		var li = $(this).html();
		inflatable_solutions_slider.push(li);
	});

	//Slides Customized Top Text
	$('.seslider_inflatable_solutions2 a.SElink').each(function(){

		var text = $(this).find('img.SEimage').attr('alt');
		$(this).find('.slide_description').before('<span class="slideHeader">'+text+'</span>');

		var description = $('.seslider_inflatable_solutions2 .slide_description').html();
		var description = description.split("*break*");
		$('.seslider_inflatable_solutions2 .slide_description').html('<div class="slider-text-top">'+description[0]+'</div><div class="slider-text-bottom">' + description[1] + '</div>'); 

	});

	//Slides Main Top Text Slide 1
	$('.seslider_sample1 li:nth-child(1) a.SElink').each(function(){

		var text = $(this).find('img.SEimage').attr('alt');
		$(this).find('.slide_description').before('<span class="slideHeader">'+text+'</span>');

		var description = $('.seslider_sample1 li:nth-child(1) .slide_description').html();
		var description = description.split("*break*");
		$('.seslider_sample1 li:nth-child(1) .slideHeader').append('<div class="slider-text-top">'+description[0]+'</div><div class="slider-text-bottom">' + description[1] + '</div>'); 

	});

	//Slides Main Top Text Slide 2
	$('.seslider_sample1 li:nth-child(2) a.SElink').each(function(){

		var text = $(this).find('img.SEimage').attr('alt');
		$(this).find('.slide_description').before('<span class="slideHeader">'+text+'</span>');

		var description = $('.seslider_sample1 li:nth-child(2) .slide_description').html();
		var description = description.split("*break*");
		$('.seslider_sample1 li:nth-child(2) .slideHeader').append('<div class="slider-text-top">'+description[0]+'</div><div class="slider-text-bottom">' + description[1] + '</div>'); 

	});

	//Slides Main Top Text Slide 3
	$('.seslider_sample1 li:nth-child(3) a.SElink').each(function(){

		var text = $(this).find('img.SEimage').attr('alt');
		$(this).find('.slide_description').before('<span class="slideHeader">'+text+'</span>');

		var description = $('.seslider_sample1 li:nth-child(3) .slide_description').html();
		var description = description.split("*break*");
		$('.seslider_sample1 li:nth-child(3) .slideHeader').append('<div class="slider-text-top">'+description[0]+'</div><div class="slider-text-bottom">' + description[1] + '</div>'); 

	});

	//Slides Main Top Text Slide 4
	$('.seslider_sample1 li:nth-child(4) a.SElink').each(function(){

		var text = $(this).find('img.SEimage').attr('alt');
		$(this).find('.slide_description').before('<span class="slideHeader">'+text+'</span>');

		var description = $('.seslider_sample1 li:nth-child(4) .slide_description').html();
		var description = description.split("*break*");
		$('.seslider_sample1 li:nth-child(4) .slideHeader').append('<div class="slider-text-top">'+description[0]+'</div><div class="slider-text-bottom">' + description[1] + '</div>'); 

	});

	//Slides Main Top Text Slide 5
	$('.seslider_sample1 li:nth-child(5) a.SElink').each(function(){

		var text = $(this).find('img.SEimage').attr('alt');
		$(this).find('.slide_description').before('<span class="slideHeader">'+text+'</span>');

		var description = $('.seslider_sample1 li:nth-child(5) .slide_description').html();
		var description = description.split("*break*");
		$('.seslider_sample1 li:nth-child(5) .slideHeader').append('<div class="slider-text-top">'+description[0]+'</div><div class="slider-text-bottom">' + description[1] + '</div>'); 

	});

	//Slides Main Top Text Slide 6
	$('.seslider_sample1 li:nth-child(6) a.SElink').each(function(){

		var text = $(this).find('img.SEimage').attr('alt');
		$(this).find('.slide_description').before('<span class="slideHeader">'+text+'</span>');

		var description = $('.seslider_sample1 li:nth-child(6) .slide_description').html();
		var description = description.split("*break*");
		$('.seslider_sample1 li:nth-child(6) .slideHeader').append('<div class="slider-text-top">'+description[0]+'</div><div class="slider-text-bottom">' + description[1] + '</div>'); 

	});

	//Apply call to action for header sliders
	$('.seslider_sample1 .slideHeader').append('<div class="slider-coa button purple-button">Find Out More</div>');

	$('.slider-text-bottom').each(function(){
		if ($(this).html()=='undefined'){
			$(this).remove();
		}
	});

	//Blog redirect hack
	$('a[title="Our Blog"]').attr('href', '/blog');

	// $(document).ajaxStop(function(){
	// 	console.log('ajax complete');
	// 	$('ul#SEslider_inflatable_solutions2 li:odd').remove();
	// 	$('.seslider_inflatable_solutions2 .se-controls .se-default-pager').empty();
	// });
	
	$(document).ajaxComplete(function(){
		console.log('ajax complete');
		$('ul#SEslider_inflatable_solutions2 li:odd').remove();
		$('.seslider_inflatable_solutions2 .se-controls .se-default-pager').empty();
	});
	
});

window.onload = function(){
	//Initial response load before window resizing
	//Must redefine for window load rather than document load
	function responsive(){
		if ($(window).width()<=480){
			$('.viewcatalog').attr('style', 'background:url("themes/inflatable-depot/img/catalog_bg.png");background-size:cover;background-position:center;height:260px');
			$('#catalog_bg').remove();
			//Nav button
			$('.cat-title').empty();
			$('#header_logo').show();
			$('header .nav').after($('#header_logo'));
			$('img.logo').css('position', 'relative');
			$('img.logo').css('top', '0px'); 
			$('img.logo').attr('src', '/themes/inflatable-depot/img/logo_mobile.png'); 
		} 
		else if ($(window).width()<=767 && $(window).width()>480){
			$('#catalog_bg').attr('src', '/themes/inflatable-depot/img/catalog_bg_mobile.jpg');
			$('header .nav').after($('#header_logo'));
			$('img.logo').css('position', 'relative');
			$('img.logo').css('top', '0px'); 
			$('img.logo').attr('src', '/themes/inflatable-depot/img/logo_mobile.png'); 
			$('.cat-title').empty();
			//Move Search Bar
			$('#search_block_top').hide();
			$('#block_top_menu').append($('#search_block_top'));
			//Change leaders block image
			$('.leaderblackMain').attr('src', '/themes/inflatable-depot/img/mobile-leaders.png');
			$('.leaderblackMain').css('max-width', '50%');
			//Apply images
			$('.viewcatalog').attr('style', 'background:url("themes/inflatable-depot/img/catalog_bg.png");background-size:cover;background-position:center;height:260px');
			$('#catalog_bg').remove();
			$('#header_logo').show();
			$('.slide_description br').remove();
		}
		else{
			$('#catalog_bg').attr('src', '/themes/inflatable-depot/img/catalog_bg.png');
			$('img.logo').attr('src', '/themes/inflatable-depot/img/logo.png');
			$('ul.sf-menu li:eq(2)').after($('#header_logo')); 
			//$('img.logo').css('position', 'absolute');
			//$('img.logo').css('top', '-16px'); 
			//Move Search Bar Back to Top
			$('.header_user_info').append($('#search_block_top'));
			$('#search_block_top').show();
			//Change leaders block image back
			$('.leaderblackMain').attr('src', '/themes/inflatable-depot/img/leaders.png');
			$('.leaderblackMain').css('max-width', '100%');
			//Apply images
			if (!$('#catalog_bg')){
				$('.viewcatalog').prepend('<img id="catalog_bg" src="/themes/inflatable-depot/img/catalog_bg_mobile.jpg" alt="">');
				$('.viewcatalog').attr('style', 'background: none !important; height: auto !important;');
			}
			$('.cat-title').html('Categories');
			$('ul.sf-menu li:eq(2)').after($('#header_logo'));
			$('#header_logo').show();
		}
		//Remove extra search bars hack
		var countsearch = $("#searchbox[method='get']").length;
		if(countsearch > 1){
  			$("#searchbox[method='get']:gt(0)").remove();
		}
	}

	responsive();
 

	//Clear search on focus
	$('#search_query_top').focus(function(){
		$(this).val('');
	});

	//Home page slider links
	links = new Array();
	$('ul#SEslider_sample1 li a').each(function(){
		links.push($(this).attr('href'));
	});

	$('.seslider_sample1 .se-pager-item').click(function(){
		window.location.href = $(this).find('a').attr('href');
	});

	var x = 0;
	$('.seslider_sample1 .se-pager-item').each(function(){
		$(this).find('a').attr('href', links[x]);
		x++;
	});

	//FAQS

	$( "body.cms-faq h6" ).click(function() {
		$( this ).next('p').toggleClass( "show" );
	});

};
