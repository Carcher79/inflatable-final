// JavaScript Document

// Start closure to prevent namespace conflicts
;(function($) {

// Perform initial setup tasks when DOM is ready
$(document).ready( function() {
	setupNavigationLinks();
});

// Set up main navigation mouseover menus
function setupNavigationLinks() {
	$('.nav-menu').each(function() {
		var menu = $(this);
		
		menu.hover(function() {
			menu.addClass('hover');
		}, function() {
			menu.removeClass('hover');
		});
	});
}

// End closure
})(jQuery);