{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<div id="contact-link">
	<a class="top-links" href="/164-specials" title="Specials">Specials</a>
	<a class="top-links" href="content/22-why-choose-inflatable-depot" title="Why Choose Depot">Why Choose Depot</a>
	<a class="top-links" href="/content/4-about-us" title="About Inflatable Depot">About Inflatable Depot</a>
	<a class="top-links" href="/content/23-case-studies" title="Case Studies">Case Studies</a>
	<!-- <a class="top-links" href="#" title="Media &amp; Studies">Media &amp; Studies</a> -->
	<a class="top-links" href="contact-us" title="{l s='Contact Us' mod='blockcontact'}">{l s='Contact us' mod='blockcontact'}</a>
	<a class="i-depot" href="http://www.idepotplay.com/" target="_blank">I-Depot Play</a>
</div>
<!-- <a class="i-depot"><img src="/themes/inflatable-depot/img/i_depot_play.png" /></a> -->
<div id="live-chat">
	<!-- <img class="livechat" src="/themes/inflatable-depot/img/live_chat.png" /> -->
	<div id='LP_DIV_1410351693416' style='width:189px;height:55px;'></div>
</div>
{if $telnumber}
	<span class="shop-phone">
		<!-- <i class="icon-phone"></i> --><img class="phoneIcon" src="/themes/inflatable-depot/img/icon-phone-lrg.png" />{l s='' mod='blockcontact'} <strong>{$telnumber}</strong>
	</span>
{/if}
	<span class="email-us">
		<img class="mailIcon" src="/themes/inflatable-depot/img/icon-email.png" /><strong><a href="mailto:info@inflatabledepot.com">info@inflatabledepot.com</a></strong>
	</span>
	<!-- <span class="register">
		<strong><a href="#">Register</a></strong>
	</span> -->
