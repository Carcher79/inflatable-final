<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class RequestControllerCore extends FrontController
{
	public $php_self = 'request';
	public $ssl = true;
//session_start();
	public function postProcess()
	{
         if(!isset($_SESSION))
            {
              session_start();
             
              
            }
            //  print_r($_POST);
        // echo "in post proseeee";exit; 
		if ($_POST['requestinfo'] == '1')
		{
                   //   print_r($_POST);
          // echo "in post proseeee";exit; 
                     
                    $cap = '';
			if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_POST['captcha'] == $_SESSION['cap_code']) {
				
                                        
                        $products = '	<table padding-top=5px width="80%">
								<tr>
									<td width="70%">Product</td>
									<td >Quantity</td>
								</tr>';
                               
                                                          for($i=1;$i<100;$i++){   
                                                             if(isset($_POST["product_".$i]) && isset($_POST["prodqty_".$i]) && $_POST["prodqty_".$i] >0)
                                                             {
                                                                 $name = explode("::",$_POST["product_".$i]);
                                                            
							$products .='	<tr>
									<td><a href="'.$name[0].'" >'.$name[1].'</a></td>
									<td align="center">'.$_POST["prodqty_".$i].'</td>
								</tr>';
                                                             }
                                                          }
						$products .='	</table>';                
                              $id_land = $this->context->language->id;   // get current language id , if you are in order page you can use this

                           // this is file name here the file name is test.html                                           $title = "Test Login Details";    // mail title 
                          
                           $templateVars['{request-phone}'] = ($_POST["request-phone-hide"]==1)?'greenButton':'greyButton'; 
                           $templateVars['{request-booklet}'] = ($_POST["request-booklet-hide"]==1)?'greenButton':'greyButton'; 
                           $templateVars['{request-quote}'] = ($_POST["request-quote-hide"]==1)?'greenButton':'greyButton'; 
                           
                           if($_POST["request-phone-hide"] == 1 && $_POST["request-booklet-hide"]!=1 && $_POST["request-quote-hide"]!=1)
                           {
                                $template_name = 'request_form'; 
                           }
                           elseif($_POST["request-phone-hide"] == 1 && $_POST["request-booklet-hide"]==1 && $_POST["request-quote-hide"]==1)
                           {
                                $template_name = 'request_form_booklet_quote'; 
                           }
                           elseif($_POST["request-phone-hide"] == 1 && $_POST["request-booklet-hide"]==1 && $_POST["request-quote-hide"]!=1)
                           {
                                $template_name = 'request_form_booklet'; 
                           }
                            elseif($_POST["request-phone-hide"] == 1 && $_POST["request-booklet-hide"]!=1 && $_POST["request-quote-hide"]==1)
                           {
                                $template_name = 'request_form_quote'; 
                           }
                           $templateVars['{fname}'] = $_POST["fname"]; 
                             $templateVars['{lname}'] = $_POST["lname"];  
                              $templateVars['{email}'] = $_POST["email"];  
                               $templateVars['{company}'] = $_POST["company"];  
                                $templateVars['{website}'] = $_POST["website"];  
                            $templateVars['{bill_cn_1}'] = $_POST["bill_cn_1"];
                             $templateVars['{bill_cn_no}'] = $_POST["bill_cn_no"];
                            $templateVars['{country}'] = $_POST["country"];
                             $templateVars['{city}'] = $_POST["city"];
                            $templateVars['{address}'] = $_POST["address"];
                            $templateVars['{address2}'] = $_POST["address2"];
                            $templateVars['{state}'] = $_POST["state"];
                            $templateVars['{zip}'] = $_POST["zip"];
                            $templateVars['{howyouheard}'] =$_POST["howyouheard"];
                             $templateVars['{zip}'] = $_POST["zip"];
                              $templateVars['{interest}'] = $_POST["interest"];
                               $templateVars['{comments}'] = $_POST["comments"];
                             
                               $templateVars['{products}'] =$products;
                              
                            $to = Configuration::get('PS_SHOP_EMAIL');
                          //  $to = 'jinal.patel07@gmail.com'; 
                            $toName = Configuration::get('PS_SHOP_NAMEL');
                            $from = $_POST["email"];
                            $fromName = $_POST["fname"]." ".$_POST["lname"];

                     
                           // mail function
                        //echo "<bR>to=".$from." - ".$fromName;
                            Mail::Send($id_land, $template_name, 'Request Details', $templateVars, $to, $toName, $from, $fromName);
                           
                            //send mail to customer as well
                            $template_name_customer = 'request_form_customercopy';
                            Mail::Send($id_land, $template_name_customer, 'Request Details', "", $from, $toName, $to, "Be Bounce Houses");
                       
                        //   echo "Mail:send function";
                         //  $this->displayConfirmation('Email eent successfully.We will cantact you shortly');
                           Tools::redirect('request?confirmation=1');             
                                           
					
				} 
				else
				{
					$this->errors[] = "Enter Correct Captcha Code";
				}
			
                                        //http://blog.belvg.com/mail-sending-in-prestashop-1-4.html
                                        //http://phpshiner.blogspot.in/2013/04/send-email-in-prestashop.html
                                                             //}
                }
        }
       
      public function addProductDropdown($catid)
        {
             $this->category = new Category($catid, $this->context->language->id);
          
            $this->cat_products = $this->category->getProducts($this->context->language->id,1,100);
          //  print_r($this->cat_products);
             $dropoption = '';
            foreach($this->cat_products as $product)
            {
            //  echo "<br>".$product['name'];  
           $dropoption .= "<option value='".$product['link']."::".$product['name']."' >".$product['reference'].' | '.$product['name']."</option>";
            }
            return $dropoption;
        }
	
      public function initContent()
     
	{
            parent::initContent();
 
           $cattree = Category::getRootCategory()->recurseLiteCategTree(0);
           if(isset($_GET['confirmation']))
          $this->context->smarty->assign('confirmation', $_GET['confirmation']);
         //  $this->addProductDropdown(18);
           //exit;
           /*   echo "<pre>";
            
               
       echo "<pre>";   
    
       $dropDown = '';   
      print_r($cattree['children']);exit;    
           
           */
       foreach($cattree['children'] as $cat)
       {
         $dropDown = '<option value="" selected> </option>'; 
          
         $dropDown .= '<optgroup label="'.$cat['name'].'" class="grptitle">';
                   
         $spacert = '';
          if(count($cat['children']) >0)
           {
            
              foreach($cat['children'] as $cat1)
                {
                    $spacert = '&nbsp;&nbsp;';
                    if($cat1['id'] != "112" && $cat1['id'] != "28") // Remove all Advertising and Accessories from the List
                    {
						if($cat1['id'] != "14")
                        {
							$dropDown .= '<optgroup label="'. $spacert.$cat1['name'].'" class="grptitle">'; 
						}
                        if(count($cat1['children']) >0)
                        {
                            foreach($cat1['children'] as $cat2)
                            {

                             $spacert = '&nbsp;&nbsp;&nbsp;&nbsp;';
                              $dropDown .= '<optgroup label="'. $spacert.$cat2['name'].'" class="grptitle">
                                 '
                                      . $this->addProductDropdown($cat2['id']).
                                      '
                                 </optgroup>'; 
                             }
                        }
                        $dropDown .= ' </optgroup>';
                    }
                }
           }
           $dropDown .= ' </optgroup>'; 
       }
       // $dropDownHtml =  $dropDown."</select>";
     
             $this->context->smarty->assign('productdropdown', $dropDown);

		
                
                //$this->display_column_left = false;
		$this->setTemplate(_PS_THEME_DIR_.'request-form.tpl');
	}

	
}