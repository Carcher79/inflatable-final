<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class SearchController extends SearchControllerCore
{
	public function initContent()
	{
		$query = Tools::replaceAccentedChars(urldecode(Tools::getValue('q')));
		if ($this->ajax_search)
		{
		    $image = new Image();
			$searchResults = Search::find((int)(Tools::getValue('id_lang')), $query, 1, 10, 'position', 'desc', true);
			foreach ($searchResults as &$product)
			{
                $product['product_link'] = $this->context->link->getProductLink($product['id_product'], $product['prewrite'], $product['crewrite']);
                $imageID = $image->getCover($product['id_product']);
        	    $product['pthumb'] = $this->context->link->getImageLink($product['prewrite'], (int)$product['id_product'].'-'.$imageID['id_image'], 'small_default');
			}
			die(Tools::jsonEncode($searchResults));
		}
		parent::initContent();

		// scomi De acá para abajo es codigo agregado por el tema de ver precios en idepotgames
		$prices = array();

		if ($this->instant_search && !is_array($query))
		{
			$this->productSort();
			$this->n = abs((int)(Tools::getValue('n', Configuration::get('PS_PRODUCTS_PER_PAGE'))));
			$this->p = abs((int)(Tools::getValue('p', 1)));
			$search = Search::find($this->context->language->id, $query, 1, 10, 'position', 'desc');
			Hook::exec('actionSearch', array('expr' => $query, 'total' => $search['total']));
			$nbProducts = $search['total'];
			$this->pagination($nbProducts);
                        
                        $prices = $this->getPrices($search['result']);
                        
			$this->context->smarty->assign(array(
				'products' => $search['result'], // DEPRECATED (since to 1.4), not use this: conflict with block_cart module
				'search_products' => $search['result'],
				'nbProducts' => $search['total'],
				'search_query' => $query,
				'instant_search' => $this->instant_search,
				'homeSize' => Image::getSize(ImageType::getFormatedName('home')),
				'view_prices' => $this->context->customer->view_prices,
                                'prices' => $prices));
		}
		else if (($query = Tools::getValue('search_query', Tools::getValue('ref'))) && !is_array($query))
		{
			$this->productSort();
			$this->n = abs((int)(Tools::getValue('n', Configuration::get('PS_PRODUCTS_PER_PAGE'))));
			$this->p = abs((int)(Tools::getValue('p', 1)));
			$query = Tools::replaceAccentedChars(urldecode($query));			
			$search = Search::find($this->context->language->id, $query, $this->p, $this->n, $this->orderBy, $this->orderWay);
			Hook::exec('actionSearch', array('expr' => $query, 'total' => $search['total']));
			$nbProducts = $search['total'];
			$this->pagination($nbProducts);
			
                        $prices = $this->getPrices($search['result']);
                        
                        $this->context->smarty->assign(array(
				'products' => $search['result'], // DEPRECATED (since to 1.4), not use this: conflict with block_cart module
				'search_products' => $search['result'],
				'nbProducts' => $search['total'],
				'search_query' => $query,
				'homeSize' => Image::getSize(ImageType::getFormatedName('home')),
				'view_prices' => $this->context->customer->view_prices,
                                'prices' => $prices));
		}
		else if (($tag = urldecode(Tools::getValue('tag'))) && !is_array($tag))
		{
			$nbProducts = (int)(Search::searchTag($this->context->language->id, $tag, true));
			$this->pagination($nbProducts);
			$result = Search::searchTag($this->context->language->id, $tag, false, $this->p, $this->n, $this->orderBy, $this->orderWay);
			Hook::exec('actionSearch', array('expr' => $tag, 'total' => count($result)));

                        $prices = $this->getPrices($result);
                        
                        $this->context->smarty->assign(array(
				'search_tag' => $tag,
				'products' => $result, // DEPRECATED (since to 1.4), not use this: conflict with block_cart module
				'search_products' => $result,
				'nbProducts' => $nbProducts,
				'homeSize' => Image::getSize(ImageType::getFormatedName('home')),
				'view_prices' => $this->context->customer->view_prices,
                                'prices' => $prices));
		}
		else
		{       
			$this->context->smarty->assign(array(
				'products' => array(),
				'search_products' => array(),
				'pages_nb' => 1,
				'nbProducts' => 0,
				'view_prices' => $this->context->customer->view_prices,
                                'prices' => $prices));
		}
		$this->context->smarty->assign(array('add_prod_display' => Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY'), 'comparator_max_item' => Configuration::get('PS_COMPARATOR_MAX_ITEM')));

		$this->setTemplate(_PS_THEME_DIR_.'search.tpl');
	}
        
        private function getPrices($products) {
            $prices = array();
            if (isset($this->context->customer)) {                    
                $groups = Group::getGroups($this->context->language->id);
                $tmp = array();
                foreach ($groups as $group)
                        $tmp[$group['id_group']] = $group;
                $groups = $tmp;

                foreach($products as $product) {
                    $priceGroup = array();
                    $specific_prices = SpecificPrice::getByProductId((int)$product['id_product']);
                    foreach ($specific_prices as $specific_price) {
                        $group = $specific_price['id_group'];
                        $price = Tools::ps_round($specific_price['price'], 2);
                        $fixed_price = Tools::displayPrice($price, $this->context->currency);
                        $group_data = $groups[$group];
                        $group_name = ($group_data['name'] == '' ? 'All' : $group_data['name']);
                        $priceGroup[$group] = '<div class="group_name">'.$group_name.':&nbsp;</div><div class="price">'.$fixed_price.'</div>';
                    }
                    $prices[$product['id_product']] = $priceGroup;
                }
            }
            
            return $prices;
        // scomi acá termina el agregado
	}
}
