<?php

class CategoryController extends CategoryControllerCore
{
    public function initContent()
	{

        // scomi De acá para abajo es codigo agregado por el tema de ver precios en idepotgames
        $this->setTemplate(_PS_THEME_DIR_.'category.tpl');
        
        if (!$this->customer_access)
            return;

        if (isset($this->context->cookie->id_compare))
            $this->context->smarty->assign('compareProducts', CompareProduct::getCompareProducts((int)$this->context->cookie->id_compare));

        $this->productSort(); // Product sort must be called before assignProductList()
        
        $this->assignScenes();
        $this->assignSubcategories();
        if ($this->category->id != 1)
            $this->assignProductList();
                
                $prices = array();
                if (isset($this->context->customer)) {                    
                    $groups = Group::getGroups($this->context->language->id);
                    $tmp = array();
                    foreach ($groups as $group)
                            $tmp[$group['id_group']] = $group;
                    $groups = $tmp;
                    
                    foreach($this->cat_products as $product) {
                        $priceGroup = array();
                        $specific_prices = SpecificPrice::getByProductId((int)$product['id_product']);
                        foreach ($specific_prices as $specific_price) {
                            $group = $specific_price['id_group'];
                            $price = Tools::ps_round($specific_price['price'], 2);
                            $fixed_price = Tools::displayPrice($price, $this->context->currency);
                            $group_data = $groups[$group];
                            $group_name = ($group_data['name'] == '' ? 'All' : $group_data['name']);
                            $priceGroup[$group] = '<div class="group_name">'.$group_name.':&nbsp;</div><div class="price">'.$fixed_price.'</div>';
                        }
                        $prices[$product['id_product']] = $priceGroup;
                    }
                } 
        // scomi acá termina el agregado     

		parent::initContent();
        $this->context->smarty->assign(array(   
            'HOOK_CATEGORY_HEADER' => Hook::exec('displayCategoryHeader'),    
			'HOOK_CATEGORY_FOOTER' => Hook::exec('displayCategoryFooter'),     
            'display_category_title' => Configuration::get('STSN_DISPLAY_CATEGORY_TITLE'),  
            'display_category_image' => Configuration::get('STSN_DISPLAY_CATEGORY_IMAGE'),  
            'display_category_desc' => Configuration::get('STSN_DISPLAY_CATEGORY_DESC'),
            'display_subcategory' => Configuration::get('STSN_DISPLAY_SUBCATE'),  
			'categorySize' => Image::getSize(ImageType::getFormatedName('category')),  
		));
	}
}

