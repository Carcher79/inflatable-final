<?php
if($_SERVER['SERVER_NAME'] == "idepotgames.com" || $_SERVER['SERVER_NAME'] == "www.idepotgames.com" ){
	session_start();
	if(isset($_POST['submit'])) {
		if($_POST['password'] == "depot2013") {
			$_SESSION['logged'] = true;
			require(dirname(__FILE__).'/config/config.inc.php');
			Dispatcher::getInstance()->dispatch();
		}
	}
	if(!$_SESSION['logged']){
		?>
		<!doctype html>
		<html lang="en">
		<head>
			<meta charset="UTF-8">
			<title>Login</title>
			<style type="text/css">
				html {
				height: 100%;
				background: #3c2370; /* Old browsers */
				background: -moz-linear-gradient(top,  #3c2370 0%, #423c6d 99%); /* FF3.6+ */
				background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#3c2370), color-stop(99%,#423c6d)); /* Chrome,Safari4+ */
				background: -webkit-linear-gradient(top,  #3c2370 0%,#423c6d 99%); /* Chrome10+,Safari5.1+ */
				background: -o-linear-gradient(top,  #3c2370 0%,#423c6d 99%); /* Opera 11.10+ */
				background: -ms-linear-gradient(top,  #3c2370 0%,#423c6d 99%); /* IE10+ */
				background: linear-gradient(to bottom,  #3c2370 0%,#423c6d 99%); /* W3C */
				filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#3c2370', endColorstr='#423c6d',GradientType=0 ); /* IE6-9 */
					}
			</style>
		</head>
		<body>
			<div style="margin-top: 200px; width: 960px; margin-left: 40%;">
			<img src="logo-depot.png" alt="logo depot" style="margin-left: 14%;" />
			<form action="<?=$_SERVER[PHP_SELF]?>" method="post" style="font-size: 16px; color: #FFF; font-family: Verdana, Geneva, sans-serif;">
				<label style="margin-right: 5px;">PASSWORD</label><input type="password" name="password"/>
				<input type="submit" name="submit" value="Login" />
			</form>
			</div>
		</body>
		</html>
<?php
	exit;
	}
}
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require(dirname(__FILE__).'/config/config.inc.php');
Dispatcher::getInstance()->dispatch();
